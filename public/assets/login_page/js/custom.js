const inputs = document.querySelectorAll('.form__input');

function addfocus() {
  let parent = this.parentNode.parentNode;
  parent.classList.add('focus');
}

function remfocus() {
  let parent = this.parentNode.parentNode;
  if (this.value == '') {
    parent.classList.remove('focus');
  }
}

inputs.forEach((input) => {
  input.addEventListener('focus', addfocus);
  input.addEventListener('blur', remfocus);
});

const login = document.getElementById('log-in');
const loginBtn = document.getElementById('login-btn');
const payBtn = document.getElementById('toggle-pay');
const payForm = document.getElementById('pay');

payBtn.addEventListener('click', () => {
  // Remove classes first if they exist
  login.classList.remove('block');
  payForm.classList.remove('none');

  // Add classes
  login.classList.toggle('none');
  payForm.classList.toggle('block');
});

loginBtn.addEventListener('click', () => {
  // Remove classes first if they exist
  login.classList.remove('none');
  payForm.classList.remove('block');

  // Add classes
  login.classList.toggle('block');
  payForm.classList.toggle('none');
});
