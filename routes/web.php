<?php

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Laravel\Socialite\Facades\Socialite;
use Laravel\Socialite\Two\InvalidStateException;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/test', function()
{

    \App\Helpers\Custom::SendMail('bbosamark@gmail.com','Welcome','Hello Mark2','This is Mark First Note2','View Note2',url('/login'));
    return 'Email was sent';

});



Route::get('/auth/redirect', function () {
    return Socialite::driver('azure')->redirect();
});
Route::get('/thankyou', function () {
    return view('thankyou');
});

Route::get('/auth/callback', function () {
    $request = new \Illuminate\Http\Request();
    //dd($request->all());
    try {
        $auser = Socialite::driver('azure')->user();
    } catch (InvalidStateException $e) {
        $auser = Socialite::driver('azure')->user();
        //dd($auser);
    }

    $myuser = User::where('email',$auser->email)->first();

    if ($myuser) {
        Auth::login($myuser, true);
        return redirect(\route('dashboard')); // or where do you want to redirect the user after login.
    }else{
        alert()->error('Sorry!', 'Invalid User');
        return redirect(\route('login'));
    }
});

Route::get('/procurement/requests/vendor/invitation/{invitation}/view', 'App\Http\Controllers\ProcurementInvitationController@view_invitation')->name('procurement.requests.vendors.invitations.view');
Route::post('/procurement/requests/vendor/invitation/{invitation}/saveunitprices', 'App\Http\Controllers\ProcurementInvitationController@save_unit_prices')->name('procurement.requests.vendors.invitations.save.unit.prices');
Route::get('/procurement/requests/vendor/invitation/{invitation}/sendcode', 'App\Http\Controllers\ProcurementInvitationController@send_code')->name('procurement.requests.vendors.invitations.send.code');
Route::get('/procurement/requests/vendor/invitation/{invitation}/submitrequest', 'App\Http\Controllers\ProcurementInvitationController@submit_request')->name('procurement.requests.vendors.invitations.submit.request');
Route::get('/activities/{activity}/participants/{participant}/confirm/attendance', 'App\Http\Controllers\ActivityParticipantController@confirm_attendance')->name('activities.participants.confirm.attendance');


Route::middleware(['auth','acl'])->group(function() {

    Route::get('/', 'App\Http\Controllers\DashboardController@index')->name('dashboard.data');
    Route::get('/dashboard', 'App\Http\Controllers\DashboardController@index')->name('dashboard.data');
    Route::get('/dashboard', 'App\Http\Controllers\DashboardController@index')->name('dashboard');
    // Users
    Route::get('/users', 'App\Http\Controllers\UserController@index')->name('users.data');
    Route::get('/users/{user}/edit', 'App\Http\Controllers\UserController@edit')->name('users.edit');
    Route::post('/users/store', 'App\Http\Controllers\UserController@store')->name('users.store');
    Route::post('/users/{user}/update', 'App\Http\Controllers\UserController@update')->name('users.update');

    // Activities
    Route::get('/my/activities/{status}', 'App\Http\Controllers\ActivityController@index')->name('my.activities.data');
    Route::get('/activities/{status}', 'App\Http\Controllers\ActivityController@index')->name('activities.data');
    Route::get('/activity/create', 'App\Http\Controllers\ActivityController@create')->name('activities.create');
    Route::get('/activity/unverified', 'App\Http\Controllers\ActivityController@un_verified')->name('activities.unverified');
    Route::get('/activity/unapproved', 'App\Http\Controllers\ActivityController@un_approved')->name('activities.unapproved');

    Route::get('/activities/{activity}/edit', 'App\Http\Controllers\ActivityController@edit')->name('activities.edit');
    Route::get('/activities/{activity}/send', 'App\Http\Controllers\ActivityController@send_for_approval')->name('activities.send');
    Route::get('/activities/{activity}/approve', 'App\Http\Controllers\ActivityController@mark_as_approved')->name('activities.mark.as.approved');
    Route::get('/activities/payments/unverified', 'App\Http\Controllers\ActivityController@show_unverified_payments')->name('activities.show.unverified.payments');
    Route::get('/activities/payments/unapproved', 'App\Http\Controllers\ActivityController@show_unapproved_payments')->name('activities.show.unapproved.payments');
    Route::get('/activities/payments/approved', 'App\Http\Controllers\ActivityController@show_approved_payments')->name('activities.show.approved.payments');
    Route::post('/activities/search', 'App\Http\Controllers\ActivityController@search')->name('activities.search');


    Route::get('/activities/{activity}/sendpaymentsforapproval', 'App\Http\Controllers\ActivityController@send_payments_for_approval')->name('activities.send.payments.for.approval');
    Route::get('/activities/{activity}/approvepayments', 'App\Http\Controllers\ActivityController@approve_payments')->name('activities.approve.payments');
    Route::get('/activities/{activity}/showpaymentslist', 'App\Http\Controllers\ActivityController@show_payments_list')->name('activities.show.payments.list');
    Route::get('/activities/{activity}/downloadpaymentlist', 'App\Http\Controllers\ActivityController@download_payment_list')->name('activities.download.payment.list');
    Route::get('/activities/{activity}/conceptnotepreview', 'App\Http\Controllers\ActivityController@concept_note_preview')->name('activities.concept.note.preview');


    Route::post('/activities/store', 'App\Http\Controllers\ActivityController@store')->name('activities.store');
    Route::post('/activities/status', 'App\Http\Controllers\ActivityController@status')->name('activities.status');
    Route::post('/activities/{activity}/update', 'App\Http\Controllers\ActivityController@update')->name('activities.update');
    Route::get('/my/activities/{activity}/addparticipants', 'App\Http\Controllers\ActivityController@add_participants')->name('my.activities.add.participants');
    Route::post('/my/activities/{activity}/storeparticipants', 'App\Http\Controllers\ActivityController@store_added_participants')->name('my.activities.store.participants');
    Route::get('/my/activities/{activity}/logattendance', 'App\Http\Controllers\ActivityController@log_attendance')->name('my.activities.log.attendance');
    Route::get('/my/activities/{activity}/sendcode', 'App\Http\Controllers\ActivityController@send_code')->name('my.activities.send.code');
    Route::post('/my/activities/{activity}/addattendance', 'App\Http\Controllers\ActivityController@add_attendance')->name('my.activities.add.attendance');
    Route::get('/my/activities/{activity}/downloadattendance', 'App\Http\Controllers\ActivityController@download_attendance')->name('my.activities.download.attendance');
    Route::get('/my/activities/{activity}/closeactivity', 'App\Http\Controllers\ActivityController@close_activity')->name('my.activities.close');
    Route::get('/my/activities/{activity}/generatepayments', 'App\Http\Controllers\ActivityController@generate_payment_list')->name('my.activities.generate.payments');
    Route::post('/my/activities/{activity}/updatepayments', 'App\Http\Controllers\ActivityController@update_payment_list')->name('my.activities.update.payments');
    Route::get('/my/activities/{activity}/sendpaymentsforverification', 'App\Http\Controllers\ActivityController@send_payments_for_verification')->name('my.activities.send.payments.for.verification');
    Route::get('/my/activities/{activity}/sentinvitations', 'App\Http\Controllers\ActivityController@send_invitations')->name('my.activities.send.invitations');

    // Vendors
    Route::get('/vendors', 'App\Http\Controllers\VendorController@index')->name('vendors.data');
    Route::get('/vendors/{vendor}/edit', 'App\Http\Controllers\VendorController@edit')->name('vendors.edit');
    Route::post('/vendors/store', 'App\Http\Controllers\VendorController@store')->name('vendors.store');
    Route::post('/vendors/{vendor}/update', 'App\Http\Controllers\VendorController@update')->name('vendors.update');

    // Unit Measures
    Route::get('/unit/measures', 'App\Http\Controllers\UnitMeasureController@index')->name('unit.measures.data');
    Route::get('/unit/measures/{measure}/edit', 'App\Http\Controllers\UnitMeasureController@edit')->name('unit.measures.edit');
    Route::post('/unit/measures/store', 'App\Http\Controllers\UnitMeasureController@store')->name('unit.measures.store');
    Route::post('/unit/measures/{measure}/update', 'App\Http\Controllers\UnitMeasureController@update')->name('unit.measures.update');

    // budget Codes
    Route::get('/budget/codes', 'App\Http\Controllers\BudgetCodeController@index')->name('budget.codes.data');
    Route::get('/budget/codes/{code}/edit', 'App\Http\Controllers\BudgetCodeController@edit')->name('budget.codes.edit');
    Route::post('/budget/codes/store', 'App\Http\Controllers\BudgetCodeController@store')->name('budget.codes.store');
    Route::post('/budget/codes/{code}/update', 'App\Http\Controllers\BudgetCodeController@update')->name('budget.codes.update');


    // Procurement
    Route::get('/procurement/requests/create', 'App\Http\Controllers\ProcurementRequestController@create')->name('procurement.requests.create');
    Route::get('/procurement/requests/{status}', 'App\Http\Controllers\ProcurementRequestController@index')->name('procurement.requests.data');
    Route::get('/procurement/requests/{request}/edit', 'App\Http\Controllers\ProcurementRequestController@edit')->name('procurement.requests.edit');
    Route::post('/procurement/requests/store', 'App\Http\Controllers\ProcurementRequestController@store')->name('procurement.requests.store');
    Route::post('/procurement/requests/{request}/update', 'App\Http\Controllers\ProcurementRequestController@update')->name('procurement.requests.update');
    Route::get('/procurement/requests/{request}/preview', 'App\Http\Controllers\ProcurementRequestController@preview')->name('procurement.requests.preview');
    Route::get('/procurement/requests/{request}/verify', 'App\Http\Controllers\ProcurementRequestController@verify')->name('procurement.requests.verify');
    Route::post('/procurement/requests/{request}/approve', 'App\Http\Controllers\ProcurementRequestController@approve')->name('procurement.requests.approve');
    Route::get('/procurement/requests/{request}/invitations', 'App\Http\Controllers\ProcurementInvitationController@index')->name('procurement.requests.invitations.data');
    Route::get('/procurement/requests/{request}/close', 'App\Http\Controllers\ProcurementRequestController@close')->name('procurement.requests.close');

    //MPO
    Route::get('/procurement/requests/{request}/mpopreview', 'App\Http\Controllers\ProcurementRequestController@mpo_preview')->name('procurement.requests.mpo.preview');

    Route::get('/procurement/requests/{request}/items', 'App\Http\Controllers\ProcurementItemController@index')->name('procurement.items.data');
    Route::get('/procurement/requests/{request}/items/{item}/edit', 'App\Http\Controllers\ProcurementItemController@edit')->name('procurement.items.edit');
    Route::post('/procurement/requests/{request}/items/store', 'App\Http\Controllers\ProcurementItemController@store')->name('procurement.items.store');
    Route::post('/procurement/requests/{request}/items/{item}/update', 'App\Http\Controllers\ProcurementItemController@update')->name('procurement.items.update');

    // Users
    Route::get('/participants', 'App\Http\Controllers\ParticipantController@index')->name('participants.data');
    Route::get('/participants/{participant}/edit', 'App\Http\Controllers\ParticipantController@edit')->name('participants.edit');
    Route::post('/participants/store', 'App\Http\Controllers\ParticipantController@store')->name('participants.store');
    Route::post('/participants/{participant}/update', 'App\Http\Controllers\ParticipantController@update')->name('participants.update');

    //Roles
    Route::get('/roles', 'App\Http\Controllers\RoleController@index')->name('roles.data');
    Route::get('/roles/{role}/edit', 'App\Http\Controllers\RoleController@edit')->name('roles.edit');
    Route::post('/roles/store', 'App\Http\Controllers\RoleController@store')->name('roles.store');
    Route::post('/roles/{role}/update', 'App\Http\Controllers\RoleController@update')->name('roles.update');

});



//Route::get('/', 'App\Http\Controllers\DashboardController@home')->name('dashboard.home');

require __DIR__.'/auth.php';
