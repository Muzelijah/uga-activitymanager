<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Participant extends Model
{
    use HasFactory;

    protected $casts = [
        'participant_first_name' => 'encrypted',
        'participant_other_names' => 'encrypted',
        'participant_mobile' => 'encrypted',
        'participant_email' => 'encrypted',
    ];
}
