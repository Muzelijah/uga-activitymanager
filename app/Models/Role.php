<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Permission;

class Role extends Model
{
    use HasFactory;

    public function syncPermissions()
    {
        // return Route::getRoutes()->getRoutes();
        $routes = collect(Route::getRoutes()->getRoutes())->reduce(function ($carry = [], $route) {

            //$carry[] = $route->uri();
            $route_name = $route->getName();
            $route_url = $route->uri();
            //get all routes in api.php that need authentication
           // if ($route_name != null && Str::startsWith($route_url, "web") && in_array("auth",$route->middleware()))
            if ($route_name != null && in_array("auth",$route->middleware()))
                $carry[] = $route->getName();
            return $carry;
        });
         //  dd($routes);
        foreach ($routes as $route) {
            Permission::updateorCreate([
                "name" => $route,
            ], [
                "name" => $route,
                "guard_name"=>"web"
            ]);
        }
        //create Super admin Role
        $role = \Spatie\Permission\Models\Role::updateorCreate([
            "name"=>"sysadmin"
        ],[
            "name"=>"sysadmin",
            "guard_name"=>"web"
        ]);
        //sync all permissions to Super Admin
        $role->syncPermissions(Permission::all()->pluck('name'));

        //create Client Role
        $role = \Spatie\Permission\Models\Role::updateorCreate([
            "name"=>"Front Desk Officer"
        ],[
            "name"=>"Front Desk Officer",
            "guard_name"=>"web"
        ]);
        //sync all permissions to Super Admin
        $role->syncPermissions(Permission::where('id',1)->pluck('name'));

    }
}
