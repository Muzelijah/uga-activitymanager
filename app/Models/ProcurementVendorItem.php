<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProcurementVendorItem extends Model
{
    use HasFactory;

    protected $guarded =[];

    public function procurement_invitation()
    {
        return $this->belongsTo(ProcurementInvitation::class,'procurement_invitation_id');
    }

    public function procurement_request()
    {
        return $this->belongsTo(ProcurementRequest::class,'procurement_request_id');
    }

    public function vendor()
    {
        return $this->belongsTo(Vendor::class,'vendor_id');
    }

    public function unit_measure()
    {
        return $this->belongsTo(UnitMeasure::class,'unit_measure_id');
    }

    public function activity()
    {
        return $this->belongsTo(Activity::class,'activity_id');
    }
}
