<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProcurementRequest extends Model
{
    use HasFactory;

    public function activity()
    {
        return $this->belongsTo(Activity::class,'activity_id');
    }

    public function items()
    {
        return $this->hasMany(ProcurementItem::class,'procurement_request_id');
    }

    public function invitations()
    {
        return $this->hasMany(ProcurementInvitation::class,'procurement_request_id');
    }
}
