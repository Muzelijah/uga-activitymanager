<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    use HasFactory;

    public function activity_type()
    {
        return $this->belongsTo(ActivityType::class,'activity_type_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    public function assign()
    {
        return $this->belongsTo(User::class,'assigned_to');
    }

    public function budget_code()
    {
        return $this->belongsTo(BudgetCode::class,'budget_code_id');
    }

    public function activity_status()
    {
        return $this->belongsTo(ActivityStatus::class,'activity_status_id');
    }

    public function payments()
    {
        return $this->hasMany(ActivityPayment::class,'activity_id');
    }
}
