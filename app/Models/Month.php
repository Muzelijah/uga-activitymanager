<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Month extends Model
{
    use HasFactory;

    public static function getMonthIdByDate($date){
        //dd($date);
        return Month::where('month_start_date','<=',$date)->where('month_end_date','>=',$date)->first();

    }
    public static function getMonthById($month_id){
        //dd($date);
        return Month::where('id',$month_id)->first();

    }
}
