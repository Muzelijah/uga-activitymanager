<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ActivityAttendance extends Model
{
    use HasFactory;

    public function participant()
    {
        return $this->belongsTo(Participant::class,'participant_id');
    }

    public function activity()
    {
        return $this->belongsTo(Activity::class,'activity_id');
    }
}
