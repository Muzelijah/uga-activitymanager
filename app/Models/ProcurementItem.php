<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProcurementItem extends Model
{
    use HasFactory;

    public function procurement_request()
    {
        return $this->belongsTo(ProcurementRequest::class,'procurement_request_id');
    }

    public function unit_measure()
    {
        return $this->belongsTo(UnitMeasure::class,'unit_measure_id');
    }
}
