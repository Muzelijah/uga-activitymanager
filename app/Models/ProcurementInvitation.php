<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProcurementInvitation extends Model
{
    use HasFactory;

    public function activity()
    {
        return $this->belongsTo(Activity::class,'activity_id');
    }

    public function procurement_request()
    {
        return $this->belongsTo(ProcurementRequest::class,'procurement_request_id');
    }

    public function vendor()
    {
        return $this->belongsTo(Vendor::class,'vendor_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    public function vendor_items()
    {
        return $this->hasMany(ProcurementVendorItem::class,'procurement_invitation_id');
    }


}
