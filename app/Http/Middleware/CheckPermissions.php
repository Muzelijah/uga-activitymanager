<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\JsonResponse;
use App\Models\User;


class CheckPermissions
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
//        return auth()->user()->hasPermissionForRoute($request->route()->getName())
//            ? $next($request)
//            : redirect()->back()->withErrors(["You don't have access to the requested page."]);
        if (auth()->user()->hasPermissionForRoute($request->route()->getName())) {
            return $next($request);
        } else {
            alert()->error("Sorry! You don't have access to the requested page.", 'Access Denied')->persistent('Close');
            return redirect()->back();
        }
    }
}
