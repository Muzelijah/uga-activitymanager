<?php

namespace App\Http\Controllers;

use App\Helpers\Custom;
use App\Models\AreaOffice;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = "Users";
        $users = User::all();
        $roles = Role::all();
        $area_offices = Custom::getAreaOffices();
        return view('user.index',compact('title','users','roles','area_offices'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        if($request->password == $request->confirm_password){
            $user = new User();
            $user->area_offices = json_encode($request->area_offices);
            $user->name = $request->name;
            $user->email = $request->email;
            $user->phone = $request->phone;
            $user->password = bcrypt($request->password);
            $user->api_token = Str::random(60);
            $user->save();
            $user->syncRoles($request->roles);

            alert()->success('User Registered Successfully', 'Good Job!')->persistent('Close');
            return redirect(route('users.data'));
        }else{
            alert()->error('', 'Password Mismatch')->persistent('Close');
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $roles = Role::all();
        $area_offices = Custom::getAreaOffices();
        $title = "Edit User";

        return view('user.edit',compact('user','roles','title','area_offices'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->password == $request->confirm_password){
            $user = User::find($id);
            $user->area_offices = json_encode($request->area_offices);
            $user->name = $request->name;
            $user->email = $request->email;
            $user->phone = $request->phone;
            $user->password = bcrypt($request->password);
            $user->api_token = Str::random(60);
            $user->save();
            $user->syncRoles($request->roles);

            alert()->success('User Updated Successfully', 'Good Job!');
            return redirect(route('users.data'));
        }else{
            alert()->error('', 'Password Mismatch');
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
