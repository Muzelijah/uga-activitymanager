<?php

namespace App\Http\Controllers;

use App\Models\Activity;
use App\Models\ActivityParticipant;
use Illuminate\Http\Request;

class ActivityParticipantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ActivityParticipant  $activityParticipant
     * @return \Illuminate\Http\Response
     */
    public function show(ActivityParticipant $activityParticipant)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ActivityParticipant  $activityParticipant
     * @return \Illuminate\Http\Response
     */
    public function edit(ActivityParticipant $activityParticipant)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ActivityParticipant  $activityParticipant
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ActivityParticipant $activityParticipant)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ActivityParticipant  $activityParticipant
     * @return \Illuminate\Http\Response
     */
    public function destroy(ActivityParticipant $activityParticipant)
    {
        //
    }

    public function confirm_attendance($activity_ref,$attendance_ref){

        $activity = Activity::where('activity_ref',$activity_ref)->first();
        $attendance_participant = ActivityParticipant::where('activity_id',$activity->id)->where('activity_participant_ref',$attendance_ref)->first();
        if(!empty($attendance_participant)){
            $attendance_participant->confirmed_attendance = 1;
            $attendance_participant->date_attendance_confirmed = date('Y-m-d H:i:s');
            $attendance_participant->save();

            return redirect(url('/thankyou'));
        }

    }
}
