<?php

namespace App\Http\Controllers;

use App\Models\AreaOffice;
use Illuminate\Http\Request;

class AreaOfficeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\AreaOffice  $areaOffice
     * @return \Illuminate\Http\Response
     */
    public function show(AreaOffice $areaOffice)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\AreaOffice  $areaOffice
     * @return \Illuminate\Http\Response
     */
    public function edit(AreaOffice $areaOffice)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\AreaOffice  $areaOffice
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AreaOffice $areaOffice)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\AreaOffice  $areaOffice
     * @return \Illuminate\Http\Response
     */
    public function destroy(AreaOffice $areaOffice)
    {
        //
    }
}
