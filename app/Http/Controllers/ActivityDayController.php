<?php

namespace App\Http\Controllers;

use App\Models\ActivityDay;
use Illuminate\Http\Request;

class ActivityDayController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ActivityDay  $activityDay
     * @return \Illuminate\Http\Response
     */
    public function show(ActivityDay $activityDay)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ActivityDay  $activityDay
     * @return \Illuminate\Http\Response
     */
    public function edit(ActivityDay $activityDay)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ActivityDay  $activityDay
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ActivityDay $activityDay)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ActivityDay  $activityDay
     * @return \Illuminate\Http\Response
     */
    public function destroy(ActivityDay $activityDay)
    {
        //
    }
}
