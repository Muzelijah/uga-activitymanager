<?php

namespace App\Http\Controllers;

use App\Helpers\Custom;
use App\Models\Activity;
use App\Models\ProcurementInvitation;
use App\Models\ProcurementItem;
use App\Models\ProcurementRequest;
use App\Models\ProcurementVendorItem;
use App\Models\UnitMeasure;
use App\Models\Vendor;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use function Symfony\Component\String\u;

class ProcurementRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($status)
    {
        $procurement_requests = ProcurementRequest::where('procurement_request_status', $status)->whereIn('area_office_id',json_decode(auth()->user()->area_offices))->get();
        $activities = Activity::where('activity_status_id', 3)->whereNotIn('id', $procurement_requests->pluck('activity_id'))->get();
        $title = "Procurement Requests";
        $area_offices = Custom::getAreaOffices();

        return view('procurement.requests.index', compact('title', 'procurement_requests', 'activities','status','area_offices'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $procurement_requests = ProcurementRequest::where('procurement_request_status', 1)->whereIn('area_office_id',json_decode(auth()->user()->area_offices))->get();
        $activities = Activity::where('activity_status_id', 3)->whereNotIn('id', $procurement_requests->pluck('activity_id'))->get();
        $title = "Create New Procurement Request";
        $area_offices = Custom::getAreaOffices();

        return view('procurement.requests.create', compact('title', 'procurement_requests', 'activities','area_offices'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $procurement_request_check = ProcurementRequest::where('rfq_reference_number', $request->rfq_reference_number)->first();
        if (empty($procurement_request_check)) {
            $procurement_request = new ProcurementRequest();
            $procurement_request->area_office_id = $request->area_office_id;
            $procurement_request->user_id = auth()->user()->id;
            $procurement_request->procurement_request_ref = Str::uuid();
            $procurement_request->rfq_reference_number = $request->rfq_reference_number;
            $procurement_request->activity_id = $request->activity_id;
            $procurement_request->exchange_rate = $request->exchange_rate;
            $procurement_request->procurement_issue_date = $request->procurement_issue_date;
            $procurement_request->procurement_due_date = $request->procurement_due_date;
            $procurement_request->delivery_from_date = $request->delivery_from_date;
            $procurement_request->delivery_to_date = $request->delivery_to_date;
            $procurement_request->payment_terms = $request->payment_terms;
            $procurement_request->delivery_address = $request->delivery_address;
            $procurement_request->date_added = date('Y-m-d');
            $procurement_request->save();

            alert()->success('Request has been Added', 'Good Job!')->persistent('Close');
            return redirect(route('procurement.requests.create'));


        } else {
            alert()->error('Procurement Request Exists', 'Sorry!')->persistent('Close');
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\ProcurementRequest $procurementRequest
     * @return \Illuminate\Http\Response
     */
    public function show(ProcurementRequest $procurementRequest)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\ProcurementRequest $procurementRequest
     * @return \Illuminate\Http\Response
     */
    public function edit($procurement_request_ref)
    {
        $procurement_request = ProcurementRequest::where('procurement_request_ref', $procurement_request_ref)->first();
        $area_offices = Custom::getAreaOffices();
        $activities = Activity::where('id',$procurement_request->activity_id)->get();
        $title = "Edit Procurement Request";

        return view('procurement.requests.edit', compact('title', 'procurement_request', 'activities','area_offices'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\ProcurementRequest $procurementRequest
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$procurement_request_ref)
    {
        //dd($request->all());
        $procurement_request = ProcurementRequest::where('procurement_request_ref', $procurement_request_ref)->first();
        if (!empty($procurement_request)) {
            //$procurement_request = new ProcurementRequest();
            $procurement_request->area_office_id = $request->area_office_id;
            $procurement_request->user_id = auth()->user()->id;
            $procurement_request->rfq_reference_number = $request->rfq_reference_number;
            $procurement_request->activity_id = $request->activity_id;
            $procurement_request->exchange_rate = $request->exchange_rate;
            $procurement_request->procurement_issue_date = $request->procurement_issue_date;
            $procurement_request->procurement_due_date = $request->procurement_due_date;
            $procurement_request->delivery_from_date = $request->delivery_from_date;
            $procurement_request->delivery_to_date = $request->delivery_to_date;
            $procurement_request->payment_terms = $request->payment_terms;
            $procurement_request->delivery_address = $request->delivery_address;
            $procurement_request->save();

            alert()->success('Request has been Updated', 'Good Job!')->persistent('Close');
            return redirect(route('procurement.requests.create'));


        } else {
            alert()->error('Procurement Request Not Found', 'Sorry!')->persistent('Close');
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\ProcurementRequest $procurementRequest
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProcurementRequest $procurementRequest)
    {
        //
    }

    public function view_items($procurement_request_ref)
    {

    }

    public function preview($procurement_request_ref)
    {

        $title = "Procurement Preview";

        $procurement_request = ProcurementRequest::where('procurement_request_ref', $procurement_request_ref)->first();
        $vendors = Vendor::where('vendor_status', 1)->get();
        $procurement_request_items = ProcurementItem::where('procurement_request_id', $procurement_request->id)->get();
        $title = "Procurement Request for " . $procurement_request->activity->activity_name . " (" . $procurement_request->activity->activity_code . ")";

        $data = array();
        $data["procurement_request"] = $procurement_request;
        $data["procurement_request_items"] = $procurement_request_items;
        $data['title'] = $title;

        $pdf = PDF::loadView('pdftemplates.procurementrequest', $data);
        $pdf->setPaper('A4', 'portrait');
        $path = public_path('/app_image/rpt');
        $file_name = Str::replace(" ", "_", $procurement_request_ref) . ".pdf";
        $pdf->save($path . '/' . $file_name);


        return view('procurement.requests.preview', compact('title', 'procurement_request_ref', 'procurement_request', 'vendors'));
    }

    public function verify($procurement_request_ref)
    {

        $procurement_request = ProcurementRequest::where('procurement_request_ref', $procurement_request_ref)->where('procurement_request_status', 1)->first();
        if (!empty($procurement_request)) {
            $procurement_request->verified = 1;
            $procurement_request->verified_by = auth()->user()->id;
            $procurement_request->verified_date = date('Y-m-d');
            $procurement_request->procurement_request_status = 2;
            $procurement_request->save();

            alert()->success('Request has been verified and sent for approval', 'Good Job!')->persistent('Close');
            return redirect(route('procurement.requests.preview', ['request' => $procurement_request_ref]));
        } else {
            alert()->error('Procurement Request Not Found', 'Sorry!')->persistent('Close');
            return redirect()->back();
        }


    }

    public function approve(Request $request, $procurement_request_ref)
    {

        //dd($request->all());
        if (count($request->vendors) > 1) {

            $procurement_request = ProcurementRequest::where('procurement_request_ref', $procurement_request_ref)->where('procurement_request_status', 2)->first();
            if (!empty($procurement_request)) {
                $procurement_request->approved = 1;
                $procurement_request->approved_by = auth()->user()->id;
                $procurement_request->approved_date = date('Y-m-d');
                $procurement_request->procurement_request_status = 3;
                $procurement_request->save();

                foreach ($request->vendors as $vendor_ref){
                    $this->generate_vendor_invitations($procurement_request_ref, $vendor_ref);
                }
                $unsent_invitations = ProcurementInvitation::where('procurement_request_id',$procurement_request->id)->where('send_invitation',0)->get();
                foreach ($unsent_invitations as $unsent_invitation){

                    $invitation = ProcurementInvitation::find($unsent_invitation->id);
                    $vendor = Vendor::find($unsent_invitation->vendor_id);

                    $sms_message ="Dear ".$vendor->vendor_name." we have sent a procurement request to your email. kindly review and respond before ".date('jS F Y',strtotime($procurement_request->procurement_due_date))."";
                    Custom::SendPremiumMessage($invitation->procurement_invitation_mobile,$sms_message);

                    $url = route('procurement.requests.vendors.invitations.view',['invitation'=>$invitation->procurement_invitation_ref]);
                    Custom::SendMail($invitation->procurement_invitation_email,"Request for Quotation ".$procurement_request->rfq_reference_number."","Dear ".$vendor->vendor_name."","Kindly review and respond to our procurement request before ".date('jS F Y',strtotime($procurement_request->procurement_due_date))."","View Procurement Request",$url);


                }


                alert()->success('Request has been approved and invitations sent to selected vendors', 'Good Job!')->persistent('Close');
                return redirect(route('procurement.requests.preview', ['request' => $procurement_request_ref]));
            }
        } else {

            alert()->error('Select more then one vendor before approving request', 'Sorry!')->persistent('Close');
            return redirect()->back();
        }
    }

    public function generate_vendor_invitations($procurement_request_ref, $vendor_ref)
    {

        $procurement_request = ProcurementRequest::where('procurement_request_ref', $procurement_request_ref)->first();

        $vendor = Vendor::where('vendor_ref', $vendor_ref)->first();
        //check if vendor invitation exists
        $procuremet_invitation_check = ProcurementInvitation::where('procurement_request_id', $procurement_request->id)->where('vendor_id', $vendor->id)->first();
        if (empty($procuremet_invitation_check)) {
            //generate vendor invitation
            $procuremet_invitation = new ProcurementInvitation();
            $procuremet_invitation->procurement_invitation_ref = Str::uuid();
            $procuremet_invitation->user_id = auth()->user()->id;
            $procuremet_invitation->invitation_submission_code = rand(100000,1000000);
            $procuremet_invitation->activity_id = $procurement_request->activity_id;
            $procuremet_invitation->procurement_request_id = $procurement_request->id;
            $procuremet_invitation->vendor_id = $vendor->id;
            $procuremet_invitation->procurement_invitation_mobile = $vendor->vendor_contact;
            $procuremet_invitation->procurement_invitation_email = $vendor->vendor_email;
            $procuremet_invitation->save();

            //add request items to invitation
            $procurement_items = ProcurementItem::where('procurement_request_id', $procurement_request->id)->get();

            foreach ($procurement_items as $procurement_item) {

                ProcurementVendorItem::updateOrCreate([
                    'procurement_item_id' => $procurement_item->id,
                    'vendor_id' => $vendor->id,
                ], [
                    'procurement_vendor_ref' => Str::uuid(),
                    'user_id' => auth()->user()->id,
                    'activity_id' => $procurement_request->activity_id,
                    'procurement_invitation_id' => $procuremet_invitation->id,
                    'procurement_request_id' => $procurement_request->id,
                    'item_description' => $procurement_item->item_description,
                    'item_qty' => $procurement_item->item_qty,
                    'unit_measure_id' => $procurement_item->unit_measure_id,
                    'total_days' => $procurement_item->total_days,

                ]);
            }


        }

    }

    public function mpo_preview($procurement_request_ref)
    {

        $title = "Procurement Preview";

        $procurement_request = ProcurementRequest::where('procurement_request_ref', $procurement_request_ref)->first();
        $vendors = Vendor::where('vendor_status', 1)->get();
        $procurement_request_items = ProcurementItem::where('procurement_request_id', $procurement_request->id)->get();
        $title = "Procurement Request for " . $procurement_request->activity->activity_name . " (" . $procurement_request->activity->activity_code . ")";

        $get_invite_with_least_total = ProcurementVendorItem::select('procurement_invitation_id')->where('procurement_request_id', $procurement_request->id)->where('line_total','!=',0)->groupBy('procurement_invitation_id')->orderByRaw('SUM(line_total) ASC')->first()->procurement_invitation_id;
       // dd($get_invite_with_least_total);
        $recommended_vendor = ProcurementInvitation::where('id',$get_invite_with_least_total)->first();

        $data = array();
        $data["procurement_request"] = $procurement_request;
        $data["procurement_request_items"] = $procurement_request_items;
        $data["recommended_vendor"] = $recommended_vendor;
        $data['title'] = $title;

        $pdf = PDF::loadView('pdftemplates.procurementrequestmpo', $data);
        $pdf->setPaper('A4', 'landscape');
        $path = public_path('/app_image/rpt/mpo');
        $file_name = Str::replace(" ", "_", $procurement_request_ref) . ".pdf";
        $pdf->save($path . '/' . $file_name);


        return view('procurement.requests.mpo.preview', compact('title', 'procurement_request_ref', 'procurement_request', 'vendors'));
    }

    public function close($procurement_request_ref)
    {

        $procurement_request = ProcurementRequest::where('procurement_request_ref', $procurement_request_ref)->where('procurement_request_status', 3)->first();
        if (!empty($procurement_request)) {
            if($procurement_request->procurement_due_date <= date('Y-m-d')){
                $procurement_request->procurement_request_status = 4;
                $procurement_request->save();

                alert()->success('Request has been closed', 'Good Job!')->persistent('Close');
                return redirect(route('procurement.requests.data', ['status' => 3]));

            }else{
                alert()->error('Request can not be closed before due date', 'Sorry!')->persistent('Close');
                return redirect()->back();
            }

        } else {
            alert()->error('Procurement Request Not Found', 'Sorry!')->persistent('Close');
            return redirect()->back();
        }


    }


}
