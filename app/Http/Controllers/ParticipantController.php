<?php

namespace App\Http\Controllers;

use App\Helpers\Custom;
use App\Models\AreaOffice;
use App\Models\Participant;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ParticipantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $participants = Participant::where('active', 1)->get();
        $area_offices = Custom::getAreaOffices();
        $title = "Participants";

        return view('participant.index', compact('title', 'participants','area_offices'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        $participant_check = Participant::where('participant_mobile', $request->participant_mobile)->orWhere('participant_email', $request->participant_email)->get()->toArray();
        if (count($participant_check) < 1) {
            $participant = new Participant();
            $participant->area_office_id = $request->area_office_id;
            $participant->participant_ref = Str::uuid();
            $participant->user_id = auth()->user()->id;
            $participant->participant_title = Str::title($request->participant_title);
            $participant->participant_first_name = Str::title($request->participant_first_name);
            $participant->participant_other_names = Str::title($request->participant_other_names);
            $participant->participant_type = $request->participant_type;
            $participant->participant_mobile = $request->participant_mobile;
            $participant->participant_email = $request->participant_email;
            $participant->participant_organization = $request->participant_organization;
            $participant->participant_sign = $this->save_signature($request->signature);
            $participant->save();

            alert()->success('Participant Successfully Registered', 'Good Job!')->persistent('Close');

            return redirect(route('participants.data'));
        } else {
            alert()->error('Participant already exists with entered mobile and email', 'Sorry!')->persistent('Close');

            return redirect(route('participants.data'));
        }

    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Participant $participant
     * @return \Illuminate\Http\Response
     */
    public function show(Participant $participant)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Participant $participant
     * @return \Illuminate\Http\Response
     */
    public function edit($participant_ref)
    {
        $title = "Edit Participant";
        $participant = Participant::where('participant_ref', $participant_ref)->first();
        return view('participant.edit', compact('title', 'participant'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Participant $participant
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $participant_ref)
    {
        $participant = Participant::where('participant_ref', $participant_ref)->first();
        $participant->user_id = auth()->user()->id;
        $participant->area_office_id = $request->area_office_id;
        $participant->participant_title = Str::title($request->participant_title);
        $participant->participant_first_name = Str::title($request->participant_first_name);
        $participant->participant_other_names = Str::title($request->participant_other_names);
        $participant->participant_type = $request->participant_type;
        $participant->participant_mobile = $request->participant_mobile;
        $participant->participant_email = $request->participant_email;
        $participant->participant_organization = $request->participant_organization;
        $participant->participant_sign = $this->save_signature($request->signature);
        $participant->save();

        alert()->success('Participant Successfully Updated', 'Good Job!')->persistent('Close');
        return redirect(route('participants.data'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Participant $participant
     * @return \Illuminate\Http\Response
     */
    public function destroy(Participant $participant)
    {
        //
    }

    public function save_signature($sign)
    {

        $folderPath = public_path('/app_image/uploads/signatures/');

        $image_parts = explode(";base64,", $sign);

        $image_type_aux = explode("image/", $image_parts[0]);

        $image_type = $image_type_aux[1];

        $image_base64 = base64_decode($image_parts[1]);

        $signature = uniqid() . '.' . $image_type;

        $file = $folderPath . $signature;

        file_put_contents($file, $image_base64);

        return $signature;
    }
}
