<?php

namespace App\Http\Controllers;

use App\Models\ActivityStatus;
use Illuminate\Http\Request;

class ActivityStatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ActivityStatus  $activityStatus
     * @return \Illuminate\Http\Response
     */
    public function show(ActivityStatus $activityStatus)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ActivityStatus  $activityStatus
     * @return \Illuminate\Http\Response
     */
    public function edit(ActivityStatus $activityStatus)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ActivityStatus  $activityStatus
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ActivityStatus $activityStatus)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ActivityStatus  $activityStatus
     * @return \Illuminate\Http\Response
     */
    public function destroy(ActivityStatus $activityStatus)
    {
        //
    }
}
