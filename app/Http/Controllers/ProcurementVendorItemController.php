<?php

namespace App\Http\Controllers;

use App\Models\ProcurementVendorItem;
use Illuminate\Http\Request;

class ProcurementVendorItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ProcurementVendorItem  $procurementVendorItem
     * @return \Illuminate\Http\Response
     */
    public function show(ProcurementVendorItem $procurementVendorItem)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ProcurementVendorItem  $procurementVendorItem
     * @return \Illuminate\Http\Response
     */
    public function edit(ProcurementVendorItem $procurementVendorItem)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ProcurementVendorItem  $procurementVendorItem
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProcurementVendorItem $procurementVendorItem)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ProcurementVendorItem  $procurementVendorItem
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProcurementVendorItem $procurementVendorItem)
    {
        //
    }
}
