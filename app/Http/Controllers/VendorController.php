<?php

namespace App\Http\Controllers;

use App\Models\Vendor;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class VendorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vendors = Vendor::where('vendor_status',1)->get();
        $title = "Vendors";

        return view('supplier.index', compact('title', 'vendors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $vendor_check = Vendor::where('vendor_contact', $request->vendor_contact)->orWhere('vendor_email', $request->vendor_email)->get()->toArray();
        if (count($vendor_check) < 1) {
            $vendor = new Vendor();
            $vendor->vendor_ref = Str::uuid();
            $vendor->user_id = auth()->user()->id;
            $vendor->vendor_name = Str::upper($request->vendor_name);
            $vendor->vendor_number = $request->vendor_number;
            $vendor->vendor_contact = $request->vendor_contact;
            $vendor->vendor_email = $request->vendor_email;
            $vendor->vendor_address = $request->vendor_address;
            $vendor->vendor_signature = $this->save_signature($request->signature);
            $vendor->save();

            alert()->success('Vendor Successfully Registered', 'Good Job!')->persistent('Close');

            return redirect(route('vendors.data'));
        } else {
            alert()->error('Vendor already exists with entered mobile and email', 'Sorry!')->persistent('Close');

            return redirect(route('vendors.data'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */
    public function show(Vendor $vendor)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */
    public function edit($vendor_ref)
    {
        $title = "Edit Vendor";
        $vendor = Vendor::where('vendor_ref', $vendor_ref)->first();
        return view('supplier.edit', compact('title', 'vendor'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$vendor_ref)
    {
        $vendor = Vendor::where('vendor_ref', $vendor_ref)->first();
        $vendor->user_id = auth()->user()->id;
        $vendor->vendor_name = Str::upper($request->vendor_name);
        $vendor->vendor_number = $request->vendor_number;
        $vendor->vendor_contact = $request->vendor_contact;
        $vendor->vendor_email = $request->vendor_email;
        $vendor->vendor_address = $request->vendor_address;
        $vendor->vendor_signature = $this->save_signature($request->signature);
        $vendor->save();

        alert()->success('Vendor Successfully Updated', 'Good Job!')->persistent('Close');

        return redirect(route('vendors.data'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */
    public function destroy(Vendor $vendor)
    {
        //
    }
    public function save_signature($sign)
    {

        $folderPath = public_path('/app_image/uploads/signatures/vendor/');

        $image_parts = explode(";base64,", $sign);

        $image_type_aux = explode("image/", $image_parts[0]);

        $image_type = $image_type_aux[1];

        $image_base64 = base64_decode($image_parts[1]);

        $signature = uniqid() . '.' . $image_type;

        $file = $folderPath . $signature;

        file_put_contents($file, $image_base64);

        return $signature;
    }
}
