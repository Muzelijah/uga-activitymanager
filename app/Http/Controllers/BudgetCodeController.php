<?php

namespace App\Http\Controllers;

use App\Models\AreaOffice;
use App\Models\BudgetCode;
use Dotenv\Util\Str;
use Illuminate\Http\Request;

class BudgetCodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $area_offices = AreaOffice::whereIn('id',json_decode(auth()->user()->area_offices))->get();
        $budget_codes = BudgetCode::all();
        $title = "Budget Codes";
        return  view('budget.code.index',compact('budget_codes','area_offices','title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $budget_code = new BudgetCode();
        $budget_code->user_id = auth()->user()->id;
        $budget_code->budget_code_ref = \Illuminate\Support\Str::uuid();
        $budget_code->budget_code_name = $request->budget_code_name;
        $budget_code->budget_code_description = $request->budget_code_description;
        $budget_code->save();

        alert()->success('Budget Code Created', 'Good Job!')->persistent('Close');

        return redirect(route('budget.codes.data'));


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\BudgetCode  $budgetCode
     * @return \Illuminate\Http\Response
     */
    public function show(BudgetCode $budgetCode)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\BudgetCode  $budgetCode
     * @return \Illuminate\Http\Response
     */
    public function edit($budgetCode)
    {
        $budget_code = BudgetCode::where('budget_code_ref',$budgetCode)->first();
        $title = "Edit Budget Code";

        return view('budget.code.edit',compact('budget_code','title'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\BudgetCode  $budgetCode
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$budgetCode)
    {
        $budget_code = BudgetCode::where('budget_code_ref',$budgetCode)->first();
        $budget_code->user_id = auth()->user()->id;
        $budget_code->budget_code_name = $request->budget_code_name;
        $budget_code->budget_code_description = $request->budget_code_description;
        $budget_code->save();

        alert()->success('Budget Code Updated', 'Good Job!')->persistent('Close');

        return redirect(route('budget.codes.data'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\BudgetCode  $budgetCode
     * @return \Illuminate\Http\Response
     */
    public function destroy(BudgetCode $budgetCode)
    {
        //
    }
}
