<?php

namespace App\Http\Controllers;

use App\Helpers\Custom;
use App\Models\Activity;
use App\Models\ActivityAttendance;
use App\Models\ActivityParticipant;
use App\Models\ActivityPayment;
use App\Models\ActivityStatus;
use App\Models\ActivityType;
use App\Models\AreaOffice;
use App\Models\BudgetCode;
use App\Models\Participant;
use App\Models\User;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;

class ActivityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($status)
    {
        $hide = 0;
        $activity_statuses = ActivityStatus::where('active',true)->where('id','>',$status)->get();
        $users = User::all();
        $area_offices = Custom::getAreaOffices();
        if(Route::currentRouteName() == "activities.data"){
            $activities = Activity::where('activity_status_id',$status)->whereIn('area_office_id',json_decode(auth()->user()->area_offices))->get();
            $title = "All Activities";
            return view('activity.index',compact('activities','title','status','activity_statuses','area_offices','hide','users'));
        }else{
            $hide = 1;
            $activities = Activity::where('activity_status_id',$status)->whereIn('area_office_id',json_decode(auth()->user()->area_offices))->where('user_id',auth()->user()->id)->get();
            $title = "My Activities";
            return view('activity.my',compact('activities','title','status','activity_statuses','area_offices','hide','users'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pending_activities = Activity::where('activity_status_id',1)->whereIn('area_office_id',json_decode(auth()->user()->area_offices))->where('user_id',auth()->user()->id)->get();
        $budget_codes = BudgetCode::where('active',true)->get();
        $area_offices = Custom::getAreaOffices();
        $activity_types = ActivityType::all();
        $users = User::all();
        $title = "Create New Activity";
        return view('activity.create',compact('pending_activities','title','budget_codes','activity_types','area_offices','users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());

        $activity = new Activity();
        $activity->area_office_id = $request->area_office_id;
        $activity->activity_ref = Str::uuid();
        $activity->activity_code = date('YmdHis');
        $activity->user_id = auth()->user()->id;
        $activity->assigned_to = auth()->user()->id;
        $activity->activity_name = $request->activity_name;
        $activity->activity_type_id = $request->activity_type_id;
        $activity->budget_code_id = $request->budget_code_id;
        $activity->estimated_budget_amount = $request->estimated_budget_amount;
        $activity->estimated_participants = $request->estimated_participants;
        $activity->activity_start_date = $request->activity_start_date;
        $activity->activity_end_date = $request->activity_end_date;
        $activity->activity_start_time = $request->activity_start_time;
        $activity->activity_end_time = $request->activity_end_time;
        $activity->activity_venue_address = $request->activity_venue_address;
        $activity->additional_information = $request->additional_information;
        if($request->hasFile('activity_concept_note')){
         $activity->activity_concept_note = $request->file('activity_concept_note')->store('/uploads/activities');
        }
        $activity->save();

        alert()->success('Activity Successfully Created', 'Good Job!')->persistent('Close');

        return redirect(route('activities.create'));

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Activity  $activity
     * @return \Illuminate\Http\Response
     */
    public function show(Activity $activity)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Activity  $activity
     * @return \Illuminate\Http\Response
     */
    public function edit($activity_ref)
    {
        $activity = Activity::where('activity_ref',$activity_ref)->first();
        $area_offices = Custom::getAreaOffices();
        $budget_codes = BudgetCode::where('active',true)->get();
        $activity_types = ActivityType::all();
        $title = "Edit Activity";
        //dd($activity);
        return view('activity.edit',compact('activity','title','budget_codes','activity_types','area_offices'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Activity  $activity
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$activity_ref)
    {
        $activity =  Activity::where('activity_ref',$activity_ref)->first();
        $activity->area_office_id = $request->area_office_id;
        $activity->user_id = auth()->user()->id;
        $activity->assigned_to = auth()->user()->id;
        $activity->activity_name = $request->activity_name;
        $activity->activity_type_id = $request->activity_type_id;
        $activity->budget_code_id = $request->budget_code_id;
        $activity->estimated_budget_amount = $request->estimated_budget_amount;
        $activity->estimated_participants = $request->estimated_participants;
        $activity->activity_start_date = $request->activity_start_date;
        $activity->activity_end_date = $request->activity_end_date;
        $activity->activity_start_time = $request->activity_start_time;
        $activity->activity_end_time = $request->activity_end_time;
        $activity->activity_venue_address = $request->activity_venue_address;
        $activity->additional_information = $request->additional_information;
        if($request->hasFile('activity_concept_note')){
            $activity->activity_concept_note = $request->file('activity_concept_note')->store('/uploads/activities');
        }
        $activity->save();

        alert()->success('Activity Successfully Updated', 'Good Job!')->persistent('Close');

        return redirect(route('activities.create'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Activity  $activity
     * @return \Illuminate\Http\Response
     */
    public function destroy(Activity $activity)
    {
        //
    }

    public function status(Request $request)
    {

        //dd($request->all());

        $title = "Activities";
        if($request->has("selected_activities") && $request->selected_activities != 1){

            $count = 0;
            $activity_status = 0;
            foreach ($request->selected_activities as $activity_ref) {
                $activity = Activity::where('activity_ref', $activity_ref)->first();
                if (!empty($activity)) {
                    $activity->activity_status_id = $request->new_activity_status_id;
                    $activity->assigned_to = $request->assigned_to;
                    $activity->activity_remark = $request->activity_remark;
                    $count = $count + 1;
                    if ($request->new_activity_status_id == 3) {
                        $activity_status = 3;
                        $msg = "" . $count . " Activities has been marked as Approved";
                    } elseif ($request->new_activity_status_id == 4) {
                        $activity_status = 4;
                        $msg = "" . $count . " Activities has been marked as Complete";
                    } elseif ($request->new_activity_status_id == 5) {
                        $activity_status = 5;
                        $msg = "" . $count . " Activities has been marked as Returned";
                    } elseif ($request->new_activity_status_id == 5) {
                        $activity_status = 5;
                        $msg = "Sorry! Activities were marked as cancelled";
                    } else {
                        $activity_status = 5;
                        $msg = "Sorry! Order were marked as completed";
                    }

                    $activity->save();



                }
            }
            alert()->success('' . $msg . '', 'Good Job!')->persistent('Close');
            return redirect(route('activities.data', ['status' => $activity_status]));

        }else{

            alert()->error('Please select at least one or more activities before processing ', 'Sorry!')->persistent('Close');
            return redirect()->back();
        }

    }

    public function send_for_approval($activity_ref){

        $activity = Activity::where('activity_ref',$activity_ref)->first();
        $activity->activity_status_id = 2;
        $activity->save();

        alert()->success('Activity has been sent for approval', 'Good Job!')->persistent('Close');
        return redirect(route('activities.unverified'));

    }

    public function add_participants($activity_ref){

        $activity = Activity::where('activity_ref',$activity_ref)->first();

        $selected_participants = ActivityParticipant::where('activity_id',$activity->id)->pluck('participant_id');

        $title = "Add Participants to ".$activity->activity_name." (".$activity->activity_code.")";

        $participants = Participant::whereNotIn('id',$selected_participants)->get(['id','participant_ref','participant_title','participant_first_name','participant_other_names']);

        $added_participants = ActivityParticipant::where('activity_id',$activity->id)->get();

        return view('activity.participant.add',compact('title','participants','activity','added_participants'));
    }

    public function store_added_participants(Request $request,$activity_ref){

        $count = 0;
        $activity = Activity::where('activity_ref',$activity_ref)->first();
        foreach ($request->total_participants as $participant){
            $activity_participant = new ActivityParticipant();
            $activity_participant->activity_participant_ref = Str::uuid();
            $activity_participant->activity_id = $activity->id;
            $activity_participant->participant_id = Participant::where('participant_ref',$participant)->first()->id;
            $activity_participant->date_added = date('Y-m-d');
            $activity_participant->save();
            $count = $count + 1;

        }
        $msg = "" . $count . " Participants have been added to activity";
        alert()->success('' . $msg . '', 'Good Job!')->persistent('Close');
        return redirect(route('my.activities.add.participants', ['activity' => $activity->activity_ref]));


    }

    public function log_attendance(Request $request,$activity_ref){

        $activity = Activity::where('activity_ref',$activity_ref)->first();
        $attendance_list = ActivityAttendance::where('activity_id',$activity->id)->where('date_added',date('Y-m-d'))->where('activity_attendance_status',3)->pluck('participant_id');
        $participants_in_activity = ActivityParticipant::where('activity_id',$activity->id)->whereNotIn('participant_id',$attendance_list)->get();
        $participants_in_attendance = ActivityAttendance::where('activity_id',$activity->id)->where('activity_attendance_status',3)->get();
        $title = "Log Attendance for ".$activity->activity_name." (".$activity->activity_code.")";

        return view('activity.participant.attendance',compact('title','activity','participants_in_attendance','participants_in_activity'));
    }

    public function send_code($activity_participant_ref){

        $activity_participant = ActivityParticipant::where('activity_participant_ref',$activity_participant_ref)->first();

        $participant = Participant::where('id',$activity_participant->participant_id)->first();
        //dd(!empty($participant));
        if(!empty($participant)){
           //  dd($participant);
            $attendance_check = ActivityAttendance::where('activity_id',$activity_participant->activity_id)->where('participant_id',$participant->id)->where('date_added',date('Y-m-d'))->first();
            if(empty($attendance_check)){
                $attendance = new ActivityAttendance();
                $attendance->activity_attendance_ref = Str::uuid();
                $attendance->activity_id = $activity_participant->activity_id;
                $attendance->participant_id = $activity_participant->participant_id;
                $attendance->activity_attendance_code = rand(100000,1000000);
                $attendance->date_added = date('Y-m-d');
                $attendance->activity_attendance_status = 1;
                $attendance->save();

                if($attendance->activity_attendance_status == 1){
                    $msg = "Dear ".$participant->participant_title." ".$participant->participant_first_name." ".$participant->participant_other_names." Your Attendance Code is ".$attendance->activity_attendance_code." Thanks";
                    Custom::SendPremiumMessage($participant->participant_mobile,$msg);
                    return true;
                }

            }else{
                $attendance_check->activity_attendance_code = rand(100000,1000000);
                $attendance_check->save();
                $msg = "Dear ".$participant->participant_title." ".$participant->participant_first_name." ".$participant->participant_other_names." Your Attendance Code is ".$attendance_check->activity_attendance_code." Thanks";
                Custom::SendPremiumMessage($participant->participant_mobile,$msg);
                return true;
            }


        }


    }

    public function add_attendance(Request $request){

        //dd($request->all());
        $activity_participant = ActivityParticipant::where('activity_participant_ref',$request->activity_participant_id)->first();
        if(!empty($activity_participant)){
            $activity = Activity::where('id',$activity_participant->activity_id)->first();
            $attendance_check = ActivityAttendance::where('activity_id',$activity_participant->activity_id)->where('participant_id',$activity_participant->participant_id)->where('date_added',date('Y-m-d'))->first();
            if(!empty($attendance_check)){
                if($attendance_check->activity_attendance_code == $request->attendance_code){
                    $attendance_check->activity_attendance_status = 3;
                    $attendance_check->date_added = date('Y-m-d');
                    $attendance_check->activity_attendance_date = date('Y-m-d H:i:s');
                    $attendance_check->activity_attendance_confirmed = date('Y-m-d H:i:s');
                    $attendance_check->save();

                    alert()->success('Attendance Added', 'Good Job!')->persistent('Close');
                    return redirect(route('my.activities.log.attendance', ['activity' => $activity->activity_ref]));

                }else{
                    alert()->error('Invalid Code', 'Sorry!')->persistent('Close');
                    return redirect(route('my.activities.log.attendance', ['activity' => $activity->activity_ref]));

                }

            }else{
                alert()->error('Attendant was already registered', 'Sorry!')->persistent('Close');
                return redirect(route('my.activities.log.attendance', ['activity' => $activity->activity_ref]));

            }
        }

    }
    public function download_attendance($activity_ref){

        $activity = Activity::where('activity_ref',$activity_ref)->first();

        $participants_in_attendance = ActivityAttendance::where('activity_id',$activity->id)->get();
        $title = "Attendance for ".$activity->activity_name." (".$activity->activity_code.")";

        $data = array();
        $data["activity"] = $activity;
        $data["attendance"] = $participants_in_attendance;
        $data['title'] = $title;

        $pdf = PDF::loadView('pdftemplates.attendance', $data);
        $pdf->setPaper('A4', 'landscape');

        $file_name = Str::replace(" ","_",$title).".pdf";


        return $pdf->download($file_name);




    }

    public function un_verified()
    {
        $unverified_activities = Activity::where('activity_status_id',1)->get();
        $title = "Unverified Activities";
        return view('activity.unverified',compact('unverified_activities','title'));
    }

    public function un_approved()
    {
        $unapproved_activities = Activity::where('activity_status_id',2)->get();
        $title = "Unapproved Activities";
        return view('activity.unapproved',compact('unapproved_activities','title'));
    }

    public function mark_as_approved($activity_ref){

        $activity = Activity::where('activity_ref',$activity_ref)->first();
        if(!empty($activity)){
            $activity->activity_status_id = 3;
            $activity->save();

            alert()->success('Activity has been Approved', 'Good Job!')->persistent('Close');
            return redirect(route('activities.unapproved'));
        }else{

        }


    }
    public function close_activity($activity_ref){

        $activity = Activity::where('activity_ref',$activity_ref)->where('user_id',auth()->user()->id)->first();
        $activity->activity_status_id = 4;
        $activity->save();

        alert()->success('Activity has been Closed', 'Good Job!')->persistent('Close');
        return redirect(route('my.activities.data',['status'=>3]));

    }

    public function generate_payment_list($activity_ref){

        $activity = Activity::where('activity_ref',$activity_ref)->first();
        if(!empty($activity)){

            $attendance_summary = ActivityAttendance::select('participant_id')->where('activity_id',$activity->id)->groupBy('participant_id')->get();
            //dd($attendance_summary);
            foreach ($attendance_summary as $attendant){
                //dd($attendant->participant_id);
                $attendant_check = ActivityPayment::where('activity_id',$activity->id)->where('participant_id',$attendant->participant_id)->first();
                if(empty($attendant_check)){
                    $participant = Participant::where('id',$attendant->participant_id)->first();
                    $activity_payment = new ActivityPayment();
                    $activity_payment->activity_payment_ref = Str::uuid();
                    $activity_payment->activity_id = $activity->id;
                    $activity_payment->participant_id = $attendant->participant_id;
                    $activity_payment->days_attended = count(ActivityAttendance::where('activity_id',$activity->id)->where('participant_id',$attendant->participant_id)->get()->toArray());
                    $activity_payment->daily_rate = 0;
                    $activity_payment->participant_payment_mobile = $participant->participant_mobile;
                    $activity_payment->participant_sign = $participant->participant_sign;
                    $activity_payment->date_added = date('Y-m-d');
                    $activity_payment->activity_payment_date = date('Y-m-d H:i:s');
                    $activity_payment->activity_payment_status_id = 1;
                    $activity_payment->save();


                }
            }

            $activity_payments = ActivityPayment::where('activity_id',$activity->id)->get();
            $title = "Activity Payments For ".$activity->activity_name." (".$activity->activity_code.")";

            return view('activity.payments',compact('activity_payments','title','activity'));

        }

    }

    public function update_payment_list(Request $request,$activity_ref){

        foreach ($request->daily_rate as $key=>$value){
            $activity_payment = ActivityPayment::where('activity_payment_ref',$key)->first();
            if(!empty($activity_payment)){
                $activity_payment->daily_rate = $value;
                $activity_payment->line_total = $activity_payment->days_attended * $value;
                $activity_payment->save();
            }
        }
        foreach ($request->mobile as $key=>$value){
            $activity_payment = ActivityPayment::where('activity_payment_ref',$key)->first();
            if(!empty($activity_payment)){
                $activity_payment->participant_payment_mobile = $value;
                $activity_payment->save();
            }
        }
        foreach ($request->fuel_refund as $key=>$value){
            $activity_payment = ActivityPayment::where('activity_payment_ref',$key)->first();
            if(!empty($activity_payment)){
                $activity_payment->fuel_refund = $value;
                $activity_payment->save();
            }
        }
        foreach ($request->amount_deducted as $key=>$value){
            $activity_payment = ActivityPayment::where('activity_payment_ref',$key)->first();
            if(!empty($activity_payment)){
                $activity_payment->amount_deducted = $value;
                $activity_payment->save();
            }
        }
        foreach ($request->mobile_names as $key=>$value){
            $activity_payment = ActivityPayment::where('activity_payment_ref',$key)->first();
            if(!empty($activity_payment)){
                $activity_payment->participant_payment_mobile_names = Str::title($value);
                $activity_payment->save();
            }
        }

        return redirect(route('my.activities.generate.payments',['activity'=>$activity_ref]));
    }

    public function send_payments_for_verification($activity_ref){

        $activity = Activity::where('activity_ref',$activity_ref)->where('activity_status_id',4)->where('user_id',auth()->user()->id)->first();
        if(!empty($activity)){
            ActivityPayment::where('activity_id',$activity->id)->update(['activity_payment_status_id'=>2]);
            $activity->activity_payment_status_id = 2;
            $activity->save();
            alert()->success('Activity payments have been sent for verification', 'Good Job!')->persistent('Close');
            return redirect(route('my.activities.generate.payments',['activity'=>$activity_ref]));

        }else{
            alert()->error('Invalid Activity', 'Sorry!')->persistent('Close');
            return redirect()->back();

        }



    }

    public function show_unverified_payments(){

        $closed_activities = Activity::where('activity_status_id',4)->where('activity_payment_status_id',2)->get();
        $title = "Unverified Payments";
        //dd($activity_payments->toSql());
        return view('activity.payments.unverified',compact('closed_activities','title'));

    }

    public function show_unapproved_payments(){

        $closed_activities = Activity::where('activity_status_id',4)->where('activity_payment_status_id',3)->get();
        $title = "Unapproved Payments";
        //dd($activity_payments->toSql());
        return view('activity.payments.unapproved',compact('closed_activities','title'));

    }

    public function send_payments_for_approval($activity_ref){

        $activity = Activity::where('activity_ref',$activity_ref)->where('activity_status_id',4)->where('activity_payment_status_id',2)->first();
        if(!empty($activity)){
            ActivityPayment::where('activity_id',$activity->id)->update(['activity_payment_status_id'=>3]);
            $activity->activity_payment_status_id = 3;
            $activity->save();
            alert()->success('Activity payments have been sent for approval', 'Good Job!')->persistent('Close');
            return redirect(route('activities.show.unverified.payments'));

        }else{
            alert()->error('Invalid Activity', 'Sorry!')->persistent('Close');
            return redirect()->back();

        }



    }

    public function show_approved_payments(){

        $closed_activities = Activity::where('activity_status_id',4)->where('activity_payment_status_id',4)->get();
        $title = "Unapproved Payments";
        //dd($activity_payments->toSql());
        return view('activity.payments.approved',compact('closed_activities','title'));

    }

    public function show_payments_list($activity_ref){

        $activity = Activity::where('activity_ref',$activity_ref)->where('activity_status_id',4)->first();

        $activity_payments = ActivityPayment::where('activity_id',$activity->id)->get();
        $title = "Activity Payments For ".$activity->activity_name." (".$activity->activity_code.")";

        return view('activity.payments.list',compact('activity_payments','title','activity'));

    }

    public function approve_payments($activity_ref){

        $activity = Activity::where('activity_ref',$activity_ref)->where('activity_status_id',4)->where('activity_payment_status_id',3)->first();
        if(!empty($activity)){
            ActivityPayment::where('activity_id',$activity->id)->update(['activity_payment_status_id'=>4]);
            $activity->activity_payment_status_id = 4;
            $activity->save();
            alert()->success('Activity payments have been approved', 'Good Job!')->persistent('Close');
            return redirect(route('activities.show.unapproved.payments'));

        }else{
            alert()->error('Invalid Activity', 'Sorry!')->persistent('Close');
            return redirect()->back();

        }
    }

    public function download_payment_list($activity_ref){

        $activity = Activity::where('activity_ref',$activity_ref)->first();

        $payment_list = ActivityPayment::where('activity_id',$activity->id)->get();
        $title = "Payment List for ".$activity->activity_name." (".$activity->activity_code.")";

        $data = array();
        $data["activity"] = $activity;
        $data["payments"] = $payment_list;
        $data['title'] = $title;

        $pdf = PDF::loadView('pdftemplates.paymentlist', $data);
        $pdf->setPaper('A4', 'landscape');

        $file_name = Str::replace(" ","_",$title).".pdf";


        return $pdf->download($file_name);




    }

    public function search(Request $request){
        $search_text = $request->search_text;
        $activities = Activity::orWhere('activity_name','LIKE','%'.$search_text.'%')
            ->orWhere('activity_code','LIKE','%'.$search_text.'%')
            ->get();
        $title = "Activity Search";

        return view('activity.search',compact('activities','title'));
    }

    public function concept_note_preview($activity_ref){
        $activity = Activity::where('activity_ref',$activity_ref)->first();
        $title = "Preview Concept Note";
        return view('activity.preview',compact('activity','title'));
    }

    public function send_invitations($activity_ref){

        $activity = Activity::where('activity_ref',$activity_ref)->first();
        $participants = ActivityParticipant::where('activity_id',$activity->id)->where('send_invitations',0)->get();
        foreach ($participants as $participant){

            $note = "Dear ".$participant->participant->participant_first_name." ".$participant->participant->participant_other_names." you have been invited to attend the ".$activity->activity_name." held on ".$activity->activity_start_date." at ".$activity->activity_venue_address.". Kindly Click on button below to confirm your attendance";

            \App\Helpers\Custom::SendMail($participant->participant->participant_email,'Welcome','Hello '.$participant->participant->participant_first_name." ".$participant->participant->participant_other_names,$note,'Click Here to Confirm Attendance',\route('activities.participants.confirm.attendance',['activity'=>$activity->activity_ref,'participant'=>$participant->activity_participant_ref]));
             $participant->send_invitations = 1;
             $participant->save();

        }

        alert()->success('Activity Invitations Sent', 'Good Job!')->persistent('Close');
        return redirect(route('my.activities.add.participants',['activity'=>$activity->activity_ref]));


    }



}
