<?php

namespace App\Http\Controllers;

use App\Models\ProcurementItem;
use App\Models\ProcurementRequest;
use App\Models\UnitMeasure;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ProcurementItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($procurement_request_ref)
    {
        $procurement_request = ProcurementRequest::where('procurement_request_ref',$procurement_request_ref)->first();
        $title = "Procurement Request Items for ".$procurement_request->activity->activity_name." (".$procurement_request->activity->activity_code.")";
        $procurement_request_items = ProcurementItem::where('procurement_request_id',$procurement_request->id)->get();
        $unit_measures = UnitMeasure::where('active',1)->orderBy('unit_measure_name')->get();


        return view('procurement.requests.item.index',compact('procurement_request','title','unit_measures','procurement_request_items'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$procurement_request_ref)
    {
        $procurement_request = ProcurementRequest::where('procurement_request_ref',$procurement_request_ref)->first();

        $procurement_item = new ProcurementItem();
        $procurement_item->procurement_item_ref = Str::uuid();
        $procurement_item->user_id = auth()->user()->id;
        $procurement_item->activity_id = $procurement_request->activity_id;
        $procurement_item->procurement_request_id = $procurement_request->id;
        $procurement_item->item_description = $request->item_description;
        $procurement_item->item_qty = $request->item_qty;
        $procurement_item->unit_measure_id = $request->unit_measure_id;
        $procurement_item->total_days = $request->total_days;
        $procurement_item->save();

        alert()->success('Procurement Item has been Added', 'Good Job!')->persistent('Close');
        return redirect(route('procurement.items.data',['request'=>$procurement_request_ref]));

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ProcurementItem  $procurementItem
     * @return \Illuminate\Http\Response
     */
    public function show(ProcurementItem $procurementItem)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ProcurementItem  $procurementItem
     * @return \Illuminate\Http\Response
     */
    public function edit($procurement_request_ref,$procurement_item_ref)
    {
        $procurement_item = ProcurementItem::where('procurement_item_ref',$procurement_item_ref)->first();

        $procurement_request = ProcurementRequest::where('procurement_request_ref',$procurement_request_ref)->first();

        $title = "Edit Procurement Item";

        $unit_measures = UnitMeasure::all();

        return view('procurement.requests.item.edit',compact('procurement_request','title','unit_measures','procurement_item'));


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ProcurementItem  $procurementItem
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$procurement_request_ref,$procurement_item_ref)
    {
        $procurement_item = ProcurementItem::where('procurement_item_ref',$procurement_item_ref)->first();
        $procurement_item->user_id = auth()->user()->id;
        $procurement_item->activity_id = $procurement_item->procurement_request->activity_id;
        $procurement_item->procurement_request_id = $procurement_item->procurement_request->id;
        $procurement_item->item_description = $request->item_description;
        $procurement_item->item_qty = $request->item_qty;
        $procurement_item->unit_measure_id = $request->unit_measure_id;
        $procurement_item->total_days = $request->total_days;
        $procurement_item->save();

        alert()->success('Procurement Item has been Updated', 'Good Job!')->persistent('Close');
        return redirect(route('procurement.items.data',['request'=>$procurement_request_ref]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ProcurementItem  $procurementItem
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProcurementItem $procurementItem)
    {
        //
    }
}
