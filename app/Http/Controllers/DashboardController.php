<?php

namespace App\Http\Controllers;

use App\Helpers\Custom;
use App\Models\Activity;
use App\Models\AreaOffice;
use App\Models\User;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clients = array();
        $branches = array();
        $settlements = array();
        $users = User::all();
        $zones = array();
        $payments = array();
        $area_offices = AreaOffice::all();
        $approved_activites = Activity::where('activity_status_id',3)->get();
        $upcoming_activities = Activity::where('activity_start_date','>=',date('Y-m-d'))->where('activity_status_id',3)->get();
        $all_activities = Activity::all();
        $messages = 0;

        $title = "Activity Manager Admin Dashboard";

        return view('dashboard.index',compact('branches','settlements','clients','users','zones','messages','payments','title','approved_activites','all_activities','upcoming_activities','area_offices'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function syncloans(){


    }

    public function home(){

        return view('welcome');
    }
}
