<?php

namespace App\Http\Controllers;

use App\Helpers\Custom;
use App\Models\ProcurementInvitation;
use App\Models\ProcurementRequest;
use App\Models\ProcurementVendorItem;
use Illuminate\Http\Request;

class ProcurementInvitationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($procurement_request_ref)
    {
        $procurement_invitations = ProcurementInvitation::where('procurement_request_id',ProcurementRequest::where('procurement_request_ref',$procurement_request_ref)->first()->id)->get();

        $title = "Procurement Request Invitations";

        return view('procurement.invitation.index',compact('title','procurement_invitations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($procurement_request_ref)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ProcurementInvitation  $procurementInvitation
     * @return \Illuminate\Http\Response
     */
    public function show(ProcurementInvitation $procurementInvitation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ProcurementInvitation  $procurementInvitation
     * @return \Illuminate\Http\Response
     */
    public function edit(ProcurementInvitation $procurementInvitation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ProcurementInvitation  $procurementInvitation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProcurementInvitation $procurementInvitation)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ProcurementInvitation  $procurementInvitation
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProcurementInvitation $procurementInvitation)
    {
        //
    }

    public function view_invitation(Request $request,$procurement_invitation_ref){

        $invitation = ProcurementInvitation::where('procurement_invitation_ref',$procurement_invitation_ref)->first();
        if(!empty($invitation)){

        $title = "Vendor Procurement Request";

        return view('procurement.invitation.preview',compact('title','invitation'));

        }else{
            dd('Sorry! Invitation Expired');
        }

    }

    public function save_unit_prices(Request $request,$procurement_invitation_ref){
        //dd($request->all());

        foreach ($request->unit_price as $key=>$value){
            $vendor_item = ProcurementVendorItem::where('procurement_vendor_ref',$key)->first();
            if(!empty($vendor_item)){
                $vendor_item->unit_price = $value;
                $vendor_item->line_total = $vendor_item->item_qty * $vendor_item->total_days * $value;
                $vendor_item->save();
            }
        }

        alert()->success('Unit Prices have been saved', 'Good Job!')->persistent('Close');
        return redirect(route('procurement.requests.vendors.invitations.view', ['invitation' => $procurement_invitation_ref]));

    }

    public function send_code($procurement_invitation_ref){

        $procurement_invitation = ProcurementInvitation::where('procurement_invitation_ref',$procurement_invitation_ref)->first();

        if(!empty($procurement_invitation)){

             $msg = "Dear ".$procurement_invitation->vendor->vendor_name." Your Submission Code is ".$procurement_invitation->invitation_submission_code." Thanks";
             Custom::SendPremiumMessage($procurement_invitation->procurement_invitation_mobile,$msg);
             return true;


        }
    }

    public function submit_request(Request $request,$procurement_invitation_ref){
        //dd($request->all());
        $procurement_invitation = ProcurementInvitation::where('procurement_invitation_ref',$procurement_invitation_ref)->first();
        if(!empty($procurement_invitation)){
            if($request->submit_code == $procurement_invitation->invitation_submission_code){
                if($request->hasFile('quotation')){
                    $procurement_invitation->quotation_copy = $request->file('quotation')->store('/uploads/quotations');
                }
                $procurement_invitation->procurement_invitation_status = 2;
                $procurement_invitation->procurement_invitation_sign = $procurement_invitation->vendor->vendor_signature;
                $procurement_invitation->save();

                alert()->success('Your Response has been submitted successfully', 'Good Job!')->persistent('Close');
                return true;
                //return redirect(route('procurement.requests.vendors.invitations.view', ['invitation' => $procurement_invitation_ref]));

            }else{
                $msg = "Dear ".$procurement_invitation->vendor->vendor_name." Your Submission Code is ".$procurement_invitation->invitation_submission_code." Thanks";
                Custom::SendPremiumMessage($procurement_invitation->procurement_invitation_mobile,$msg);

                alert()->error('Your Submit Code is Invalid a new code has been sent to your phone', 'Sorry !')->persistent('Close');
                return true;
                //return redirect(route('procurement.requests.vendors.invitations.view', ['invitation' => $procurement_invitation_ref]));

            }

        }

    }
}
