<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $role =  new \App\Models\Role();
        $role->syncPermissions();
        $title = "Roles";
        $roles = Role::all();
        $permissions = Permission::all();
        return view('role.index',compact('title','permissions','roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       //dd($request->all());
        $role = \Spatie\Permission\Models\Role::updateorCreate([
            "name"=>$request->role_name
        ],[
            "name"=>$request->role_name,
            "guard_name"=>"web"
        ]);
        //sync all permissions to Super Admin
        $role->syncPermissions($request->permissions);

        alert()->success('Role Added Successfully', 'Good Job!')->persistent('Close');
        return redirect(route('roles.data'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role::findById($id);
        $role_permissions = $role->permissions()->get()->pluck('name')->toArray();
        $permissions = Permission::all();
        $title = "Edit Role";
        return view('role.edit',compact('role','permissions','title','role_permissions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $role = Role::findById($id);
        $role->name = $request->role_name;
        $role->save();

        $role->syncPermissions($request->permissions);

        alert()->success('Role Updated Successfully', 'Good Job!')->persistent('Close');
        return redirect(route('roles.data'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
