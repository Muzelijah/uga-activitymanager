<?php

namespace App\Http\Controllers;

use App\Models\ActivityPayment;
use Illuminate\Http\Request;

class ActivityPaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ActivityPayment  $activityPayment
     * @return \Illuminate\Http\Response
     */
    public function show(ActivityPayment $activityPayment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ActivityPayment  $activityPayment
     * @return \Illuminate\Http\Response
     */
    public function edit(ActivityPayment $activityPayment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ActivityPayment  $activityPayment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ActivityPayment $activityPayment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ActivityPayment  $activityPayment
     * @return \Illuminate\Http\Response
     */
    public function destroy(ActivityPayment $activityPayment)
    {
        //
    }
}
