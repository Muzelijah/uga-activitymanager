<?php

namespace App\Http\Controllers;

use App\Models\UnitMeasure;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class UnitMeasureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $unit_measures = UnitMeasure::all();
        $title = "Unit Measures";
        return view('unit.measure.index',compact('title','unit_measures'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $unit_measure = new UnitMeasure();
        $unit_measure->user_id = auth()->user()->id;
        $unit_measure->unit_measure_ref = Str::uuid();
        $unit_measure->unit_measure_name = $request->unit_measure_name;
        $unit_measure->save();

        alert()->success('Unit Measure has been Added', 'Good Job!')->persistent('Close');
        return redirect(route('unit.measures.data'));


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\UnitMeasure  $unitMeasure
     * @return \Illuminate\Http\Response
     */
    public function show(UnitMeasure $unitMeasure)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\UnitMeasure  $unitMeasure
     * @return \Illuminate\Http\Response
     */
    public function edit($unit_measure_ref)
    {
        $unit_measure = UnitMeasure::where('unit_measure_ref',$unit_measure_ref)->first();
        $unit_measures = UnitMeasure::all();
        $title = "Edit Unit Measure";
        return view('unit.measure.edit',compact('title','unit_measure','unit_measures'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\UnitMeasure  $unitMeasure
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $unit_measure_ref)
    {
        $unit_measure = UnitMeasure::where('unit_measure_ref',$unit_measure_ref)->first();
        $unit_measure->user_id = auth()->user()->id;
        $unit_measure->unit_measure_name = $request->unit_measure_name;
        $unit_measure->save();

        alert()->success('Unit Measure has been Updated', 'Good Job!')->persistent('Close');
        return redirect(route('unit.measures.data'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\UnitMeasure  $unitMeasure
     * @return \Illuminate\Http\Response
     */
    public function destroy(UnitMeasure $unitMeasure)
    {
        //
    }
}
