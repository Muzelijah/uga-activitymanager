<?php

namespace App\Http\Controllers;

use App\Models\ActivityAttendance;
use Illuminate\Http\Request;

class ActivityAttendanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ActivityAttendance  $activityAttendance
     * @return \Illuminate\Http\Response
     */
    public function show(ActivityAttendance $activityAttendance)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ActivityAttendance  $activityAttendance
     * @return \Illuminate\Http\Response
     */
    public function edit(ActivityAttendance $activityAttendance)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ActivityAttendance  $activityAttendance
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ActivityAttendance $activityAttendance)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ActivityAttendance  $activityAttendance
     * @return \Illuminate\Http\Response
     */
    public function destroy(ActivityAttendance $activityAttendance)
    {
        //
    }
}
