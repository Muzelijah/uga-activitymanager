<?php


namespace App\Helpers;



use AfricasTalking\SDK\AfricasTalking;
use App\Models\AreaOffice;
use App\Models\Month;
use App\Models\ProcurementVendorItem;
use App\Models\Student;
use App\Models\SystemConfiguration;
use App\Notifications\SendMail;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use http\Exception;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Notification;


class Custom
{

    /**
     * @param $replyto
     * @param $subject
     * @param $greeting
     * @param $notification
     * @param $notification_action
     * @param $notification_url
     * @param array $cc
     * @param array $bcc
     */
    public static function SendMail($replyto, $subject, $greeting, $notification, $notification_action, $notification_url, $cc = array(), $bcc = array())
    {

        $message = array(
            'email' => $replyto,
            'subject' => $subject,
            'cc' => $cc,
            'bcc' => $bcc,
            'greeting' => $greeting,
            'notification' => $notification,
            'notification_action' => $notification_action,
            'notification_url' => $notification_url,
        );
        //$when = now()->addSeconds(10)->toDateTime();
        Notification::route('mail', $message['email'])->notify(new SendMail($message));
    }

    /***
     *
     * https://github.com/AfricasTalkingLtd/africastalking-php#sms
     *
     * SMS Provider Package For sending SMS
     *
     * @param $numbers
     * @param $message
     * @return bool
     */
    public static function SendPremiumMessage($numbers,$message)
    {
        $formatted_numbers = "";
        $number_list = explode(',',$numbers);
        foreach ($number_list as $number){
            $phone = "";
            $code = "+256";
            if (strpos($number, '0') === 0) {
                // It starts with 'http'
                $formatted_numbers .= $code.ltrim($number,"0").",";
            }
            if (strpos($number, $code) === 0) {
                // It starts with 'http'
                $formatted_numbers .= $number;
            }
        }
        $formatted_numbers = rtrim($formatted_numbers,",");

// Set your app credentials
        $username   = config('app.at_username');
        $apiKey     = config('app.at_api_key');

// Initialize the SDK
        $AT         = new AfricasTalking($username, $apiKey);
        // Get the SMS service
        $sms        = $AT->sms();

// Set the numbers you want to send to in international format
        $recipients = $formatted_numbers;

// Set your message
        $message    = $message.":-KAO";

// Set your shortCode or senderId
        $from       = "";


        try {
            // Thats it, hit send and we'll take care of the rest
            $result = $sms->send([
                'to'      => $recipients,
                'message' => $message,
                'from'    => $from
            ]);

            if($result["data"]->SMSMessageData->Recipients[0]->status =="Success"){
                return true;
            }else{
                dd($result);
            }

            //
            //print_r($result);

        } catch (Exception $e) {
            echo "Error: ".$e->getMessage();
        }
    }


    public static function SyncCounter($clientid)
    {

        $client  = \App\Models\Client::find($clientid);
        $date_now = Carbon::now();
        $client_join_date = Carbon::createFromFormat('Y-m-d', $client->client_join_date);
//
        $start_date = $client_join_date;
        while (Month::getMonthIdByDate($start_date)->id <= Month::getMonthIdByDate(date('Y-m-d'))->id){

            $penalty = 0;
            $amount = $client->client_signed_amount;
            if(Month::getMonthIdByDate($start_date)->id < $client->client_start_month_id){
                $amount = 0;
            }
            //  $results = $results."/".$start_date->toDateString();
            //$start_date = $start_date->addMonths(1);
            $counter_check = ClientCounter::where('month_id',Month::getMonthIdByDate($start_date)->id)->where('client_id',$client->id)->first();

            if($counter_check == null) {
                $client_loan_counter = new ClientCounter();
                $client_loan_counter->client_id = $client->id;
                $client_loan_counter->month_id = Month::getMonthIdByDate($start_date)->id;
                $client_loan_counter->amount = $amount;
                $client_loan_counter->save();
                $start_date = $start_date->addMonths(1);

                // dd($loan_date);
            }else{
                $start_date = $start_date->addMonths(1);
            }
        }
        // dd($results);

    }

    public static function getSystemConfig($configuration_name){

        return SystemConfiguration::where('system_configuration_name',$configuration_name)->first()->system_configuration_value;

    }


    public static function getItem($item_id,$vendor_id){

        return ProcurementVendorItem::where('procurement_item_id',$item_id)->where('vendor_id',$vendor_id)->first();
    }
    public static function getInvitationTotal($invitation_id,$vendor_id){

        return ProcurementVendorItem::where('procurement_invitation_id',$invitation_id)->where('vendor_id',$vendor_id)->get()->sum('line_total');
    }

    public static function getAreaOffices(){

        return AreaOffice::whereIn('id',json_decode(auth()->user()->area_offices))->orderBy('area_office_name')->get();
    }
}
