<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PaymentTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $payment_types = array(
            ['payment_type_code'=>'10','payment_type_name'=>'BANK'],
            ['payment_type_code'=>'11','payment_type_name'=>'CASH'],
            ['payment_type_code'=>'12','payment_type_name'=>'CHEQUE'],
            ['payment_type_code'=>'13','payment_type_name'=>'MOBILE MONEY'],
            ['payment_type_code'=>'14','payment_type_name'=>'FLUTTERWAVE']
        );
        DB::table('payment_types')->insert($payment_types);
    }
}
