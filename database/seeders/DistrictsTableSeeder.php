<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DistrictsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $districts =["Abim ","Adjumani ","Agago ","Alebtong ","Amolatar ","Amudat ","Amuria ","Amuru ","Apac ","Arua ","Budaka ","Bugiri ","Buhweju ","Buikwe ","Bukedea ","Bukomansimbi ","Bukwo ","Bulambuli ","Buliisa ","Bundibugyo ","Bushenyi ","Busia ","Butaleja ","Butambala ","Buvuma ","Buyende ","Dokolo ","Gomba ","Gulu ","Hoima ","Ibanda ","Iganga ","Isingiro ","Jinja ","Kaabong ","Kabarole ","Kaberamaido ","Kalangala ","Kaliro ","Kalungu ","Kampala ","Kamuli ","Kamwenge ","Kanungu ","Kapchorwa ","Kasese ","Katakwi ","Kayunga ","Kibaale ","Kiboga ","Kibuku ","Kiruhura ","Kiryandongo ","Kisoro ","Kitgum ","Koboko ","Kole ","Kotido ","Kumi ","Kween ","Kyankwanzi ","Kyegegwa ","Kyenjojo ","Lamwo ","Lira ","Luuka ","Luweero ","Lwengo ","Lyantonde ","Manafwa ","Maracha ","Masaka ","Masindi ","Mayuge ","Mbale ","Mbarara ","Mitooma ","Mityana ","Moroto ","Moyo ","Mpigi ","Mubende ","Mukono ","Nakapiripirit ","Nakapiripirit ","Nakaseke ","Nakasongola ","Namayingo ","Namutumba ","Napak ","Nebbi ","Ngora ","Ntoroko ","Ntungamo ","Nwoya ","Otuke ","Oyam ","Pader ","Pallisa ","Rakai ","Rubirizi ","Rukungiri ","Sembabule ","Serere ","Sheema ","Sironko ","Soroti ","Tororo ","Wakiso ","Yumbe ","Zombo" ];
        foreach ($districts as $district){
            DB::table('districts')->insert(['district_name' => strtoupper(trim($district))]);
        }
    }
}
