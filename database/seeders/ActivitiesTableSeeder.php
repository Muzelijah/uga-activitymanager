<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class ActivitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $activities = array();
        for($i=1;$i<=100;$i++){
            $activity = array(
                'activity_ref'=>Str::uuid(),
                'user_id'=> rand(1,2),
                'activity_code'=> date('YmdH').rand(100,1000).rand(100,1000),
                'activity_name'=> 'Activity '.$i,
                'activity_type_id'=> rand(1,3),
                'activity_start_date'=> date('Y-m-d'),
                'activity_end_date'=> date('Y-m-d'),
                'activity_start_time'=> '09:00:00',
                'activity_end_time'=> '12:00:00',
                'budget_code_id'=> rand(1,3),
                'estimated_budget_amount'=> rand(5,15)*1000000 ,
                'estimated_participants'=> rand(50,100) ,
                'activity_venue_address'=> 'Venue '.$i,
                'additional_information'=> 'Add Info '.$i,
                'activity_status_id'=> rand(1,5),
            );
            array_push($activities,$activity);
        }

        DB::table('activities')->insert($activities);
    }
}
