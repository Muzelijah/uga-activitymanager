<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class MonthsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $months =array();
        for($i = 2020;$i<=2050;$i++){

            for($j=1;$j<=12;$j++){
                $mon = str_pad($j,1);
                $date_string = "".$i."-".$mon."-01";
                $date = Carbon::createFromFormat('Y-m-d', $date_string);
                $year = $i;
                $start_date = $date->startOfMonth()->format('Y-m-d');
                $end_date = $date->endOfMonth()->format('Y-m-d');
                $month_date = $date->format('F')." ".$i;
                $month = array(
                    'year'=>$year,
                    'month_name'=>$month_date,
                    'month_start_date'=>$start_date,
                    'month_end_date'=>$end_date,
                );
                array_push($months,$month);


            }
        }
        DB::table('months')->insert($months);
    }
}
