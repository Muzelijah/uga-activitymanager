<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SystemConfigurationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $configs = array(
            ["system_configuration_name" => "STUDENT_BALANCE_LIMIT","system_configuration_value"=>"10000"],
            ["system_configuration_name" => "STUDENT_SMS_LIMIT","system_configuration_value"=>"5"],
            ["system_configuration_name" => "SCHOOL_CODE","system_configuration_value"=>"197"],
            ["system_configuration_name" => "SCHOOL_WALLET_FEES","system_configuration_value"=>"100"],
            ["system_configuration_name" => "SCHOOL_CHARGE","system_configuration_value"=>"0"],
        );
        DB::table('system_configurations')->insert($configs);
    }
}
