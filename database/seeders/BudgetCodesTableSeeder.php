<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class BudgetCodesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $budget_codes = array(
            ['budget_code_ref'=>Str::uuid(),'budget_code_name'=>'U0001','budget_code_description'=>'U1'],
            ['budget_code_ref'=>Str::uuid(),'budget_code_name'=>'U0002','budget_code_description'=>'U2'],
            ['budget_code_ref'=>Str::uuid(),'budget_code_name'=>'U0003','budget_code_description'=>'U3'],
        );
        DB::table('budget_codes')->insert($budget_codes);
    }
}
