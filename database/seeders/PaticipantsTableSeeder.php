<?php

namespace Database\Seeders;

use Faker\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class PaticipantsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $participants = array();
        for ($i=1;$i<=100;$i++){
            $faker = Factory::create();
            $name = explode(" ", $faker->name);
            $participant = array(
                'participant_ref'=>Str::uuid(),
                'participant_title'=>$faker->title,
                'participant_type'=>($i%10) ? 'Facilitator' : 'Participant',
                'participant_first_name'=>$name[0],
                'participant_other_names'=>$name[1],
                'participant_mobile'=>$faker->phoneNumber,
                'participant_email'=>$faker->email,
                'participant_organization'=>'WFP',
            );
            array_push($participants,$participant);
        }
        DB::table('participants')->insert($participants);
    }
}
