<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class AreaOfficesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $area_offices = array(
            ['area_office_ref'=>Str::uuid(),'area_office_name'=>'Karamoja'],
            ['area_office_ref'=>Str::uuid(),'area_office_name'=>'Arua'],
            ['area_office_ref'=>Str::uuid(),'area_office_name'=>'SWAO'],
            ['area_office_ref'=>Str::uuid(),'area_office_name'=>'UG-CO'],
        );
        DB::table('area_offices')->insert($area_offices);
    }
}
