<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class ActivityTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $activity_types = array(
            ['activity_type_ref'=>Str::uuid(),'activity_type_name'=>'Meeting'],
            ['activity_type_ref'=>Str::uuid(),'activity_type_name'=>'Training'],
            ['activity_type_ref'=>Str::uuid(),'activity_type_name'=>'Workshop'],
        );
        DB::table('activity_types')->insert($activity_types);
    }
}
