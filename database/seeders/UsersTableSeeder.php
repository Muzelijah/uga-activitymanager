<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = array(
            ['name'=>'Super Admin','email'=>'admin@kao.com','phone'=>'0778890754','password'=>bcrypt('987654321'),'api_token'=>Str::random(60),'area_offices'=>'[1,2,3,4]'],
            ['name'=>'Front Desk','email'=>'frontdesk@kao.com','phone'=>'0706508498','password'=>bcrypt('12345678'),'api_token'=>Str::random(60),'area_offices'=>'[1]']

        );

        DB::table('users')->insert($users);
        $user = User::find(1);
        $user->syncRoles(["sysadmin"]);
        $user = User::find(2);
        $user->syncRoles(["Front Desk Officer"]);
    }
}
