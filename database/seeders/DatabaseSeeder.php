<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->call(PermissionsTableSeeder::class);
        $this->call(DistrictsTableSeeder::class);
        $this->call(MonthsTableSeeder::class);
        $this->call(AreaOfficesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(PaymentTypesTableSeeder::class);
        $this->call(ActivityTypesTableSeeder::class);
        $this->call(BudgetCodesTableSeeder::class);
        $this->call(ActivityStatusesTableSeeder::class);
       //$this->call(ActivitiesTableSeeder::class);
       //$this->call(PaticipantsTableSeeder::class);
        $this->call(SystemConfigurationsTableSeeder::class);


    }
}
