<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class ActivityStatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $statuses = array(
            ['activity_status_ref'=>Str::uuid(),'activity_status_name'=>'Pending'],
            ['activity_status_ref'=>Str::uuid(),'activity_status_name'=>'Pending Approval'],
            ['activity_status_ref'=>Str::uuid(),'activity_status_name'=>'Approved'],
            ['activity_status_ref'=>Str::uuid(),'activity_status_name'=>'Complete'],
            ['activity_status_ref'=>Str::uuid(),'activity_status_name'=>'Returned'],
        );
        DB::table('activity_statuses')->insert($statuses);
    }
}
