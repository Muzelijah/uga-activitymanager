<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActivityAttendancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activity_attendances', function (Blueprint $table) {
            $table->id();
            $table->uuid('activity_attendance_ref');
            $table->unsignedBigInteger('activity_id');
            $table->unsignedBigInteger('participant_id');
            $table->bigInteger('activity_attendance_code');
            $table->date('date_added')->default(date('Y-m-d'));
            $table->dateTime('activity_attendance_date')->default(date('Y-m-d H:i:s'));
            $table->integer('activity_attendance_status')->default(1);
            $table->dateTime('activity_attendance_confirmed')->default(date('Y-m-d H:i:s'));
            $table->text('activity_attendance_sign')->nullable();
            $table->timestamps();

            $table->foreign('activity_id')->references('id')->on('activities');
            $table->foreign('participant_id')->references('id')->on('participants');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activity_attendances');
    }
}
