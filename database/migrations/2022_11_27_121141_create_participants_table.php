<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateParticipantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('participants', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('area_office_id')->default(1);
            $table->unsignedBigInteger('user_id')->default(1);
            $table->uuid('participant_ref');
            $table->string('participant_title');
            $table->enum('participant_type',['Participant','Facilitator'])->default('Participant');
            $table->string('participant_first_name');
            $table->string('participant_other_names');
            $table->string('participant_mobile')->unique();
            $table->string('participant_email')->nullable();
            $table->string('participant_organization')->nullable();
            $table->text('participant_sign')->nullable();
            $table->integer('participant_status')->default(1);
            $table->boolean('active')->default(true);
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('area_office_id')->references('id')->on('area_offices');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('participants');
    }
}
