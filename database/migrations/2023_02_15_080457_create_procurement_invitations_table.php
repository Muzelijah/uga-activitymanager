<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProcurementInvitationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('procurement_invitations', function (Blueprint $table) {
            $table->id();
            $table->uuid('procurement_invitation_ref');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('activity_id');
            $table->unsignedBigInteger('procurement_request_id');
            $table->unsignedBigInteger('vendor_id');
            $table->integer('invitation_submission_code')->default(0);
            $table->string('procurement_invitation_mobile');
            $table->string('procurement_invitation_email');
            $table->integer('procurement_invitation_status')->default(1);
            $table->string('procurement_invitation_sign')->nullable();
            $table->string('quotation_copy')->nullable();
            $table->integer('send_invitation')->default(0);
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('activity_id')->references('id')->on('activities');
            $table->foreign('procurement_request_id')->references('id')->on('procurement_requests');
            $table->foreign('vendor_id')->references('id')->on('vendors');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('procurement_invitations');
    }
}
