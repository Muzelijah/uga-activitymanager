<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProcurementVendorItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('procurement_vendor_items', function (Blueprint $table) {
            $table->id();
            $table->uuid('procurement_vendor_ref');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('activity_id');
            $table->unsignedBigInteger('procurement_item_id');
            $table->unsignedBigInteger('procurement_invitation_id');
            $table->unsignedBigInteger('procurement_request_id');
            $table->unsignedBigInteger('vendor_id');
            $table->text('item_description');
            $table->integer('item_qty')->default(1);
            $table->unsignedBigInteger('unit_measure_id');
            $table->integer('total_days')->default(1);
            $table->integer('unit_price')->default(0);
            $table->integer('line_total')->default(0);
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('activity_id')->references('id')->on('activities');
            $table->foreign('procurement_item_id')->references('id')->on('procurement_items');
            $table->foreign('procurement_invitation_id')->references('id')->on('procurement_invitations');
            $table->foreign('procurement_request_id')->references('id')->on('procurement_requests');
            $table->foreign('vendor_id')->references('id')->on('vendors');
            $table->foreign('unit_measure_id')->references('id')->on('unit_measures');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('procurement_vendor_items');
    }
}
