<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActivityPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activity_payments', function (Blueprint $table) {
            $table->id();
            $table->uuid('activity_payment_ref');
            $table->unsignedBigInteger('activity_id');
            $table->unsignedBigInteger('participant_id');
            $table->integer('days_attended')->default(0);
            $table->integer('daily_rate')->default(0);
            $table->integer('line_total')->default(0);
            $table->integer('fuel_refund')->default(0);
            $table->string('participant_payment_mobile')->nullable();
            $table->string('participant_sign')->nullable();
            $table->date('date_added')->default(date('Y-m-d'));
            $table->dateTime('activity_payment_date')->default(date('Y-m-d H:i:s'));
            $table->integer('activity_payment_status_id')->default(1);
            $table->timestamps();

            $table->foreign('activity_id')->references('id')->on('activities');
            $table->foreign('participant_id')->references('id')->on('participants');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activity_payments');
    }
}
