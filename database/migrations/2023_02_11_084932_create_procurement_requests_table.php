<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProcurementRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('procurement_requests', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('area_office_id')->default(1);
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('activity_id');
            $table->uuid('procurement_request_ref');
            $table->string('rfq_reference_number')->unique();
            $table->integer('exchange_rate')->default(4000);
            $table->date('procurement_issue_date')->default(date('Y-m-d'));
            $table->date('procurement_due_date')->default(date('Y-m-d'));
            $table->date('delivery_from_date')->default(date('Y-m-d'));
            $table->date('delivery_to_date')->default(date('Y-m-d'));
            $table->text('delivery_address')->nullable();
            $table->text('payment_terms')->nullable();
            $table->integer('verified')->default(0);
            $table->unsignedBigInteger('verified_by')->default(1);
            $table->date('verified_date')->default(date('Y-m-d'));
            $table->integer('approved')->default(0);
            $table->unsignedBigInteger('approved_by')->default(1);
            $table->integer('procurement_request_status')->default(1);
            $table->date('approved_date')->default(date('Y-m-d'));
            $table->date('date_added')->default(date('Y-m-d'));
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('verified_by')->references('id')->on('users');
            $table->foreign('approved_by')->references('id')->on('users');
            $table->foreign('area_office_id')->references('id')->on('area_offices');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('procurement_requests');
    }
}
