<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActivityParticipantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activity_participants', function (Blueprint $table) {
            $table->id();
            $table->uuid('activity_participant_ref');
            $table->unsignedBigInteger('activity_id');
            $table->unsignedBigInteger('participant_id');
            $table->dateTime('date_added')->default(date('Y-m-d'));
            $table->integer('send_invitations')->default(0);
            $table->dateTime('date_invite_sent')->default(date('Y-m-d H:i:s'));
            $table->integer('confirmed_attendance')->default(0);
            $table->dateTime('date_attendance_confirmed')->default(date('Y-m-d H:i:s'));
            $table->timestamps();

            $table->foreign('activity_id')->references('id')->on('activities');
            $table->foreign('participant_id')->references('id')->on('participants');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activity_participants');
    }
}
