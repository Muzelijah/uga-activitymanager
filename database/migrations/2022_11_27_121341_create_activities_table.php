<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activities', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('area_office_id')->default(1);
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('assigned_to');
            $table->uuid('activity_ref');
            $table->string('activity_code')->unique();
            $table->string('activity_name');
            $table->unsignedBigInteger('activity_type_id');
            $table->text('activity_description')->nullable();
            $table->date('activity_start_date');
            $table->date('activity_end_date');
            $table->time('activity_start_time',0);
            $table->time('activity_end_time',0);
            $table->unsignedBigInteger('budget_code_id');
            $table->integer('estimated_budget_amount')->default(0);
            $table->integer('estimated_participants')->default(0);
            $table->text('activity_remark')->nullable();
            $table->text('activity_venue_address');
            $table->text('additional_information')->nullable();
            $table->string('activity_concept_note')->nullable();
            $table->unsignedBigInteger('activity_status_id')->default(1);
            $table->integer('activity_payment_status_id')->default(1);
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('assigned_to')->references('id')->on('users');
            $table->foreign('budget_code_id')->references('id')->on('budget_codes');
            $table->foreign('area_office_id')->references('id')->on('area_offices');
            $table->foreign('activity_type_id')->references('id')->on('activity_types');
            $table->foreign('activity_status_id')->references('id')->on('activity_statuses');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activities');
    }
}
