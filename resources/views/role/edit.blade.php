@extends('layouts.main')
@section('content')
    <section class="panel">
        <div class="tabs-custom">
            <ul class="nav nav-tabs">
                <li>
                    <a href="{{route('roles.data')}}">
                        <i class="fas fa-list-ul"></i> Roles List
                    </a>
                </li>
                <li class="active">
                    <a href="#edit" data-toggle="tab">
                        <i class="far fa-edit"></i> Edit Role
                    </a>
                </li>

            </ul>
            <div class="tab-content">
{{--                <div class="tab-pane box active mb-md" id="list">--}}
{{--                    <table class="table table-bordered table-hover mb-none table-condensed table-export">--}}
{{--                        <thead>--}}
{{--                        <tr>--}}
{{--                            <th>SN</th>--}}
{{--                            <th>Reference</th>--}}
{{--                            <th>Names</th>--}}
{{--                            <th>Gender</th>--}}
{{--                            <th>NIN</th>--}}
{{--                            <th>Age</th>--}}
{{--                            <th>Contact</th>--}}
{{--                            <th>Next of Kin</th>--}}
{{--                            <th>Kin Contact</th>--}}
{{--                            <th>Pre Medical</th>--}}
{{--                            <th>Own Passport</th>--}}
{{--                            <th>District</th>--}}
{{--                            <th>Last Updated By</th>--}}
{{--                            <th>Actions</th>--}}
{{--                        </tr>--}}
{{--                        </thead>--}}
{{--                        <tbody>--}}
{{--                        @if(!empty($applicants))--}}
{{--                            @foreach($applicants as $applicant)--}}
{{--                                <tr>--}}
{{--                                    <td>{{$applicant->id}}</td>--}}
{{--                                    <td>{{$applicant->applicant_ref}}</td>--}}
{{--                                    <td>{{$applicant->applicant_sur_name}} {{$applicant->applicant_given_name}}</td>--}}
{{--                                    <td>{{$applicant->applicant_gender}}</td>--}}
{{--                                    <td>{{$applicant->applicant_nin}}</td>--}}
{{--                                    <td>{{$applicant->applicant_age}}</td>--}}
{{--                                    <td>{{$applicant->applicant_contact}}</td>--}}
{{--                                    <td>{{$applicant->applicant_next_of_kin}}</td>--}}
{{--                                    <td>{{$applicant->applicant_kin_mobile}}</td>--}}
{{--                                    <td>{{$applicant->applicant_pre_medical}}</td>--}}
{{--                                    <td>{{$applicant->applicant_own_passport}}</td>--}}
{{--                                    <td>{{$applicant->district->district_name}}</td>--}}
{{--                                    <td>{{$applicant->user->name}}</td>--}}
{{--                                    <td><a href="{{route('applicants.edit',['applicant'=>$applicant->id])}}"--}}
{{--                                           data-toggle="tooltip" data-original-title="Edit"--}}
{{--                                           class="btn btn-circle btn-default icon">--}}
{{--                                            <i class="fas fa-pen-nib"></i>--}}
{{--                                        </a></td>--}}
{{--                                </tr>--}}
{{--                            @endforeach--}}
{{--                        @endif--}}
{{--                        </tbody>--}}
{{--                    </table>--}}
{{--                </div>--}}

                <div class="tab-pane active" id="edit">
                    <form action="{{route('roles.update',['role'=>$role->id])}}" method="post">
                        @csrf
                        <div class="panel-body">

                            <!-- academic details-->
                            <div class="row">
                                <div class="col-md-12 mb-sm">
                                    <div class="form-group">
                                        <label class="control-label">Role Name<span
                                                class="required">*</span></label>
                                        <input type="text" class="form-control" required name="role_name"
                                               value="{{old('role_name', $role->name)}}"/>
                                        <span class="error"></span>
                                    </div>
                                </div>
                                @foreach($permissions as $permission)
                                    <div class="col-md-3 mb-sm checked-area" width="60">
                                        <div class="checkbox-replace">
                                            <label class="i-checks">
                                                <input type="checkbox"  name="permissions[]" @if(in_array($permission->name, $role_permissions)) checked @endif value="{{$permission->name}}"> <i></i>
                                            </label>{{$permission->name}}
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>

                        <footer class="panel-footer">
                            <div class="row">
                                <div class="col-md-offset-10 col-md-2">
                                    <button type="submit" name="save" value="1" class="btn btn btn-default btn-block">
                                        <i class="fas fa-plus-circle"></i> Save
                                    </button>
                                </div>
                            </div>
                        </footer>
                    </form>
                </div>

            </div>
        </div>
    </section>
@endsection
