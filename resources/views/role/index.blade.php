@extends('layouts.main')
@section('content')
    <section class="panel">
        <div class="tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#list" data-toggle="tab">
                        <i class="fas fa-list-ul"></i> Roles List
                    </a>
                </li>
                <li>
                    <a href="#add" data-toggle="tab">
                        <i class="far fa-edit"></i> Register New Role
                    </a>
                </li>

            </ul>
            <div class="tab-content">
                <div class="tab-pane box active mb-md" id="list">
                    <table class="table table-bordered table-hover mb-none table-condensed table-export">
                        <thead>
                        <tr>
                            <th>SN</th>
                            <th>Role Name</th>
                            <th>Guard Name</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(!empty($roles))
                            @foreach($roles as $role)
                                <tr>
                                    <td>{{$role->id}}</td>
                                    <td>{{$role->name}}</td>
                                    <td>{{$role->guard_name}}</td>
                                    <td><a href="{{route('roles.edit',['role'=>$role->id])}}"
                                           data-toggle="tooltip" data-original-title="Edit"
                                           class="btn btn-circle btn-default icon">
                                            <i class="fas fa-pen-nib"></i>
                                        </a></td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>

                <div class="tab-pane" id="add">
                    <form action="{{route('roles.store')}}" method="post">
                        @csrf
                        <div class="panel-body">

                            <!-- academic details-->
                            <div class="headers-line">
                                <i class="fas fa-school"></i> General Information
                            </div>


                            <div class="row">
                                <div class="col-md-12 mb-sm">
                                    <div class="form-group">
                                        <label class="control-label">Role Name<span
                                                class="required">*</span></label>
                                        <input type="text" class="form-control" required name="role_name"
                                               value="{{old('name', '')}}"/>
                                        <span class="error"></span>
                                    </div>
                                </div>
                                @foreach($permissions as $permission)
                                    <div class="col-md-3 mb-sm checked-area" width="60">
                                        <div class="checkbox-replace">
                                            <label class="i-checks">
                                                <input type="checkbox" name="permissions[]" value="{{$permission->name}}"> <i></i>
                                            </label>{{$permission->name}}
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>

                        <footer class="panel-footer">
                            <div class="row">
                                <div class="col-md-offset-10 col-md-2">
                                    <button type="submit" name="save" value="1" class="btn btn btn-default btn-block">
                                        <i class="fas fa-plus-circle"></i> Save
                                    </button>
                                </div>
                            </div>
                        </footer>
                    </form>
                </div>

            </div>
        </div>
    </section>
@endsection
