@extends('layouts.main')
@section('content')
    <section class="panel" xmlns="http://www.w3.org/1999/html">
        <div class="tabs-custom">
            <ul class="nav nav-tabs">
                <li>
                    <a href="{{route('participants.data')}}">
                        <i class="fas fa-list-ul"></i> Participants List
                    </a>
                </li>
                <li  class="active">
                    <a href="#add"  data-toggle="tab">
                        <i class="fas fa-list-ul"></i> Edit Participant
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane box  mb-md" id="list">


                </div>

                <div class="tab-pane active" id="add">
                    <form action="{{route('participants.update',['participant'=>$participant->participant_ref])}}" id="participant_form" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="panel-body">

                            <div class="row">
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Area Office<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <select name="area_office_id"
                                                class="form-control select2-hidden-accessible"
                                                id="product_category_id" data-plugin-selecttwo="" data-width="100%"
                                                tabindex="-1"
                                                aria-hidden="true" required>
                                            @if(!empty($area_offices))
                                                @foreach($area_offices as $area_office)
                                                    <option value="{{$area_office->id}}" @if($participant->area_office_id == $area_office->id) selected @endif>{{$area_office->area_office_name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Title<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <input type="text" required class="form-control"
                                               name="participant_title"
                                               value="{{old('participant_title',$participant->participant_title)}}"/>
                                        <span class="error"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">First Name<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <input type="text" required class="form-control"
                                               name="participant_first_name"
                                               value="{{old('participant_first_name',$participant->participant_first_name)}}"/>
                                        <span class="error"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Other Names<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <input type="text" required class="form-control"
                                               name="participant_other_names"
                                               value="{{old('participant_other_names',$participant->participant_other_names)}}"/>
                                        <span class="error"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Participant Type<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <select name="participant_type"
                                                class="form-control select2-hidden-accessible"
                                                id="product_category_id" data-plugin-selecttwo="" data-width="100%"
                                                tabindex="-1"
                                                aria-hidden="true" required>
                                            <option value="">Select</option>
                                            <option value="Participant" @if($participant->participant_type == "Participant") selected @endif>Participant</option>
                                            <option value="Facilitator" @if($participant->participant_type == "Facilitator") selected @endif>Facilitator</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Mobile<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <input type="text" required class="form-control"
                                               name="participant_mobile"
                                               value="{{old('participant_mobile',$participant->participant_mobile)}}"/>
                                        <span class="error"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Email<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <input type="email" required class="form-control"
                                               name="participant_email"
                                               value="{{old('participant_email',$participant->participant_email)}}"/>
                                        <span class="error"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Organisation<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <input type="text" required class="form-control"
                                               name="participant_organization"
                                               value="{{old('participant_organization',$participant->participant_organization)}}"/>
                                        <span class="error"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Signature<span
                                            class="required">*</span></label>
                                    <div class="col-md-4">
                                        <div id="signature-pad">
                                            <div
                                                style="border:solid 1px teal; width:360px;height:110px;padding:3px;position:relative;">
                                                <div id="note" onmouseover="my_function();">The signature should be
                                                    inside box
                                                </div>
                                                <canvas id="the_canvas" width="350px" height="100px"></canvas>
                                              </div>
                                            <div style="margin:10px;">
                                                <input type="hidden" id="signature" name="signature">
                                                <button type="button" id="clear_btn" class="btn btn-danger"
                                                        data-action="clear"><span
                                                        class="glyphicon glyphicon-remove"></span> Clear Signature
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <img src="{{asset('public/app_image/uploads/signatures/'.$participant->participant_sign)}}" alt="No Signature">

                                    </div>
                                </div>
                            </div>
                        </div>
                        <footer class="panel-footer">
                            <div class="row">
                                <div class="col-md-offset-10 col-md-2">
                                    <button type="button" id="save_participant" name="save" value="1"
                                            class="btn btn btn-dark btn-block center" data-action="save-png">
                                        <i class="fas fa-plus-circle"></i> Save
                                    </button>
                                </div>
                            </div>
                        </footer>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('myscript')
    <script>
        var wrapper = document.getElementById("signature-pad");
        var clearButton = wrapper.querySelector("[data-action=clear]");
        var savePNGButton = wrapper.querySelector("[data-action=save-png]");
        var canvas = wrapper.querySelector("canvas");
        var el_note = document.getElementById("note");
        var save_participant = document.getElementById("save_participant");
        var signaturePad;
        signaturePad = new SignaturePad(canvas);

        clearButton.addEventListener("click", function (event) {
            document.getElementById("note").innerHTML = "The signature should be inside box";
            signaturePad.clear();
        });
        document.getElementById('save_participant').addEventListener("click", function() {
            if (signaturePad.isEmpty()) {
                alert("Please provide signature first.");
                event.preventDefault();
            } else {
                var canvas = document.getElementById("the_canvas");
                var dataUrl = canvas.toDataURL();
                document.getElementById("signature").value = dataUrl;
                document.getElementById("participant_form").submit();
            }
        });
        // savePNGButton.addEventListener("click", function (event) {
        //     if (signaturePad.isEmpty()) {
        //         alert("Please provide signature first.");
        //         event.preventDefault();
        //     } else {
        //         var canvas = document.getElementById("the_canvas");
        //         var dataUrl = canvas.toDataURL();
        //         document.getElementById("signature").value = dataUrl;
        //         document.getElementById("participant_form").submit();
        //     }
        // });

        function my_function() {
            document.getElementById("note").innerHTML = "";
        }
    </script>
@endsection
