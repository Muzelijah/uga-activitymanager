@extends('layouts.main')
@section('content')
    <section class="panel" xmlns="http://www.w3.org/1999/html">
        <div class="tab-content">
        <div class="row">
            <div class="col-lg-9">
                <div class="headers-line">
                    <i class="fas fa-list"></i> Procurement Preview
                </div>
                <object data="{{url('/public/app_image/rpt/'.$procurement_request_ref.'.pdf#view=FitH')}}"
                        type="application/pdf" width=100% height=1200px
                        data="Assembly.pdf?#zoom=100&scrollbar=1&toolbar=1&navpanes=0&view=FitH"></object>
            </div>
            <div class="col-lg-3">
                <div class="headers-line">
                    <i class="fas fa-gear"></i> Actions
                </div>
                @if($procurement_request->procurement_request_status == 1)
                    <a href="{{route('procurement.requests.verify',['request' => $procurement_request_ref])}}"
                       class="btn btn-primary btn-block center">
                        Verify Request
                    </a>
                @endif
                @if($procurement_request->procurement_request_status == 2)
                    <form action="{{route('procurement.requests.approve',['request'=>$procurement_request_ref])}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label class="control-label text-left">Vendors<span
                                    class="required">*</span></label>
                            <div class="row col-lg-12">
                                <select multiple name="vendors[]"
                                        class="form-control select2-hidden-accessible"
                                        id="product_category_id" data-plugin-selecttwo="" data-width="100%"
                                        tabindex="-1"
                                        aria-hidden="true" required>
                                    <option value="">Select</option>
                                    @if(!empty($vendors))
                                        @foreach($vendors as $vendor)
                                            <option value="{{$vendor->vendor_ref}}">{{$vendor->vendor_name}}
                                                ( {{$vendor->vendor_number}})
                                            </option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>

                        </div>
                        <div class="form-group">
                            <button type="submit" name="save" value="1"
                                    class="btn btn btn-dark btn-block center">
                                <i class="fas fa-check"></i> Approve Request
                            </button>
                        </div>
                    </form>
                @endif
            </div>
        </div>
        </div>
    </section>
@endsection
