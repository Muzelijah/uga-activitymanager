@extends('layouts.main')
@section('content')
    <section class="panel" xmlns="http://www.w3.org/1999/html">
        <div class="tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#list" data-toggle="tab">
                        <i class="fas fa-list-ul"></i>Unverified Procurement Requests
                    </a>
                </li>
                <li>
                    <a href="#add" data-toggle="tab">
                        <i class="fas fa-edit"></i> Create New Procurement Request
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane box active mb-md" id="list">
                    {{--                    <div class="row mt-5 pt-5">--}}
                    {{--                        <div class="col-lg-12">--}}
                    {{--                            <div class="headers-line">--}}
                    {{--                                <i class="fas fa-list"></i> Order Details--}}
                    {{--                            </div>--}}
                    {{--                        </div>--}}
                    {{--                    </div>--}}
                    <form action="{{route('activities.status')}}" method="post">

                        @csrf
                        <table class="table table-bordered table-hover mb-none table-condensed table-export">
                            <thead>
                            <tr>
                                <th>
                                    SN
                                </th>
                                <th>RFQ Reference</th>
                                <th>Activity</th>
                                <th>E/Rate</th>
                                <th>Issue Date</th>
                                <th>Due Date</th>
                                <th>Delivery From</th>
                                <th>Delivery To</th>
                                <th>Delivery Address</th>
                                <th>Payment Terms</th>
                                <th>Date Added</th>
                                <th>Status</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(!empty($procurement_requests))
                                @foreach($procurement_requests as $procurement_request)
                                    <tr>
                                        <td class="checked-area">
                                            {{$procurement_request->id}}
                                        </td>
                                        <td>{{$procurement_request->rfq_reference_number}}</td>
                                        <td>{{$procurement_request->activity->activity_name}}
                                            ( {{$procurement_request->activity->activity_code}} )
                                        </td>
                                        <td>{{number_format($procurement_request->exchange_rate,0)}}</td>
                                        <td>{{$procurement_request->procurement_issue_date}}</td>
                                        <td>{{$procurement_request->procurement_due_date}}</td>
                                        <td>{{$procurement_request->delivery_from_date}}</td>
                                        <td>{{$procurement_request->delivery_to_date}}</td>
                                        <td>{{$procurement_request->delivery_address}}</td>
                                        <td>{{$procurement_request->payment_terms}}</td>
                                        <td>{{$procurement_request->date_added}}</td>
                                        <td>{{$procurement_request->procurement_request_status}}</td>
                                        <td>
                                            <a href="{{route('procurement.requests.edit',['request' => $procurement_request->procurement_request_ref])}}"
                                               data-toggle="tooltip" data-original-title="Edit"
                                               class="btn btn-circle btn-primary icon">
                                                <i class="fas fa-pen-nib"></i>
                                            </a>
                                            <a href="{{route('procurement.items.data',['request' => $procurement_request->procurement_request_ref])}}"
                                               data-toggle="tooltip" data-original-title="View Items"
                                               class="btn btn-circle btn-dark icon">
                                                <i class="fas fa-eye"></i>
                                            </a>
                                            <a href="{{route('procurement.requests.preview',['request' => $procurement_request->procurement_request_ref])}}"
                                               data-toggle="tooltip" data-original-title="Preview"
                                               class="btn btn-circle btn-primary icon">
                                                <i class="fas fa-eye"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                <tr>
                                    <th>Total</th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            @endif
                            </tbody>
                        </table>

                    </form>

                </div>

                <div class="tab-pane" id="add">
                    <form action="{{route('procurement.requests.store')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="panel-body">

                            <div class="row">
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Area Office<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <select name="area_office_id"
                                                class="form-control select2-hidden-accessible"
                                                id="product_category_id" data-plugin-selecttwo="" data-width="100%"
                                                tabindex="-1"
                                                aria-hidden="true" required>
                                            @if(!empty($area_offices))
                                                @foreach($area_offices as $area_office)
                                                    <option value="{{$area_office->id}}">{{$area_office->area_office_name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">RFQ Reference<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <input type="text" required class="form-control text-uppercase"
                                               name="rfq_reference_number"
                                               value="{{old('rfq_reference_number')}}"/>
                                        <span class="error"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Activity<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <select name="activity_id"
                                                class="form-control select2-hidden-accessible"
                                                id="product_category_id" data-plugin-selecttwo="" data-width="100%"
                                                tabindex="-1"
                                                aria-hidden="true" required>
                                            <option value="">Select</option>
                                            @if(!empty($activities))
                                                @foreach($activities as $activity)
                                                    <option value="{{$activity->id}}">{{$activity->activity_name}}
                                                        ( {{$activity->activity_code}})
                                                    </option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Exchange Rate<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <input type="number"  required class="form-control text-uppercase"
                                               name="exchange_rate"
                                               value="{{old('exchange_rate')}}"/>
                                        <span class="error"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Issue Date<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" data-plugin-datepicker
                                               data-plugin-options='{"todayHighlight" : true}'
                                               name="procurement_issue_date" value="{{date('Y-m-d')}}"
                                               autocomplete="off"/>
                                        <span class="error"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Due Date<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" data-plugin-datepicker
                                               data-plugin-options='{"todayHighlight" : true}'
                                               name="procurement_due_date" value="{{date('Y-m-d')}}"
                                               autocomplete="off"/>
                                        <span class="error"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Delivery from<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" data-plugin-datepicker
                                               data-plugin-options='{"todayHighlight" : true}' name="delivery_from_date"
                                               value="{{date('Y-m-d')}}" autocomplete="off"/>
                                        <span class="error"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Delivery To<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" data-plugin-datepicker
                                               data-plugin-options='{"todayHighlight" : true}' name="delivery_to_date"
                                               value="{{date('Y-m-d')}}" autocomplete="off"/>
                                        <span class="error"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Payment Terms<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <input type="text" required class="form-control text-uppercase"
                                               name="payment_terms"
                                               value="{{old('payment_terms')}}"/>
                                        <span class="error"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Delivery Address<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <textarea required class="form-control" name="delivery_address"></textarea>
                                        <span class="error"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <footer class="panel-footer">
                            <div class="row">
                                <div class="col-md-offset-10 col-md-2">
                                    <button type="submit" name="save" value="1"
                                            class="btn btn btn-dark btn-block center">
                                        <i class="fas fa-plus-circle"></i> Save
                                    </button>
                                </div>
                            </div>
                        </footer>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
