@extends('layouts.main')
@section('content')
    <section class="panel" xmlns="http://www.w3.org/1999/html">
        <div class="tabs-custom">
            <ul class="nav nav-tabs">
                <li @if($status == 2) class="active" @endif>
                    <a href="{{route('procurement.requests.data',['status'=>2])}}">
                        <i class="fas fa-list-ul"></i> Pending Approval
                    </a>
                </li>
                <li @if($status == 3) class="active" @endif>
                    <a href="{{route('procurement.requests.data',['status'=>3])}}">
                        <i class="fas fa-list-ul"></i>Approved
                    </a>
                </li>
                <li @if($status == 4) class="active" @endif>
                    <a href="{{route('procurement.requests.data',['status'=>4])}}">
                        <i class="fas fa-list-ul"></i> Closed
                    </a>
                </li>
                <li @if($status == 5) class="active" @endif>
                    <a href="{{route('procurement.requests.data',['status'=>5])}}">
                        <i class="fas fa-list-ul"></i> Cancelled
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane box active mb-md" id="list">
                    <form action="{{route('activities.status')}}" method="post">

                        @csrf
                        <table class="table table-bordered table-hover mb-none table-condensed table-export">
                            <thead>
                            <tr>
                                <th>
                                    SN
                                </th>
                                <th>RFQ Reference</th>
                                <th>Activity</th>
                                <th>E/Rate</th>
                                <th>Issue Date</th>
                                <th>Due Date</th>
                                <th>Delivery From</th>
                                <th>Delivery To</th>
                                <th>Delivery Address</th>
                                <th>Payment Terms</th>
                                <th>Date Added</th>
                                <th>Status</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(!empty($procurement_requests))
                                @foreach($procurement_requests as $procurement_request)
                                    <tr>
                                        <td class="checked-area">
                                            {{$procurement_request->id}}
                                        </td>
                                        <td>{{$procurement_request->rfq_reference_number}}</td>
                                        <td>{{$procurement_request->activity->activity_name}}
                                            ( {{$procurement_request->activity->activity_code}} )
                                        </td>
                                        <td>{{number_format($procurement_request->exchange_rate,0)}}</td>
                                        <td>{{$procurement_request->procurement_issue_date}}</td>
                                        <td>{{$procurement_request->procurement_due_date}}</td>
                                        <td>{{$procurement_request->delivery_from_date}}</td>
                                        <td>{{$procurement_request->delivery_to_date}}</td>
                                        <td>{{$procurement_request->delivery_address}}</td>
                                        <td>{{$procurement_request->payment_terms}}</td>
                                        <td>{{$procurement_request->date_added}}</td>
                                        <td>{{$procurement_request->procurement_request_status}}</td>
                                        <td>
                                            <a href="{{route('procurement.requests.preview',['request' => $procurement_request->procurement_request_ref])}}"
                                               data-toggle="tooltip" data-original-title="Preview"
                                               class="btn btn-circle btn-primary icon">
                                                <i class="fas fa-eye"></i>
                                            </a>
                                            <a href="{{route('procurement.requests.invitations.data',['request' => $procurement_request->procurement_request_ref])}}"
                                               data-toggle="tooltip" data-original-title="View Invitations"
                                               class="btn btn-circle btn-dark icon">
                                                <i class="fas fa-eye"></i>
                                            </a>
                                            @if($procurement_request->procurement_request_status == 3)
                                                <a href="{{route('procurement.requests.close',['request' => $procurement_request->procurement_request_ref])}}"
                                                   data-toggle="tooltip" data-original-title="Close"
                                                   class="btn btn-circle btn-danger icon">
                                                    <i class="fas fa-check-circle"></i>
                                                </a>
                                            @endif
                                            @if($procurement_request->procurement_request_status == 4)
                                                <a target="_blank" href="{{route('procurement.requests.mpo.preview',['request' => $procurement_request->procurement_request_ref])}}"
                                                   data-toggle="tooltip" data-original-title="Preview MPO"
                                                   class="btn btn-circle btn-success icon">
                                                    <i class="fas fa-dollar-sign"></i>
                                                </a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                <tr>
                                    <th>Total</th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            @endif
                            </tbody>
                        </table>

                    </form>

                </div>

                <div class="tab-pane" id="add">
{{--                    <form action="{{route('procurement.requests.store')}}" method="post" enctype="multipart/form-data">--}}
{{--                        @csrf--}}
{{--                        <div class="panel-body">--}}

{{--                            <div class="row">--}}
{{--                                <div class="form-group">--}}
{{--                                    <label class="col-md-3 control-label text-right">RFQ Reference<span--}}
{{--                                            class="required">*</span></label>--}}
{{--                                    <div class="col-md-6">--}}
{{--                                        <input type="text" required class="form-control text-uppercase"--}}
{{--                                               name="rfq_reference_number"--}}
{{--                                               value="{{old('rfq_reference_number')}}"/>--}}
{{--                                        <span class="error"></span>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="form-group">--}}
{{--                                    <label class="col-md-3 control-label text-right">Activity<span--}}
{{--                                            class="required">*</span></label>--}}
{{--                                    <div class="col-md-6">--}}
{{--                                        <select name="activity_id"--}}
{{--                                                class="form-control select2-hidden-accessible"--}}
{{--                                                id="product_category_id" data-plugin-selecttwo="" data-width="100%"--}}
{{--                                                tabindex="-1"--}}
{{--                                                aria-hidden="true" required>--}}
{{--                                            <option value="">Select</option>--}}
{{--                                            @if(!empty($activities))--}}
{{--                                                @foreach($activities as $activity)--}}
{{--                                                    <option value="{{$activity->id}}">{{$activity->activity_name}}--}}
{{--                                                        ( {{$activity->activity_code}})--}}
{{--                                                    </option>--}}
{{--                                                @endforeach--}}
{{--                                            @endif--}}
{{--                                        </select>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="form-group">--}}
{{--                                    <label class="col-md-3 control-label text-right">Exchange Rate<span--}}
{{--                                            class="required">*</span></label>--}}
{{--                                    <div class="col-md-6">--}}
{{--                                        <input type="number"  required class="form-control text-uppercase"--}}
{{--                                               name="exchange_rate"--}}
{{--                                               value="{{old('exchange_rate')}}"/>--}}
{{--                                        <span class="error"></span>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="form-group">--}}
{{--                                    <label class="col-md-3 control-label text-right">Issue Date<span--}}
{{--                                            class="required">*</span></label>--}}
{{--                                    <div class="col-md-6">--}}
{{--                                        <input type="text" class="form-control" data-plugin-datepicker--}}
{{--                                               data-plugin-options='{"todayHighlight" : true}'--}}
{{--                                               name="procurement_issue_date" value="{{date('Y-m-d')}}"--}}
{{--                                               autocomplete="off"/>--}}
{{--                                        <span class="error"></span>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="form-group">--}}
{{--                                    <label class="col-md-3 control-label text-right">Due Date<span--}}
{{--                                            class="required">*</span></label>--}}
{{--                                    <div class="col-md-6">--}}
{{--                                        <input type="text" class="form-control" data-plugin-datepicker--}}
{{--                                               data-plugin-options='{"todayHighlight" : true}'--}}
{{--                                               name="procurement_due_date" value="{{date('Y-m-d')}}"--}}
{{--                                               autocomplete="off"/>--}}
{{--                                        <span class="error"></span>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="form-group">--}}
{{--                                    <label class="col-md-3 control-label text-right">Delivery from<span--}}
{{--                                            class="required">*</span></label>--}}
{{--                                    <div class="col-md-6">--}}
{{--                                        <input type="text" class="form-control" data-plugin-datepicker--}}
{{--                                               data-plugin-options='{"todayHighlight" : true}' name="delivery_from_date"--}}
{{--                                               value="{{date('Y-m-d')}}" autocomplete="off"/>--}}
{{--                                        <span class="error"></span>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="form-group">--}}
{{--                                    <label class="col-md-3 control-label text-right">Delivery To<span--}}
{{--                                            class="required">*</span></label>--}}
{{--                                    <div class="col-md-6">--}}
{{--                                        <input type="text" class="form-control" data-plugin-datepicker--}}
{{--                                               data-plugin-options='{"todayHighlight" : true}' name="delivery_to_date"--}}
{{--                                               value="{{date('Y-m-d')}}" autocomplete="off"/>--}}
{{--                                        <span class="error"></span>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="form-group">--}}
{{--                                    <label class="col-md-3 control-label text-right">Payment Terms<span--}}
{{--                                            class="required">*</span></label>--}}
{{--                                    <div class="col-md-6">--}}
{{--                                        <input type="text" required class="form-control text-uppercase"--}}
{{--                                               name="payment_terms"--}}
{{--                                               value="{{old('payment_terms')}}"/>--}}
{{--                                        <span class="error"></span>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="form-group">--}}
{{--                                    <label class="col-md-3 control-label text-right">Delivery Address<span--}}
{{--                                            class="required">*</span></label>--}}
{{--                                    <div class="col-md-6">--}}
{{--                                        <textarea required class="form-control" name="delivery_address"></textarea>--}}
{{--                                        <span class="error"></span>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <footer class="panel-footer">--}}
{{--                            <div class="row">--}}
{{--                                <div class="col-md-offset-10 col-md-2">--}}
{{--                                    <button type="submit" name="save" value="1"--}}
{{--                                            class="btn btn btn-dark btn-block center">--}}
{{--                                        <i class="fas fa-plus-circle"></i> Save--}}
{{--                                    </button>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </footer>--}}
{{--                    </form>--}}
                </div>
            </div>
        </div>
    </section>
@endsection
