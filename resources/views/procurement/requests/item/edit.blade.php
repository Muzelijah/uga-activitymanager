@extends('layouts.main')
@section('content')
    <section class="panel" xmlns="http://www.w3.org/1999/html">
        <div class="tabs-custom">
            <ul class="nav nav-tabs">
                <li >
                    <a href="{{route('procurement.items.data',['request' => $procurement_request->procurement_request_ref])}}">
                        <i class="fas fa-list-ul"></i> Procurement Request Items
                    </a>
                </li>
                <li class="active">
                    <a href="#add" data-toggle="tab">
                        <i class="fas fa-edit"></i> Edit Procurement Request Item
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane box  mb-md" id="list">

                </div>

                <div class="tab-pane active" id="add">
                    <form action="{{route('procurement.items.update',['request'=>$procurement_request->procurement_request_ref,'item'=>$procurement_item->procurement_item_ref])}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="panel-body">

                            <div class="row">
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Item Description<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <input type="text" required class="form-control"
                                               name="item_description"
                                               value="{{$procurement_item->item_description}}"/>
                                        <span class="error"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Item Quantity<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <input type="number" min="1" required class="form-control"
                                               name="item_qty"
                                               value="{{$procurement_item->item_qty}}"/>
                                        <span class="error"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Unit Measure<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <select name="unit_measure_id"
                                                class="form-control select2-hidden-accessible"
                                                id="unit_measure_id" data-plugin-selecttwo="" data-width="100%"
                                                tabindex="-1"
                                                aria-hidden="true" required>
                                            <option value="">Select</option>
                                            @if(!empty($unit_measures))
                                                @foreach($unit_measures as $unit_measure)
                                                    <option value="{{$unit_measure->id}}" @if($procurement_item->unit_measure->id == $unit_measure->id) selected @endif>{{$unit_measure->unit_measure_name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Total Days<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <input type="number" min="1"  required class="form-control text-uppercase"
                                               name="total_days"
                                               value="{{$procurement_item->total_days}}"/>
                                        <span class="error"></span>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <footer class="panel-footer">
                            <div class="row">
                                <div class="col-md-offset-10 col-md-2">
                                    <button type="submit" name="save" value="1"
                                            class="btn btn btn-dark btn-block center">
                                        <i class="fas fa-plus-circle"></i> Save
                                    </button>
                                </div>
                            </div>
                        </footer>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
