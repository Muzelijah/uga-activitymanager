@extends('layouts.main')
@section('content')
    <section class="panel" xmlns="http://www.w3.org/1999/html">
        <div class="tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#list" data-toggle="tab">
                        <i class="fas fa-list-ul"></i> Procurement Request Items
                    </a>
                </li>
                <li>
                    <a href="#add" data-toggle="tab">
                        <i class="fas fa-edit"></i> Add New Procurement Request Item
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane box active mb-md" id="list">
                    {{--                    <div class="row mt-5 pt-5">--}}
                    {{--                        <div class="col-lg-12">--}}
                    {{--                            <div class="headers-line">--}}
                    {{--                                <i class="fas fa-list"></i> Order Details--}}
                    {{--                            </div>--}}
                    {{--                        </div>--}}
                    {{--                    </div>--}}
                    <form action="{{route('activities.status')}}" method="post">

                        @csrf
                        <table class="table table-bordered table-hover mb-none table-condensed table-export">
                            <thead>
                            <tr>
                                <th>
                                    SN
                                </th>
                                <th>Item Description</th>
                                <th>Quantity</th>
                                <th>Measure</th>
                                <th>Total Days</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(!empty($procurement_request_items))
                                @php $rows = 1 @endphp
                                @foreach($procurement_request_items as $procurement_request_item)
                                    <tr>
                                        <td>{{$rows++}}</td>
                                        <td>{{$procurement_request_item->item_description}}</td>
                                        <td>{{$procurement_request_item->item_qty}}</td>
                                        <td>{{$procurement_request_item->unit_measure->unit_measure_name}}</td>
                                        <td>{{$procurement_request_item->total_days}}</td>
                                        <td>
                                            <a href="{{route('procurement.items.edit',['request' => $procurement_request_item->procurement_request->procurement_request_ref,'item'=>$procurement_request_item->procurement_item_ref])}}"
                                               data-toggle="tooltip" data-original-title="Edit"
                                               class="btn btn-circle btn-dark icon">
                                                <i class="fas fa-pen-nib"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>

                    </form>

                </div>

                <div class="tab-pane" id="add">
                    <form action="{{route('procurement.items.store',['request'=>$procurement_request->procurement_request_ref])}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="panel-body">

                            <div class="row">
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Item Description<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <input type="text" required class="form-control"
                                               name="item_description"
                                               value="{{old('item_description')}}"/>
                                        <span class="error"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Item Quantity<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <input type="number" min="1" required class="form-control"
                                               name="item_qty"
                                               value="{{old('item_qty')}}"/>
                                        <span class="error"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Unit Measure<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <select name="unit_measure_id"
                                                class="form-control select2-hidden-accessible"
                                                id="unit_measure_id" data-plugin-selecttwo="" data-width="100%"
                                                tabindex="-1"
                                                aria-hidden="true" required>
                                            <option value="">Select</option>
                                            @if(!empty($unit_measures))
                                                @foreach($unit_measures as $unit_measure)
                                                    <option value="{{$unit_measure->id}}">{{$unit_measure->unit_measure_name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Total Days<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <input type="number" min="1"  required class="form-control text-uppercase"
                                               name="total_days"
                                               value="{{old('total_days')}}"/>
                                        <span class="error"></span>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <footer class="panel-footer">
                            <div class="row">
                                <div class="col-md-offset-10 col-md-2">
                                    <button type="submit" name="save" value="1"
                                            class="btn btn btn-dark btn-block center">
                                        <i class="fas fa-plus-circle"></i> Save
                                    </button>
                                </div>
                            </div>
                        </footer>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
