@extends('layouts.main')
@section('content')
    <section class="panel" xmlns="http://www.w3.org/1999/html">
        <div class="tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#list" data-toggle="tab">
                        <i class="fas fa-list-ul"></i> Procurement Request Invitations
                    </a>
                </li>
{{--                <li>--}}
{{--                    <a href="#add" data-toggle="tab">--}}
{{--                        <i class="fas fa-edit"></i> Create New Procurement Request--}}
{{--                    </a>--}}
{{--                </li>--}}
            </ul>
            <div class="tab-content">
                <div class="tab-pane box active mb-md" id="list">
                    <form action="{{route('activities.status')}}" method="post">

                        @csrf
                        <table class="table table-bordered table-hover mb-none table-condensed table-export">
                            <thead>
                            <tr>
                                <th>
                                    SN
                                </th>
                                <th>Activity</th>
                                <th>Vendor</th>
                                <th>Email</th>
                                <th>Contacts</th>
                                <th class="text-right">Quotation Value</th>
                                <th>Status</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(!empty($procurement_invitations))
                                @php $rows = 1 @endphp
                                @foreach($procurement_invitations as $procurement_invitation)
                                    <tr>
                                        <td>
                                            {{$rows++}}
                                        </td>
                                        <td>{{$procurement_invitation->activity->activity_name}}</td>
                                        <td>{{$procurement_invitation->vendor->vendor_name}}</td>
                                        <td>{{$procurement_invitation->procurement_invitation_email}}</td>
                                        <td>{{$procurement_invitation->procurement_invitation_mobile}}</td>
                                        <td class="text-right">{{number_format($procurement_invitation->vendor_items->sum('line_total'),0)}}</td>
                                        <td>@if($procurement_invitation->procurement_invitation_status == 2)
                                                Response Submitted
                                            @else
                                                Pending Response
                                            @endif
                                        </td>
                                        <td>
                                            <a target="_blank" href="{{route('procurement.requests.vendors.invitations.view',['invitation' => $procurement_invitation->procurement_invitation_ref])}}"
                                               data-toggle="tooltip" data-original-title="Preview Invitation"
                                               class="btn btn-circle btn-primary icon">
                                                <i class="fas fa-eye"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                <tr>
                                    <th>Total</th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            @endif
                            </tbody>
                        </table>

                    </form>

                </div>

                <div class="tab-pane" id="add">
{{--                    <form action="{{route('procurement.requests.store')}}" method="post" enctype="multipart/form-data">--}}
{{--                        @csrf--}}
{{--                        <div class="panel-body">--}}

{{--                            <div class="row">--}}
{{--                                <div class="form-group">--}}
{{--                                    <label class="col-md-3 control-label text-right">RFQ Reference<span--}}
{{--                                            class="required">*</span></label>--}}
{{--                                    <div class="col-md-6">--}}
{{--                                        <input type="text" required class="form-control text-uppercase"--}}
{{--                                               name="rfq_reference_number"--}}
{{--                                               value="{{old('rfq_reference_number')}}"/>--}}
{{--                                        <span class="error"></span>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="form-group">--}}
{{--                                    <label class="col-md-3 control-label text-right">Activity<span--}}
{{--                                            class="required">*</span></label>--}}
{{--                                    <div class="col-md-6">--}}
{{--                                        <select name="activity_id"--}}
{{--                                                class="form-control select2-hidden-accessible"--}}
{{--                                                id="product_category_id" data-plugin-selecttwo="" data-width="100%"--}}
{{--                                                tabindex="-1"--}}
{{--                                                aria-hidden="true" required>--}}
{{--                                            <option value="">Select</option>--}}
{{--                                            @if(!empty($activities))--}}
{{--                                                @foreach($activities as $activity)--}}
{{--                                                    <option value="{{$activity->id}}">{{$activity->activity_name}}--}}
{{--                                                        ( {{$activity->activity_code}})--}}
{{--                                                    </option>--}}
{{--                                                @endforeach--}}
{{--                                            @endif--}}
{{--                                        </select>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="form-group">--}}
{{--                                    <label class="col-md-3 control-label text-right">Exchange Rate<span--}}
{{--                                            class="required">*</span></label>--}}
{{--                                    <div class="col-md-6">--}}
{{--                                        <input type="number"  required class="form-control text-uppercase"--}}
{{--                                               name="exchange_rate"--}}
{{--                                               value="{{old('exchange_rate')}}"/>--}}
{{--                                        <span class="error"></span>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="form-group">--}}
{{--                                    <label class="col-md-3 control-label text-right">Issue Date<span--}}
{{--                                            class="required">*</span></label>--}}
{{--                                    <div class="col-md-6">--}}
{{--                                        <input type="text" class="form-control" data-plugin-datepicker--}}
{{--                                               data-plugin-options='{"todayHighlight" : true}'--}}
{{--                                               name="procurement_issue_date" value="{{date('Y-m-d')}}"--}}
{{--                                               autocomplete="off"/>--}}
{{--                                        <span class="error"></span>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="form-group">--}}
{{--                                    <label class="col-md-3 control-label text-right">Due Date<span--}}
{{--                                            class="required">*</span></label>--}}
{{--                                    <div class="col-md-6">--}}
{{--                                        <input type="text" class="form-control" data-plugin-datepicker--}}
{{--                                               data-plugin-options='{"todayHighlight" : true}'--}}
{{--                                               name="procurement_due_date" value="{{date('Y-m-d')}}"--}}
{{--                                               autocomplete="off"/>--}}
{{--                                        <span class="error"></span>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="form-group">--}}
{{--                                    <label class="col-md-3 control-label text-right">Delivery from<span--}}
{{--                                            class="required">*</span></label>--}}
{{--                                    <div class="col-md-6">--}}
{{--                                        <input type="text" class="form-control" data-plugin-datepicker--}}
{{--                                               data-plugin-options='{"todayHighlight" : true}' name="delivery_from_date"--}}
{{--                                               value="{{date('Y-m-d')}}" autocomplete="off"/>--}}
{{--                                        <span class="error"></span>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="form-group">--}}
{{--                                    <label class="col-md-3 control-label text-right">Delivery To<span--}}
{{--                                            class="required">*</span></label>--}}
{{--                                    <div class="col-md-6">--}}
{{--                                        <input type="text" class="form-control" data-plugin-datepicker--}}
{{--                                               data-plugin-options='{"todayHighlight" : true}' name="delivery_to_date"--}}
{{--                                               value="{{date('Y-m-d')}}" autocomplete="off"/>--}}
{{--                                        <span class="error"></span>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="form-group">--}}
{{--                                    <label class="col-md-3 control-label text-right">Payment Terms<span--}}
{{--                                            class="required">*</span></label>--}}
{{--                                    <div class="col-md-6">--}}
{{--                                        <input type="text" required class="form-control text-uppercase"--}}
{{--                                               name="payment_terms"--}}
{{--                                               value="{{old('payment_terms')}}"/>--}}
{{--                                        <span class="error"></span>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="form-group">--}}
{{--                                    <label class="col-md-3 control-label text-right">Delivery Address<span--}}
{{--                                            class="required">*</span></label>--}}
{{--                                    <div class="col-md-6">--}}
{{--                                        <textarea required class="form-control" name="delivery_address"></textarea>--}}
{{--                                        <span class="error"></span>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <footer class="panel-footer">--}}
{{--                            <div class="row">--}}
{{--                                <div class="col-md-offset-10 col-md-2">--}}
{{--                                    <button type="submit" name="save" value="1"--}}
{{--                                            class="btn btn btn-dark btn-block center">--}}
{{--                                        <i class="fas fa-plus-circle"></i> Save--}}
{{--                                    </button>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </footer>--}}
{{--                    </form>--}}
                </div>
            </div>
        </div>
    </section>
@endsection
