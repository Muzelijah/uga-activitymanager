@extends('layouts.app')
@section('content')
    <section class="panel">

        <div class="tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#list" data-toggle="tab">
                        <i class="fas fa-dollar-sign"></i> Procurement Request Preview
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane box active mb-md" id="list">

                    <form
                        action="{{route('procurement.requests.vendors.invitations.save.unit.prices',$invitation->procurement_invitation_ref)}}"
                        id="PaymentForm" method="post" enctype="multipart/form-data">
                        <input type="hidden" id="invitation_ref" value="{{$invitation->procurement_invitation_ref}}"
                               text="invitation_ref">
                        @csrf
                        <div class="panel-body">
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-md-12 control-label text-left"><span style="text-align: justify">World Food Programme (WFP) invites you to submit a quote in accordance with the requirements of this request for quotes. Quotes must be received by WFP no later than the Date and Time indicated in the table below:</span></label>
                                    <div class="col-md-12">
                                        <table style="border: 1px solid black;  border-collapse: collapse;" border="1"
                                               width="100%">
                                            <tr style="border: 1px solid black; border-collapse: collapse;">
                                                <td width="35%"><span
                                                        class="text-left"><strong>RFQ Refence Number</strong></span>
                                                </td>
                                                <td width="65%">{{$invitation->procurement_request->rfq_reference_number}}</td>
                                            </tr>
                                            <tr style="border: 1px solid black; border-collapse: collapse;">
                                                <td><span
                                                        class="text-right"><strong>Request for Quotes Issue Date:</strong></span>
                                                </td>
                                                <td>{{date('jS F Y',strtotime($invitation->procurement_request->procurement_issue_date))}}</td>
                                            </tr>
                                            <tr style="border: 1px solid black; border-collapse: collapse;">
                                                <td><span
                                                        class="text-right"><strong>Due Date and Time for Questions:</strong></span>
                                                </td>
                                                <td>{{date('jS F Y',strtotime($invitation->procurement_request->procurement_due_date))}}</td>
                                            </tr>
                                            <tr style="border: 1px solid black; border-collapse: collapse;">
                                                <td><span class="text-right"><strong>Delivery Period</strong></span>
                                                </td>
                                                <td>From
                                                    : {{date('jS F Y',strtotime($invitation->procurement_request->delivery_from_date))}}
                                                    To {{date('jS F Y',strtotime($invitation->procurement_request->delivery_to_date))}}</td>
                                            </tr>
                                            <tr style="border: 1px solid black; border-collapse: collapse;">
                                                <td><span class="text-right"><strong>Delivery Address</strong></span>
                                                </td>
                                                <td>{{$invitation->procurement_request->delivery_address}}</td>
                                            </tr>
                                            <tr style="border: 1px solid black; border-collapse: collapse;">
                                                <td><span class="text-right"><strong>Payment Terms:</strong></span></td>
                                                <td>{{$invitation->procurement_request->payment_terms}}</td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <div class="text-center"><i>Quotes submitted after the deadline has
                                                            passed or that do not include all of the information
                                                            requested may be rejected.</i></div>
                                                </td>
                                            </tr>

                                        </table>
                                    </div>
                                    <div class="col-md-12">
                                        <h4>Complete Description of goods/Scope of Work/Specifications as below</h4>
                                    </div>
                                    <div class="col-md-12">
                                        <table style="border: 1px solid black;  border-collapse: collapse;" border="1"
                                               width="100%">
                                            <tr style="border: 1px solid black;">
                                                <th style="border: 1px solid black;  text-align: center;" width="5%">
                                                    SN
                                                </th>
                                                <th style="border: 1px solid black;  text-align: center;" width="40%">
                                                    Description
                                                </th>
                                                <th style="border: 1px solid black;  text-align: center;" width="5%">
                                                    Qty
                                                </th>
                                                <th style="border: 1px solid black;  text-align: center;" width="10%">
                                                    Measure
                                                </th>
                                                <th style="border: 1px solid black;  text-align: center;" width="10%">
                                                    Total Days
                                                </th>
                                                <th style="border: 1px solid black;  text-align: right;" width="10%">
                                                    Unit Price
                                                </th>
                                                <th style="border: 1px solid black;  text-align: right;" width="10%">
                                                    Total Price
                                                </th>
                                            </tr>
                                            @php
                                                $rows = 1;
                                                $line_total = 0;
                                            foreach($invitation->vendor_items as $item){
                                                $line_total = $line_total + $item->line_total;
                                            @endphp
                                            <tr style="border: 1px solid black;">
                                                <td style="border: 1px solid black; text-align: center;">{{$rows++}}</td>
                                                <td style="border: 1px solid black; text-align: center;">{{$item->item_description}}</td>
                                                <td style="border: 1px solid black; text-align: center;">{{$item->item_qty}}</td>
                                                <td style="border: 1px solid black; text-align: center;">{{$item->unit_measure->unit_measure_name}}{{($item->item_qty > 1) ? "s":""}}</td>
                                                <td style="border: 1px solid black; text-align: center;">{{$item->total_days}}</td>
                                                <td class="text-right">
                                                    @if($invitation->procurement_invitation_status == 1)
                                                        <input style="border: 1px solid white; text-align: right;"
                                                               class="form-control bg-dark text-right" type="number"
                                                               name="unit_price[{{$item->procurement_vendor_ref}}]"
                                                               id="desc"
                                                               value="{{$item->unit_price}}"
                                                               required="required"/>
                                                    @else
                                                        {{number_format($item->unit_price,0)}}
                                                    @endif</td>
                                                <td style="border: 1px solid black; text-align: right;">{{number_format($item->line_total,0)}}</td>
                                            </tr>
                                            @php
                                                }
                                            @endphp
                                            <tr style="border: 1px solid black;">
                                                <th style="border: 1px solid black;  text-align: center;" width="5%">
                                                    Total
                                                </th>
                                                <th style="border: 1px solid black;  text-align: center;"
                                                    width="40%"></th>
                                                <th style="border: 1px solid black;  text-align: center;"
                                                    width="5%"></th>
                                                <th style="border: 1px solid black;  text-align: center;"
                                                    width="10%"></th>
                                                <th style="border: 1px solid black;  text-align: center;"
                                                    width="10%"></th>
                                                <th style="border: 1px solid black;  text-align: right;"
                                                    width="10%"></th>
                                                <th style="border: 1px solid black;  text-align: right;"
                                                    width="10%">{{number_format($line_total,0)}}</th>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                @if($invitation->procurement_invitation_status == 1)
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <span class="text-danger">Note : Click <span class="text-primary">Get Submit Code</span> to get code that will authorize you to submit your response.</span>
                                    </div>
                                </div>
                                @endif
                                <div class="form-group" id="display_submit_code" style="display: none;">
                                    <div class="col-md-12">
                                        <label class="control-label text-left">Submit Code<span
                                                class="required">*</span></label>

                                    </div>
                                    <div class="col-md-2">
                                        <input id="submit_code" type="number"
                                               class="form-control text-uppercase"
                                               name="submit_code"
                                               value="{{old('submit_code')}}"/>
                                        <span class="error"></span>
                                    </div>
                                    <div class="col-md-12">
                                        <label class="control-label text-left">Attach a coppy of your quotation before submitting<span
                                                class="required">*</span></label>

                                    </div>
                                    <div class="col-md-2">
                                        <input id="quotation" type="file"
                                               class="form-control text-uppercase"
                                               name="quotation"
                                               value="{{old('quotation')}}"/>
                                        <span class="error"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <footer class="panel-footer">
                            <div class="row">
                                @if($invitation->procurement_invitation_status == 1)
                                <div class="col-md-4">
                                    <button type="submit" id="process_payment22" class="btn btn-dark btn-block"
                                            data-loading-text="<i class='fas fa-spinner fa-spin'></i> Processing">
                                        Save Unit Prices
                                    </button>
                                </div>
                                <div class="col-md-4">
                                    <button type="button" id="btn_get_submit_code" class="btn btn-primary btn-block"
                                            data-loading-text="<i class='fas fa-spinner fa-spin'></i> Processing">
                                        Get Submit Code
                                    </button>
                                </div>
                                <div class="col-md-4">
                                    <button type="button" id="btn_submit_request" class="btn btn-dark btn-block"
                                            data-loading-text="<i class='fas fa-spinner fa-spin'></i> Processing">
                                        Submit Request
                                    </button>
                                </div>
                                @else
                                    <h2 class="text-primary text-center">Your submission was successful </h2>
                                @endif

                            </div>
                        </footer>
                    </form>
                </div>
            </div>
        </div>
    </section>


@endsection
@section('myscript')
    <script type="text/javascript">
        function getRndInteger(min, max) {
            return Math.floor(Math.random() * (max - min + 1)) + min;
        }

        $('#btn_get_submit_code').on("click", function () {
            send_code();
        });
        $('#btn_submit_request').on("click", function () {
            var submit_code = document.getElementById("submit_code");
            if (submit_code.value != "") {
                send_request();
            } else {
                alert('Enter Submit Code');

            }

        });

        function send_code() {
            var invitation_ref = null;
            var invitation_ref = document.getElementById("invitation_ref");
            //console.log(activity_participant);
            var my_url = "{{url('/procurement/requests/vendor/invitation/')}}";
            my_url = my_url + '/' + invitation_ref.value + '/sendcode';
            console.log(my_url);
            if (invitation_ref !== "") {
                $.ajax({
                    type: 'GET', //THIS NEEDS TO BE GET
                    url: my_url,
                    success: function (data) {
                        var txt_code = document.getElementById('display_submit_code');
                        var btn_code = document.getElementById('btn_get_submit_code');
                        txt_code.style.display = 'block';
                        btn_code.style.display = 'none';
                    },
                    error: function () {
                        console.log(data);
                    }
                });

                // alert(activity_participant);
            } else {
                alert('Select an Attendant');
            }

        }

        function send_request() {
            var invitation_ref = null;
            var invitation_ref = document.getElementById("invitation_ref");
            var submit_code = document.getElementById("submit_code");
            var quotation = document.getElementById('quotation');
            //console.log(activity_participant);
            var my_url = "{{url('/procurement/requests/vendor/invitation/')}}";
            my_url = my_url + '/' + invitation_ref.value + '/submitrequest';
            console.log(my_url);
            if (invitation_ref !== "") {
                $.ajax({
                    type: 'GET', //THIS NEEDS TO BE GET
                    url: my_url,
                    data: {'submit_code': submit_code.value},
                    success: function (data) {
                        location.reload();
                    },
                    error: function () {
                        console.log(data);
                    }
                });

                // alert(activity_participant);
            } else {
                alert('Select an Attendant');
            }

        }
    </script>
@endsection
{{--<x-guest-layout>--}}
{{--    <x-auth-card>--}}
{{--        <x-slot name="logo">--}}
{{--            <a href="/">Kamwe Trading Company External Recruitment Agency--}}
{{--                <x-application-logo class="w-20 h-20 fill-current text-gray-500" />--}}
{{--            </a>--}}
{{--        </x-slot>--}}

{{--        <!-- Session Status -->--}}
{{--        <x-auth-session-status class="mb-4" :status="session('status')" />--}}

{{--        <!-- Validation Errors -->--}}
{{--        <x-auth-validation-errors class="mb-4" :errors="$errors" />--}}

{{--        --}}
{{--    </x-auth-card>--}}
{{--</x-guest-layout>--}}

