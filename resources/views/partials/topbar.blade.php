<header class="header bg-primary">
	<div class="logo-env  bg-primary">
		<a href="{{ url('/dashboard') }}" class="logo">
			<img src="{{ asset('public/app_image/logo.png') }}" height="40">
		</a>

		<div class="visible-xs toggle-sidebar-left" data-toggle-class="sidebar-left-opened" data-target="html" data-fire-event="sidebar-left-opened">
			<i class="fa fa-bars" aria-label="Toggle sidebar"></i>
		</div>
	</div>

	<div class="header-left hidden-xs  bg-primary">
		<ul class="header-menu">
			<!-- sidebar toggle button -->
			<li>
				<div class="header-menu-icon sidebar-toggle" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
					<i class="fas fa-bars" aria-label="Toggle sidebar"></i>
				</div>
			</li>
			<!-- full screen button -->
			<li>
				<div class="header-menu-icon s-expand">
					<i class="fas fa-expand"></i>
				</div>
			</li>
			<!-- shortcut box -->
{{--   			<li>--}}
{{--				<div class="header-menu-icon dropdown-toggle" data-toggle="dropdown">--}}
{{--					<i class="fas fa-th"></i>--}}
{{--				</div>--}}
{{--				<div class="dropdown-menu header-menubox qk-menu">--}}
{{--					<div class="qk-menu-p">--}}
{{--						<div class="menu-icon-grid">--}}
{{--							<a href="#"><i class="fas fa-users"></i> Admission</a>--}}
{{--						    <a href="#"><i class="fas fa-donate"></i> Payment</a>--}}
{{--							<a href="#"><i class="fas fa-fill-drip"></i> Leave</a>--}}
{{--							<a href="#"><i class="fas fa-video"></i> Live Class</a>--}}
{{--							<a href="#"><i class="fas fa-hand-holding-usd"></i> Invoice</a>--}}
{{--							<a href="#"><i class="fab fa-wpforms"></i> Payments</a>--}}

{{--						</div>--}}
{{--					</div>--}}
{{--				</div>--}}
{{--			</li>--}}

		</ul>

		<!-- search bar -->

			<span class="separator hidden-sm"></span>
			<form class="search nav-form" method="post" action="{{route('activities.search')}}">
                @csrf
				<div class="input-group input-search">
					<input type="text" class="form-control" name="search_text" id="search_text" placeholder="Search">
					<span class="input-group-btn">
						<button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
					</span>
				</div>
			</form>

	</div>

	<div class="header-right  bg-primary">
		<ul class="header-menu">
			<!-- website link -->
{{--			<li>--}}
{{--				<a href="#" target="_blank" class="header-menu-icon" data-toggle="tooltip" data-placement="bottom"--}}
{{--				data-original-title="">--}}
{{--					<i class="fas fa-globe"></i>--}}
{{--				</a>--}}
{{--			</li>--}}

			<!-- session switcher box -->
{{--			<li>--}}
{{--				<a href="#" class="dropdown-toggle header-menu-icon" data-toggle="dropdown">--}}
{{--					<i class="far fa-calendar-alt"></i>--}}
{{--				</a>--}}
{{--				<div class="dropdown-menu header-menubox mh-oh">--}}
{{--					<div class="notification-title">--}}
{{--						<i class="far fa-calendar-alt"></i> Session--}}
{{--					</div>--}}
{{--					<div class="content hbox pr-none">--}}
{{--						<div class="scrollable visible-slider dh-tf" data-plugin-scrollable>--}}
{{--							<div class="scrollable-content">--}}
{{--								<ul>--}}
{{--	<li>--}}
{{--		<a href="#">Year</a>--}}
{{--	</li>--}}

{{--								</ul>--}}
{{--							</div>--}}
{{--						</div>--}}
{{--					</div>--}}
{{--				</div>--}}
{{--			</li>--}}

			<!-- languages switcher box -->
{{--			<li>--}}
{{--				<a href="#" class="dropdown-toggle header-menu-icon" data-toggle="dropdown">--}}
{{--					<i class="far fa-flag"></i>--}}
{{--				</a>--}}
{{--				<div class="dropdown-menu header-menubox mh-oh">--}}
{{--					<div class="notification-title">--}}
{{--						<i class="far fa-flag"></i> Language--}}
{{--					</div>--}}
{{--					<div class="content hbox lb-pr">--}}
{{--						<div class="scrollable visible-slider dh-tf" data-plugin-scrollable>--}}
{{--							<div class="scrollable-content">--}}
{{--								<ul>--}}

{{--	<li>--}}
{{--		<a href="#">--}}
{{--		<img class="ln-img" src=""--}}
{{--		alt=""> English<i class="fas fa-check"></i>--}}
{{--		</a>--}}
{{--	</li>--}}

{{--								</ul>--}}
{{--							</div>--}}
{{--						</div>--}}
{{--					</div>--}}
{{--				</div>--}}
{{--			</li>--}}
			<!-- message alert box -->
{{--			<li>--}}
{{--				<a href="#" class="dropdown-toggle header-menu-icon" data-toggle="dropdown">--}}
{{--					<i class="far fa-bell"></i>--}}

{{--							<span class="badge">0</span>';--}}

{{--				</a>--}}
{{--				<div class="dropdown-menu header-menubox qmsg-box-mw">--}}
{{--					<div class="notification-title">--}}
{{--						<i class="far fa-bell"></i> Messages--}}
{{--					</div>--}}
{{--					<div class="content">--}}
{{--						<ul>--}}
{{--							<li>--}}
{{--									<a href="#" class="clearfix">--}}
{{--										<!-- preview of sender image -->--}}
{{--										<figure class="image pull-right">--}}
{{--											<img src="#" height="40px" width="40px" class="img-circle">--}}
{{--										</figure>--}}
{{--										<!-- preview of sender name and date -->--}}
{{--										<span class="title line"><strong>Username</strong>--}}
{{--										<small>2021-01-01</small>  </span>--}}
{{--										<!-- preview of the last unread message sub-string -->--}}
{{--										<span class="message">Hello</span>--}}
{{--									</a>--}}
{{--								</li>--}}
{{--							<li class="text-center">You do not have any new messages</li>--}}

{{--						</ul>--}}
{{--					</div>--}}
{{--					<div class="notification-footer">--}}
{{--						<div class="text-right">--}}
{{--							<a href="#" class="view-more">All Messages</a>--}}
{{--						</div>--}}
{{--					</div>--}}
{{--				</div>--}}
{{--			</li>--}}
		</ul>

		<!-- user profile box -->
		<span class="separator"></span>
		<div id="userbox" class="userbox  bg-primary">
			<a href="#" data-toggle="dropdown">
				<figure class="profile-picture">
					<img src="{{ asset('public/app_image/defualt.png') }}" alt="user-image" class="img-circle" height="35">
				</figure>
			</a>
			<div class="dropdown-menu  bg-primary">
				<ul class="dropdown-user list-unstyled">
					<li class="user-p-box">
						<div class="dw-user-box">
							<div class="u-img">
								<img src="{{ asset('public/app_image/defualt.png') }}" alt="user">
							</div>
							<div class="u-text">
								<h4>{{auth()->user()->name}}</h4>
								<p class="text-white">{{auth()->user()->getRoleNames()->first()}}</p>
                                <form method="POST" action="{{ route('logout') }}">
                                    @csrf
                                    <button type="submit" class="btn btn-danger btn-xs"><i class="fas fa-sign-out-alt"></i> Logout</button>
                                </form>
							</div>
						</div>
					</li>
{{--					<li role="separator" class="divider"></li>--}}
<!--					<li><a href="--><?php //echo base_url('profile');?><!--"><i class="fas fa-user-shield"></i> --><?php //echo translate('profile');?><!--</a></li>-->
<!--					<li><a href="--><?php //echo base_url('profile/password');?><!--"><i class="fas fa-mars-stroke-h"></i> --><?php //echo translate('reset_password');?><!--</a></li>-->
{{--					<li><a href="#"><i class="far fa-envelope"></i> Mail</a></li>--}}

{{--						<li role="separator" class="divider"></li>--}}
{{--						<li><a href="#"><i class="fas fa-toolbox"></i> Settings</a></li>--}}
{{--						<li role="separator" class="divider"></li>--}}
{{--						<li><a href="#"><i class="fas fa-school"></i> Sub Settings</a></li>--}}

{{--					<li role="separator" class="divider"></li>--}}
{{--					<li><a href="#"><i class="fas fa-sign-out-alt"></i> Logout</a></li>--}}
				</ul>
			</div>
		</div>
	</div>
</header>
