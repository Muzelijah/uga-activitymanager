<head>
	<meta charset="UTF-8">
	<meta name="keywords" content="">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="description" content="Ramom School Management System">
	<meta name="author" content="RamomCoder">
	<title> @if(!empty($title)) {{ $title }} | @endif {{ config('app.name') }}  </title>
{{--    <link rel="shortcut icon" href="{{ asset('public/public/assets/images/favicon.png')}}">--}}
    <link rel="icon" type="image/x-icon" href="{{url('public/app_image/favicon-16x16.png')}}">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<!-- include stylesheet -->
	@include('partials.stylesheet')




	<!-- ramom css -->
	<link rel="stylesheet" href="{{ url('public/assets/css/ramomnew.css')}}">
    <style>
        .kbw-signature { width: 100%; height: 180px;}
        #signaturePad canvas{
            width: 100% !important;
            height: auto;
        }
    </style>

	<!-- If user have enabled CSRF proctection this function will take care of the ajax requests and append custom header for CSRF -->
{{--    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>--}}
    <script src="{{ url('public/js/signature.js')}}"></script>
	<script type="text/javascript">
		{{--var base_url = "{{ url() }}";--}}
		{{--var csrfData = "{{ csrf_token() }}";--}}
		{{--$(function($) {--}}
		{{--	$.ajaxSetup({--}}
		{{--		data: csrfData--}}
		{{--	});--}}
		{{--});--}}
	</script>

</head>
