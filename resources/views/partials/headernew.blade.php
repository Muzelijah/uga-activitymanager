<head>
	<meta charset="UTF-8">
	<meta name="keywords" content="">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="description" content="School Wallet Management System">
	<meta name="author" content="Cyndrone">
	<title> @if(!empty($title)) {{ $title }} | @endif {{ config('app.name') }}  </title>
{{--    <link rel="shortcut icon" href="{{ asset('public/assets/images/favicon.png')}}">--}}
    <link rel="icon" type="image/x-icon" href="{{asset('public/app_image/favicon-16x16.png')}}">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<!-- include stylesheet -->
	@include('partials.stylesheetnew')




	<!-- ramom css -->
	<link rel="stylesheet" href="{{ asset('assets/css/ramom.css')}}">


	<!-- If user have enabled CSRF proctection this function will take care of the ajax requests and append custom header for CSRF -->
{{--    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>--}}
{{--    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.slim.min.js"></script>--}}
	<script type="text/javascript">
		{{--var base_url = "{{ url() }}";--}}
		{{--var csrfData = "{{ csrf_token() }}";--}}
		{{--$(function($) {--}}
		{{--	$.ajaxSetup({--}}
		{{--		data: csrfData--}}
		{{--	});--}}
		{{--});--}}
	</script>
    <script type="text/javascript">

        $("#selectAllchkbox").on("change", function(ev)
        {

            var $chcks = $(".checked-area input[type='checkbox']");

            if($(this).is(':checked'))
            {
                $chcks.prop('checked', true).trigger('change');
            } else {
                $chcks.prop('checked', false).trigger('change');
            }
        });
    </script>
</head>
