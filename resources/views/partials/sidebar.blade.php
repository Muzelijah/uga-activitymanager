<aside id="sidebar-left" class="sidebar-left bg-dark">
    <div class="sidebar-header bg-dark">
        <div class="sidebar-title">
            Main
        </div>
    </div>

    <div class="nano">
        <div class="nano-content  bg-dark">
            <nav id="menu" class="nav-main  bg-dark" role="navigation">
                <ul class="nav nav-main">

                    <li class="{{request()->routeIs('dashboard.index') ? 'nav-active' : ''}}">
                        <a href="{{route('dashboard.data')}}">
                            <i class="icons icon-grid"></i><span>Dashboard</span>
                        </a>
                    </li>
                    <li class="nav-parent">
                        <a><i class="fas fa-cog"></i><span>Activity Setup</span></a>
                        <ul class="nav nav-children">
                            @if(auth()->user()->hasPermissionForRoute('activities.create'))
                                <li class="{{request()->routeIs('activities.create') ? 'nav-active' : ''}}">
                                    <a href="{{route('activities.create')}}">
                                        <i class="fas fa-edit"></i><span>Create New Activity</span>
                                    </a>
                                </li>
                            @endif
                            @if(auth()->user()->hasPermissionForRoute('activities.data'))
                                <li class="{{request()->routeIs('activities.data',['status'=>2]) ? 'nav-active' : ''}}">
                                    <a href="{{route('activities.data',['status'=>2])}}">
                                        <i class="fas fa-tasks"></i><span>All Activities</span>
                                    </a>
                                </li>
                            @endif
                            @if(auth()->user()->hasPermissionForRoute('my.activities.data'))
                                <li class="{{request()->routeIs('my.activities.data',['status'=>2]) ? 'nav-active' : ''}}">
                                    <a href="{{route('my.activities.data',['status'=>2])}}">
                                        <i class="fas fa-tasks"></i><span>My Activities</span>
                                    </a>
                                </li>
                            @endif
                            @if(auth()->user()->hasPermissionForRoute('activities.unverified'))
                                <li class="{{request()->routeIs('activities.unverified') ? 'nav-active' : ''}}">
                                    <a href="{{route('activities.unverified')}}">
                                        <i class="fas fa-check-circle"></i><span>Verify Activity</span>
                                    </a>
                                </li>
                            @endif
                            @if(auth()->user()->hasPermissionForRoute('activities.unapproved'))
                                <li class="{{request()->routeIs('activities.unapproved') ? 'nav-active' : ''}}">
                                    <a href="{{route('activities.unapproved')}}">
                                        <i class="fas fa-check-circle"></i><span>Approve Activity</span>
                                    </a>
                                </li>
                            @endif
                            @if(auth()->user()->hasPermissionForRoute('activities.show.unverified.payments'))
                                <li class="{{request()->routeIs('activities.show.unverified.payments') ? 'nav-active' : ''}}">
                                    <a href="{{route('activities.show.unverified.payments')}}">
                                        <i class="fas fa-dollar-sign"></i><span>Verify Payments</span>
                                    </a>
                                </li>
                            @endif
                            @if(auth()->user()->hasPermissionForRoute('activities.show.unapproved.payments'))
                                <li class="{{request()->routeIs('activities.show.unapproved.payments') ? 'nav-active' : ''}}">
                                    <a href="{{route('activities.show.unapproved.payments')}}">
                                        <i class="fas fa-dollar-sign"></i><span>Approve Payments</span>
                                    </a>
                                </li>
                            @endif
                            @if(auth()->user()->hasPermissionForRoute('activities.show.approved.payments'))
                                <li class="{{request()->routeIs('activities.show.approved.payments') ? 'nav-active' : ''}}">
                                    <a href="{{route('activities.show.approved.payments')}}">
                                        <i class="fas fa-dollar-sign"></i><span>Approved Payments</span>
                                    </a>
                                </li>
                            @endif
                        </ul>
                    </li>
                    <li class="nav-parent">
                        <a><i class="fas fa-cog"></i><span>Activity Logistics</span></a>
                        <ul class="nav nav-children">
                            @if(auth()->user()->hasPermissionForRoute('vendors.data'))
                                <li class="{{request()->routeIs('vendors.data') ? 'nav-active' : ''}}">
                                    <a href="{{route('vendors.data')}}">
                                        <i class="fas fa-truck"></i><span>Vendors</span>
                                    </a>
                                </li>
                            @endif
                            @if(auth()->user()->hasPermissionForRoute('procurement.requests.create'))
                                <li class="{{request()->routeIs('procurement.requests.create') ? 'nav-active' : ''}}">
                                    <a href="{{route('procurement.requests.create')}}">
                                        <i class="fas fa-edit"></i><span>Create Procurement Request</span>
                                    </a>
                                </li>
                            @endif
                            @if(auth()->user()->hasPermissionForRoute('procurement.requests.data'))
                                <li class="{{request()->routeIs('procurement.requests.data') ? 'nav-active' : ''}}">
                                    <a href="{{route('procurement.requests.data',['status'=>2])}}">
                                        <i class="fas fa-list"></i><span>Procurement Requests</span>
                                    </a>
                                </li>
                            @endif
                        </ul>
                    </li>
                    <!-- branch -->
                    @if(auth()->user()->hasPermissionForRoute('participants.data'))
                        <li class="{{request()->routeIs('participants.data') ? 'nav-active' : ''}}">
                            <a href="{{route('participants.data')}}">
                                <i class="fas fa-users"></i><span>Participants</span>
                            </a>
                        </li>
                    @endif
                    <li class="nav-parent">
                        <a><i class="fas fa-cogs"></i><span>Settings</span></a>
                        <ul class="nav nav-children">
                            @if(auth()->user()->hasPermissionForRoute('budget.codes.data'))
                                <li class="{{request()->routeIs('budget.codes.data') ? 'nav-active' : ''}}">
                                    <a href="{{route('budget.codes.data')}}">
                                        <i class="fas fa-list"></i><span>Budget Codes</span>
                                    </a>
                                </li>
                            @endif
                            @if(auth()->user()->hasPermissionForRoute('unit.measures.data'))
                                <li class="{{request()->routeIs('unit.measures.data') ? 'nav-active' : ''}}">
                                    <a href="{{route('unit.measures.data')}}">
                                        <i class="fas fa-list"></i><span>Unit Measures</span>
                                    </a>
                                </li>
                            @endif
                            @if(auth()->user()->hasPermissionForRoute('users.data'))
                                <li class="{{request()->routeIs('users.data') ? 'nav-active' : ''}}">
                                    <a href="{{route('users.data')}}">
                                        <i class="fas fa-users"></i><span>Users</span>
                                    </a>
                                </li>
                            @endif
                            @if(auth()->user()->hasPermissionForRoute('roles.data'))
                                <li class="{{request()->routeIs('roles.data') ? 'nav-active' : ''}}">
                                    <a href="{{route('roles.data')}}">
                                        <i class="fas fa-list"></i><span>Roles</span>
                                    </a>
                                </li>
                            @endif
                        </ul>
                    </li>

                    {{--                    <li class="nav-parent">--}}
                    {{--                        <a><i class="fas fa-print"></i><span>Reports</span></a>--}}
                    {{--                        <ul class="nav nav-children">--}}
                    {{--                            <li class="{{request()->routeIs('branches.data') ? 'nav-active' : ''}}">--}}
                    {{--                                <a href="{{route('branches.data')}}">--}}
                    {{--                                    <i class="fas fa-list-alt"></i><span>Settlements</span>--}}
                    {{--                                </a>--}}
                    {{--                            </li>--}}
                    {{--                            <li class="{{request()->routeIs('branches.data') ? 'nav-active' : ''}}">--}}
                    {{--                                <a href="{{route('branches.data')}}">--}}
                    {{--                                    <i class="fas fa-list-alt"></i><span>Terminal Settlements</span>--}}
                    {{--                                </a>--}}
                    {{--                            </li>--}}


                    {{--                        </ul>--}}
                    {{--                    </li>--}}
                <!-- Patient Details -->
                    {{--                        <li class="nav-parent nav-expanded nav-active">--}}
                    {{--                            <a><i class="fas fa-globe"></i><span>Frontend</span></a>--}}
                    {{--                            <ul class="nav nav-children">--}}
                    {{--                                    <li class="nav-active">--}}
                    {{--                                        <a href="#">--}}
                    {{--                                            <span><i class="fas fa-caret-right"--}}
                    {{--                                                     aria-hidden="true"></i>Settings</span>--}}
                    {{--                                        </a>--}}
                    {{--                                    </li>--}}
                    {{--                                    <li class="nav-active">--}}
                    {{--                                        <a href="#">--}}
                    {{--                                            <span><i class="fas fa-caret-right"--}}
                    {{--                                                     aria-hidden="true"></i>Menu</span>--}}
                    {{--                                        </a>--}}
                    {{--                                    </li>--}}
                    {{--                                    <li class="nav-active">--}}
                    {{--                                        <a href="#">--}}
                    {{--                                            <span><i class="fas fa-caret-right"--}}
                    {{--                                                     aria-hidden="true"></i>Page Section</span>--}}
                    {{--                                        </a>--}}
                    {{--                                    </li>--}}
                    {{--                                    <li class="nav-active">--}}
                    {{--                                        <a href="#">--}}
                    {{--                                            <span><i class="fas fa-caret-right"--}}
                    {{--                                                     aria-hidden="true"></i>Manage Page</span>--}}
                    {{--                                        </a>--}}
                    {{--                                    </li>--}}
                    {{--                                   <li class="nav-active">--}}
                    {{--                                        <a href="#">--}}
                    {{--                                            <span><i class="fas fa-caret-right"></i>Slider</span>--}}
                    {{--                                        </a>--}}
                    {{--                                    </li>--}}
                    {{--                                    <li class="">--}}
                    {{--                                        <a href="#">--}}
                    {{--                                            <span><i class="fas fa-caret-right"></i>Features</span>--}}
                    {{--                                        </a>--}}
                    {{--                                    </li>--}}
                    {{--                                    <li class="">--}}
                    {{--                                        <a href="">--}}
                    {{--                                            <span><i class="fas fa-caret-right"></i>Testimonial</span>--}}
                    {{--                                        </a>--}}
                    {{--                                    </li>--}}
                    {{--                                   <li class="<?php if ($sub_page == 'frontend/services' || $sub_page == 'frontend/services_edit') echo 'nav-active'; ?>">--}}
                    {{--                                        <a href="<?php echo url('frontend/services'); ?>">--}}
                    {{--                                            <span><i class="fas fa-caret-right"></i><?php echo url('service'); ?></span>--}}
                    {{--                                        </a>--}}
                    {{--                                    </li>--}}
                    {{--                                <?php }--}}
                    {{--                                if (get_permission('frontend_faq', 'is_view')) { ?>--}}
                    {{--                                    <li class="<?php if ($sub_page == 'frontend/faq' || $sub_page == 'frontend/faq_edit') echo 'nav-active'; ?>">--}}
                    {{--                                        <a href="<?php echo url('frontend/faq'); ?>">--}}
                    {{--                                            <span><i class="fas fa-caret-right"></i><?php echo url('faq'); ?></span>--}}
                    {{--                                        </a>--}}
                    {{--                                    </li>--}}
                    {{--                                <?php } ?>--}}
                    {{--                            </ul>--}}
                    {{--                        </li>--}}
                    {{--                    <?php } ?>--}}

                    {{--                    <?php--}}
                    {{--                    if (get_permission('student', 'is_add') ||--}}
                    {{--                        get_permission('multiple_import', 'is_add') ||--}}
                    {{--                        get_permission('online_admission', 'is_view') ||--}}
                    {{--                        get_permission('student_category', 'is_view')) {--}}
                    {{--                        ?>--}}
                    {{--                        <!-- admission -->--}}
                    {{--                        <li class="nav-parent <?php if ($main_menu == 'admission') echo 'nav-expanded nav-active'; ?>">--}}
                    {{--                            <a>--}}
                    {{--                                <i class="far fa-edit"></i><span><?= url('admission') ?></span>--}}
                    {{--                            </a>--}}
                    {{--                            <ul class="nav nav-children">--}}
                    {{--                                <?php if (get_permission('student', 'is_add')) { ?>--}}
                    {{--                                    <li class="<?php if ($sub_page == 'student/add') echo 'nav-active'; ?>">--}}
                    {{--                                        <a href="<?= url('student/add') ?>">--}}
                    {{--                                            <span><i class="fas fa-caret-right"--}}
                    {{--                                                     aria-hidden="true"></i><?= url('create_admission') ?></span>--}}
                    {{--                                        </a>--}}
                    {{--                                    </li>--}}
                    {{--                                <?php }--}}
                    {{--                                if (get_permission('online_admission', 'is_view')) { ?>--}}
                    {{--                                    <li class="<?php if ($sub_page == 'online_admission/index') echo 'nav-active'; ?>">--}}
                    {{--                                        <a href="<?= url('online_admission/index') ?>">--}}
                    {{--                                            <span><i class="fas fa-caret-right"--}}
                    {{--                                                     aria-hidden="true"></i><?= url('online_admission') ?></span>--}}
                    {{--                                        </a>--}}
                    {{--                                    </li>--}}
                    {{--                                <?php }--}}
                    {{--                                if (get_permission('multiple_import', 'is_add')) { ?>--}}
                    {{--                                    <li class="<?php if ($sub_page == 'student/multi_add') echo 'nav-active'; ?>">--}}
                    {{--                                        <a href="<?= url('student/csv_import') ?>">--}}
                    {{--                                            <span><i class="fas fa-caret-right"--}}
                    {{--                                                     aria-hidden="true"></i><?= url('multiple_import') ?></span>--}}
                    {{--                                        </a>--}}
                    {{--                                    </li>--}}
                    {{--                                <?php }--}}
                    {{--                                if (get_permission('student_category', 'is_view')) { ?>--}}
                    {{--                                    <li class="<?php if ($sub_page == 'student/category') echo 'nav-active'; ?>">--}}
                    {{--                                        <a href="<?= url('student/category') ?>">--}}
                    {{--                                            <span><i class="fas fa-caret-right"--}}
                    {{--                                                     aria-hidden="true"></i><?= url('category') ?></span>--}}
                    {{--                                        </a>--}}
                    {{--                                    </li>--}}
                    {{--                                <?php } ?>--}}
                    {{--                            </ul>--}}
                    {{--                        </li>--}}
                    {{--                    <?php } ?>--}}

                    {{--                    <?php--}}
                    {{--                    if (get_permission('student', 'is_view') ||--}}
                    {{--                        get_permission('student_disable_authentication', 'is_view')) {--}}
                    {{--                        ?>--}}
                    {{--                        <!-- student details -->--}}
                    {{--                        <li class="nav-parent <?php if ($main_menu == 'student') echo 'nav-expanded nav-active'; ?>">--}}
                    {{--                            <a>--}}
                    {{--                                <i class="icon-graduation icons"></i><span><?= url('student_details') ?></span>--}}
                    {{--                            </a>--}}
                    {{--                            <ul class="nav nav-children">--}}
                    {{--                                <?php if (get_permission('student', 'is_view')) { ?>--}}
                    {{--                                    <li class="<?php if ($sub_page == 'student/view' || $sub_page == 'student/profile') echo 'nav-active'; ?>">--}}
                    {{--                                        <a href="<?= url('student/view') ?>">--}}
                    {{--                                            <span><i class="fas fa-caret-right"--}}
                    {{--                                                     aria-hidden="true"></i><?= url('student_list') ?></span>--}}
                    {{--                                        </a>--}}
                    {{--                                    </li>--}}
                    {{--                                <?php }--}}
                    {{--                                if (get_permission('student_disable_authentication', 'is_view')) { ?>--}}
                    {{--                                    <li class="<?php if ($sub_page == 'student/disable_authentication') echo 'nav-active'; ?>">--}}
                    {{--                                        <a href="<?= url('student/disable_authentication') ?>">--}}
                    {{--                                            <span><i class="fas fa-caret-right"--}}
                    {{--                                                     aria-hidden="true"></i><?= url('login_deactivate') ?></span>--}}
                    {{--                                        </a>--}}
                    {{--                                    </li>--}}
                    {{--                                <?php } ?>--}}
                    {{--                            </ul>--}}
                    {{--                        </li>--}}
                    {{--                    <?php } ?>--}}
                    {{--                    <?php--}}
                    {{--                    if (get_permission('parent', 'is_view') ||--}}
                    {{--                        get_permission('parent', 'is_add') ||--}}
                    {{--                        get_permission('parent_disable_authentication', 'is_view')) {--}}
                    {{--                        ?>--}}
                    {{--                        <!-- parents -->--}}
                    {{--                        <li class="nav-parent <?php if ($main_menu == 'parents') echo 'nav-expanded nav-active'; ?>">--}}
                    {{--                            <a>--}}
                    {{--                                <i class="icons icon-user-follow"></i><span><?= url('parents') ?></span>--}}
                    {{--                            </a>--}}
                    {{--                            <ul class="nav nav-children">--}}
                    {{--                                <?php if (get_permission('parent', 'is_view')) { ?>--}}
                    {{--                                    <li class="<?php if ($sub_page == 'parents/view' || $sub_page == 'parents/profile') echo 'nav-active'; ?>">--}}
                    {{--                                        <a href="<?= url('parents/view') ?>">--}}
                    {{--                                            <span><i class="fas fa-caret-right"></i><?= url('parents_list') ?></span>--}}
                    {{--                                        </a>--}}
                    {{--                                    </li>--}}
                    {{--                                <?php }--}}
                    {{--                                if (get_permission('parent', 'is_add')) { ?>--}}
                    {{--                                    <li class="<?php if ($sub_page == 'parents/add') echo 'nav-active'; ?>">--}}
                    {{--                                        <a href="<?= url('parents/add') ?>">--}}
                    {{--                                            <span><i class="fas fa-caret-right"></i><?= url('add_parent') ?></span>--}}
                    {{--                                        </a>--}}
                    {{--                                    </li>--}}
                    {{--                                <?php }--}}
                    {{--                                if (get_permission('parent_disable_authentication', 'is_view')) { ?>--}}
                    {{--                                    <li class="<?php if ($sub_page == 'parents/disable_authentication') echo 'nav-active'; ?>">--}}
                    {{--                                        <a href="<?= url('parents/disable_authentication') ?>">--}}
                    {{--                                            <span><i class="fas fa-caret-right"--}}
                    {{--                                                     aria-hidden="true"></i><?= url('login_deactivate') ?></span>--}}
                    {{--                                        </a>--}}
                    {{--                                    </li>--}}
                    {{--                                <?php } ?>--}}
                    {{--                            </ul>--}}
                    {{--                        </li>--}}
                    {{--                    <?php } ?>--}}
                    {{--                    <?php--}}
                    {{--                    if (get_permission('employee', 'is_view') ||--}}
                    {{--                        get_permission('employee', 'is_add') ||--}}
                    {{--                        get_permission('designation', 'is_view') ||--}}
                    {{--                        get_permission('designation', 'is_add') ||--}}
                    {{--                        get_permission('department', 'is_view') ||--}}
                    {{--                        get_permission('employee_disable_authentication', 'is_view')) {--}}
                    {{--                        ?>--}}
                    {{--                        <!-- Employees -->--}}
                    {{--                        <li class="nav-parent <?php if ($main_menu == 'employee') echo 'nav-expanded nav-active'; ?>">--}}
                    {{--                            <a><i class="fas fa-users"></i><span><?php echo url('employee'); ?></span></a>--}}
                    {{--                            <ul class="nav nav-children">--}}
                    {{--                                <?php if (get_permission('employee', 'is_view')) { ?>--}}
                    {{--                                    <li class="<?php if ($sub_page == 'employee/view' || $sub_page == 'employee/profile') echo 'nav-active'; ?>">--}}
                    {{--                                        <a href="<?php echo url('employee/view'); ?>">--}}
                    {{--                                            <span><i class="fas fa-caret-right"--}}
                    {{--                                                     aria-hidden="true"></i><?php echo url('employee_list'); ?></span>--}}
                    {{--                                        </a>--}}
                    {{--                                    </li>--}}
                    {{--                                <?php }--}}
                    {{--                                if (get_permission('department', 'is_view') || get_permission('department', 'is_add')) { ?>--}}
                    {{--                                    <li class="<?php if ($sub_page == 'employee/department') echo 'nav-active'; ?>">--}}
                    {{--                                        <a href="<?php echo url('employee/department'); ?>">--}}
                    {{--                                            <span><i class="fas fa-caret-right"--}}
                    {{--                                                     aria-hidden="true"></i><?php echo url('add_department'); ?></span>--}}
                    {{--                                        </a>--}}
                    {{--                                    </li>--}}
                    {{--                                <?php }--}}
                    {{--                                if (get_permission('designation', 'is_view') || get_permission('designation', 'is_add')) { ?>--}}
                    {{--                                    <li class="<?php if ($sub_page == 'employee/designation') echo 'nav-active'; ?>">--}}
                    {{--                                        <a href="<?php echo url('employee/designation'); ?>">--}}
                    {{--                                            <span><i class="fas fa-caret-right"--}}
                    {{--                                                     aria-hidden="true"></i><?php echo url('add_designation'); ?></span>--}}
                    {{--                                        </a>--}}
                    {{--                                    </li>--}}
                    {{--                                <?php }--}}
                    {{--                                if (get_permission('employee', 'is_add')) { ?>--}}
                    {{--                                    <li class="<?php if ($sub_page == 'employee/add') echo 'nav-active'; ?>">--}}
                    {{--                                        <a href="<?php echo url('employee/add'); ?>">--}}
                    {{--                                            <span><i class="fas fa-caret-right"--}}
                    {{--                                                     aria-hidden="true"></i><?php echo url('add_employee'); ?></span>--}}
                    {{--                                        </a>--}}
                    {{--                                    </li>--}}
                    {{--                                <?php }--}}
                    {{--                                if (get_permission('employee_disable_authentication', 'is_view')) { ?>--}}
                    {{--                                    <li class="<?php if ($sub_page == 'employee/disable_authentication') echo 'nav-active'; ?>">--}}
                    {{--                                        <a href="<?php echo url('employee/disable_authentication'); ?>">--}}
                    {{--                                            <span><i class="fas fa-caret-right"--}}
                    {{--                                                     aria-hidden="true"></i><?php echo url('login_deactivate'); ?></span>--}}
                    {{--                                        </a>--}}
                    {{--                                    </li>--}}
                    {{--                                <?php } ?>--}}
                    {{--                            </ul>--}}
                    {{--                        </li>--}}
                    {{--                    <?php } ?>--}}

                    {{--                    <?php--}}
                    {{--                    if (get_permission('id_card_templete', 'is_view') ||--}}
                    {{--                        get_permission('generate_student_idcard', 'is_view') ||--}}
                    {{--                        get_permission('admit_card_templete', 'is_view') ||--}}
                    {{--                        get_permission('generate_admit_card', 'is_view') ||--}}
                    {{--                        get_permission('generate_employee_idcard', 'is_view')) {--}}
                    {{--                        ?>--}}
                    {{--                        <li class="nav-parent <?php if ($main_menu == 'card_manage') echo 'nav-expanded nav-active'; ?>">--}}
                    {{--                            <a>--}}
                    {{--                                <i class="far fa-clipboard"></i><span><?= url('card_management') ?></span>--}}
                    {{--                            </a>--}}
                    {{--                            <ul class="nav nav-children">--}}
                    {{--                                <?php if (get_permission('id_card_templete', 'is_view')) { ?>--}}
                    {{--                                    <li class="<?php if ($sub_page == 'card_manage/id_card_templete' || $sub_page == 'card_manage/id_card_templete_edit') echo 'nav-active'; ?>">--}}
                    {{--                                        <a href="<?php echo url('card_manage/id_card_templete'); ?>">--}}
                    {{--                                            <span><i class="fas fa-caret-right"--}}
                    {{--                                                     aria-hidden="true"></i><?php echo url('id_card') . " " . url('templete'); ?></span>--}}
                    {{--                                        </a>--}}
                    {{--                                    </li>--}}
                    {{--                                <?php }--}}
                    {{--                                if (get_permission('generate_student_idcard', 'is_view')) { ?>--}}
                    {{--                                    <li class="<?php if ($sub_page == 'card_manage/generate_student_idcard') echo 'nav-active'; ?>">--}}
                    {{--                                        <a href="<?php echo url('card_manage/generate_student_idcard'); ?>">--}}
                    {{--                                            <span><i class="fas fa-caret-right"--}}
                    {{--                                                     aria-hidden="true"></i><?php echo url('student') . " " . url('id_card'); ?></span>--}}
                    {{--                                        </a>--}}
                    {{--                                    </li>--}}
                    {{--                                <?php }--}}
                    {{--                                if (get_permission('generate_employee_idcard', 'is_view')) { ?>--}}
                    {{--                                    <li class="<?php if ($sub_page == 'card_manage/generate_employee_idcard') echo 'nav-active'; ?>">--}}
                    {{--                                        <a href="<?php echo url('card_manage/generate_employee_idcard'); ?>">--}}
                    {{--                                            <span><i class="fas fa-caret-right"--}}
                    {{--                                                     aria-hidden="true"></i><?php echo url('employee') . " " . url('id_card'); ?></span>--}}
                    {{--                                        </a>--}}
                    {{--                                    </li>--}}
                    {{--                                <?php }--}}
                    {{--                                if (get_permission('admit_card_templete', 'is_view')) { ?>--}}
                    {{--                                    <li class="<?php if ($sub_page == 'card_manage/admit_card_templete' || $sub_page == 'card_manage/admit_card_templete_edit') echo 'nav-active'; ?>">--}}
                    {{--                                        <a href="<?php echo url('card_manage/admit_card_templete'); ?>">--}}
                    {{--                                            <span><i class="fas fa-caret-right"--}}
                    {{--                                                     aria-hidden="true"></i><?php echo url('admit_card') . " " . url('templete'); ?></span>--}}
                    {{--                                        </a>--}}
                    {{--                                    </li>--}}
                    {{--                                <?php }--}}
                    {{--                                if (get_permission('generate_admit_card', 'is_view')) { ?>--}}
                    {{--                                    <li class="<?php if ($sub_page == 'card_manage/generate_student_admitcard') echo 'nav-active'; ?>">--}}
                    {{--                                        <a href="<?php echo url('card_manage/generate_student_admitcard'); ?>">--}}
                    {{--                                            <span><i class="fas fa-caret-right"--}}
                    {{--                                                     aria-hidden="true"></i><?php echo url('generate') . " " . url('admit_card'); ?></span>--}}
                    {{--                                        </a>--}}
                    {{--                                    </li>--}}
                    {{--                                <?php } ?>--}}
                    {{--                            </ul>--}}
                    {{--                        </li>--}}
                    {{--                    <?php } ?>--}}

                    {{--                    <?php--}}
                    {{--                    if (get_permission('certificate_templete', 'is_view') ||--}}
                    {{--                        get_permission('generate_student_certificate', 'is_view') ||--}}
                    {{--                        get_permission('generate_employee_certificate', 'is_view')) {--}}
                    {{--                        ?>--}}
                    {{--                        <li class="nav-parent <?php if ($main_menu == 'certificate') echo 'nav-expanded nav-active'; ?>">--}}
                    {{--                            <a>--}}
                    {{--                                <i class="icons icon-social-spotify"></i><span><?= url('certificate') ?></span>--}}
                    {{--                            </a>--}}
                    {{--                            <ul class="nav nav-children">--}}
                    {{--                                <?php if (get_permission('certificate_templete', 'is_view')) { ?>--}}
                    {{--                                    <li class="<?php if ($sub_page == 'certificate/index' || $sub_page == 'certificate/edit') echo 'nav-active'; ?>">--}}
                    {{--                                        <a href="<?php echo url('certificate'); ?>">--}}
                    {{--                                            <span><i class="fas fa-caret-right"--}}
                    {{--                                                     aria-hidden="true"></i><?php echo url('certificate') . " " . url('templete'); ?></span>--}}
                    {{--                                        </a>--}}
                    {{--                                    </li>--}}
                    {{--                                <?php }--}}
                    {{--                                if (get_permission('generate_student_certificate', 'is_view')) { ?>--}}
                    {{--                                    <li class="<?php if ($sub_page == 'certificate/generate_student') echo 'nav-active'; ?>">--}}
                    {{--                                        <a href="<?php echo url('certificate/generate_student'); ?>">--}}
                    {{--                                            <span><i class="fas fa-caret-right"--}}
                    {{--                                                     aria-hidden="true"></i><?php echo url('generate') . " " . url('student'); ?></span>--}}
                    {{--                                        </a>--}}
                    {{--                                    </li>--}}
                    {{--                                <?php }--}}
                    {{--                                if (get_permission('generate_employee_certificate', 'is_view')) { ?>--}}
                    {{--                                    <li class="<?php if ($sub_page == 'certificate/generate_employee') echo 'nav-active'; ?>">--}}
                    {{--                                        <a href="<?php echo url('certificate/generate_employee'); ?>">--}}
                    {{--                                            <span><i class="fas fa-caret-right"--}}
                    {{--                                                     aria-hidden="true"></i><?php echo url('generate') . " " . url('employee'); ?></span>--}}
                    {{--                                        </a>--}}
                    {{--                                    </li>--}}
                    {{--                                <?php } ?>--}}
                    {{--                            </ul>--}}
                    {{--                        </li>--}}
                    {{--                    <?php } ?>--}}



                    {{--                    <?php--}}
                    {{--                    if (get_permission('salary_template', 'is_view') ||--}}
                    {{--                        get_permission('salary_assign', 'is_view') ||--}}
                    {{--                        get_permission('salary_payment', 'is_view') ||--}}
                    {{--                        get_permission('advance_salary_manage', 'is_view') ||--}}
                    {{--                        get_permission('advance_salary_request', 'is_view') ||--}}
                    {{--                        get_permission('leave_category', 'is_view') ||--}}
                    {{--                        get_permission('leave_category', 'is_add') ||--}}
                    {{--                        get_permission('leave_request', 'is_view') ||--}}
                    {{--                        get_permission('leave_manage', 'is_view') ||--}}
                    {{--                        get_permission('award', 'is_view')) {--}}
                    {{--                        ?>--}}
                    {{--                        <!-- human resource -->--}}
                    {{--                        <li class="nav-parent <?php if ($main_menu == 'payroll' || $main_menu == 'advance_salary' || $main_menu == 'leave' || $main_menu == 'award') echo 'nav-expanded nav-active'; ?>">--}}
                    {{--                            <a>--}}
                    {{--                                <i class="icons icon-loop"></i><span><?= url('hrm') ?></span>--}}
                    {{--                            </a>--}}
                    {{--                            <ul class="nav nav-children">--}}
                    {{--                                <?php--}}
                    {{--                                if (get_permission('salary_template', 'is_view') ||--}}
                    {{--                                    get_permission('salary_assign', 'is_view') ||--}}
                    {{--                                    get_permission('salary_payment', 'is_view')) {--}}
                    {{--                                    ?>--}}
                    {{--                                    <!-- payroll -->--}}
                    {{--                                    <li class="nav-parent <?php if ($main_menu == 'payroll') echo 'nav-expanded nav-active'; ?>">--}}
                    {{--                                        <a>--}}
                    {{--                                            <i class="far fa-address-card" aria-hidden="true"></i>--}}
                    {{--                                            <span><?= url('payroll') ?></span>--}}
                    {{--                                        </a>--}}
                    {{--                                        <ul class="nav nav-children">--}}
                    {{--                                            <?php if (get_permission('salary_template', 'is_view')) { ?>--}}
                    {{--                                                <li class="<?php if ($sub_page == 'payroll/salary_templete' || $sub_page == 'payroll/salary_templete_edit') echo 'nav-active'; ?>">--}}
                    {{--                                                    <a href="<?= url('payroll/salary_template') ?>">--}}
                    {{--                                                        <span><?= url('salary_template') ?></span>--}}
                    {{--                                                    </a>--}}
                    {{--                                                </li>--}}
                    {{--                                            <?php }--}}
                    {{--                                            if (get_permission('salary_assign', 'is_view')) { ?>--}}
                    {{--                                                <li class="<?php if ($sub_page == 'payroll/salary_assign') echo 'nav-active'; ?>">--}}
                    {{--                                                    <a href="<?= url('payroll/salary_assign') ?>">--}}
                    {{--                                                        <span><?= url('salary_assign') ?></span>--}}
                    {{--                                                    </a>--}}
                    {{--                                                </li>--}}
                    {{--                                            <?php }--}}
                    {{--                                            if (get_permission('salary_payment', 'is_view')) { ?>--}}
                    {{--                                                <li class="<?php if ($sub_page == 'payroll/salary_payment' || $sub_page == 'payroll/create' || $sub_page == 'payroll/invoice') echo 'nav-active'; ?>">--}}
                    {{--                                                    <a href="<?= url('payroll') ?>">--}}
                    {{--                                                        <span><?= url('salary_payment') ?></span>--}}
                    {{--                                                    </a>--}}
                    {{--                                                </li>--}}
                    {{--                                            <?php } ?>--}}
                    {{--                                        </ul>--}}
                    {{--                                    </li>--}}
                    {{--                                <?php } ?>--}}
                    {{--                                <?php--}}
                    {{--                                if (get_permission('advance_salary_manage', 'is_view') ||--}}
                    {{--                                    get_permission('advance_salary_request', 'is_view')) {--}}
                    {{--                                    ?>--}}
                    {{--                                    <!-- advance salary managements -->--}}
                    {{--                                    <li class="nav-parent <?php--}}
                    {{--                                    if ($main_menu == 'advance_salary') echo 'nav-expanded nav-active'; ?>">--}}
                    {{--                                        <a>--}}
                    {{--                                            <i class="fas fa-funnel-dollar" aria-hidden="true"></i>--}}
                    {{--                                            <span><?= url('advance_salary') ?></span>--}}
                    {{--                                        </a>--}}
                    {{--                                        <ul class="nav nav-children">--}}
                    {{--                                            <?php if (get_permission('advance_salary_request', 'is_view')) { ?>--}}
                    {{--                                                <li class="<?php if ($sub_page == 'advance_salary/request') echo 'nav-active'; ?>">--}}
                    {{--                                                    <a href="<?= url('advance_salary/request') ?>">--}}
                    {{--                                                        <span><?= url('my_application') ?></span>--}}
                    {{--                                                    </a>--}}
                    {{--                                                </li>--}}
                    {{--                                            <?php }--}}
                    {{--                                            if (get_permission('advance_salary_manage', 'is_view')) { ?>--}}
                    {{--                                                <li class="<?php if ($sub_page == 'advance_salary/index') echo 'nav-active'; ?>">--}}
                    {{--                                                    <a href="<?= url('advance_salary') ?>">--}}
                    {{--                                                        <span><?= url('manage_application') ?></span>--}}
                    {{--                                                    </a>--}}
                    {{--                                                </li>--}}
                    {{--                                            <?php } ?>--}}
                    {{--                                        </ul>--}}
                    {{--                                    </li>--}}
                    {{--                                <?php } ?>--}}
                    {{--                                <?php--}}
                    {{--                                if (get_permission('leave_category', 'is_view') ||--}}
                    {{--                                    get_permission('leave_manage', 'is_view') ||--}}
                    {{--                                    get_permission('leave_request', 'is_view')) {--}}
                    {{--                                    ?>--}}
                    {{--                                    <!-- leave managements -->--}}
                    {{--                                    <li class="nav-parent <?php--}}
                    {{--                                    if ($main_menu == 'leave') echo 'nav-expanded nav-active'; ?>">--}}
                    {{--                                        <a>--}}
                    {{--                                            <i class="fas fa-umbrella-beach" aria-hidden="true"></i>--}}
                    {{--                                            <span><?= url('leave') ?></span>--}}
                    {{--                                        </a>--}}
                    {{--                                        <ul class="nav nav-children">--}}
                    {{--                                            <?php if (get_permission('leave_category', 'is_view')) { ?>--}}
                    {{--                                                <li class="<?php if ($sub_page == 'leave/category') echo 'nav-active'; ?>">--}}
                    {{--                                                    <a href="<?= url('leave/category') ?>">--}}
                    {{--                                                        <span><?= url('category') ?></span>--}}
                    {{--                                                    </a>--}}
                    {{--                                                </li>--}}
                    {{--                                            <?php }--}}
                    {{--                                            if (get_permission('leave_request', 'is_view')) { ?>--}}
                    {{--                                                <li class="<?php if ($sub_page == 'leave/request') echo 'nav-active'; ?>">--}}
                    {{--                                                    <a href="<?= url('leave/request') ?>">--}}
                    {{--                                                        <span><?= url('my_application') ?></span>--}}
                    {{--                                                    </a>--}}
                    {{--                                                </li>--}}
                    {{--                                            <?php }--}}
                    {{--                                            if (get_permission('leave_manage', 'is_view')) { ?>--}}
                    {{--                                                <li class="<?php if ($sub_page == 'leave/index') echo 'nav-active'; ?>">--}}
                    {{--                                                    <a href="<?= url('leave') ?>">--}}
                    {{--                                                        <span><?= url('manage_application') ?></span>--}}
                    {{--                                                    </a>--}}
                    {{--                                                </li>--}}
                    {{--                                            <?php } ?>--}}
                    {{--                                        </ul>--}}
                    {{--                                    </li>--}}
                    {{--                                <?php } ?>--}}
                    {{--                                <?php if (get_permission('award', 'is_view')) { ?>--}}
                    {{--                                    <li class="<?php if ($sub_page == 'award/index' || $sub_page == 'award/edit') echo 'nav-active'; ?>">--}}
                    {{--                                        <a href="<?= url('award') ?>">--}}
                    {{--                                            <i class="fas fa-crown"></i>--}}
                    {{--                                            <span><?= url('award') ?></span>--}}
                    {{--                                        </a>--}}
                    {{--                                    </li>--}}
                    {{--                                <?php } ?>--}}
                    {{--                            </ul>--}}
                    {{--                        </li>--}}
                    {{--                    <?php } ?>--}}
                    {{--                    <?php--}}
                    {{--                    if (get_permission('classes', 'is_view') ||--}}
                    {{--                        get_permission('section', 'is_view') ||--}}
                    {{--                        get_permission('assign_class_teacher', 'is_view') ||--}}
                    {{--                        get_permission('subject', 'is_view') ||--}}
                    {{--                        get_permission('subject_class_assign', 'is_view') ||--}}
                    {{--                        get_permission('subject_teacher_assign', 'is_view') ||--}}
                    {{--                        get_permission('class_timetable', 'is_view')) {--}}
                    {{--                        ?>--}}
                    {{--                        <!-- academic -->--}}
                    {{--                        <li class="nav-parent <?php if ($main_menu == 'classes' ||--}}
                    {{--                            $main_menu == 'sections' ||--}}
                    {{--                            $main_menu == 'timetable' ||--}}
                    {{--                            $main_menu == 'subject' ||--}}
                    {{--                            $main_menu == 'transfer') echo 'nav-expanded nav-active'; ?>">--}}
                    {{--                            <a>--}}
                    {{--                                <i class="icons icon-home"--}}
                    {{--                                   aria-hidden="true"></i><span><?= url('academic') ?></span>--}}
                    {{--                            </a>--}}

                    {{--                            <ul class="nav nav-children">--}}
                    {{--                                <?php--}}
                    {{--                                if (get_permission('classes', 'is_view') ||--}}
                    {{--                                    get_permission('section', 'is_view') ||--}}
                    {{--                                    get_permission('assign_class_teacher', 'is_view')) {--}}
                    {{--                                    ?>--}}
                    {{--                                    <!-- class -->--}}
                    {{--                                    <li class="nav-parent <?php--}}
                    {{--                                    if ($main_menu == 'classes' || $main_menu == 'sections' || $main_menu == 'class_teacher_allocation') echo 'nav-expanded nav-active'; ?>">--}}
                    {{--                                        <a>--}}
                    {{--                                            <i class="fas fa-tasks" aria-hidden="true"></i>--}}
                    {{--                                            <span><?= url('class') . " & " . url('section') ?></span>--}}
                    {{--                                        </a>--}}
                    {{--                                        <ul class="nav nav-children">--}}
                    {{--                                            <?php if (get_permission('classes', 'is_view') || get_permission('section', 'is_view')) { ?>--}}
                    {{--                                                <li class="<?php if ($sub_page == 'classes/index' ||--}}
                    {{--                                                    $sub_page == 'classes/edit' ||--}}
                    {{--                                                    $sub_page == 'sections/index' ||--}}
                    {{--                                                    $sub_page == 'sections/edit') echo 'nav-active'; ?>">--}}
                    {{--                                                    <a href="<?= get_permission('classes', 'is_view') ? url('classes') : url('sections'); ?>">--}}
                    {{--                                                        <span><?= url('control_classes') ?></span>--}}
                    {{--                                                    </a>--}}
                    {{--                                                </li>--}}
                    {{--                                            <?php } ?>--}}
                    {{--                                            <?php if (get_permission('assign_class_teacher', 'is_view')) { ?>--}}
                    {{--                                                <li class="<?php if ($sub_page == 'classes/teacher_allocation') echo 'nav-active'; ?>">--}}
                    {{--                                                    <a href="<?= url('classes/teacher_allocation') ?>">--}}
                    {{--                                                        <span><?= url('assign_class_teacher') ?></span>--}}
                    {{--                                                    </a>--}}
                    {{--                                                </li>--}}
                    {{--                                            <?php } ?>--}}
                    {{--                                        </ul>--}}
                    {{--                                    </li>--}}
                    {{--                                <?php } ?>--}}
                    {{--                                <?php--}}
                    {{--                                if (get_permission('subject', 'is_view') ||--}}
                    {{--                                    get_permission('subject_class_assign', 'is_view') ||--}}
                    {{--                                    get_permission('subject_teacher_assign', 'is_view')) {--}}
                    {{--                                    ?>--}}
                    {{--                                    <!-- subject -->--}}
                    {{--                                    <li class="nav-parent <?php if ($main_menu == 'subject') echo 'nav-expanded'; ?>">--}}
                    {{--                                        <a>--}}
                    {{--                                            <i class="fas fa-book-reader"></i><?= url('subject') ?>--}}
                    {{--                                        </a>--}}
                    {{--                                        <ul class="nav nav-children">--}}
                    {{--                                            <?php if (get_permission('subject', 'is_view')) { ?>--}}
                    {{--                                                <li class="<?php if ($sub_page == 'subject/index' || $sub_page == 'subject/edit') echo 'nav-active'; ?>">--}}
                    {{--                                                    <a href="<?= url('subject/index') ?>">--}}
                    {{--                                                        <span><?= url('subject') ?></span>--}}
                    {{--                                                    </a>--}}
                    {{--                                                </li>--}}
                    {{--                                            <?php }--}}
                    {{--                                            if (get_permission('subject_class_assign', 'is_view')) { ?>--}}
                    {{--                                                <li class="<?php if ($sub_page == 'subject/class_assign') echo 'nav-active'; ?>">--}}
                    {{--                                                    <a href="<?= url('subject/class_assign') ?>">--}}
                    {{--                                                        <span><?= url('class_assign') ?></span>--}}
                    {{--                                                    </a>--}}
                    {{--                                                </li>--}}
                    {{--                                            <?php }--}}
                    {{--                                            if (get_permission('subject_teacher_assign', 'is_view')) { ?>--}}
                    {{--                                                <li class="<?php if ($sub_page == 'subject/teacher_assign') echo 'nav-active'; ?>">--}}
                    {{--                                                    <a href="<?= url('subject/teacher_assign') ?>">--}}
                    {{--                                                        <span><?= url('teacher') . ' ' . url('assign') ?></span>--}}
                    {{--                                                    </a>--}}
                    {{--                                                </li>--}}
                    {{--                                            <?php } ?>--}}
                    {{--                                        </ul>--}}
                    {{--                                    </li>--}}
                    {{--                                <?php } ?>--}}
                    {{--                                <?php if (get_permission('class_timetable', 'is_view')) { ?>--}}
                    {{--                                    <li class="<?php if ($sub_page == 'timetable/viewclass' || $sub_page == 'timetable/update_classwise' || $sub_page == 'timetable/set_classwise') echo 'nav-active'; ?>">--}}
                    {{--                                        <a href="<?= url('timetable/viewclass') ?>">--}}
                    {{--                                            <span><i class="fas fa-dna"--}}
                    {{--                                                     aria-hidden="true"></i><?= url('class') . " " . url('schedule') ?></span>--}}
                    {{--                                        </a>--}}
                    {{--                                    </li>--}}
                    {{--                                <?php } ?>--}}
                    {{--                                <?php if (get_permission('student_promotion', 'is_view')) { ?>--}}
                    {{--                                    <!-- student promotion -->--}}
                    {{--                                    <li class="<?php if ($sub_page == 'student/transfer') echo 'nav-active'; ?>">--}}
                    {{--                                        <a href="<?= url('student/transfer') ?>">--}}
                    {{--                                            <span><i class="fab fa-deviantart"--}}
                    {{--                                                     aria-hidden="true"></i><?= url('promotion') ?></span>--}}
                    {{--                                        </a>--}}
                    {{--                                    </li>--}}
                    {{--                                <?php } ?>--}}
                    {{--                            </ul>--}}
                    {{--                        </li>--}}
                    {{--                    <?php } ?>--}}
                    {{--                    <?php if (get_permission('live_class', 'is_view')) { ?>--}}
                    {{--                        <li class="nav-parent <?php if ($main_menu == 'live_class') echo 'nav-expanded nav-active'; ?>">--}}
                    {{--                            <a>--}}
                    {{--                                <i class="icons icon-earphones-alt"></i><span><?= url('live_class_rooms') ?></span>--}}
                    {{--                            </a>--}}
                    {{--                            <ul class="nav nav-children">--}}
                    {{--                                <li class="<?php if ($sub_page == 'live_class/index') echo 'nav-active'; ?>">--}}
                    {{--                                    <a href="<?= url('live_class') ?>">--}}
                    {{--                                        <span><i class="fas fa-caret-right"--}}
                    {{--                                                 aria-hidden="true"></i> <?= url('live_class_rooms') ?></span>--}}
                    {{--                                    </a>--}}
                    {{--                                </li>--}}
                    {{--                                <li class="<?php if ($sub_page == 'live_class/reports') echo 'nav-active'; ?>">--}}
                    {{--                                    <a href="<?= url('live_class/reports') ?>">--}}
                    {{--                                        <span><i class="fas fa-caret-right"--}}
                    {{--                                                 aria-hidden="true"></i> <?= url(' live_class_reports') ?></span>--}}
                    {{--                                    </a>--}}
                    {{--                                </li>--}}

                    {{--                            </ul>--}}
                    {{--                        </li>--}}
                    {{--                    <?php } ?>--}}
                    {{--                    <?php--}}
                    {{--                    if (get_permission('attachments', 'is_view') ||--}}
                    {{--                        get_permission('attachment_type', 'is_view')) {--}}
                    {{--                        ?>--}}
                    {{--                        <!-- attachments upload -->--}}
                    {{--                        <li class="nav-parent <?php if ($main_menu == 'attachments') echo 'nav-expanded nav-active'; ?>">--}}
                    {{--                            <a>--}}
                    {{--                                <i class="icons icon-cloud-upload"></i><span><?= url('attachments_book') ?></span>--}}
                    {{--                            </a>--}}
                    {{--                            <ul class="nav nav-children">--}}
                    {{--                                <?php if (get_permission('attachments', 'is_view')) { ?>--}}
                    {{--                                    <li class="<?php if ($sub_page == 'attachments/index') echo 'nav-active'; ?>">--}}
                    {{--                                        <a href="<?= url('attachments') ?>">--}}
                    {{--                                            <span><i class="fas fa-caret-right"--}}
                    {{--                                                     aria-hidden="true"></i><?= url('upload_content') ?></span>--}}
                    {{--                                        </a>--}}
                    {{--                                    </li>--}}
                    {{--                                <?php }--}}
                    {{--                                if (get_permission('attachment_type', 'is_view')) { ?>--}}
                    {{--                                    <li class="<?php if ($sub_page == 'attachments/type') echo 'nav-active'; ?>">--}}
                    {{--                                        <a href="<?= url('attachments/type') ?>">--}}
                    {{--                                            <span><i class="fas fa-caret-right"--}}
                    {{--                                                     aria-hidden="true"></i><?= url('attachment_type') ?></span>--}}
                    {{--                                        </a>--}}
                    {{--                                    </li>--}}
                    {{--                                <?php } ?>--}}
                    {{--                            </ul>--}}
                    {{--                        </li>--}}
                    {{--                    <?php } ?>--}}
                    {{--                    <?php--}}
                    {{--                    if (get_permission('homework', 'is_view') ||--}}
                    {{--                        get_permission('evaluation_report', 'is_view')) {--}}
                    {{--                        ?>--}}
                    {{--                        <!-- attachments upload -->--}}
                    {{--                        <li class="nav-parent <?php if ($main_menu == 'homework') echo 'nav-expanded nav-active'; ?>">--}}
                    {{--                            <a>--}}
                    {{--                                <i class="icons icon-note"></i><span><?= url('homework') ?></span>--}}
                    {{--                            </a>--}}
                    {{--                            <ul class="nav nav-children">--}}
                    {{--                                <?php if (get_permission('homework', 'is_view')) { ?>--}}
                    {{--                                    <li class="<?php if ($sub_page == 'homework/index' || $sub_page == 'homework/add' || $sub_page == 'homework/evaluate_list' || $sub_page == 'homework/edit') echo 'nav-active'; ?>">--}}
                    {{--                                        <a href="<?= url('homework') ?>">--}}
                    {{--                                            <span><i class="fas fa-caret-right"--}}
                    {{--                                                     aria-hidden="true"></i><?= url('homework') ?></span>--}}
                    {{--                                        </a>--}}
                    {{--                                    </li>--}}
                    {{--                                <?php }--}}
                    {{--                                if (get_permission('evaluation_report', 'is_view')) { ?>--}}
                    {{--                                    <li class="<?php if ($sub_page == 'homework/report') echo 'nav-active'; ?>">--}}
                    {{--                                        <a href="<?= url('homework/report') ?>">--}}
                    {{--                                            <span><i class="fas fa-caret-right"--}}
                    {{--                                                     aria-hidden="true"></i><?= url('evaluation_report') ?></span>--}}
                    {{--                                        </a>--}}
                    {{--                                    </li>--}}
                    {{--                                <?php } ?>--}}
                    {{--                            </ul>--}}
                    {{--                        </li>--}}
                    {{--                    <?php } ?>--}}
                    {{--                    <?php--}}
                    {{--                    if (get_permission('exam', 'is_view') ||--}}
                    {{--                        get_permission('exam_term', 'is_view') ||--}}
                    {{--                        get_permission('mark_distribution', 'is_view') ||--}}
                    {{--                        get_permission('exam_hall', 'is_view') ||--}}
                    {{--                        get_permission('exam_timetable', 'is_view') ||--}}
                    {{--                        get_permission('exam_mark', 'is_view') ||--}}
                    {{--                        get_permission('exam_grade', 'is_view')) {--}}
                    {{--                        ?>--}}
                    {{--                        <!-- exam master -->--}}
                    {{--                        <li class="nav-parent <?php if ($main_menu == 'exam' || $main_menu == 'mark' || $main_menu == 'exam_timetable') echo 'nav-expanded nav-active'; ?>">--}}
                    {{--                            <a>--}}
                    {{--                                <i class="icons icon-book-open"--}}
                    {{--                                   aria-hidden="true"></i><span><?= url('exam_master') ?></span>--}}
                    {{--                            </a>--}}
                    {{--                            <ul class="nav nav-children">--}}
                    {{--                                <?php--}}
                    {{--                                if (get_permission('exam', 'is_view') ||--}}
                    {{--                                    get_permission('exam_term', 'is_view') ||--}}
                    {{--                                    get_permission('mark_distribution', 'is_view') ||--}}
                    {{--                                    get_permission('exam_hall', 'is_view')) {--}}
                    {{--                                    ?>--}}
                    {{--                                    <!-- exam -->--}}
                    {{--                                    <li class="nav-parent <?php if ($main_menu == 'exam' || $main_menu == 'exam_term' || $main_menu == 'exam_hall') echo 'nav-expanded nav-active'; ?>">--}}
                    {{--                                        <a>--}}
                    {{--                                            <i class="fas fa-flask"></i> <span><?= url('exam') ?></span>--}}
                    {{--                                        </a>--}}
                    {{--                                        <ul class="nav nav-children">--}}
                    {{--                                            <?php if (get_permission('exam_term', 'is_view')) { ?>--}}
                    {{--                                                <li class="<?php if ($sub_page == 'exam/term') echo 'nav-active'; ?>">--}}
                    {{--                                                    <a href="<?= url('exam/term') ?>">--}}
                    {{--                                                        <span><?= url('exam_term') ?></span>--}}
                    {{--                                                    </a>--}}
                    {{--                                                </li>--}}
                    {{--                                            <?php }--}}
                    {{--                                            if (get_permission('exam_hall', 'is_view')) { ?>--}}
                    {{--                                                <li class="<?php if ($sub_page == 'exam/hall') echo 'nav-active'; ?>">--}}
                    {{--                                                    <a href="<?= url('exam/hall') ?>">--}}
                    {{--                                                        <span><?= url('exam_hall') ?></span>--}}
                    {{--                                                    </a>--}}
                    {{--                                                </li>--}}
                    {{--                                            <?php }--}}
                    {{--                                            if (get_permission('mark_distribution', 'is_view')) { ?>--}}
                    {{--                                                <li class="<?php if ($sub_page == 'exam/mark_distribution') echo 'nav-active'; ?>">--}}
                    {{--                                                    <a href="<?= url('exam/mark_distribution') ?>">--}}
                    {{--                                                        <span><?= url('distribution') ?></span>--}}
                    {{--                                                    </a>--}}
                    {{--                                                </li>--}}
                    {{--                                            <?php }--}}
                    {{--                                            if (get_permission('exam', 'is_view')) { ?>--}}
                    {{--                                                <li class="<?php if ($sub_page == 'exam/index') echo 'nav-active'; ?>">--}}
                    {{--                                                    <a href="<?= url('exam') ?>">--}}
                    {{--                                                        <span><?= url('exam_setup') ?></span>--}}
                    {{--                                                    </a>--}}
                    {{--                                                </li>--}}
                    {{--                                            <?php } ?>--}}
                    {{--                                        </ul>--}}
                    {{--                                    </li>--}}
                    {{--                                <?php } ?>--}}
                    {{--                                <?php--}}
                    {{--                                if (get_permission('exam_timetable', 'is_view')) {--}}
                    {{--                                    ?>--}}
                    {{--                                    <!-- exam schedule -->--}}
                    {{--                                    <li class="nav-parent <?php if ($main_menu == 'exam_timetable') echo 'nav-expanded nav-active'; ?>">--}}
                    {{--                                        <a>--}}
                    {{--                                            <i class="fas fa-dna"></i>--}}
                    {{--                                            <span><?= url('exam') . " " . url('schedule') ?></span>--}}
                    {{--                                        </a>--}}
                    {{--                                        <ul class="nav nav-children">--}}
                    {{--                                            <?php if (get_permission('exam_timetable', 'is_view')) { ?>--}}
                    {{--                                                <li class="<?php if ($sub_page == 'timetable/viewexam') echo 'nav-active'; ?>">--}}
                    {{--                                                    <a href="<?= url('timetable/viewexam') ?>">--}}
                    {{--                                                        <span><?= url('schedule') ?></span>--}}
                    {{--                                                    </a>--}}
                    {{--                                                </li>--}}
                    {{--                                            <?php }--}}
                    {{--                                            if (get_permission('exam_timetable', 'is_view')) { ?>--}}
                    {{--                                                <li class="<?php if ($sub_page == 'timetable/set_examwise') echo 'nav-active'; ?>">--}}
                    {{--                                                    <a href="<?= url('timetable/set_examwise') ?>">--}}
                    {{--                                                        <span><?= url('add') . " " . url('schedule') ?></span>--}}
                    {{--                                                    </a>--}}
                    {{--                                                </li>--}}
                    {{--                                            <?php } ?>--}}
                    {{--                                        </ul>--}}
                    {{--                                    </li>--}}
                    {{--                                <?php } ?>--}}
                    {{--                                <?php--}}
                    {{--                                if (get_permission('exam_mark', 'is_view') ||--}}
                    {{--                                    get_permission('exam_grade', 'is_view')) {--}}
                    {{--                                    ?>--}}
                    {{--                                    <!-- marks -->--}}
                    {{--                                    <li class="nav-parent <?php if ($main_menu == 'mark') echo 'nav-expanded nav-active'; ?>">--}}
                    {{--                                        <a>--}}
                    {{--                                            <i class="fas fa-marker"></i><span><?= url('marks') ?></span>--}}
                    {{--                                        </a>--}}
                    {{--                                        <ul class="nav nav-children">--}}
                    {{--                                            <?php if (get_permission('exam_mark', 'is_view')) { ?>--}}
                    {{--                                                <li class="<?php if ($sub_page == 'exam/marks_register') echo 'nav-active'; ?>">--}}
                    {{--                                                    <a href="<?= url('exam/mark_entry') ?>">--}}
                    {{--                                                        <span><?= url('mark_entries') ?></span>--}}
                    {{--                                                    </a>--}}
                    {{--                                                </li>--}}
                    {{--                                            <?php }--}}
                    {{--                                            if (get_permission('exam_grade', 'is_view')) { ?>--}}
                    {{--                                                <li class="<?php if ($sub_page == 'exam/grade') echo 'nav-active'; ?>">--}}
                    {{--                                                    <a href="<?= url('exam/grade') ?>">--}}
                    {{--                                                        <span><?= url('grades_range') ?></span>--}}
                    {{--                                                    </a>--}}
                    {{--                                                </li>--}}
                    {{--                                            <?php } ?>--}}
                    {{--                                        </ul>--}}
                    {{--                                    </li>--}}
                    {{--                                <?php } ?>--}}
                    {{--                            </ul>--}}
                    {{--                        </li>--}}
                    {{--                    <?php } ?>--}}
                    {{--                    <?php--}}
                    {{--                    if (get_permission('hostel', 'is_view') ||--}}
                    {{--                        get_permission('hostel_category', 'is_view') ||--}}
                    {{--                        get_permission('hostel_room', 'is_view') ||--}}
                    {{--                        get_permission('hostel_allocation', 'is_view') ||--}}
                    {{--                        get_permission('transport_route', 'is_view') ||--}}
                    {{--                        get_permission('transport_vehicle', 'is_view') ||--}}
                    {{--                        get_permission('transport_stoppage', 'is_view') ||--}}
                    {{--                        get_permission('transport_assign', 'is_view') ||--}}
                    {{--                        get_permission('transport_allocation', 'is_view')) {--}}
                    {{--                        ?>--}}
                    {{--                        <!-- supervision -->--}}
                    {{--                        <li class="nav-parent <?php if ($main_menu == 'hostels' || $main_menu == 'transport') echo 'nav-expanded nav-active'; ?>">--}}
                    {{--                            <a>--}}
                    {{--                                <i class="icons icon-feed"--}}
                    {{--                                   aria-hidden="true"></i><span><?= url('supervision') ?></span>--}}
                    {{--                            </a>--}}
                    {{--                            <ul class="nav nav-children">--}}
                    {{--                                <?php--}}
                    {{--                                if (get_permission('hostel', 'is_view') ||--}}
                    {{--                                    get_permission('hostel_category', 'is_view') ||--}}
                    {{--                                    get_permission('hostel_room', 'is_view') ||--}}
                    {{--                                    get_permission('hostel_allocation', 'is_view')) {--}}
                    {{--                                    ?>--}}
                    {{--                                    <!-- hostels -->--}}
                    {{--                                    <li class="nav-parent <?php if ($main_menu == 'hostels') echo 'nav-expanded nav-active'; ?>">--}}
                    {{--                                        <a>--}}
                    {{--                                            <i class="fas fa-store-alt"></i><span><?= url('hostel') ?></span>--}}
                    {{--                                        </a>--}}
                    {{--                                        <ul class="nav nav-children">--}}
                    {{--                                            <?php if (get_permission('hostel', 'is_view')) { ?>--}}
                    {{--                                                <li class="<?php if ($sub_page == 'hostels/index' || $sub_page == 'hostels/edit') echo 'nav-active'; ?>">--}}
                    {{--                                                    <a href="<?= url('hostels') ?>">--}}
                    {{--                                                        <span><?= url('hostel_master') ?></span>--}}
                    {{--                                                    </a>--}}
                    {{--                                                </li>--}}
                    {{--                                            <?php }--}}
                    {{--                                            if (get_permission('hostel_room', 'is_view')) { ?>--}}
                    {{--                                                <li class="<?php if ($sub_page == 'hostels/room' || $sub_page == 'hostels/room_edit') echo 'nav-active'; ?>">--}}
                    {{--                                                    <a href="<?= url('hostels/room') ?>">--}}
                    {{--                                                        <span><?= url('hostel_room') ?></span>--}}
                    {{--                                                    </a>--}}
                    {{--                                                </li>--}}
                    {{--                                            <?php }--}}
                    {{--                                            if (get_permission('hostel_category', 'is_view')) { ?>--}}
                    {{--                                                <li class="<?php if ($sub_page == 'hostels/category') echo 'nav-active'; ?>">--}}
                    {{--                                                    <a href="<?= url('hostels/category') ?>">--}}
                    {{--                                                        <span><?= url('category') ?></span>--}}
                    {{--                                                    </a>--}}
                    {{--                                                </li>--}}
                    {{--                                            <?php }--}}
                    {{--                                            if (get_permission('hostel_allocation', 'is_view')) { ?>--}}
                    {{--                                                <li class="<?php if ($sub_page == 'hostels/allocation') echo 'nav-active'; ?>">--}}
                    {{--                                                    <a href="<?= url('hostels/allocation_report') ?>">--}}
                    {{--                                                        <span><?= url('allocation_report') ?></span>--}}
                    {{--                                                    </a>--}}
                    {{--                                                </li>--}}
                    {{--                                            <?php } ?>--}}
                    {{--                                        </ul>--}}
                    {{--                                    </li>--}}
                    {{--                                <?php } ?>--}}
                    {{--                                <?php--}}
                    {{--                                if (get_permission('transport_route', 'is_view') ||--}}
                    {{--                                    get_permission('transport_vehicle', 'is_view') ||--}}
                    {{--                                    get_permission('transport_stoppage', 'is_view') ||--}}
                    {{--                                    get_permission('transport_assign', 'is_view') ||--}}
                    {{--                                    get_permission('transport_allocation', 'is_view')) {--}}
                    {{--                                    ?>--}}
                    {{--                                    <!-- transport -->--}}
                    {{--                                    <li class="nav-parent <?php if ($main_menu == 'transport') echo 'nav-expanded nav-active'; ?>">--}}
                    {{--                                        <a>--}}
                    {{--                                            <i class="fas fa-bus"></i><span><?= url('transport') ?></span>--}}
                    {{--                                        </a>--}}
                    {{--                                        <ul class="nav nav-children">--}}
                    {{--                                            <?php if (get_permission('transport_route', 'is_view')) { ?>--}}
                    {{--                                                <li class="<?php if ($sub_page == 'transport/route' || $sub_page == 'transport/route_edit') echo 'nav-active'; ?>">--}}
                    {{--                                                    <a href="<?= url('transport/route') ?>">--}}
                    {{--                                                        <span><?= url('route_master') ?></span>--}}
                    {{--                                                    </a>--}}
                    {{--                                                </li>--}}
                    {{--                                            <?php }--}}
                    {{--                                            if (get_permission('transport_vehicle', 'is_view')) { ?>--}}
                    {{--                                                <li class="<?php if ($sub_page == 'transport/vehicle' || $sub_page == 'transport/vehicle_edit') echo 'nav-active'; ?>">--}}
                    {{--                                                    <a href="<?= url('transport/vehicle') ?>">--}}
                    {{--                                                        <span><?= url('vehicle_master') ?></span>--}}
                    {{--                                                    </a>--}}
                    {{--                                                </li>--}}
                    {{--                                            <?php }--}}
                    {{--                                            if (get_permission('transport_stoppage', 'is_view')) { ?>--}}
                    {{--                                                <li class="<?php if ($sub_page == 'transport/stoppage' || $sub_page == 'transport/stoppage_edit') echo 'nav-active'; ?>">--}}
                    {{--                                                    <a href="<?= url('transport/stoppage') ?>">--}}
                    {{--                                                        <span><?= url('stoppage') ?></span>--}}
                    {{--                                                    </a>--}}
                    {{--                                                </li>--}}
                    {{--                                            <?php }--}}
                    {{--                                            if (get_permission('transport_assign', 'is_view')) { ?>--}}
                    {{--                                                <li class="<?php if ($sub_page == 'transport/assign' || $sub_page == 'transport/assign_edit') echo 'nav-active'; ?>">--}}
                    {{--                                                    <a href="<?= url('transport/assign') ?>">--}}
                    {{--                                                        <span><?= url('assign_vehicle') ?></span>--}}
                    {{--                                                    </a>--}}
                    {{--                                                </li>--}}
                    {{--                                            <?php }--}}
                    {{--                                            if (get_permission('transport_allocation', 'is_view')) { ?>--}}
                    {{--                                                <li class="<?php if ($sub_page == 'transport/allocation') echo 'nav-active'; ?>">--}}
                    {{--                                                    <a href="<?= url('transport/report') ?>">--}}
                    {{--                                                        <span><?= url('allocation_report') ?></span>--}}
                    {{--                                                    </a>--}}
                    {{--                                                </li>--}}
                    {{--                                            <?php } ?>--}}
                    {{--                                        </ul>--}}
                    {{--                                    </li>--}}
                    {{--                                <?php } ?>--}}
                    {{--                            </ul>--}}
                    {{--                        </li>--}}
                    {{--                    <?php } ?>--}}
                    {{--                    <?php--}}
                    {{--                    if (get_permission('student_attendance', 'is_add') ||--}}
                    {{--                        get_permission('employee_attendance', 'is_add') ||--}}
                    {{--                        get_permission('exam_attendance', 'is_add')) {--}}
                    {{--                        ?>--}}
                    {{--                        <!-- attendance control -->--}}
                    {{--                        <li class="nav-parent <?php if ($main_menu == 'attendance') echo 'nav-expanded nav-active'; ?>">--}}
                    {{--                            <a>--}}
                    {{--                                <i class="icons icon-chart"></i><span><?= url('attendance') ?></span>--}}
                    {{--                            </a>--}}
                    {{--                            <ul class="nav nav-children">--}}
                    {{--                                <?php if (get_permission('student_attendance', 'is_add')) { ?>--}}
                    {{--                                    <li class="<?php if ($sub_page == 'attendance/student_entries') echo 'nav-active'; ?>">--}}
                    {{--                                        <a href="<?= url('attendance/student_entry') ?>">--}}
                    {{--                                            <span><i class="fas fa-caret-right"></i><?= url('student') ?></span>--}}
                    {{--                                        </a>--}}
                    {{--                                    </li>--}}
                    {{--                                <?php }--}}
                    {{--                                if (get_permission('employee_attendance', 'is_add')) { ?>--}}
                    {{--                                    <li class="<?php if ($sub_page == 'attendance/employees_entries') echo 'nav-active'; ?>">--}}
                    {{--                                        <a href="<?= url('attendance/employees_entry') ?>">--}}
                    {{--                                            <span><i class="fas fa-caret-right"></i><?= url('employee') ?></span>--}}
                    {{--                                        </a>--}}
                    {{--                                    </li>--}}
                    {{--                                <?php }--}}
                    {{--                                if (get_permission('exam_attendance', 'is_add')) { ?>--}}
                    {{--                                    <li class="<?php if ($sub_page == 'attendance/exam_entries') echo 'nav-active'; ?>">--}}
                    {{--                                        <a href="<?= url('attendance/exam_entry') ?>">--}}
                    {{--                                            <span><i class="fas fa-caret-right"></i><?= url('exam') ?></span>--}}
                    {{--                                        </a>--}}
                    {{--                                    </li>--}}
                    {{--                                <?php } ?>--}}
                    {{--                            </ul>--}}
                    {{--                        </li>--}}
                    {{--                    <?php } ?>--}}
                    {{--                    <?php--}}
                    {{--                    if (get_permission('book', 'is_view') ||--}}
                    {{--                        get_permission('book_category', 'is_view') ||--}}
                    {{--                        get_permission('book_manage', 'is_view') ||--}}
                    {{--                        get_permission('book_request', 'is_view')) {--}}
                    {{--                        ?>--}}
                    {{--                        <!-- library -->--}}
                    {{--                        <li class="nav-parent <?php if ($main_menu == 'library') echo 'nav-expanded nav-active'; ?>">--}}
                    {{--                            <a>--}}
                    {{--                                <i class="icons icon-notebook"></i><span><?= url('library') ?></span>--}}
                    {{--                            </a>--}}
                    {{--                            <ul class="nav nav-children">--}}
                    {{--                                <?php if (get_permission('book', 'is_view')) { ?>--}}
                    {{--                                    <li class="<?php if ($sub_page == 'library/book') echo 'nav-active'; ?>">--}}
                    {{--                                        <a href="<?= url('library/book') ?>">--}}
                    {{--                                            <span><i class="fas fa-caret-right"></i><?= url('books') ?></span>--}}
                    {{--                                        </a>--}}
                    {{--                                    </li>--}}
                    {{--                                <?php }--}}
                    {{--                                if (get_permission('book_category', 'is_view')) { ?>--}}
                    {{--                                    <li class="<?php if ($sub_page == 'library/category') echo 'nav-active'; ?>">--}}
                    {{--                                        <a href="<?= url('library/category') ?>">--}}
                    {{--                                            <span><i class="fas fa-caret-right"></i><?= url('books_category') ?></span>--}}
                    {{--                                        </a>--}}
                    {{--                                    </li>--}}
                    {{--                                <?php }--}}
                    {{--                                if (get_permission('book_request', 'is_view')) { ?>--}}
                    {{--                                    <li class="<?php if ($sub_page == 'library/request') echo 'nav-active'; ?>">--}}
                    {{--                                        <a href="<?= url('library/request') ?>">--}}
                    {{--                                            <span><i class="fas fa-caret-right"></i>My Issued Book</span>--}}
                    {{--                                        </a>--}}
                    {{--                                    </li>--}}
                    {{--                                <?php }--}}
                    {{--                                if (get_permission('book_manage', 'is_view')) { ?>--}}
                    {{--                                    <li class="<?php if ($sub_page == 'library/book_manage') echo 'nav-active'; ?>">--}}
                    {{--                                        <a href="<?= url('library/book_manage') ?>">--}}
                    {{--                                            <span><i class="fas fa-caret-right"></i>Book Issue/Return</span>--}}
                    {{--                                        </a>--}}
                    {{--                                    </li>--}}
                    {{--                                <?php } ?>--}}
                    {{--                            </ul>--}}
                    {{--                        </li>--}}
                    {{--                    <?php } ?>--}}
                    {{--                    <?php--}}
                    {{--                    if (get_permission('event', 'is_view') ||--}}
                    {{--                        get_permission('event_type', 'is_view')) {--}}
                    {{--                        ?>--}}
                    {{--                        <!-- envant -->--}}
                    {{--                        <li class="nav-parent <?php if ($main_menu == 'event') echo 'nav-expanded nav-active'; ?>">--}}
                    {{--                            <a>--}}
                    {{--                                <i class="icons icon-speech"></i><span><?= url('events') ?></span>--}}
                    {{--                            </a>--}}
                    {{--                            <ul class="nav nav-children">--}}
                    {{--                                <?php if (get_permission('event_type', 'is_view')) { ?>--}}
                    {{--                                    <li class="<?php if ($sub_page == 'event/types') echo 'nav-active'; ?>">--}}
                    {{--                                        <a href="<?= url('event/types') ?>">--}}
                    {{--                                            <span><i class="fas fa-caret-right"></i><?= url('event_type') ?></span>--}}
                    {{--                                        </a>--}}
                    {{--                                    </li>--}}
                    {{--                                <?php }--}}
                    {{--                                if (get_permission('event', 'is_view')) { ?>--}}
                    {{--                                    <li class="<?php if ($sub_page == 'event/index') echo 'nav-active'; ?>">--}}
                    {{--                                        <a href="<?= url('event') ?>">--}}
                    {{--                                            <span><i class="fas fa-caret-right"></i><?= url('events') ?></span>--}}
                    {{--                                        </a>--}}
                    {{--                                    </li>--}}
                    {{--                                <?php } ?>--}}
                    {{--                            </ul>--}}
                    {{--                        </li>--}}
                    {{--                    <?php } ?>--}}
                    {{--                    <?php--}}
                    {{--                    if (get_permission('sendsmsmail', 'is_add') ||--}}
                    {{--                        get_permission('sendsmsmail_template', 'is_view') ||--}}
                    {{--                        get_permission('sendsmsmail_reports', 'is_view')) {--}}
                    {{--                        ?>--}}
                    {{--                        <!-- SMS -->--}}
                    {{--                        <li class="nav-parent <?php if ($main_menu == 'sendsmsmail') echo 'nav-expanded nav-active'; ?>">--}}
                    {{--                            <a>--}}
                    {{--                                <i class="icons icon-bell"></i><span><?= url('bulk_sms_and_email') ?></span>--}}
                    {{--                            </a>--}}
                    {{--                            <ul class="nav nav-children">--}}
                    {{--                                <?php if (get_permission('sendsmsmail', 'is_add')) { ?>--}}
                    {{--                                    <li class="<?php if ($sub_page == 'sendsmsmail/sms' || $sub_page == 'sendsmsmail/email') echo 'nav-active'; ?>">--}}
                    {{--                                        <a href="<?= url('sendsmsmail/sms') ?>">--}}
                    {{--                                            <span><i class="fas fa-caret-right"></i><?= url('send') ?> Sms / Email</span>--}}
                    {{--                                        </a>--}}
                    {{--                                    </li>--}}
                    {{--                                    <li class="<?php if ($sub_page == 'sendsmsmail/campaign_reports') echo 'nav-active'; ?>">--}}
                    {{--                                        <a href="<?= url('sendsmsmail/campaign_reports') ?>">--}}
                    {{--                                            <span><i class="fas fa-caret-right"></i>Sms / Email <?= url('report') ?></span>--}}
                    {{--                                        </a>--}}
                    {{--                                    </li>--}}
                    {{--                                <?php }--}}
                    {{--                                if (get_permission('sendsmsmail_template', 'is_view')) { ?>--}}
                    {{--                                    <li class="<?php if ($sub_page == 'sendsmsmail/template_sms' || $sub_page == 'sendsmsmail/template_edit_sms') echo 'nav-active'; ?>">--}}
                    {{--                                        <a href="<?= url('sendsmsmail/template/sms') ?>">--}}
                    {{--                                            <span><i class="fas fa-caret-right"></i> <?= url('sms') . " " . url('template') ?></span>--}}
                    {{--                                        </a>--}}
                    {{--                                    </li>--}}
                    {{--                                    <li class="<?php if ($sub_page == 'sendsmsmail/template_email' || $sub_page == 'sendsmsmail/template_edit_email') echo 'nav-active'; ?>">--}}
                    {{--                                        <a href="<?= url('sendsmsmail/template/email') ?>">--}}
                    {{--                                            <span><i class="fas fa-caret-right"></i> <?= url('email') . " " . url('template') ?></span>--}}
                    {{--                                        </a>--}}
                    {{--                                    </li>--}}
                    {{--                                <?php } ?>--}}
                    {{--                            </ul>--}}
                    {{--                        </li>--}}
                    {{--                    <?php } ?>--}}
                    {{--                    <?php--}}
                    {{--                    if (get_permission('fees_type', 'is_view') ||--}}
                    {{--                        get_permission('fees_group', 'is_view') ||--}}
                    {{--                        get_permission('fees_fine_setup', 'is_view') ||--}}
                    {{--                        get_permission('fees_allocation', 'is_view') ||--}}
                    {{--                        get_permission('invoice', 'is_view') ||--}}
                    {{--                        get_permission('due_invoice', 'is_view') ||--}}
                    {{--                        get_permission('fees_reminder', 'is_view')) {--}}
                    {{--                        ?>--}}
                    {{--                        <!-- student accounting -->--}}
                    {{--                        <li class="nav-parent <?php if ($main_menu == 'fees') echo 'nav-expanded nav-active'; ?>">--}}
                    {{--                            <a>--}}
                    {{--                                <i class="icons icon-calculator"></i><span><?= url('student_accounting') ?></span>--}}
                    {{--                            </a>--}}
                    {{--                            <ul class="nav nav-children">--}}
                    {{--                                <?php if (get_permission('fees_type', 'is_view')) { ?>--}}
                    {{--                                    <li class="<?php if ($sub_page == 'fees/type') echo 'nav-active'; ?>">--}}
                    {{--                                        <a href="<?= url('fees/type') ?>"><span><i--}}
                    {{--                                                        class="fas fa-caret-right"></i><?= url('fees_type') ?></span></a>--}}
                    {{--                                    </li>--}}
                    {{--                                <?php }--}}
                    {{--                                if (get_permission('fees_group', 'is_view')) { ?>--}}
                    {{--                                    <li class="<?php if ($sub_page == 'fees/group') echo 'nav-active'; ?>">--}}
                    {{--                                        <a href="<?= url('fees/group') ?>"><span><i--}}
                    {{--                                                        class="fas fa-caret-right"></i><?= url('fees_group') ?></span></a>--}}
                    {{--                                    </li>--}}
                    {{--                                <?php }--}}
                    {{--                                if (get_permission('fees_fine_setup', 'is_view')) { ?>--}}
                    {{--                                    <li class="<?php if ($sub_page == 'fees/fine_setup') echo 'nav-active'; ?>">--}}
                    {{--                                        <a href="<?= url('fees/fine_setup') ?>"><span><i--}}
                    {{--                                                        class="fas fa-caret-right"></i><?= url('fine_setup') ?></span></a>--}}
                    {{--                                    </li>--}}
                    {{--                                <?php }--}}
                    {{--                                if (get_permission('fees_allocation', 'is_view')) { ?>--}}
                    {{--                                    <li class="<?php if ($sub_page == 'fees/allocation') echo 'nav-active'; ?>">--}}
                    {{--                                        <a href="<?= url('fees/allocation') ?>"><span><i--}}
                    {{--                                                        class="fas fa-caret-right"></i><?= url('fees_allocation') ?></span></a>--}}
                    {{--                                    </li>--}}
                    {{--                                <?php }--}}
                    {{--                                if (get_permission('invoice', 'is_view')) { ?>--}}
                    {{--                                    <li class="<?php if ($sub_page == 'fees/invoice_list' || $sub_page == 'fees/collect') echo 'nav-active'; ?>">--}}
                    {{--                                        <a href="<?= url('fees/invoice_list') ?>"><span><i--}}
                    {{--                                                        class="fas fa-caret-right"></i><?= url('payments_history') ?></span></a>--}}
                    {{--                                    </li>--}}
                    {{--                                <?php }--}}
                    {{--                                if (get_permission('due_invoice', 'is_view')) { ?>--}}
                    {{--                                    <li class="<?php if ($sub_page == 'fees/due_invoice') echo 'nav-active'; ?>">--}}
                    {{--                                        <a href="<?= url('fees/due_invoice') ?>"><span><i--}}
                    {{--                                                        class="fas fa-caret-right"></i><?= url('due_fees_invoice') ?></span></a>--}}
                    {{--                                    </li>--}}
                    {{--                                <?php }--}}
                    {{--                                if (get_permission('fees_reminder', 'is_view')) { ?>--}}
                    {{--                                    <li class="<?php if ($sub_page == 'fees/reminder') echo 'nav-active'; ?>">--}}
                    {{--                                        <a href="<?= url('fees/reminder') ?>"><span><i--}}
                    {{--                                                        class="fas fa-caret-right"></i><?= url('fees_reminder') ?></span></a>--}}
                    {{--                                    </li>--}}
                    {{--                                <?php } if(get_permission('fees_reminder', 'is_view')) { ?>--}}
                    {{--                                    <li class="<?php if ($sub_page == 'fees/upload') echo 'nav-active';?>">--}}
                    {{--                                        <a href="<?=url('fees/upload')?>"><span><i class="fas fa-caret-right"></i>Fees Upload<?=url('')?></span></a>--}}
                    {{--                                    </li>--}}
                    {{--                                <?php } ?>--}}
                    {{--                            </ul>--}}
                    {{--                        </li>--}}
                    {{--                    <?php } ?>--}}
                    {{--                    <?php--}}
                    {{--                    if (get_permission('account', 'is_view') ||--}}
                    {{--                        get_permission('voucher_head', 'is_view') ||--}}
                    {{--                        get_permission('deposit', 'is_view') ||--}}
                    {{--                        get_permission('expense', 'is_view') ||--}}
                    {{--                        get_permission('all_transactions', 'is_view')) {--}}
                    {{--                        ?>--}}
                    {{--                        <!-- office accounting -->--}}
                    {{--                        <li class="nav-parent <?php if ($main_menu == 'accounting') echo 'nav-expanded nav-active'; ?>">--}}
                    {{--                            <a>--}}
                    {{--                                <i class="icon-credit-card icons"></i><span><?= url('office_accounting') ?></span>--}}
                    {{--                            </a>--}}
                    {{--                            <ul class="nav nav-children">--}}
                    {{--                                <?php if (get_permission('account', 'is_view')) { ?>--}}
                    {{--                                    <li class="<?php if ($sub_page == 'accounting/index' || $sub_page == 'accounting/edit') echo 'nav-active'; ?>">--}}
                    {{--                                        <a href="<?php echo url('accounting'); ?>">--}}
                    {{--                                            <span><i class="fas fa-caret-right"--}}
                    {{--                                                     aria-hidden="true"></i><?php echo url('account'); ?></span>--}}
                    {{--                                        </a>--}}
                    {{--                                    </li>--}}
                    {{--                                <?php }--}}
                    {{--                                if (get_permission('deposit', 'is_view')) { ?>--}}
                    {{--                                    <li class="<?php if ($sub_page == 'accounting/voucher_deposit' || $sub_page == 'accounting/voucher_deposit_edit') echo 'nav-active'; ?>">--}}
                    {{--                                        <a href="<?php echo url('accounting/voucher_deposit'); ?>">--}}
                    {{--                                            <span><i class="fas fa-caret-right"--}}
                    {{--                                                     aria-hidden="true"></i><?php echo url('new_deposit'); ?></span>--}}
                    {{--                                        </a>--}}
                    {{--                                    </li>--}}
                    {{--                                <?php }--}}
                    {{--                                if (get_permission('expense', 'is_view')) { ?>--}}
                    {{--                                    <li class="<?php if ($sub_page == 'accounting/voucher_expense' || $sub_page == 'accounting/voucher_expense_edit') echo 'nav-active'; ?>">--}}
                    {{--                                        <a href="<?php echo url('accounting/voucher_expense'); ?>">--}}
                    {{--                                            <span><i class="fas fa-caret-right"--}}
                    {{--                                                     aria-hidden="true"></i><?php echo url('new_expense'); ?></span>--}}
                    {{--                                        </a>--}}
                    {{--                                    </li>--}}
                    {{--                                <?php }--}}
                    {{--                                if (get_permission('all_transactions', 'is_view')) { ?>--}}
                    {{--                                    <li class="<?php if ($sub_page == 'accounting/all_transactions') echo 'nav-active'; ?>">--}}
                    {{--                                        <a href="<?php echo url('accounting/all_transactions'); ?>">--}}
                    {{--                                            <span><i class="fas fa-caret-right"--}}
                    {{--                                                     aria-hidden="true"></i><?php echo url('all_transactions'); ?></span>--}}
                    {{--                                        </a>--}}
                    {{--                                    </li>--}}
                    {{--                                <?php }--}}
                    {{--                                if (get_permission('voucher_head', 'is_view') || get_permission('voucher_head', 'is_add')) { ?>--}}
                    {{--                                    <li class="<?php if ($sub_page == 'accounting/voucher_head') echo 'nav-active'; ?>">--}}
                    {{--                                        <a href="<?php echo url('accounting/voucher_head'); ?>">--}}
                    {{--                                            <span><i class="fas fa-caret-right"--}}
                    {{--                                                     aria-hidden="true"></i><?php echo url('voucher') . " " . url('head'); ?></span>--}}
                    {{--                                        </a>--}}
                    {{--                                    </li>--}}
                    {{--                                <?php } ?>--}}
                    {{--                            </ul>--}}
                    {{--                        </li>--}}
                    {{--                    <?php } ?>--}}
                    {{--                    <!-- message -->--}}
                    {{--                    <li class="<?php if ($main_menu == 'message') echo 'nav-active'; ?>">--}}
                    {{--                        <a href="<?= url('communication/mailbox/inbox') ?>">--}}
                    {{--                            <i class="icons icon-envelope-open"></i><span><?= url('message') ?></span>--}}
                    {{--                        </a>--}}
                    {{--                    </li>--}}

                    {{--                    <?php--}}
                    {{--                    $attendance_report = false;--}}
                    {{--                    if (get_permission('student_attendance_report', 'is_view') ||--}}
                    {{--                        get_permission('employee_attendance_report', 'is_view') ||--}}
                    {{--                        get_permission('exam_attendance_report', 'is_view')) {--}}
                    {{--                        $attendance_report = true;--}}
                    {{--                    }--}}

                    {{--                    if (get_permission('fees_reports', 'is_view') ||--}}
                    {{--                        get_permission('accounting_reports', 'is_view') ||--}}
                    {{--                        get_permission('salary_summary_report', 'is_view') ||--}}
                    {{--                        get_permission('leave_reports', 'is_view') ||--}}
                    {{--                        ($attendance_report == true) ||--}}
                    {{--                        get_permission('report_card', 'is_view') ||--}}
                    {{--                        get_permission('tabulation_sheet', 'is_view')) {--}}
                    {{--                        ?>--}}
                    {{--                        <!-- reports -->--}}
                    {{--                        <li class="nav-parent <?php if ($main_menu == 'accounting_repots' ||--}}
                    {{--                            $main_menu == 'fees_repots' ||--}}
                    {{--                            $main_menu == 'attendance_report' ||--}}
                    {{--                            $main_menu == 'payroll_reports' ||--}}
                    {{--                            $main_menu == 'leave_reports' ||--}}
                    {{--                            $main_menu == 'exam_reports') echo 'nav-expanded nav-active'; ?>">--}}
                    {{--                            <a>--}}
                    {{--                                <i class="icons icon-pie-chart icons"></i><span><?= url('reports') ?></span>--}}
                    {{--                            </a>--}}
                    {{--                            <ul class="nav nav-children">--}}
                    {{--                                <?php if (get_permission('fees_reports', 'is_view')) { ?>--}}
                    {{--                                    <li class="nav-parent <?php if ($main_menu == 'fees_repots') echo 'nav-expanded nav-active'; ?>">--}}
                    {{--                                        <a><i class="fas fa-print"></i><span><?php echo url('fees_reports'); ?></span></a>--}}
                    {{--                                        <ul class="nav nav-children">--}}
                    {{--                                            <li class="<?php if ($sub_page == 'fees/student_fees_report') echo 'nav-active'; ?>">--}}
                    {{--                                                <a href="<?= url('fees/student_fees_report') ?>"><?= url('fees_report') ?></a>--}}
                    {{--                                            </li>--}}
                    {{--                                            <li class="<?php if ($sub_page == 'fees/payment_history') echo 'nav-active'; ?>">--}}
                    {{--                                                <a href="<?= url('fees/payment_history') ?>"><?= url('receipts_report') ?></a>--}}
                    {{--                                            </li>--}}
                    {{--                                            <li class="<?php if ($sub_page == 'fees/due_report') echo 'nav-active'; ?>">--}}
                    {{--                                                <a href="<?= url('fees/due_report') ?>"><?= url('due_fees_report') ?></a>--}}
                    {{--                                            </li>--}}
                    {{--                                            <li class="<?php if ($sub_page == 'fees/fine_report') echo 'nav-active'; ?>">--}}
                    {{--                                                <a href="<?= url('fees/fine_report') ?>"><?= url('fine_report') ?></a>--}}
                    {{--                                            </li>--}}


                    {{--                                        </ul>--}}
                    {{--                                    </li>--}}
                    {{--                                <?php } ?>--}}
                    {{--                                <?php if (get_permission('accounting_reports', 'is_view')) { ?>--}}
                    {{--                                    <li class="nav-parent <?php if ($main_menu == 'accounting_repots') echo 'nav-expanded nav-active'; ?>">--}}
                    {{--                                        <a><i class="fas fa-print"></i><span><?php echo url('financial_reports'); ?></span></a>--}}
                    {{--                                        <ul class="nav nav-children">--}}
                    {{--                                            <li class="<?php if ($sub_page == 'accounting/account_statement') echo 'nav-active'; ?>">--}}
                    {{--                                                <a href="<?php echo url('accounting/account_statement'); ?>"><?php echo url('account') . " " . url('statement'); ?></a>--}}
                    {{--                                            </li>--}}
                    {{--                                            <li class="<?php if ($sub_page == 'accounting/income_repots') echo 'nav-active'; ?>">--}}
                    {{--                                                <a href="<?php echo url('accounting/income_repots'); ?>"><?php echo url('income') . " " . url('repots'); ?></a>--}}
                    {{--                                            </li>--}}
                    {{--                                            <li class="<?php if ($sub_page == 'accounting/expense_repots') echo 'nav-active'; ?>">--}}
                    {{--                                                <a href="<?php echo url('accounting/expense_repots'); ?>"> <?php echo url('expense') . " " . url('repots'); ?></a>--}}
                    {{--                                            </li>--}}
                    {{--                                            <li class="<?php if ($sub_page == 'accounting/transitions_repots') echo 'nav-active'; ?>">--}}
                    {{--                                                <a href="<?php echo url('accounting/transitions_repots'); ?>"> <?php echo url('transitions') . " " . url('reports'); ?></a>--}}
                    {{--                                            </li>--}}
                    {{--                                            <li class="<?php if ($sub_page == 'accounting/balance_sheet') echo 'nav-active'; ?>">--}}
                    {{--                                                <a href="<?php echo url('accounting/balance_sheet'); ?>"><?php echo url('balance') . " " . url('sheet'); ?></a>--}}
                    {{--                                            </li>--}}
                    {{--                                            <li class="<?php if ($sub_page == 'accounting/income_vs_expense') echo 'nav-active'; ?>">--}}
                    {{--                                                <a href="<?php echo url('accounting/incomevsexpense'); ?>"> <?php echo url('income_vs_expense'); ?></a>--}}
                    {{--                                            </li>--}}

                    {{--                                        </ul>--}}
                    {{--                                    </li>--}}
                    {{--                                <?php } ?>--}}
                    {{--                                <?php if ($attendance_report == true) { ?>--}}
                    {{--                                    <li class="nav-parent <?php if ($main_menu == 'attendance_report') echo 'nav-expanded nav-active'; ?>">--}}
                    {{--                                        <a><i class="fas fa-print"></i><span><?php echo url('attendance_reports'); ?></span></a>--}}
                    {{--                                        <ul class="nav nav-children">--}}
                    {{--                                            <?php if (get_permission('student_attendance_report', 'is_view')) { ?>--}}
                    {{--                                                <li class="<?php if ($sub_page == 'attendance/student_report') echo 'nav-active'; ?>">--}}
                    {{--                                                    <a href="<?= url('attendance/studentwise_report') ?>">--}}
                    {{--                                                        <?= url('student') . ' ' . url('reports') ?>--}}
                    {{--                                                    </a>--}}
                    {{--                                                </li>--}}
                    {{--                                            <?php }--}}
                    {{--                                            if (get_permission('employee_attendance_report', 'is_view')) { ?>--}}
                    {{--                                                <li class="<?php if ($sub_page == 'attendance/employees_report') echo 'nav-active'; ?>">--}}
                    {{--                                                    <a href="<?= url('attendance/employeewise_report') ?>">--}}
                    {{--                                                        <?= url('employee') . ' ' . url('reports') ?>--}}
                    {{--                                                    </a>--}}
                    {{--                                                </li>--}}
                    {{--                                            <?php }--}}
                    {{--                                            if (get_permission('exam_attendance_report', 'is_view')) { ?>--}}
                    {{--                                                <li class="<?php if ($sub_page == 'attendance/exam_report') echo 'nav-active'; ?>">--}}
                    {{--                                                    <a href="<?= url('attendance/examwise_report') ?>">--}}
                    {{--                                                        <?= url('exam') . ' ' . url('reports') ?>--}}
                    {{--                                                    </a>--}}
                    {{--                                                </li>--}}
                    {{--                                            <?php } ?>--}}
                    {{--                                        </ul>--}}
                    {{--                                    </li>--}}
                    {{--                                <?php } ?>--}}

                    {{--                                <?php if (get_permission('salary_summary_report', 'is_view') || get_permission('leave_reports', 'is_view')) { ?>--}}
                    {{--                                    <li class="nav-parent <?php if ($main_menu == 'payroll_reports' || $main_menu == 'leave_reports') echo 'nav-expanded nav-active'; ?>">--}}
                    {{--                                        <a><i class="fas fa-print"></i><span><?php echo url('hrm'); ?></span></a>--}}
                    {{--                                        <ul class="nav nav-children">--}}
                    {{--                                            <?php if (get_permission('salary_summary_report', 'is_view')) { ?>--}}
                    {{--                                                <li class="<?php if ($sub_page == 'payroll/salary_statement') echo 'nav-active'; ?>">--}}
                    {{--                                                    <a href="<?= url('payroll/salary_statement') ?>">--}}
                    {{--                                                        <span><?= url('payroll_summary') ?></span>--}}
                    {{--                                                    </a>--}}
                    {{--                                                </li>--}}
                    {{--                                            <?php }--}}
                    {{--                                            if (get_permission('leave_reports', 'is_view')) { ?>--}}
                    {{--                                                <li class="<?php if ($sub_page == 'leave/reports') echo 'nav-active'; ?>">--}}
                    {{--                                                    <a href="<?= url('leave/reports') ?>">--}}
                    {{--                                                        <span><?= url('leave') . " " . url('reports') ?></span>--}}
                    {{--                                                    </a>--}}
                    {{--                                                </li>--}}
                    {{--                                            <?php } ?>--}}
                    {{--                                        </ul>--}}
                    {{--                                    </li>--}}
                    {{--                                <?php } ?>--}}
                    {{--                                <?php if (get_permission('report_card', 'is_view') || get_permission('tabulation_sheet', 'is_view')) { ?>--}}
                    {{--                                    <li class="nav-parent <?php if ($main_menu == 'exam_reports') echo 'nav-expanded nav-active'; ?>">--}}
                    {{--                                        <a><i class="fas fa-print"></i><span><?php echo url('examination'); ?></span></a>--}}
                    {{--                                        <ul class="nav nav-children">--}}
                    {{--                                            <?php if (get_permission('report_card', 'is_view')) { ?>--}}
                    {{--                                                <li class="<?php if ($sub_page == 'exam/marksheet') echo 'nav-active'; ?>">--}}
                    {{--                                                    <a href="<?= url('exam/marksheet') ?>">--}}
                    {{--                                                        <span><?= url('report_card') ?></span>--}}
                    {{--                                                    </a>--}}
                    {{--                                                </li>--}}
                    {{--                                            <?php }--}}
                    {{--                                            if (get_permission('tabulation_sheet', 'is_view')) { ?>--}}
                    {{--                                                <li class="<?php if ($sub_page == 'exam/tabulation_sheet') echo 'nav-active'; ?>">--}}
                    {{--                                                    <a href="<?= url('exam/tabulation_sheet') ?>">--}}
                    {{--                                                        <span><?= url('tabulation_sheet') ?></span>--}}
                    {{--                                                    </a>--}}
                    {{--                                                </li>--}}
                    {{--                                            <?php } ?>--}}
                    {{--                                        </ul>--}}
                    {{--                                    </li>--}}
                    {{--                                <?php } ?>--}}
                    {{--                            </ul>--}}
                    {{--                        </li>--}}
                    {{--                    <?php } ?>--}}
                    {{--                    <?php--}}

                    {{--                    $schoolSettings = false;--}}
                    {{--                    if (get_permission('school_settings', 'is_view') ||--}}
                    {{--                        get_permission('live_class_config', 'is_view') ||--}}
                    {{--                        get_permission('payment_settings', 'is_view') ||--}}
                    {{--                        get_permission('sms_settings', 'is_view') ||--}}
                    {{--                        get_permission('email_settings', 'is_view') ||--}}
                    {{--                        get_permission('accounting_links', 'is_view')) {--}}
                    {{--                        $schoolSettings = true;--}}
                    {{--                    }--}}
                    {{--                    if (get_permission('global_settings', 'is_view') ||--}}
                    {{--                        ($schoolSettings == true) ||--}}
                    {{--                        get_permission('translations', 'is_view') ||--}}
                    {{--                        get_permission('cron_job', 'is_view') ||--}}
                    {{--                        get_permission('system_update', 'is_add') ||--}}
                    {{--                        get_permission('custom_field', 'is_view') ||--}}
                    {{--                        get_permission('backup', 'is_view')) {--}}
                    {{--                        ?>--}}
                    {{--                        <!-- setting -->--}}
                    {{--                        <li class="nav-parent <?php if ($main_menu == 'settings' || $main_menu == 'school_m') echo 'nav-expanded nav-active'; ?>">--}}
                    {{--                            <a>--}}
                    {{--                                <i class="icons icon-briefcase"></i><span><?= url('settings') ?></span>--}}
                    {{--                            </a>--}}
                    {{--                            <ul class="nav nav-children">--}}
                    {{--                                <?php if (get_permission('global_settings', 'is_view')) { ?>--}}
                    {{--                                    <li class="<?php if ($sub_page == 'settings/universal') echo 'nav-active'; ?>">--}}
                    {{--                                        <a href="<?= url('settings/universal') ?>">--}}
                    {{--                                            <span><i class="fas fa-caret-right"--}}
                    {{--                                                     aria-hidden="true"></i><?= url('global_settings') ?></span>--}}
                    {{--                                        </a>--}}
                    {{--                                    </li>--}}
                    {{--                                <?php }--}}
                    {{--                                if ($schoolSettings == true) { ?>--}}
                    {{--                                    <li class="<?php if ($main_menu == 'school_m') echo 'nav-active'; ?>">--}}
                    {{--                                        <a href="<?= url('school_settings') ?>">--}}
                    {{--                                            <span><i class="fas fa-caret-right"--}}
                    {{--                                                     aria-hidden="true"></i><?= url('school_settings') ?></span>--}}
                    {{--                                        </a>--}}
                    {{--                                    </li>--}}
                    {{--                                <?php }--}}
                    {{--                                if (is_superadmin_loggedin()) { ?>--}}
                    {{--                                    <li class="<?php if ($sub_page == 'role/index' || $sub_page == 'role/permission') echo 'nav-active'; ?>">--}}
                    {{--                                        <a href="<?= url('role') ?>">--}}
                    {{--                                            <span><i class="fas fa-caret-right"--}}
                    {{--                                                     aria-hidden="true"></i><?= url('role_permission') ?></span>--}}
                    {{--                                        </a>--}}
                    {{--                                    </li>--}}
                    {{--                                <?php }--}}
                    {{--                                if (is_superadmin_loggedin()) { ?>--}}
                    {{--                                    <li class="<?php if ($sub_page == 'sessions/index') echo 'nav-active'; ?>">--}}
                    {{--                                        <a href="<?= url('sessions') ?>">--}}
                    {{--                                            <span><i class="fas fa-caret-right"--}}
                    {{--                                                     aria-hidden="true"></i><?= url('session_settings') ?></span>--}}
                    {{--                                        </a>--}}
                    {{--                                    </li>--}}
                    {{--                                <?php }--}}
                    {{--                                if (get_permission('translations', 'is_view')) { ?>--}}
                    {{--                                    <li class="<?php if ($sub_page == 'language/index') echo 'nav-active'; ?>">--}}
                    {{--                                        <a href="<?= url('translations') ?>">--}}
                    {{--                                            <span><i class="fas fa-caret-right"--}}
                    {{--                                                     aria-hidden="true"></i><?= url('translations') ?></span>--}}
                    {{--                                        </a>--}}
                    {{--                                    </li>--}}
                    {{--                                <?php }--}}
                    {{--                                if (get_permission('cron_job', 'is_view')) { ?>--}}
                    {{--                                    <li class="<?php if ($sub_page == 'cron_api/index') echo 'nav-active'; ?>">--}}
                    {{--                                        <a href="<?= url('cron_api') ?>">--}}
                    {{--                                            <span><i class="fas fa-caret-right"--}}
                    {{--                                                     aria-hidden="true"></i><?= url('cron_job') ?></span>--}}
                    {{--                                        </a>--}}
                    {{--                                    </li>--}}
                    {{--                                <?php }--}}
                    {{--                                if (get_permission('custom_field', 'is_view')) { ?>--}}
                    {{--                                    <li class="<?php if ($sub_page == 'custom_field/index') echo 'nav-active'; ?>">--}}
                    {{--                                        <a href="<?= url('custom_field') ?>">--}}
                    {{--                                            <span><i class="fas fa-caret-right"--}}
                    {{--                                                     aria-hidden="true"></i><?= url('custom_field') ?></span>--}}
                    {{--                                        </a>--}}
                    {{--                                    </li>--}}
                    {{--                                <?php }--}}
                    {{--                                if (get_permission('backup', 'is_view')) { ?>--}}
                    {{--                                    <li class="<?php if ($sub_page == 'database_backup/index') echo 'nav-active'; ?>">--}}
                    {{--                                        <a href="<?= url('backup') ?>">--}}
                    {{--                                            <span><i class="fas fa-caret-right"--}}
                    {{--                                                     aria-hidden="true"></i><?= url('database_backup') ?></span>--}}
                    {{--                                        </a>--}}
                    {{--                                    </li>--}}
                    {{--                                <?php }--}}
                    {{--                                if (get_permission('system_update', 'is_add')) { ?>--}}
                    {{--                                    <li class="<?php if ($sub_page == 'system_update/index') echo 'nav-active'; ?>">--}}
                    {{--                                        <a href="<?= url('system_update') ?>">--}}
                    {{--                                            <span><i class="fas fa-caret-right"--}}
                    {{--                                                     aria-hidden="true"></i><?= url('system_update') ?></span>--}}
                    {{--                                        </a>--}}
                    {{--                                    </li>--}}
                    {{--                                <?php } ?>--}}
                    {{--                            </ul>--}}
                    {{--                        </li>--}}
                    {{--                    <?php } ?>--}}
                </ul>
            </nav>
        </div>
        <script>
            // maintain scroll position
            if (typeof localStorage !== 'undefined') {
                if (localStorage.getItem('sidebar-left-position') !== null) {
                    var initialPosition = localStorage.getItem('sidebar-left-position'),
                        sidebarLeft = document.querySelector('#sidebar-left .nano-content');
                    sidebarLeft.scrollTop = initialPosition;
                }
            }
        </script>
    </div>
</aside>
<!-- end sidebar -->
