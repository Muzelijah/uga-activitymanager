@extends('layouts.main')
@section('content')
    <section class="panel" xmlns="http://www.w3.org/1999/html">
        <div class="tabs-custom">
            <ul class="nav nav-tabs">
                <li>
                    <a href="{{route('vendors.data')}}">
                        <i class="fas fa-list-ul"></i> Vendor List
                    </a>
                </li>
                <li  class="active">
                    <a href="#add"  data-toggle="tab">
                        <i class="fas fa-list-ul"></i> Edit Vendor
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane box  mb-md" id="list">


                </div>

                <div class="tab-pane active" id="add">
                    <form action="{{route('vendors.update',['vendor'=>$vendor->vendor_ref])}}" id="vendor_form" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="panel-body">

                            <div class="row">
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Names<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <input type="text" required class="form-control"
                                               name="vendor_name"
                                               value="{{old('vendor_name',$vendor->vendor_name)}}"/>
                                        <span class="error"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Number<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control"
                                               name="vendor_number"
                                               value="{{old('vendor_number',$vendor->vendor_number)}}"/>
                                        <span class="error"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Contact<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <input type="text" required class="form-control"
                                               name="vendor_contact"
                                               value="{{old('vendor_contact',$vendor->vendor_contact)}}"/>
                                        <span class="error"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Email<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <input type="text" required class="form-control"
                                               name="vendor_email"
                                               value="{{old('vendor_email',$vendor->vendor_email)}}"/>
                                        <span class="error"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Address<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <input type="text" required class="form-control"
                                               name="vendor_address"
                                               value="{{old('vendor_address',$vendor->vendor_address)}}"/>
                                        <span class="error"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Signature<span
                                            class="required">*</span></label>
                                    <div class="col-md-4">
                                        <div id="signature-pad">
                                            <div
                                                style="border:solid 1px teal; width:360px;height:110px;padding:3px;position:relative;">
                                                <div id="note" onmouseover="my_function();">The signature should be
                                                    inside box
                                                </div>
                                                <canvas id="the_canvas" width="350px" height="100px"></canvas>
                                              </div>
                                            <div style="margin:10px;">
                                                <input type="hidden" id="signature" name="signature">
                                                <button type="button" id="clear_btn" class="btn btn-danger"
                                                        data-action="clear"><span
                                                        class="glyphicon glyphicon-remove"></span> Clear Signature
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <img src="{{asset('public/app_image/uploads/signatures/vendor/'.$vendor->vendor_signature)}}" alt="No Signature">

                                    </div>
                                </div>
                            </div>
                        </div>
                        <footer class="panel-footer">
                            <div class="row">
                                <div class="col-md-offset-10 col-md-2">
                                    <button type="button" id="save_vendor" name="save" value="1"
                                            class="btn btn btn-dark btn-block center" data-action="save-png">
                                        <i class="fas fa-plus-circle"></i> Save
                                    </button>
                                </div>
                            </div>
                        </footer>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('myscript')
    <script>
        var wrapper = document.getElementById("signature-pad");
        var clearButton = wrapper.querySelector("[data-action=clear]");
        var savePNGButton = wrapper.querySelector("[data-action=save-png]");
        var canvas = wrapper.querySelector("canvas");
        var el_note = document.getElementById("note");
        var save_vendor = document.getElementById("save_vendor");
        var signaturePad;
        signaturePad = new SignaturePad(canvas);

        clearButton.addEventListener("click", function (event) {
            document.getElementById("note").innerHTML = "The signature should be inside box";
            signaturePad.clear();
        });
        document.getElementById('save_vendor').addEventListener("click", function() {
            if (signaturePad.isEmpty()) {
                alert("Please provide signature first.");
                event.preventDefault();
            } else {
                var canvas = document.getElementById("the_canvas");
                var dataUrl = canvas.toDataURL();
                document.getElementById("signature").value = dataUrl;
                document.getElementById("vendor_form").submit();
            }
        });
        // savePNGButton.addEventListener("click", function (event) {
        //     if (signaturePad.isEmpty()) {
        //         alert("Please provide signature first.");
        //         event.preventDefault();
        //     } else {
        //         var canvas = document.getElementById("the_canvas");
        //         var dataUrl = canvas.toDataURL();
        //         document.getElementById("signature").value = dataUrl;
        //         document.getElementById("participant_form").submit();
        //     }
        // });

        function my_function() {
            document.getElementById("note").innerHTML = "";
        }
    </script>
@endsection
