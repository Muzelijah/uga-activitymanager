@extends('layouts.main')
@section('content')
    <section class="panel">
        <header class="panel-heading">
            <h4 class="panel-title">Select Ground</h4>
        </header>
        <form method="post" action="{{route('wallet.withdrawals.reports')}}">
            @csrf
            <div class="panel-body">
                <div class="row mb-sm">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">Student Name <span class="required">*</span></label>
                            <select name="student_id" class="form-control select2-hidden-accessible"
                                    id="student_id" data-plugin-selecttwo="" data-width="100%" tabindex="-1"
                                    aria-hidden="true" required>
                                <option value="">Select</option>
                                @if(!empty($students))
                                    @foreach($students as $student)
                                        <option
                                            value="{{$student->id}}" @if($student->id == $selected_student->id) selected @endif>{{$student->student_sur_name}} {{$student->student_other_names}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>

                    <div class="col-md-4 mb-sm">
                        <div class="form-group">
                            <label class="control-label">Date From</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="far fa-calendar-alt"></i></span>
                                <input type="text" class="form-control" name="date_from"
                                       value="{{old('client_join_date',date('Y-m-d', strtotime("-1 months")))}}"
                                       data-plugin-datepicker
                                       data-plugin-options='{ "todayHighlight" : true }'/>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 mb-sm">
                        <div class="form-group">
                            <label class="control-label">Date To</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="far fa-calendar-alt"></i></span>
                                <input type="text" class="form-control" name="date_to"
                                       value="{{old('client_join_date',date('Y-m-d'))}}"
                                       data-plugin-datepicker
                                       data-plugin-options='{ "todayHighlight" : true }'/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <footer class="panel-footer">
                <div class="row">
                    <div class="col-md-offset-10 col-md-2">
                        <button type="submit" name="search" value="1" class="btn btn-default btn-block"><i
                                class="fas fa-filter"></i> Filter
                        </button>
                    </div>
                </div>
            </footer>
        </form>

    </section>
    @if(!empty($withdrawals))
        <section class="panel">
            <header class="panel-heading">
                <h4 class="panel-title"><i class="fas fa-list-ol"></i> Wallet Withdrawals</h4>
            </header>
            <div class="panel-body">
                <div class="mb-md mt-md">

                    <table class="table table-bordered table-hover mb-none table-condensed table-export">
                        <thead>
                        <tr>
                            <th>SN</th>
                            <th>Terminal Names</th>
                            <th>Student Names</th>
                            <th>Receipt No</th>
                            <th>Wallet No</th>
                            <th>Amount</th>
                            <th>Withdrawn Date</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php $rows = 1; @endphp
                        @foreach($withdrawals as $withdrawal)
                            <tr>
                                <td>{{$rows++}}</td>
                                <td>{{$withdrawal->wallet_terminal->wallet_terminal_name}}</td>
                                <td>{{$withdrawal->student->student_sur_name}} {{$withdrawal->student->student_other_names}}</td>
                                <td>{{$withdrawal->receipt_no}}</td>
                                <td>{{$withdrawal->wallet_card_no}}</td>
                                <td>{{number_format($withdrawal->wallet_withdrawn_amount,0)}}</td>
                                <td>{{$withdrawal->wallet_withdrawal_date}}</td>
                            </tr>
                        @endforeach

                        </tbody>
                        <tr>
                            <td>Total</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>{{number_format($withdrawals->sum('wallet_withdrawn_amount'),0)}}</td>
                            <td></td>
                        </tr>
                    </table>

                </div>
            </div>
        </section>
    @endif
@endsection
