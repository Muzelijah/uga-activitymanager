@extends('layouts.main')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <section class="panel">
                <form action="" method="POST">
                    @csrf
                    <header class="panel-heading">
                        <h4 class="panel-title"><i class="fas fa-home"></i> Dashboard</h4>
                    </header>
                    <div class="panel-body">

                        <!-- academic details-->
                        <div class="row">
                            <div class="col-md-12 col-lg-12 col-sm-12">
                                <div class="panel">
                                    <div class="row widget-row-in">
                                        <div class="col-lg-3 col-sm-2 ">
                                            <div class="panel-body">
                                                <div class="widget-col-in row">
                                                    <div class="col-md-6 col-sm-6 col-xs-6"> <i class="fas fa-share-alt"></i>
                                                        <h5 class="text-muted">Area Offices</h5>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                                        <h3 class="counter text-right mt-md text-primary">{{count($area_offices)}}</h3>
                                                    </div>
                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                        <div class="box-top-line line-color-primary">
                                                            <span class="text-muted text-uppercase"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-sm-2">
                                            <div class="panel-body">
                                                <div class="widget-col-in row">
                                                    <div class="col-md-6 col-sm-6 col-xs-6"> <i class="fas fa-tasks"></i>
                                                        <h5 class="text-muted">Approved Activities</h5> </div>
                                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                                        <h3 class="counter text-right mt-md text-primary">{{count($approved_activites)}}</h3>
                                                    </div>
                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                        <div class="box-top-line line-color-primary">
                                                            <span class="text-muted text-uppercase"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-sm-2 ">
                                            <div class="panel-body">
                                                <div class="widget-col-in row">
                                                    <div class="col-md-6 col-sm-6 col-xs-6"> <i class="fas fa-tasks" ></i>
                                                        <h5 class="text-muted">Running Activities</h5></div>
                                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                                        <h3 class="counter text-right mt-md text-primary">{{count($all_activities)}}</h3>
                                                    </div>
                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                        <div class="box-top-line line-color-primary">
                                                            <span class="text-muted text-uppercase"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-sm-2 ">
                                            <div class="panel-body">
                                                <div class="widget-col-in row">
                                                    <div class="col-md-6 col-sm-6 col-xs-6"> <i class="fas fa-paper-plane" ></i>
                                                        <h5 class="text-muted">Messages Available</h5></div>
                                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                                        <h3 class="counter text-right mt-md text-primary">{{$messages}}</h3>
                                                    </div>
                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                        <div class="box-top-line line-color-primary">
                                                            <span class="text-muted text-uppercase"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="headers-line">
                                    <i class="fas fa-list-alt"></i> Upcoming Activities
                                </div>
                                <table class="table table-bordered table-hover mb-none table-condensed table-export">
                                    <thead>
                                    <tr>
                                        <th>Activity Code</th>
                                        <th>Activity Name</th>
                                        <th>Activity Type</th>
                                        <th>Activity Lead</th>
                                        <th>Location</th>
                                        <th>Start Date</th>
                                        <th>End Date</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(!empty($upcoming_activities))
                                        @foreach($upcoming_activities as $activity)
                                            <tr>
                                                <td>{{$activity->activity_code}}</td>
                                                <td>{{$activity->activity_name}}</td>
                                                <td>{{$activity->activity_type->activity_type_name}}</td>
                                                <td>{{$activity->user->name}}</td>
                                                <td>{{$activity->activity_venue_address}}</td>
                                                <td>{{$activity->activity_start_date}}</td>
                                                <td>{{$activity->activity_end_date}}</td>
                                            </tr>
                                        @endforeach

                                    @endif
                                    </tbody>
                                </table>
                            </div>

                        </div>

                    </div>

                    <footer class="panel-footer">

                    </footer>
                </form>
            </section>
        </div>
    </div>
@endsection

