
<!doctype html>
<html class="fixed sidebar-left-sm sidebar-light">
<!-- html header -->
@include('partials.header')
<!-- <body class="loading-overlay-showing" data-loading-overlay> -->
<body class="loading-overlay-showing" data-loading-overlay>
	<!-- page preloader -->
	<div class="loading-overlay dark">
        <div class="ring-loader">
            Loading <span></span>
        </div>
    </div>
{{--    @include('sweet.alert')--}}
{{--    @include('sweetalert::alert')--}}
    <section class="body">
		<!-- top navbar-->

        @include('partials.topbar')
		<div class="inner-wrapper">
			<!-- sidebar -->
            @include('partials.sidebar')
			<!-- page main content -->
			<section role="main" class="content-body">
				<header class="page-header">
					<a class="page-title-icon" href="{{url('/dashboard')}}"><i class="fas fa-home"></i></a>
					<h2>{{ $title }}</h2>
				</header>
                @yield('content')
			</section>
		</div>
	</section>

	<!-- JS Script -->
    @include('partials.script')
    @yield('myscript')
    @include('sweetalert::alert', ['cdn' => "https://cdn.jsdelivr.net/npm/sweetalert2@9"])
    @php
	$alertclass = "";
	$alert_message = "";

	@endphp

{{--		<script type="text/javascript">--}}
{{--			swal({--}}
{{--				toast: true,--}}
{{--				position: 'top-end',--}}
{{--				type: '@php echo $alertclass @endphp',--}}
{{--				title: '@php echo $alert_message @endphp',--}}
{{--				confirmButtonClass: 'btn btn-default',--}}
{{--				buttonsStyling: false,--}}
{{--				timer: 8000--}}
{{--			})--}}
{{--		</script>--}}


	<!-- sweetalert box -->
	<script type="text/javascript">
		function confirm_modal(delete_url) {
			swal({
				title: "Are You Sure You",
				text: "Want to delete this",
				type: "warning",
				showCancelButton: true,
				confirmButtonClass: "btn btn-default swal2-btn-default",
				cancelButtonClass: "btn btn-default swal2-btn-default",
				confirmButtonText: "Yes",
				cancelButtonText: "Cancel",
				buttonsStyling: false,
				footer: "Deleted Note"
			}).then((result) => {
				if (result.value) {
					$.ajax({
						url: delete_url,
						type: "POST",
						success:function(data) {
							swal({
							title: "Deleted",
							text: "Information Deleted",
							buttonsStyling: false,
							showCloseButton: true,
							focusConfirm: false,
							confirmButtonClass: "btn btn-default swal2-btn-default",
							type: "success"
							}).then((result) => {
								if (result.value) {
									location.reload();
								}
							});
						}
					});
				}
			});
		}
	</script>

</body>
</html>
