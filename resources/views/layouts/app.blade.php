
<!doctype html>
<html class="fixed sidebar-left-sm sidebar-light">
{{--<script type="text/javascript" src="http://js.nicedit.com/nicEdit-latest.js"></script>--}}
{{--<script type="text/javascript">--}}
{{--    //<![CDATA[--}}
{{--    bkLib.onDomLoaded(function() {--}}
{{--        new nicEditor({fullPanel : true}).panelInstance('area1');--}}
{{--    });--}}
{{--    </script>--}}
<script src="//cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        $('.ckeditor').ckeditor();

    });
</script>
<!-- html header -->
@include('partials.headernew')
<!-- <body class="loading-overlay-showing" data-loading-overlay> -->
<body class="loading-overlay-showing" data-loading-overlay>
{{--<!-- page preloader -->--}}
<div class="loading-overlay dark">
    <div class="ring-loader">
        Loading <span></span>
    </div>
</div>
{{--@include('sweet.alert')--}}

<section class="body">
    <!-- top navbar-->
    {{--    @include('partials.topbar')--}}
    <div class="inner-wrapper">

        <!-- sidebar -->
    {{--    @include('partials.sidebar')--}}
    <!-- page main content -->
        <div class="container">
        {{--            <section class="panel">--}}
        <!--                --><?php //echo form_open(base_url('flutterwave/create_transaction'), array('class' => 'form-horizontal frm-submit')); ?>
            {{--                <header class="panel-heading">--}}
            {{--                    <h3 class="panel-title">{{$title}}</h3>--}}
            {{--                </header>--}}
            @yield('content')
            {{--            </section>--}}
        </div>

    </div>
</section>

<!-- JS Script -->
@include('partials.scriptnew')
@yield('myscript')
@include('sweetalert::alert', ['cdn' => "https://cdn.jsdelivr.net/npm/sweetalert2@9"])
@php
    $alertclass = "";
    $alert_message = "";

@endphp

<script type="text/javascript">
    {{--swal({--}}
    {{--    toast: true,--}}
    {{--    position: 'top-end',--}}
    {{--    type: '@php echo $alertclass @endphp',--}}
    {{--    title: '@php echo $alert_message @endphp',--}}
    {{--    confirmButtonClass: 'btn btn-default',--}}
    {{--    buttonsStyling: false,--}}
    {{--    timer: 8000--}}
    {{--})--}}
</script>


<!-- sweetalert box -->
<script type="text/javascript">
    function confirm_modal(delete_url) {
        swal({
            title: "Are You Sure You",
            text: "Want to delete this",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn btn-default swal2-btn-default",
            cancelButtonClass: "btn btn-default swal2-btn-default",
            confirmButtonText: "Yes",
            cancelButtonText: "Cancel",
            buttonsStyling: false,
            footer: "Deleted Note"
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: delete_url,
                    type: "POST",
                    success:function(data) {
                        swal({
                            title: "Deleted",
                            text: "Information Deleted",
                            buttonsStyling: false,
                            showCloseButton: true,
                            focusConfirm: false,
                            confirmButtonClass: "btn btn-default swal2-btn-default",
                            type: "success"
                        }).then((result) => {
                            if (result.value) {
                                location.reload();
                            }
                        });
                    }
                });
            }
        });
    }
</script>

</body>
</html>
