@extends('layouts.main')
@section('content')
    <section class="panel">
        <div class="tabs-custom">
            <ul class="nav nav-tabs">
                <li>
                    <a href="{{route('users.data')}}">
                        <i class="fas fa-list-ul"></i> Users List
                    </a>
                </li>
                <li class="active">
                    <a href="#edit" data-toggle="tab">
                        <i class="far fa-edit"></i> Edit User
                    </a>
                </li>

            </ul>
            <div class="tab-content">
{{--                <div class="tab-pane box active mb-md" id="list">--}}
{{--                    <table class="table table-bordered table-hover mb-none table-condensed table-export">--}}
{{--                        <thead>--}}
{{--                        <tr>--}}
{{--                            <th>SN</th>--}}
{{--                            <th>Reference</th>--}}
{{--                            <th>Names</th>--}}
{{--                            <th>Gender</th>--}}
{{--                            <th>NIN</th>--}}
{{--                            <th>Age</th>--}}
{{--                            <th>Contact</th>--}}
{{--                            <th>Next of Kin</th>--}}
{{--                            <th>Kin Contact</th>--}}
{{--                            <th>Pre Medical</th>--}}
{{--                            <th>Own Passport</th>--}}
{{--                            <th>District</th>--}}
{{--                            <th>Last Updated By</th>--}}
{{--                            <th>Actions</th>--}}
{{--                        </tr>--}}
{{--                        </thead>--}}
{{--                        <tbody>--}}
{{--                        @if(!empty($applicants))--}}
{{--                            @foreach($applicants as $applicant)--}}
{{--                                <tr>--}}
{{--                                    <td>{{$applicant->id}}</td>--}}
{{--                                    <td>{{$applicant->applicant_ref}}</td>--}}
{{--                                    <td>{{$applicant->applicant_sur_name}} {{$applicant->applicant_given_name}}</td>--}}
{{--                                    <td>{{$applicant->applicant_gender}}</td>--}}
{{--                                    <td>{{$applicant->applicant_nin}}</td>--}}
{{--                                    <td>{{$applicant->applicant_age}}</td>--}}
{{--                                    <td>{{$applicant->applicant_contact}}</td>--}}
{{--                                    <td>{{$applicant->applicant_next_of_kin}}</td>--}}
{{--                                    <td>{{$applicant->applicant_kin_mobile}}</td>--}}
{{--                                    <td>{{$applicant->applicant_pre_medical}}</td>--}}
{{--                                    <td>{{$applicant->applicant_own_passport}}</td>--}}
{{--                                    <td>{{$applicant->district->district_name}}</td>--}}
{{--                                    <td>{{$applicant->user->name}}</td>--}}
{{--                                    <td><a href="{{route('applicants.edit',['applicant'=>$applicant->id])}}"--}}
{{--                                           data-toggle="tooltip" data-original-title="Edit"--}}
{{--                                           class="btn btn-circle btn-default icon">--}}
{{--                                            <i class="fas fa-pen-nib"></i>--}}
{{--                                        </a></td>--}}
{{--                                </tr>--}}
{{--                            @endforeach--}}
{{--                        @endif--}}
{{--                        </tbody>--}}
{{--                    </table>--}}
{{--                </div>--}}

                <div class="tab-pane active" id="edit">
                    <form action="{{route('users.update',['user'=>$user->id])}}" method="post">
                        @csrf
                        <div class="panel-body">

                            <!-- academic details-->
                            <div class="headers-line">
                                <i class="fas fa-school"></i> General Information
                            </div>


                            <div class="row">
                                <div class="col-md-3 mb-sm">
                                    <div class="form-group">
                                        <label class="control-label">Area Offices<span
                                                class="required">*</span></label>
                                        <select multiple name="area_offices[]" class="form-control select2-hidden-accessible"
                                                id="roles" data-plugin-selecttwo="" data-width="100%" tabindex="-1"
                                                aria-hidden="true">
                                            <option value="">Select</option>
                                            @if(!empty($area_offices))
                                                @foreach($area_offices as $area_office)
                                                    <option
                                                        value="{{$area_office->id}}">{{$area_office->area_office_name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3 mb-sm">
                                    <div class="form-group">
                                        <label class="control-label">Names<span
                                                class="required">*</span></label>
                                        <input type="text" class="form-control" required name="name"
                                               value="{{old('name', $user->name)}}"/>
                                        <span class="error"></span>
                                    </div>
                                </div>
                                <div class="col-md-3 mb-sm">
                                    <div class="form-group">
                                        <label class="control-label">Email<span
                                                class="required">*</span></label>
                                        <input type="text" class="form-control" required name="email"
                                               value="{{old('name', $user->email)}}"/>
                                        <span class="error"></span>
                                    </div>
                                </div>
                                <div class="col-md-3 mb-sm">
                                    <div class="form-group">
                                        <label class="control-label">Phone<span
                                                class="required">*</span></label>
                                        <input type="text" class="form-control" required name="phone"
                                               value="{{old('name', $user->phone)}}"/>
                                        <span class="error"></span>
                                    </div>
                                </div>
                                <div class="col-md-3 mb-sm">
                                    <div class="form-group">
                                        <label class="control-label">Roles<span
                                                class="required">*</span></label>
                                        <select name="roles[]" class="form-control select2-hidden-accessible"
                                                id="roles" data-plugin-selecttwo="" data-width="100%" tabindex="-1"
                                                aria-hidden="true">
                                            <option value="">Select</option>
                                            @if(!empty($roles))
                                                @foreach($roles as $role)
                                                    <option
                                                        value="{{$role->name}}" @if($role->name == $user->getRoleNames()->first())) selected @endif>{{$role->name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3 mb-sm">
                                    <div class="form-group">
                                        <label class="control-label">Password<span
                                                class="required">*</span></label>
                                        <input type="password" class="form-control" required name="password"
                                               value="{{old('name', '')}}"/>
                                        <span class="error"></span>
                                    </div>
                                </div>
                                <div class="col-md-3 mb-sm">
                                    <div class="form-group">
                                        <label class="control-label">Confirm Password<span
                                                class="required">*</span></label>
                                        <input type="password" class="form-control" required name="confirm_password"
                                               value="{{old('name', '')}}"/>
                                        <span class="error"></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <footer class="panel-footer">
                            <div class="row">
                                <div class="col-md-offset-10 col-md-2">
                                    <button type="submit" name="save" value="1" class="btn btn btn-default btn-block">
                                        <i class="fas fa-plus-circle"></i> Save
                                    </button>
                                </div>
                            </div>
                        </footer>
                    </form>
                </div>

            </div>
        </div>
    </section>
@endsection
