@extends('layouts.main')
@section('content')
    <section class="panel">
        <div class="tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#list" data-toggle="tab">
                        <i class="fas fa-list-ul"></i> Users List
                    </a>
                </li>
                <li>
                    <a href="#add" data-toggle="tab">
                        <i class="far fa-edit"></i> Register New User
                    </a>
                </li>

            </ul>
            <div class="tab-content">
                <div class="tab-pane box active mb-md" id="list">
                    <table class="table table-bordered table-hover mb-none table-condensed table-export">
                        <thead>
                        <tr>
                            <th>SN</th>
                            <th>Names</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Role</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(!empty($users))
                            @foreach($users as $user)
                                <tr>
                                    <td>{{$user->id}}</td>
                                    <td>{{$user->name}}</td>
                                    <td>{{$user->email}}</td>
                                    <td>{{$user->phone}}</td>
                                    <td>{{$user->getRoleNames()->first()}}</td>
                                    <td><a href="{{route('users.edit',['user'=>$user->id])}}"
                                           data-toggle="tooltip" data-original-title="Edit"
                                           class="btn btn-circle btn-default icon">
                                            <i class="fas fa-pen-nib"></i>
                                        </a></td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>

                <div class="tab-pane" id="add">
                    <form action="{{route('users.store')}}" method="post">
                        @csrf
                        <div class="panel-body">

                            <!-- academic details-->
                            <div class="headers-line">
                                <i class="fas fa-school"></i> General Information
                            </div>


                            <div class="row">
                                <div class="col-md-3 mb-sm">
                                    <div class="form-group">
                                        <label class="control-label">Area Offices<span
                                                class="required">*</span></label>
                                        <select multiple name="area_offices[]" class="form-control select2-hidden-accessible"
                                                id="roles" data-plugin-selecttwo="" data-width="100%" tabindex="-1"
                                                aria-hidden="true">
                                            <option value="">Select</option>
                                            @if(!empty($area_offices))
                                                @foreach($area_offices as $area_office)
                                                    <option
                                                        value="{{$area_office->id}}">{{$area_office->area_office_name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3 mb-sm">
                                    <div class="form-group">
                                        <label class="control-label">Names<span
                                                class="required">*</span></label>
                                        <input type="text" class="form-control" required name="name"
                                               value="{{old('name', '')}}"/>
                                        <span class="error"></span>
                                    </div>
                                </div>
                                <div class="col-md-3 mb-sm">
                                    <div class="form-group">
                                        <label class="control-label">Email<span
                                                class="required">*</span></label>
                                        <input type="text" class="form-control" required name="email"
                                               value="{{old('name', '')}}"/>
                                        <span class="error"></span>
                                    </div>
                                </div>
                                <div class="col-md-3 mb-sm">
                                    <div class="form-group">
                                        <label class="control-label">Phone<span
                                                class="required">*</span></label>
                                        <input type="text" class="form-control" required name="phone"
                                               value="{{old('name', '')}}"/>
                                        <span class="error"></span>
                                    </div>
                                </div>
                                <div class="col-md-3 mb-sm">
                                    <div class="form-group">
                                        <label class="control-label">Roles<span
                                                class="required">*</span></label>
                                        <select name="roles[]" class="form-control select2-hidden-accessible"
                                                id="roles" data-plugin-selecttwo="" data-width="100%" tabindex="-1"
                                                aria-hidden="true">
                                            <option value="">Select</option>
                                            @if(!empty($roles))
                                                @foreach($roles as $role)
                                                    <option
                                                        value="{{$role->name}}">{{$role->name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3 mb-sm">
                                    <div class="form-group">
                                        <label class="control-label">Password<span
                                                class="required">*</span></label>
                                        <input type="password" class="form-control" required name="password"
                                               value="{{old('name', '')}}"/>
                                        <span class="error"></span>
                                    </div>
                                </div>
                                <div class="col-md-3 mb-sm">
                                    <div class="form-group">
                                        <label class="control-label">Confirm Password<span
                                                class="required">*</span></label>
                                        <input type="password" class="form-control" required name="confirm_password"
                                               value="{{old('name', '')}}"/>
                                        <span class="error"></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <footer class="panel-footer">
                            <div class="row">
                                <div class="col-md-offset-10 col-md-2">
                                    <button type="submit" name="save" value="1" class="btn btn btn-default btn-block">
                                        <i class="fas fa-plus-circle"></i> Save
                                    </button>
                                </div>
                            </div>
                        </footer>
                    </form>
                </div>

            </div>
        </div>
    </section>
@endsection
