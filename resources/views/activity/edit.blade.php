@extends('layouts.main')
@section('content')
    <section class="panel" xmlns="http://www.w3.org/1999/html">
        <div class="tabs-custom">
            <ul class="nav nav-tabs">
                <li>
                    <a href="{{route('activities.create')}}">
                        <i class="fas fa-list-ul"></i> Unverified Activities
                    </a>
                </li>
                <li  class="active">
                    <a href="#add"  data-toggle="tab">
                        <i class="fas fa-list-ul"></i> Edit Activity
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane box  mb-md" id="list">


                </div>

                <div class="tab-pane active" id="add">
                    <form action="{{route('activities.update',['activity'=>$activity->activity_ref])}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="panel-body">
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Area Office<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <select name="area_office_id"
                                                class="form-control select2-hidden-accessible"
                                                id="product_category_id" data-plugin-selecttwo="" data-width="100%"
                                                tabindex="-1"
                                                aria-hidden="true" required>
                                            @if(!empty($area_offices))
                                                @foreach($area_offices as $area_office)
                                                    <option value="{{$area_office->id}}" @if($area_office->id == $activity->area_office_id) selected @endif>{{$area_office->area_office_name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Activity Name<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <input type="text" required class="form-control text-uppercase"
                                               name="activity_name"
                                               value="{{old('activity_name',$activity->activity_name)}}"/>
                                        <span class="error"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Activity Type<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <select name="activity_type_id"
                                                class="form-control select2-hidden-accessible"
                                                id="product_category_id" data-plugin-selecttwo="" data-width="100%"
                                                tabindex="-1"
                                                aria-hidden="true" required>
                                            <option value="">Select</option>
                                            @if(!empty($activity_types))
                                                @foreach($activity_types as $activity_type)
                                                    <option value="{{$activity_type->id}}" @if($activity_type->id == $activity->activity_type_id) selected @endif>{{$activity_type->activity_type_name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Budget Code<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <select name="budget_code_id"
                                                class="form-control select2-hidden-accessible"
                                                id="budget_code_id" data-plugin-selecttwo="" data-width="100%"
                                                tabindex="-1"
                                                aria-hidden="true" required>
                                            <option value="">Select</option>
                                            @if(!empty($budget_codes))
                                                @foreach($budget_codes as $budget_code)
                                                    <option value="{{$budget_code->id}}" @if($budget_code->id == $activity->budget_code_id) selected @endif>{{$budget_code->budget_code_name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Budget Amount<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <input type="number" min="10000" required class="form-control text-uppercase"
                                               name="estimated_budget_amount"
                                               value="{{old('estimated_budget_amount',$activity->estimated_budget_amount)}}"/>
                                        <span class="error"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Expected Participants<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <input type="number" min="5" required class="form-control text-uppercase"
                                               name="estimated_participants"
                                               value="{{old('estimated_participants',$activity->estimated_participants)}}"/>
                                        <span class="error"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Start Date<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" data-plugin-datepicker
                                               data-plugin-options='{"todayHighlight" : true}' name="activity_start_date" value="{{$activity->activity_start_date}}" autocomplete="off" />
                                        <span class="error"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">End Date<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" data-plugin-datepicker
                                               data-plugin-options='{"todayHighlight" : true}' name="activity_end_date" value="{{$activity->activity_end_date}}" autocomplete="off" />
                                        <span class="error"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Activity Time<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="far fa-clock"></i></span>
                                                    <select class="form-control" name="activity_start_time">
                                                        <option value="00:00" @if(date('H',strtotime($activity->activity_start_time)) == "00") selected @endif>00:00 AM</option>
                                                        <option value="01:00" @if(date('H',strtotime($activity->activity_start_time)) == "01") selected @endif>01:00 AM</option>
                                                        <option value="02:00" @if(date('H',strtotime($activity->activity_start_time)) == "02") selected @endif>02:00 AM</option>
                                                        <option value="03:00" @if(date('H',strtotime($activity->activity_start_time)) == "03") selected @endif>03:00 AM</option>
                                                        <option value="04:00" @if(date('H',strtotime($activity->activity_start_time)) == "04") selected @endif>04:00 AM</option>
                                                        <option value="05:00" @if(date('H',strtotime($activity->activity_start_time)) == "05") selected @endif>05:00 AM</option>
                                                        <option value="06:00" @if(date('H',strtotime($activity->activity_start_time)) == "06") selected @endif>06:00 AM</option>
                                                        <option value="07:00" @if(date('H',strtotime($activity->activity_start_time)) == "07") selected @endif>07:00 AM</option>
                                                        <option value="08:00" @if(date('H',strtotime($activity->activity_start_time)) == "08") selected @endif>08:00 AM</option>
                                                        <option value="09:00" @if(date('H',strtotime($activity->activity_start_time)) == "09") selected @endif>09:00 AM</option>
                                                        <option value="10:00" @if(date('H',strtotime($activity->activity_start_time)) == "10") selected @endif>10:00 AM</option>
                                                        <option value="11:00" @if(date('H',strtotime($activity->activity_start_time)) == "11") selected @endif>11:00 AM</option>
                                                        <option value="12:00" @if(date('H',strtotime($activity->activity_start_time)) == "12") selected @endif>12:00 PM</option>
                                                        <option value="13:00" @if(date('H',strtotime($activity->activity_start_time)) == "13") selected @endif>01:00 PM</option>
                                                        <option value="14:00" @if(date('H',strtotime($activity->activity_start_time)) == "14") selected @endif>02:00 PM</option>
                                                        <option value="15:00" @if(date('H',strtotime($activity->activity_start_time)) == "15") selected @endif>03:00 PM</option>
                                                        <option value="16:00" @if(date('H',strtotime($activity->activity_start_time)) == "16") selected @endif>04:00 PM</option>
                                                        <option value="17:00" @if(date('H',strtotime($activity->activity_start_time)) == "17") selected @endif>05:00 PM</option>
                                                        <option value="18:00" @if(date('H',strtotime($activity->activity_start_time)) == "18") selected @endif>06:00 PM</option>
                                                        <option value="19:00" @if(date('H',strtotime($activity->activity_start_time)) == "19") selected @endif>07:00 PM</option>
                                                        <option value="20:00" @if(date('H',strtotime($activity->activity_start_time)) == "20") selected @endif>08:00 PM</option>
                                                        <option value="21:00" @if(date('H',strtotime($activity->activity_start_time)) == "21") selected @endif>09:00 PM</option>
                                                        <option value="22:00" @if(date('H',strtotime($activity->activity_start_time)) == "22") selected @endif>10:00 PM</option>
                                                        <option value="23:00" @if(date('H',strtotime($activity->activity_start_time)) == "23") selected @endif>11:00 PM</option>
                                                    </select>
                                                </div>
                                                <span class="error"></span>
                                            </div>
                                            <div class="col-xs-6">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="far fa-clock"></i></span>
                                                    <select class="form-control" name="activity_end_time">
                                                        <option value="00:00" @if(date('H',strtotime($activity->activity_end_time)) == "00") selected @endif>00:00 AM</option>
                                                        <option value="01:00" @if(date('H',strtotime($activity->activity_end_time)) == "01") selected @endif>01:00 AM</option>
                                                        <option value="02:00" @if(date('H',strtotime($activity->activity_end_time)) == "02") selected @endif>02:00 AM</option>
                                                        <option value="03:00" @if(date('H',strtotime($activity->activity_end_time)) == "03") selected @endif>03:00 AM</option>
                                                        <option value="04:00" @if(date('H',strtotime($activity->activity_end_time)) == "04") selected @endif>04:00 AM</option>
                                                        <option value="05:00" @if(date('H',strtotime($activity->activity_end_time)) == "05") selected @endif>05:00 AM</option>
                                                        <option value="06:00" @if(date('H',strtotime($activity->activity_end_time)) == "06") selected @endif>06:00 AM</option>
                                                        <option value="07:00" @if(date('H',strtotime($activity->activity_end_time)) == "07") selected @endif>07:00 AM</option>
                                                        <option value="08:00" @if(date('H',strtotime($activity->activity_end_time)) == "08") selected @endif>08:00 AM</option>
                                                        <option value="09:00" @if(date('H',strtotime($activity->activity_end_time)) == "09") selected @endif>09:00 AM</option>
                                                        <option value="10:00" @if(date('H',strtotime($activity->activity_end_time)) == "10") selected @endif>10:00 AM</option>
                                                        <option value="11:00" @if(date('H',strtotime($activity->activity_end_time)) == "11") selected @endif>11:00 AM</option>
                                                        <option value="12:00" @if(date('H',strtotime($activity->activity_end_time)) == "12") selected @endif>12:00 PM</option>
                                                        <option value="13:00" @if(date('H',strtotime($activity->activity_end_time)) == "13") selected @endif>01:00 PM</option>
                                                        <option value="14:00" @if(date('H',strtotime($activity->activity_end_time)) == "14") selected @endif>02:00 PM</option>
                                                        <option value="15:00" @if(date('H',strtotime($activity->activity_end_time)) == "15") selected @endif>03:00 PM</option>
                                                        <option value="16:00" @if(date('H',strtotime($activity->activity_end_time)) == "16") selected @endif>04:00 PM</option>
                                                        <option value="17:00" @if(date('H',strtotime($activity->activity_end_time)) == "17") selected @endif>05:00 PM</option>
                                                        <option value="18:00" @if(date('H',strtotime($activity->activity_end_time)) == "18") selected @endif>06:00 PM</option>
                                                        <option value="19:00" @if(date('H',strtotime($activity->activity_end_time)) == "19") selected @endif>07:00 PM</option>
                                                        <option value="20:00" @if(date('H',strtotime($activity->activity_end_time)) == "20") selected @endif>08:00 PM</option>
                                                        <option value="21:00" @if(date('H',strtotime($activity->activity_end_time)) == "21") selected @endif>09:00 PM</option>
                                                        <option value="22:00" @if(date('H',strtotime($activity->activity_end_time)) == "22") selected @endif>10:00 PM</option>
                                                        <option value="23:00" @if(date('H',strtotime($activity->activity_end_time)) == "23") selected @endif>11:00 PM</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Venue Address<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <textarea required class="form-control"  name="activity_venue_address">{{$activity->activity_venue_address}}</textarea>
                                        <span class="error"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Additional Information<span
                                            class="required"></span></label>
                                    <div class="col-md-6">
                                        <textarea class="form-control"  name="additional_information">{{$activity->additional_information}}</textarea>
                                        <span class="error"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Concept Note<span
                                            class="required"></span></label>
                                    <div class="col-md-6">
                                        <input type="file"  name="activity_concept_note"/>
                                        <span class="error"></span>
                                    </div>
                                </div>


                            </div>
                        </div>
                        <footer class="panel-footer">
                            <div class="row">
                                <div class="col-md-offset-10 col-md-2">
                                    <button type="submit" name="save" value="1"
                                            class="btn btn btn-dark btn-block center">
                                        <i class="fas fa-plus-circle"></i> Save
                                    </button>
                                </div>
                            </div>
                        </footer>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
