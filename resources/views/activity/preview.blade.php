@extends('layouts.main')
@section('content')
    <section class="panel" xmlns="http://www.w3.org/1999/html">
        <div class="tab-content">
        <div class="row">
            <div class="col-lg-9">
                <div class="headers-line">
                    <i class="fas fa-list"></i> Concept Note Preview
                </div>
                <object data="{{url('/public/app_image/'.$activity->activity_concept_note.'#view=FitH')}}"
                        type="application/pdf" width=100% height=1200px
                        data="Assembly.pdf?#zoom=100&scrollbar=1&toolbar=1&navpanes=0&view=FitH"></object>
            </div>
            <div class="col-lg-3">

            </div>
        </div>
        </div>
    </section>
@endsection
