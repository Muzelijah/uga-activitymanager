@extends('layouts.main')
@section('content')
    <section class="panel" xmlns="http://www.w3.org/1999/html">
        <div class="tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#list"  data-toggle="tab">
                        <i class="fas fa-list-ul"></i> Add Attendants
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane box active mb-md" id="list">
                    <form action="{{route('my.activities.add.attendance',['activity'=>$activity->activity_ref])}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="panel-body">

                            <div class="row">
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Select Attendant<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <select name="activity_participant_id" id="activity_participant_id" class="form-control select2-hidden-accessible" id="question_id" data-plugin-selecttwo="" data-width="100%" tabindex="-1" aria-hidden="true">
                                            <option value="">Select</option>
                                            @if(!empty($participants_in_activity))
                                                @foreach($participants_in_activity as $participant)
                                                    <option value="{{$participant->activity_participant_ref}}">{{$participant->participant->participant_title}} {{$participant->participant->participant_first_name}} {{$participant->participant->participant_other_names}} - ({{$participant->participant->participant_mobile}})</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group"  style="display: block;" id="sms_code">
                                    <label class="col-md-3 control-label text-right"><span
                                            class="required"></span></label>
                                    <div class="col-md-6">
                                        <button type="button" onclick="send_code();" id="btn_send_code" name="btn_send_code" value="1"
                                                class="btn btn btn-dark btn-block center">
                                            <i class="fas fa-paper-plane"></i> Send Code
                                        </button>
                                    </div>
                                </div>
                                <div class="form-group" id="scode" style="display: none">
                                    <label class="col-md-3 control-label text-right">Enter Code<span
                                            class="required"></span></label>
                                    <div class="col-md-6">
                                        <input type="number" min="10000" required class="form-control text-uppercase"
                                               name="attendance_code"
                                               value="{{old('attendance_code')}}"/>
                                        <span class="error"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label text-right"><span
                                    class="required"></span></label>
                            <div class="col-md-6">
                                <button type="submit" name="save" value="1"
                                        class="btn btn btn-primary text-dark btn-block center">
                                    <i class="fas fa-user-edit"></i> Add Attendant
                                </button>
                            </div>
                        </div>

                    </form>
                    <div class="row mt-5 pt-5">
                        <div class="col-lg-12">
                            <div class="headers-line">
                                <i class="fas fa-list"></i> Attendance List
                            </div>
                        </div>
                    </div>
                    <table class="table table-bordered table-hover mb-none table-condensed table_default">
                        <thead>
                        <tr>
                            <th>
                                SN
                            </th>
                            <th>Participant Names</th>
                            <th>Title</th>
                            <th>Participant Mobile</th>
                            <th>Participant Email</th>
                            <th>ORG</th>
                            <th>Logged Date and Time</th>
                            <th>Signature</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(!empty($participants_in_attendance))
                            @foreach($participants_in_attendance as $participant)
                                @php
                                    $path = asset('public/app_image/uploads/signatures/'.$participant->participant->participant_sign); //this is the image path
                                    $type = pathinfo($path, PATHINFO_EXTENSION);
                                    $data = file_get_contents($path);
                                    $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
                                @endphp
                                <tr>
                                    <td>{{$participant->id}}</td>
                                    <td>{{$participant->participant->participant_first_name}} {{$participant->participant->participant_other_names}}</td>
                                    <td>{{$participant->participant->participant_title}}</td>
                                    <td>{{$participant->participant->participant_mobile}}</td>
                                    <td>{{$participant->participant->participant_email}}</td>
                                    <td>{{$participant->participant->participant_organization}}</td>
                                    <td>{{$participant->activity_attendance_date}}</td>
                                    <td><img height="40px" src="@php echo $base64;  @endphp" alt="No Signature"></td>

                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                    <a href="{{route('my.activities.download.attendance',['activity' => $activity->activity_ref])}}"
                       data-toggle="tooltip" data-original-title="Download Attendance with Signatures"
                       class="btn btn-circle btn-dark icon">
                        <i class="fas fa-download"></i>
                    </a>

                </div>
            </div>
        </div>
    </section>
@endsection
@section('myscript')
    <script type="text/javascript">
        function send_code() {
            var activity_participant = null;
            var participant = document.getElementById("activity_participant_id");
            activity_participant = participant.options[participant.selectedIndex].value;
           //console.log(activity_participant);
            var my_url = "{{url('/my/activities/')}}";
            my_url = my_url+'/'+activity_participant+'/sendcode';
            console.log(my_url);
            if(activity_participant !== ""){
                $.ajax({
                    type: 'GET', //THIS NEEDS TO BE GET
                    url: my_url,
                    success: function (data) {
                        var txt_code  = document.getElementById('scode');
                        var btn_code  = document.getElementById('sms_code');
                        txt_code.style.display = 'block';
                        btn_code.style.display = 'none';
                    },
                    error: function() {
                        console.log(data);
                    }
                });

               // alert(activity_participant);
            }else{
                alert('Select an Attendant');
            }

        }
    </script>
@endsection
