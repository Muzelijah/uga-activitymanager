@extends('layouts.main')
@section('content')
    <section class="panel" xmlns="http://www.w3.org/1999/html">
        <div class="tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#list"  data-toggle="tab">
                        <i class="fas fa-list-ul"></i> Added Participants
                    </a>
                </li>
                <li>
                    <a href="#add"  data-toggle="tab">
                        <i class="fas fa-edit"></i> Add Participants
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane box active mb-md" id="list">
{{--                    <div class="row mt-5 pt-5">--}}
{{--                        <div class="col-lg-12">--}}
{{--                            <div class="headers-line">--}}
{{--                                <i class="fas fa-list"></i> Order Details--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
                    <div class="row">
                        <div class="col-md-2">
                            <a class="btn btn-block btn-dark" href="{{route('my.activities.send.invitations',['activity'=>$activity->activity_ref])}}">Send Invitations</a>
                        </div>
                        <div class="col-md-10">

                        </div>

                    </div>
                    <table class="table table-bordered table-hover mb-none table-condensed table-export">
                        <thead>
                        <tr>
                            <th>
                                SN
                            </th>
                            <th>Participant Names</th>
                            <th>Participant Mobile</th>
                            <th>Participant Email</th>
                            <th>Invitation Sent</th>
                            <th>Confirmed Attendance</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(!empty($added_participants))
                            @foreach($added_participants as $participant)
                                <tr>
                                    <td>{{$participant->id}}</td>
                                    <td>{{$participant->participant->participant_title}} {{$participant->participant->participant_first_name}} {{$participant->participant->participant_other_names}}</td>
                                    <td>{{$participant->participant->participant_mobile}}</td>
                                    <td>{{$participant->participant->participant_email}}</td>
                                    <td>@if($participant->send_invitations)
                                            <span class='label label-success-custom'>Yes</span>
                                        @else
                                            <span class='label label-danger-custom'>No</span>
                                        @endif
                                    </td>
                                    <td>@if($participant->confirmed_attendance)
                                            <span class='label label-success-custom'>Yes</span>
                                        @else
                                            <span class='label label-danger-custom'>No</span>
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{route('participants.edit',['participant' => $participant->activity_participant_ref])}}"
                                           data-toggle="tooltip" data-original-title="Edit"
                                           class="btn btn-circle btn-dark icon">
                                            <i class="fas fa-pen-nib"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>


                </div>

                <div class="tab-pane" id="add">
                    <form action="{{route('my.activities.store.participants',['activity'=>$activity->activity_ref])}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="panel-body">

                            <div class="row">
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Select Participants<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <select name="total_participants[]" class="form-control select2-hidden-accessible" multiple="" id="question_id" data-plugin-selecttwo="" data-width="100%" tabindex="-1" aria-hidden="true">
                                            @if(!empty($participants))
                                                @foreach($participants as $participant)
                                                    <option value="{{$participant->participant_ref}}">{{$participant->participant_title}} {{$participant->participant_first_name}} {{$participant->participant_other_names}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <footer class="panel-footer">
                            <div class="row">
                                <div class="col-md-offset-10 col-md-2">
                                    <button type="submit" name="save" value="1"
                                            class="btn btn btn-dark btn-block center">
                                        <i class="fas fa-plus-circle"></i> Save
                                    </button>
                                </div>
                            </div>
                        </footer>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
