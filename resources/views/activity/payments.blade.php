@extends('layouts.main')
@section('content')
    <section class="panel" xmlns="http://www.w3.org/1999/html">
        <div class="tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#list" data-toggle="tab">
                        <i class="fas fa-list-ul"></i> Activity Payment List
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane box active mb-md" id="list">
                    {{--                    <div class="row mt-5 pt-5">--}}
                    {{--                        <div class="col-lg-12">--}}
                    {{--                            <div class="headers-line">--}}
                    {{--                                <i class="fas fa-list"></i> Order Details--}}
                    {{--                            </div>--}}
                    {{--                        </div>--}}
                    {{--                    </div>--}}
                    <form method="post"
                          action="{{route('my.activities.update.payments',['activity'=>$activity->activity_ref])}}"
                          enctype="multipart/form-data">
                        @csrf
                        <table class="table table-bordered table-hover mb-none table-condensed table-export">
                            <thead>
                            <tr>
                                <th>
                                    SN
                                </th>
                                <th>Participant Names</th>
                                <th>Title</th>
                                <th>Organization</th>
                                <th>Days Attended</th>
                                <th>Daily Rate</th>
                                <th class="text-right">Line Total</th>
                                <th class="text-right">Fuel Refund</th>
                                <th>Deductibles</th>
                                <th>Mobile Number</th>
                                <th>Mobile Names</th>
                                <th>Signature</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(!empty($activity_payments))
                                @foreach($activity_payments as $activity_payment)
                                    <tr>
                                        <td>{{$activity_payment->id}}</td>
                                        <td>{{$activity_payment->participant->participant_first_name}} {{$activity_payment->participant->participant_other_names}}</td>
                                        <td>{{$activity_payment->participant->participant_title}}</td>
                                        <td>{{$activity_payment->participant->participant_organization}}</td>
                                        <td>{{$activity_payment->days_attended}}</td>
                                        <td>
                                            @if($activity_payment->activity_payment_status_id == 1)
                                                <input type="number"
                                                       name="daily_rate[{{$activity_payment->activity_payment_ref}}]"
                                                       id="desc" value="{{$activity_payment->daily_rate}}"
                                                       required="required"/>
                                            @else
                                                {{number_format($activity_payment->daily_rate,0)}}
                                            @endif
                                        </td>
                                        <td class="text-right">{{number_format($activity_payment->line_total,0)}}</td>
                                        <td class="text-right">
                                            @if($activity_payment->activity_payment_status_id == 1)
                                                <input type="number"
                                                       name="fuel_refund[{{$activity_payment->activity_payment_ref}}]"
                                                       id="desc"
                                                       value="{{$activity_payment->fuel_refund}}"
                                                       required="required"/>
                                            @else
                                                {{number_format($activity_payment->fuel_refund,0)}}
                                            @endif
                                        </td>
                                        <td class="text-right">
                                            @if($activity_payment->activity_payment_status_id == 1)
                                                <input type="number"
                                                       name="amount_deducted[{{$activity_payment->activity_payment_ref}}]"
                                                       id="desc"
                                                       value="{{$activity_payment->amount_deducted}}"
                                                       required="required"/>
                                            @else
                                                {{number_format($activity_payment->amount_deducted,0)}}
                                            @endif
                                        </td>
                                        <td>
                                            @if($activity_payment->activity_payment_status_id == 1)
                                                <input type="text"
                                                       name="mobile[{{$activity_payment->activity_payment_ref}}]"
                                                       id="desc"
                                                       value="{{$activity_payment->participant_payment_mobile}}"
                                                       required="required"/>
                                            @else
                                                {{$activity_payment->participant_payment_mobile}}
                                            @endif
                                        </td>
                                        <td>
                                            @if($activity_payment->activity_payment_status_id == 1)
                                                <input type="text"
                                                       name="mobile_names[{{$activity_payment->activity_payment_ref}}]"
                                                       id="desc"
                                                       value="{{$activity_payment->participant_payment_mobile_names}}"
                                                       required="required"/>
                                            @else
                                                {{$activity_payment->participant_payment_mobile_names}}
                                            @endif
                                        </td>
                                        <td><img height="30px"
                                                 src="{{asset('public/app_image/uploads/signatures/'.$activity_payment->participant_sign)}}">
                                        </td>
                                    </tr>
                                @endforeach
                                <tr>
                                    <th>
                                        Total
                                    </th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th class="text-right">{{number_format($activity_payments->sum('line_total'))}}</th>
                                    <th class="text-right">{{number_format($activity_payments->sum('fuel_refund'))}}</th>
                                    <th class="text-right">{{number_format($activity_payments->sum('amount_deducted'))}}</th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                        <div class="row">
                            @if($activity_payments->max('activity_payment_status_id') == 1)
                                <div class="col-lg-4">
                                    <button type="submit" name="save" value="1"
                                            class="btn btn btn-dark btn-block center">
                                        <i class="fas fa-edit"></i> Update Payment Information
                                    </button>
                                </div>
                                <div class="col-lg-4">
                                    <a class="btn btn btn-primary btn-block center"
                                       href="{{route('my.activities.send.payments.for.verification',['activity'=>$activity->activity_ref])}}">
                                        <i class="fas fa-check"></i> Send For Verification</a>
                                </div>
                                <div class="col-lg-4">

                                </div>
                            @endif
                        </div>

                    </form>
                </div>

                <div class="tab-pane" id="add">

                </div>
            </div>
        </div>
    </section>
@endsection
@section('myscript')
    <script>
        var wrapper = document.getElementById("signature-pad");
        var clearButton = wrapper.querySelector("[data-action=clear]");
        var savePNGButton = wrapper.querySelector("[data-action=save-png]");
        var canvas = wrapper.querySelector("canvas");
        var el_note = document.getElementById("note");
        var save_participant = document.getElementById("save_participant");
        var signaturePad;
        signaturePad = new SignaturePad(canvas);

        clearButton.addEventListener("click", function (event) {
            document.getElementById("note").innerHTML = "The signature should be inside box";
            signaturePad.clear();
        });
        document.getElementById('save_participant').addEventListener("click", function () {
            if (signaturePad.isEmpty()) {
                alert("Please provide signature first.");
                event.preventDefault();
            } else {
                var canvas = document.getElementById("the_canvas");
                var dataUrl = canvas.toDataURL();
                document.getElementById("signature").value = dataUrl;
                document.getElementById("participant_form").submit();
            }
        });
        // savePNGButton.addEventListener("click", function (event) {
        //     if (signaturePad.isEmpty()) {
        //         alert("Please provide signature first.");
        //         event.preventDefault();
        //     } else {
        //         var canvas = document.getElementById("the_canvas");
        //         var dataUrl = canvas.toDataURL();
        //         document.getElementById("signature").value = dataUrl;
        //         document.getElementById("participant_form").submit();
        //     }
        // });

        function my_function() {
            document.getElementById("note").innerHTML = "";
        }
    </script>
@endsection
