@extends('layouts.main')
@section('content')
    <section class="panel">
        <div class="tabs-custom">
            <ul class="nav nav-tabs">
                <li @if($status == 2) class="active" @endif>
                    <a href="{{route('my.activities.data',['status'=>2])}}">
                        <i class="fas fa-list-ul"></i> Pending Approval
                    </a>
                </li>
                <li @if($status == 3) class="active" @endif>
                    <a href="{{route('my.activities.data',['status'=>3])}}">
                        <i class="fas fa-list-ul"></i>Approved
                    </a>
                </li>
                <li @if($status == 4) class="active" @endif>
                    <a href="{{route('my.activities.data',['status'=>4])}}">
                        <i class="fas fa-list-ul"></i> Complete
                    </a>
                </li>
                <li @if($status == 5) class="active" @endif>
                    <a href="{{route('my.activities.data',['status'=>5])}}">
                        <i class="fas fa-list-ul"></i> Returned
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane box active mb-md" id="list">
                    {{--                    <div class="row mt-5 pt-5">--}}
                    {{--                        <div class="col-lg-12">--}}
                    {{--                            <div class="headers-line">--}}
                    {{--                                <i class="fas fa-list"></i> Order Details--}}
                    {{--                            </div>--}}
                    {{--                        </div>--}}
                    {{--                    </div>--}}
                    <form action="{{route('activities.status')}}" method="post">

                        @csrf
                        <table class="table table-bordered table-hover mb-none table-condensed table-export">
                            <thead>
                            <tr>
                                <th>
                                    <div class="checkbox-replace">
                                        <label class="i-checks">
                                            <input type="checkbox" id="selectAllchkbox" name="selected_activities"
                                                   value="1">
                                            <i></i>
                                        </label>
                                    </div>
                                </th>
                                <th>Activity Code</th>
                                <th>Activity Name</th>
                                <th>Activity Type</th>
                                <th>Activity Lead</th>
                                <th>Budget Code</th>
                                <th class="text-right">Budget Amount</th>
                                <th>Start Date</th>
                                <th>End Date</th>
                                <th>Status</th>
                                <th>Remark</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(!empty($activities))
                                @foreach($activities as $activity)
                                    <tr>
                                        <td class="checked-area">
                                            <div class="checkbox-replace">
                                                <label class="i-checks">
                                                    <input type="checkbox" class="user_checkbox"
                                                           name="selected_activities[]"
                                                           value="{{$activity->activity_ref}}">
                                                    <i></i>
                                                </label>
                                            </div>
                                        </td>
                                        <td>{{$activity->activity_code}}</td>
                                        <td>{{$activity->activity_name}}</td>
                                        <td>{{$activity->activity_type->activity_type_name}}</td>
                                        <td>{{$activity->user->name}}</td>
                                        <td>{{$activity->budget_code->budget_code_name}}</td>
                                        <td class="text-right">{{number_format($activity->estimated_budget_amount,0)}}</td>
                                        <td>{{$activity->activity_start_date}}</td>
                                        <td>{{$activity->activity_end_date}}</td>
                                        <td>{{$activity->activity_status->activity_status_name}}</td>
                                        <td>{{$activity->activity_remark}}</td>
                                        <td>
                                            @if($activity->activity_concept_note != null || $activity->activity_concept_note != "")
                                                <a target="_blank" href="{{url('public/app_image/'.$activity->activity_concept_note)}}"
                                                   data-toggle="tooltip" data-original-title="Preview Concept Note"
                                                   class="btn btn-circle btn-dark icon">
                                                    <i class="fas fa-eye"></i>
                                                </a>
                                            @endif
                                            @if($status == 3)
                                                <a href="{{route('my.activities.add.participants',['activity' => $activity->activity_ref])}}"
                                                   data-toggle="tooltip" data-original-title="Add Participants"
                                                   class="btn btn-circle btn-dark icon">
                                                    <i class="fas fa-users"></i>
                                                </a>
                                                <a href="{{route('my.activities.log.attendance',['activity' => $activity->activity_ref])}}"
                                                   data-toggle="tooltip" data-original-title="Log Attendance"
                                                   class="btn btn-circle btn-dark icon">
                                                    <i class="fas fa-tasks"></i>
                                                </a>
                                                <a href="{{route('my.activities.close',['activity' => $activity->activity_ref])}}"
                                                   data-toggle="tooltip" data-original-title="Close Activity"
                                                   class="btn btn-circle btn-danger icon">
                                                    <i class="icon icon-close"></i>
                                                </a>
                                            @endif
                                            @if($status == 4)
                                                    <a href="{{route('my.activities.generate.payments',['activity' => $activity->activity_ref])}}"
                                                       data-toggle="tooltip" data-original-title="Generate Payments"
                                                       class="btn btn-circle btn-dark icon">
                                                        <i class="fas fa-dollar-sign"></i>
                                                    </a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                <tr>
                                    <th>Total</th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th class="text-right">{{number_format($activities->sum('estimated_budget_amount'),0)}}</th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                        @if($hide == 0)
                            <div class="row">
                                <div class="col-md-6">
                                    <textarea class="form-control pb-5" name="activity_remark"
                                              placeholder="Enter Remark" required></textarea>
                                </div>
                                <div class="col-md-2">
                                    <select name="new_activity_status_id" class="form-control select2-hidden-accessible"
                                            id="school_class_id" data-plugin-selecttwo="" data-width="100%"
                                            tabindex="-1"
                                            aria-hidden="true" required>
                                        <option value="">--Select Status--</option>
                                        @if(!empty($activity_statuses))
                                            @foreach($activity_statuses as $activity_status)
                                                <option
                                                    value="{{$activity_status->id}}">{{$activity_status->activity_status_name}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <button type="submit" name="save" value="1"
                                            class="form-control btn btn btn-dark right">
                                        <i class="fas fa-sync"></i> Process Activities
                                    </button>
                                </div>

                                <div class="col-md-2 left">
                                </div>
                            </div>
                        @endif
                    </form>

                </div>

                <div class="tab-pane" id="add">
                    <form action="#" method="post">
                        @csrf
                        <div class="panel-body">

                            <div class="row">
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Product Category<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <select name="product_category_id"
                                                class="form-control select2-hidden-accessible"
                                                id="product_category_id" data-plugin-selecttwo="" data-width="100%"
                                                tabindex="-1"
                                                aria-hidden="true" required>
                                            <option value="">Select</option>
                                            @if(!empty($categories))
                                                @foreach($categories as $category)
                                                    <option
                                                        value="{{$category->id}}">{{$category->product_category_name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Product Code<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <input type="text" required class="form-control text-uppercase"
                                               name="product_code"
                                               value="{{old('product_code')}}"/>
                                        <span class="error"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Product Name<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <input type="text" required class="form-control text-uppercase"
                                               name="product_name"
                                               value="{{old('product_name')}}"/>
                                        <span class="error"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Product Unit Price<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <input type="number" required class="form-control text-uppercase"
                                               name="product_unit_price"
                                               value="{{old('product_unit_price')}}"/>
                                        <span class="error"></span>
                                    </div>
                                </div>


                            </div>
                        </div>
                        <footer class="panel-footer">
                            <div class="row">
                                <div class="col-md-offset-10 col-md-2">
                                    <button type="submit" name="save" value="1"
                                            class="btn btn btn-dark btn-block center">
                                        <i class="fas fa-plus-circle"></i> Save
                                    </button>
                                </div>
                            </div>
                        </footer>
                    </form>
                </div>
            </div>
    </section>
@endsection
