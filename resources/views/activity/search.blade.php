@extends('layouts.main')
@section('content')
    <section class="panel">
        <div class="tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#">
                        <i class="fas fa-list-ul"></i> Activities
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane box active mb-md" id="list">
                    {{--                    <div class="row mt-5 pt-5">--}}
                    {{--                        <div class="col-lg-12">--}}
                    {{--                            <div class="headers-line">--}}
                    {{--                                <i class="fas fa-list"></i> Order Details--}}
                    {{--                            </div>--}}
                    {{--                        </div>--}}
                    {{--                    </div>--}}

                    <table class="table table-bordered table-hover mb-none table-condensed table-export">
                        <thead>
                        <tr>
                            <th>Activity Code</th>
                            <th>Activity Name</th>
                            <th>Activity Type</th>
                            <th>Activity Lead</th>
                            <th>Location</th>
                            <th>Start Date</th>
                            <th>End Date</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(!empty($activities))
                            @foreach($activities as $activity)
                                <tr>
                                    <td>{{$activity->activity_code}}</td>
                                    <td>{{$activity->activity_name}}</td>
                                    <td>{{$activity->activity_type->activity_type_name}}</td>
                                    <td>{{$activity->user->name}}</td>
                                    <td>{{$activity->activity_venue_address}}</td>
                                    <td>{{$activity->activity_start_date}}</td>
                                    <td>{{$activity->activity_end_date}}</td>
                                </tr>
                            @endforeach

                        @endif
                        </tbody>
                    </table>

                </div>

                <div class="tab-pane" id="add">

                </div>
            </div>
    </section>
@endsection
