@extends('layouts.main')
@section('content')
    <section class="panel" xmlns="http://www.w3.org/1999/html">
        <div class="tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#list"  data-toggle="tab">
                        <i class="fas fa-list-ul"></i> Unapproved Payments
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane box active mb-md" id="list">
{{--                    <div class="row mt-5 pt-5">--}}
{{--                        <div class="col-lg-12">--}}
{{--                            <div class="headers-line">--}}
{{--                                <i class="fas fa-list"></i> Order Details--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
                    <form action="{{route('activities.status')}}" method="post">

                        @csrf
                        <table class="table table-bordered table-hover mb-none table-condensed table_default">
                            <thead>
                            <tr>
                                <th>Activity Code</th>
                                <th>Activity Name</th>
                                <th>Activity Type</th>
                                <th>Activity Lead</th>
                                <th>Budget Code</th>
                                <th class="text-right">Budget Amount</th>
                                <th>Start Date</th>
                                <th>End Date</th>
{{--                                <th>Remark</th>--}}
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(!empty($closed_activities))
                                @php $total = 0; @endphp
                                @foreach($closed_activities as $closed_activity)
                                        @php $total = $total + $closed_activity->payments->sum('line_total') @endphp
                                    <tr>
                                        <td>{{$closed_activity->activity_code}}</td>
                                        <td>{{$closed_activity->activity_name}}</td>
                                        <td>{{$closed_activity->activity_type->activity_type_name}}</td>
                                        <td>{{$closed_activity->user->name}}</td>
                                        <td>{{$closed_activity->budget_code->budget_code_name}}</td>
                                       <td class="text-right">{{number_format($closed_activity->payments->sum('line_total'),0)}}</td>
                                        <td>{{$closed_activity->activity_start_date}}</td>
                                        <td>{{$closed_activity->activity_end_date}}</td>
{{--                                        <td>{{$activity->activity_remark}}</td>--}}
                                        <td>
                                            <a href="{{route('activities.approve.payments',['activity' => $closed_activity->activity_ref])}}"
                                               data-toggle="tooltip" data-original-title="Approve"
                                               class="btn btn-circle btn-primary icon">
                                                <i class="fas fa-check-double"></i>
                                            </a>
                                            <a href="{{route('activities.show.payments.list',['activity' => $closed_activity->activity_ref])}}"
                                               data-toggle="tooltip" target="_blank" data-original-title="View Payment List"
                                               class="btn btn-circle btn-dark icon">
                                                <i class="fas fa-eye"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                <tr>
                                    <th>Total</th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th class="text-right">{{number_format($total,0)}}</th>
                                    <th></th>
                                    <th></th>
{{--                                    <th></th>--}}
                                    <th></th>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                        <div class="row">


                            <div class="col-md-2">
{{--                                <button type="submit" name="save" value="1"--}}
{{--                                        class="form-control btn btn btn-dark right">--}}
{{--                                    <i class="fas fa-send"></i> Send For Approval--}}
{{--                                </button>--}}
                            </div>
                            <div class="col-md-2">
                                <input type="hidden" name="new_activity_status_id" value="2">
                                <input type="hidden" name="activity_remark" value="Sent For Approval">
                            </div>

                            <div class="col-md-10 left">
                            </div>
                        </div>
                    </form>

                </div>

                <div class="tab-pane" id="add">
                    <form action="{{route('activities.store')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="panel-body">

                            <div class="row">
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Activity Name<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <input type="text" required class="form-control text-uppercase"
                                               name="activity_name"
                                               value="{{old('activity_name')}}"/>
                                        <span class="error"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Activity Type<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <select name="activity_type_id"
                                                class="form-control select2-hidden-accessible"
                                                id="product_category_id" data-plugin-selecttwo="" data-width="100%"
                                                tabindex="-1"
                                                aria-hidden="true" required>
                                            <option value="">Select</option>
                                            @if(!empty($activity_types))
                                                @foreach($activity_types as $activity_type)
                                                    <option value="{{$activity_type->id}}">{{$activity_type->activity_type_name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Budget Code<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <select name="budget_code_id"
                                                class="form-control select2-hidden-accessible"
                                                id="budget_code_id" data-plugin-selecttwo="" data-width="100%"
                                                tabindex="-1"
                                                aria-hidden="true" required>
                                            <option value="">Select</option>
                                            @if(!empty($budget_codes))
                                                @foreach($budget_codes as $budget_code)
                                                    <option value="{{$budget_code->id}}">{{$budget_code->budget_code_name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Budget Amount<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <input type="number" min="10000" required class="form-control text-uppercase"
                                               name="estimated_budget_amount"
                                               value="{{old('estimated_budget_amount')}}"/>
                                        <span class="error"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Expected Participants<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <input type="number" min="5" required class="form-control text-uppercase"
                                               name="estimated_participants"
                                               value="{{old('estimated_participants')}}"/>
                                        <span class="error"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Start Date<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" data-plugin-datepicker
                                               data-plugin-options='{"todayHighlight" : true}' name="activity_start_date" value="{{date('Y-m-d')}}" autocomplete="off" />
                                        <span class="error"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">End Date<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" data-plugin-datepicker
                                               data-plugin-options='{"todayHighlight" : true}' name="activity_end_date" value="{{date('Y-m-d')}}" autocomplete="off" />
                                        <span class="error"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Activity Time<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="far fa-clock"></i></span>
                                                    <select class="form-control" name="activity_start_time">
                                                        <option value="00:00" @if(date('H') == "00") selected @endif>00:00 AM</option>
                                                        <option value="01:00" @if(date('H') == "01") selected @endif>01:00 AM</option>
                                                        <option value="02:00" @if(date('H') == "02") selected @endif>02:00 AM</option>
                                                        <option value="03:00" @if(date('H') == "03") selected @endif>03:00 AM</option>
                                                        <option value="04:00" @if(date('H') == "04") selected @endif>04:00 AM</option>
                                                        <option value="05:00" @if(date('H') == "05") selected @endif>05:00 AM</option>
                                                        <option value="06:00" @if(date('H') == "06") selected @endif>06:00 AM</option>
                                                        <option value="07:00" @if(date('H') == "07") selected @endif>07:00 AM</option>
                                                        <option value="08:00" @if(date('H') == "08") selected @endif>08:00 AM</option>
                                                        <option value="09:00" @if(date('H') == "09") selected @endif>09:00 AM</option>
                                                        <option value="10:00" @if(date('H') == "10") selected @endif>10:00 AM</option>
                                                        <option value="11:00" @if(date('H') == "11") selected @endif>11:00 AM</option>
                                                        <option value="12:00" @if(date('H') == "12") selected @endif>12:00 PM</option>
                                                        <option value="13:00" @if(date('H') == "13") selected @endif>01:00 PM</option>
                                                        <option value="14:00" @if(date('H') == "14") selected @endif>02:00 PM</option>
                                                        <option value="15:00" @if(date('H') == "15") selected @endif>03:00 PM</option>
                                                        <option value="16:00" @if(date('H') == "16") selected @endif>04:00 PM</option>
                                                        <option value="17:00" @if(date('H') == "17") selected @endif>05:00 PM</option>
                                                        <option value="18:00" @if(date('H') == "18") selected @endif>06:00 PM</option>
                                                        <option value="19:00" @if(date('H') == "19") selected @endif>07:00 PM</option>
                                                        <option value="20:00" @if(date('H') == "20") selected @endif>08:00 PM</option>
                                                        <option value="21:00" @if(date('H') == "21") selected @endif>09:00 PM</option>
                                                        <option value="22:00" @if(date('H') == "22") selected @endif>10:00 PM</option>
                                                        <option value="23:00" @if(date('H') == "23") selected @endif>11:00 PM</option>
                                                    </select>
                                                </div>
                                                <span class="error"></span>
                                            </div>
                                            <div class="col-xs-6">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="far fa-clock"></i></span>
                                                    <select class="form-control" name="activity_end_time">
                                                        <option value="00:00" @if(date('H') == "00") selected @endif>00:00 AM</option>
                                                        <option value="01:00" @if(date('H') == "01") selected @endif>01:00 AM</option>
                                                        <option value="02:00" @if(date('H') == "02") selected @endif>02:00 AM</option>
                                                        <option value="03:00" @if(date('H') == "03") selected @endif>03:00 AM</option>
                                                        <option value="04:00" @if(date('H') == "04") selected @endif>04:00 AM</option>
                                                        <option value="05:00" @if(date('H') == "05") selected @endif>05:00 AM</option>
                                                        <option value="06:00" @if(date('H') == "06") selected @endif>06:00 AM</option>
                                                        <option value="07:00" @if(date('H') == "07") selected @endif>07:00 AM</option>
                                                        <option value="08:00" @if(date('H') == "08") selected @endif>08:00 AM</option>
                                                        <option value="09:00" @if(date('H') == "09") selected @endif>09:00 AM</option>
                                                        <option value="10:00" @if(date('H') == "10") selected @endif>10:00 AM</option>
                                                        <option value="11:00" @if(date('H') == "11") selected @endif>11:00 AM</option>
                                                        <option value="12:00" @if(date('H') == "12") selected @endif>12:00 PM</option>
                                                        <option value="13:00" @if(date('H') == "13") selected @endif>01:00 PM</option>
                                                        <option value="14:00" @if(date('H') == "14") selected @endif>02:00 PM</option>
                                                        <option value="15:00" @if(date('H') == "15") selected @endif>03:00 PM</option>
                                                        <option value="16:00" @if(date('H') == "16") selected @endif>04:00 PM</option>
                                                        <option value="17:00" @if(date('H') == "17") selected @endif>05:00 PM</option>
                                                        <option value="18:00" @if(date('H') == "18") selected @endif>06:00 PM</option>
                                                        <option value="19:00" @if(date('H') == "19") selected @endif>07:00 PM</option>
                                                        <option value="20:00" @if(date('H') == "20") selected @endif>08:00 PM</option>
                                                        <option value="21:00" @if(date('H') == "21") selected @endif>09:00 PM</option>
                                                        <option value="22:00" @if(date('H') == "22") selected @endif>10:00 PM</option>
                                                        <option value="23:00" @if(date('H') == "23") selected @endif>11:00 PM</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Venue Address<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <textarea required class="form-control"  name="activity_venue_address"></textarea>
                                        <span class="error"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Additional Information<span
                                            class="required"></span></label>
                                    <div class="col-md-6">
                                        <textarea class="form-control"  name="additional_information"></textarea>
                                        <span class="error"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Concept Note<span
                                            class="required"></span></label>
                                    <div class="col-md-6">
                                        <input type="file"  name="activity_concept_note"/>
                                        <span class="error"></span>
                                    </div>
                                </div>


                            </div>
                        </div>
                        <footer class="panel-footer">
                            <div class="row">
                                <div class="col-md-offset-10 col-md-2">
                                    <button type="submit" name="save" value="1"
                                            class="btn btn btn-dark btn-block center">
                                        <i class="fas fa-plus-circle"></i> Save
                                    </button>
                                </div>
                            </div>
                        </footer>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
