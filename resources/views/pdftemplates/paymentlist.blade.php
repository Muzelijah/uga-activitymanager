<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"  "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <style>
        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
        }

        .text-right {
            text-align: right;
        }

        .text-center {
            text-align: center;
        }

        .text-left {
            text-align: left;
        }
    </style>
    <title>{{ $title }}</title>
</head>
<body>
<h2>{{ $title }}</h2>
<table width="100%" border="1" class="table table-bordered table-hover mb-none table-condensed table_default">
    <thead>
    <tr>
        <th class="text-center">
            SN
        </th>
        <th class="text-left">Participant Names</th>
        <th class="text-left">Registered </br>Number</th>
        <th class="text-left">Registered </br>Names</th>
        <th class="text-center">Title</th>
        <th>Organization</th>
        <th class="text-right">Daily</br> Rate</th>
        <th class="text-center">Days</br>Attended</th>
        <th class="text-right">Total</br>Paid</th>
        <th class="text-right">Transport</br>Refund</th>
        <th class="text-center">Amount</br>Deducted</th>
        <th class="text-center">Signature</th>
        <th class="text-center">Total</br>to be Paid</th>
    </tr>
    </thead>
    <tbody>
    @if(!empty($payments))
        @php
            $line_total=0;
            $fuel_refund=0;
            $amount_deducted=0;
            $rows = 1;
        @endphp
        @foreach($payments as $payment)
            @php
                $path = asset('public/app_image/uploads/signatures/'.$payment['participant_sign']); //this is the image path
                $type = pathinfo($path, PATHINFO_EXTENSION);
                $data = file_get_contents($path);
                $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
            $line_total = $line_total + $payment['line_total'];
            $fuel_refund = $fuel_refund + $payment['fuel_refund'];
            $amount_deducted = $amount_deducted + $payment['amount_deducted'];

            @endphp
            <tr>
                <td class="text-center">{{$rows++}}</td>
                <td class="text-left">{{$payment['>participant']['participant_first_name']}} {{$payment['participant']['participant_other_names']}}</td>
                <td class="text-center">{{$payment['participant_payment_mobile']}}</td>
                <td class="text-center">{{$payment['participant_payment_mobile_names']}}</td>
                <td class="text-center">{{$payment['participant']['participant_title']}}</td>
                <td>{{$payment['participant']['participant_organization']}}</td>
                <td class="text-right">{{number_format($payment['daily_rate'],0)}}</td>
                <td class="text-center">{{$payment['days_attended']}}</td>
                <td class="text-right">{{number_format($payment['line_total'],0)}}</td>
                <td class="text-right">{{number_format($payment['fuel_refund'],0)}}</td>
                <td class="text-right">{{number_format($payment['amount_deducted'],0)}}</td>
                <td class="text-center"><img height="40px" src="{{$base64}}" alt="No Signature"></td>
                <td class="text-right">{{number_format($payment['line_total']+$payment['fuel_refund']-$payment['amount_deducted'],0)}}</td>
            </tr>
        @endforeach
        <tr>
            <th class="text-center">
                Total
            </th>
            <th class="text-left"></th>
            <th class="text-left"></th>
            <th class="text-left"></th>
            <th class="text-center"></th>
            <th></th>
            <th class="text-right"></th>
            <th class="text-center"></th>
            <th class="text-right"></th>
            <th class="text-right"></th>
            <th class="text-center"></th>
            <th class="text-center"></th>
            <th class="text-right">{{number_format($line_total+$fuel_refund-$amount_deducted,0)}}</th>
        </tr>
    @endif
    </tbody>
</table>
</body>
</html>
