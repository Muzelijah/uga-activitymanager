<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"  "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <style>
        .text-right {
            text-align: right;
        }

        .text-center {
            text-align: center;
        }

        .text-left {
            text-align: left;
        }
    </style>
    <title>{{ $title }}</title>
</head>
<body>
<table border="0" style="border: 0;" cellspacing="0" cellpadding="0" width="100%">
    <tr>
        <td>
            <table width="100%" border="0">
                <tr>
                    <td width="33%"></td>
                    <td width="33%"><h3 style="text-align: center">MPO PRICE ANALYSIS SHEET</h3></td>
                    <td width="33%"></td>
                </tr>
                <tr>
                    <td width="33%"></td>
                    <td width="33%">
                        <div style="text-align: center">RFQ Reference
                            : {{$procurement_request['rfq_reference_number']}}</div>
                    </td>
                    <td width="33%"></td>
                </tr>
                <tr>
                    <td width="33%"></td>
                    <td width="33%">
                        <div style="text-align: center">{{$procurement_request['activity']['activity_name']}} </div>
                    </td>
                    <td width="33%"></td>
                </tr>
                <tr>
                    <td width="33%"></td>
                    <td width="33%">
                        <div style="text-align: center">
                            From {{date('jS F Y',strtotime($procurement_request['delivery_from_date']))}}
                            To {{date('jS F Y',strtotime($procurement_request['delivery_to_date']))}}</div>
                    </td>
                    <td width="33%"></td>
                </tr>
                <tr>
                    <td width="33%"></td>
                    <td width="33%">
                        <div style="text-align: center">Exchange Rate
                            : {{number_format($procurement_request['exchange_rate'])}}</div>
                    </td>
                    <td width="33%"></td>
                </tr>
            </table>
        </td>
    </tr>

    <tr>
        <td><h4 style="text-align: center">Responses / offers of invited vendors</h4></td>
    </tr>
    <tr>
        <td>
            <table style="border: 1px solid black;  border-collapse: collapse;" border="1" width="100%">
                <tr style="border: 1px solid black;">
                    <th style="border: 1px solid black;">SN</th>
                    <th style="border: 1px solid black;">Description</th>
                    <th style="border: 1px solid black;">Measure</th>
                    <th style="border: 1px solid black;">Qty</th>
                    <th style="border: 1px solid black;">Total Days</th>
                    @php
                        foreach($procurement_request['invitations'] as $invitation){
                    @endphp
                    <th colspan="2" style="border: 1px solid black;">{{$invitation['vendor']['vendor_name']}}</th>

                    @php
                        }
                    @endphp
                </tr>
                <tr style="border: 1px solid black;">
                    <th style="border: 1px solid black;"></th>
                    <th style="border: 1px solid black;"></th>
                    <th style="border: 1px solid black;"></th>
                    <th style="border: 1px solid black;"></th>
                    <th style="border: 1px solid black;"></th>
                    @php
                        foreach($procurement_request['invitations'] as $invitation){
                    @endphp
                    <th style="border: 1px solid black; text-align: right;">Unit Price</th>
                    <th style="border: 1px solid black; text-align: right;">Total Price</th>
                    @php
                        }
                    @endphp
                </tr>
                @php
                    $rows = 1;
                foreach($procurement_request_items as $item){

                @endphp
                <tr style="border: 1px solid black;">
                    <td style="border: 1px solid black; text-align: center;">{{$rows++}}</td>
                    <td style="border: 1px solid black; text-align: center;">{{$item['item_description']}}</td>
                    <td style="border: 1px solid black; text-align: center;">{{$item['unit_measure']['unit_measure_name']}}{{($item['item_qty'] > 1) ? "s":""}}</td>
                    <td style="border: 1px solid black; text-align: center;">{{$item['item_qty']}}</td>
                    <td style="border: 1px solid black; text-align: center;">{{$item['total_days']}}</td>
                    @php
                        foreach($procurement_request['invitations'] as $invitation){

                    @endphp
                    <td style="border: 1px solid black; text-align: right;">{{number_format(\App\Helpers\Custom::getItem($item['id'],$invitation['vendor_id'])->unit_price,0)}}</td>
                    <td style="border: 1px solid black; text-align: right;">{{number_format(\App\Helpers\Custom::getItem($item['id'],$invitation['vendor_id'])->line_total,0)}}</td>
                    @php
                        }
                    @endphp
                </tr>

                @php
                    }
                @endphp
                <tr style="border: 1px solid black;" >
                    <th style="border: 1px solid black;" >Total</th>
                    <th style="border: 1px solid black;"></th>
                    <th style="border: 1px solid black;"></th>
                    <th style="border: 1px solid black;"></th>
                    <th style="border: 1px solid black;"></th>
                    @php
                        foreach($procurement_request['invitations'] as $invitation){
                    @endphp
                    <th style="border: 1px solid black; text-align: right;"></th>
                    <th style="border: 1px solid black; text-align: right;">{{number_format(\App\Helpers\Custom::getInvitationTotal($invitation['id'],$invitation['vendor_id']),0)}}</th>

                    @php
                        }
                    @endphp
                </tr>
                <tr style="border: 1px solid black;" >
                    <th style="border: 1px solid black;" >VAT</th>
                    <th style="border: 1px solid black;"></th>
                    <th style="border: 1px solid black;"></th>
                    <th style="border: 1px solid black;"></th>
                    <th style="border: 1px solid black;"></th>
                    @php
                        foreach($procurement_request['invitations'] as $invitation){
                    @endphp
                    <th style="border: 1px solid black; text-align: right;"></th>
                    <th style="border: 1px solid black; text-align: right;">{{number_format(0,0)}}</th>


                    @php
                        }
                    @endphp
                </tr>
                <tr style="border: 1px solid black;" >
                    <th style="border: 1px solid black;" >Total Vat Inc</th>
                    <th style="border: 1px solid black;"></th>
                    <th style="border: 1px solid black;"></th>
                    <th style="border: 1px solid black;"></th>
                    <th style="border: 1px solid black;"></th>
                    @php
                        foreach($procurement_request['invitations'] as $invitation){
                    @endphp
                    <th style="border: 1px solid black; text-align: right;"></th>
                    <th style="border: 1px solid black; text-align: right;">{{number_format(0,0)}}</th>


                    @php
                        }
                    @endphp
                </tr>
                <tr style="border: 1px solid black;" >
                    <th style="border: 1px solid black;" >Total USD</th>
                    <th style="border: 1px solid black;"></th>
                    <th style="border: 1px solid black;"></th>
                    <th style="border: 1px solid black;"></th>
                    <th style="border: 1px solid black;"></th>
                    @php
                        foreach($procurement_request['invitations'] as $invitation){
                    @endphp
                    <th style="border: 1px solid black; text-align: right;"></th>
                    <th style="border: 1px solid black; text-align: right;">$ {{number_format(\App\Helpers\Custom::getInvitationTotal($invitation['id'],$invitation['vendor_id'])/$procurement_request['exchange_rate'],0)}}</th>


                    @php
                        }
                    @endphp
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td><h4>Remarks</h4></td>
    </tr>
    <tr>
        <td>Responses/ offers from the {{str_pad(count($procurement_request['invitations']),2,"0",STR_PAD_LEFT)}} of the invited vendors were received as sumamrised in the table above</td>
    </tr>
    <tr>
        <td>

        </td>
    </tr>
    <tr>
        <td><h4>Recommendations</h4></td>
    </tr>
    <tr>
        <td>
            <strong>{{$recommended_vendor["vendor"]["vendor_name"]}}</strong> at a total cost of <strong>UGX {{number_format(\App\Helpers\Custom::getInvitationTotal($recommended_vendor['id'],$recommended_vendor['vendor_id']),0)}}</strong> is recomended for award
        </td>
    </tr>
</table>

</body>
</html>
