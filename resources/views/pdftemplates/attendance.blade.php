<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"  "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <style>
        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
        }
    </style>
    <title>{{ $title }}</title>
</head>
<body>
<h2>{{ $title }}</h2>
<table width="100%" border="1" class="table table-bordered table-hover mb-none table-condensed table_default">
    <thead>
    <tr>
        <th>
            SN
        </th>
        <th>Participant Names</th>
        <th>Title</th>
        <th>Participant Mobile</th>
        <th>Participant Email</th>
        <th>ORG</th>
        <th>Logged Date and Time</th>
        <th>Signature</th>
    </tr>
    </thead>
    <tbody>
    @if(!empty($attendance))
        @foreach($attendance as $participant)
            @php
                $path = asset('public/app_image/uploads/signatures/'.$participant['participant']['participant_sign']); //this is the image path
                $type = pathinfo($path, PATHINFO_EXTENSION);
                $data = file_get_contents($path);
                $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
            @endphp
            <tr>
                <td>{{$participant['id']}}</td>
                <td>{{$participant['participant']['participant_first_name']}} {{$participant['participant']['participant_other_names']}}</td>
                <td>{{$participant['participant']['participant_title']}}</td>
                <td>{{$participant['participant']['participant_mobile']}}</td>
                <td>{{$participant['participant']['participant_email']}}</td>
                <td>{{$participant['participant']['participant_organization']}}</td>
                <td>{{$participant['activity_attendance_date']}}</td>
                <td><img height="40px" src="{{$base64}}" alt="No Signature"></td>
{{--                <td><img height="40px" src="{{$participant['participant']['participant_sign']}}" alt="No Signature"></td>--}}

            </tr>
        @endforeach
    @endif
    </tbody>
</table>
</body>
</html>
