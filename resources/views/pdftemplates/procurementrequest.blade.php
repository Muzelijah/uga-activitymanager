<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"  "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <style>
        .text-right{
            text-align: right;
        }
        .text-center{
            text-align: center;
        }
        .text-left{
            text-align: left;
        }
    </style>
    <title>{{ $title }}</title>
</head>
<body>
<table border="0" style="border: 0;" cellspacing="0" cellpadding="0" width="100%">
    <tr><td><table width="100%" border="0"><tr><td width="25%"></td><td width="25%"></td><td width="25%"></td><td width="25%">Date : {{date('d/m/Y',strtotime($procurement_request['procurement_issue_date']))}}</td></tr></table></td></tr>
    <tr><td><table width="100%" border="0"><tr><td width="33%"></td><td width="33%"><h3 style="text-align: center">Procurement Request</h3></td><td width="33%"></td></tr></table></td></tr>
    <tr><td><span style="text-align: justify">World Food Programme (WFP) invites you to submit a quote in accordance with the
requirements of this request for quotes. Quotes must be received by WFP no later than the
Date and Time indicated in the table below:</span></td></tr>
    <tr><td>
            <table style="border: 1px solid black;  border-collapse: collapse;" border="1" width="100%">
                <tr style="border: 1px solid black; border-collapse: collapse;"><td width="35%"><span class="text-left"><strong>RFQ Refence Number</strong></span></td><td  width="65%">{{$procurement_request['rfq_reference_number']}}</td></tr>
                <tr style="border: 1px solid black; border-collapse: collapse;"><td><span class="text-right"><strong>Request for Quotes Issue Date:</strong></span></td><td>{{date('jS F Y',strtotime($procurement_request['procurement_issue_date']))}}</td></tr>
                <tr style="border: 1px solid black; border-collapse: collapse;"><td><span class="text-right"><strong>Due Date and Time for Questions:</strong></span></td><td>{{date('jS F Y',strtotime($procurement_request['procurement_due_date']))}}</td></tr>
                <tr style="border: 1px solid black; border-collapse: collapse;"><td><span class="text-right"><strong>Delivery Period</strong></span></td><td>From : {{date('jS F Y',strtotime($procurement_request['delivery_from_date']))}} To {{date('jS F Y',strtotime($procurement_request['delivery_to_date']))}}</td></tr>
                <tr style="border: 1px solid black; border-collapse: collapse;"><td><span class="text-right"><strong>Delivery Address</strong></span></td><td>{{$procurement_request['delivery_address']}}</td></tr>
                <tr style="border: 1px solid black; border-collapse: collapse;"><td><span class="text-right"><strong>Payment Terms:</strong></span></td><td>{{$procurement_request['payment_terms']}}</td></tr>
                <tr><td colspan="2"><div class="text-center"><i>Quotes submitted after the deadline has passed or that do not include all of the information requested may be rejected.</i></div></td></tr>

            </table></td></tr>
    <tr><td><h4>Complete Description of goods/Scope of Work/Specifications as below</h4></td></tr>
    <tr><td>
            <table style="border: 1px solid black;  border-collapse: collapse;" border="1" width="100%">
                <tr style="border: 1px solid black;" >
                    <th style="border: 1px solid black;"  width="5%" >SN</th>
                    <th style="border: 1px solid black;" width="50%" >Description</th>
                    <th style="border: 1px solid black;" width="5%" >Qty</th>
                    <th style="border: 1px solid black;" width="20%" >Measure</th>
                    <th style="border: 1px solid black;" width="10%" >Total Days</th>
                </tr>
                @php
                $rows = 1;
            foreach($procurement_request_items as $item){

                @endphp
                <tr style="border: 1px solid black;" >
                    <td style="border: 1px solid black; text-align: center;" >{{$rows++}}</td>
                    <td style="border: 1px solid black; text-align: center;" >{{$item['item_description']}}</td>
                    <td style="border: 1px solid black; text-align: center;" >{{$item['item_qty']}}</td>
                    <td style="border: 1px solid black; text-align: center;" >{{$item['unit_measure']['unit_measure_name']}}{{($item['item_qty'] > 1) ? "s":""}}</td>
                    <td style="border: 1px solid black; text-align: center;" >{{$item['total_days']}}</td>
                </tr>
                @php
            }
            @endphp
            </table>
        </td></tr>
    <tr><td><h4>Vendors are required to take note of the following.</h4></td></tr>
    <tr><td>
            <ul>
                <li>Supplier shall provide quotation using official letterhead/stationary inclusive of Complete vendor information – (physical address, full legal name etc.)</li>
                <li>Total price offered for the needed goods and/or services, including associated costs such as shipping or installation</li>
                <li>Delivery period must clearly be indicated on the offer</li>
                <li>Quotes will be evaluated based the lowest price, most technically acceptable evaluation concept.</li>

            </ul>
        </td></tr>
    <tr><td></td></tr>
    <tr><td></td></tr>
</table>

</body>
</html>
