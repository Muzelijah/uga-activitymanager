@extends('layouts.main')
@section('content')
    <section class="panel" xmlns="http://www.w3.org/1999/html">
        <div class="tabs-custom">
            <ul class="nav nav-tabs">
                <li>
                    <a href="{{route('unit.measures.data')}}">
                        <i class="fas fa-list-ul"></i> Unit Measures
                    </a>
                </li>
                <li class="active">
                    <a href="#add" data-toggle="tab">
                        <i class="fas fa-edit"></i> Edit Unit Measure
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane box  mb-md" id="list">

                </div>

                <div class="tab-pane active" id="add">
                    <form action="{{route('unit.measures.update',['measure'=>$unit_measure->unit_measure_ref])}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="panel-body">

                            <div class="row">
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Unit Measure Name<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <input type="text" required class="form-control"
                                               name="unit_measure_name"
                                               value="{{$unit_measure->unit_measure_name}}"/>
                                        <span class="error"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <footer class="panel-footer">
                            <div class="row">
                                <div class="col-md-offset-10 col-md-2">
                                    <button type="submit" name="save" value="1"
                                            class="btn btn btn-dark btn-block center">
                                        <i class="fas fa-plus-circle"></i> Save
                                    </button>
                                </div>
                            </div>
                        </footer>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
