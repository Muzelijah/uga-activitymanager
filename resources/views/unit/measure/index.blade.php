@extends('layouts.main')
@section('content')
    <section class="panel" xmlns="http://www.w3.org/1999/html">
        <div class="tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#list" data-toggle="tab">
                        <i class="fas fa-list-ul"></i> Unit Measures
                    </a>
                </li>
                <li>
                    <a href="#add" data-toggle="tab">
                        <i class="fas fa-edit"></i> Create New Unit Measure
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane box active mb-md" id="list">
                    {{--                    <div class="row mt-5 pt-5">--}}
                    {{--                        <div class="col-lg-12">--}}
                    {{--                            <div class="headers-line">--}}
                    {{--                                <i class="fas fa-list"></i> Order Details--}}
                    {{--                            </div>--}}
                    {{--                        </div>--}}
                    {{--                    </div>--}}
                    <form action="{{route('activities.status')}}" method="post">

                        @csrf
                        <table class="table table-bordered table-hover mb-none table-condensed table-export">
                            <thead>
                            <tr>
                                <th>
                                    SN
                                </th>
                                <th>Unit Measure Name</th>
                                <th>Status</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(!empty($unit_measures))
                                @foreach($unit_measures as $unit_measure)
                                    <tr>
                                        <td class="checked-area">
                                            {{$unit_measure->id}}
                                        </td>
                                        <td>{{$unit_measure->unit_measure_name}}</td>
                                        <td>{{$unit_measure->active}}</td>
                                        <td>
                                            <a href="{{route('unit.measures.edit',['measure' => $unit_measure->unit_measure_ref])}}"
                                               data-toggle="tooltip" data-original-title="Edit"
                                               class="btn btn-circle btn-dark icon">
                                                <i class="fas fa-pen-nib"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>

                    </form>

                </div>

                <div class="tab-pane" id="add">
                    <form action="{{route('unit.measures.store')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="panel-body">

                            <div class="row">
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Unit Measure Name<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <input type="text" required class="form-control"
                                               name="unit_measure_name"
                                               value="{{old('unit_measure_name')}}"/>
                                        <span class="error"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <footer class="panel-footer">
                            <div class="row">
                                <div class="col-md-offset-10 col-md-2">
                                    <button type="submit" name="save" value="1"
                                            class="btn btn btn-dark btn-block center">
                                        <i class="fas fa-plus-circle"></i> Save
                                    </button>
                                </div>
                            </div>
                        </footer>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
