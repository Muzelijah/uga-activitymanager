@extends('layouts.guest')
@section('content')
    <div class="bg__img">
    <div class="min-h-screen flex flex-col sm:justify-center items-center pt-6 sm:pt-0 bg-gray-100">

        <div class="mt-8 bg-white dark:bg-gray-800 overflow-hidden shadow sm:rounded-lg">
        <div class="grid grid-cols-1 md:grid-cols-1">
{{--            <div class="p-6 items-center justify-center bg-transparent">--}}
{{--                <div class="flex items-center">--}}
{{--                    <img class="center" src="{{asset('public/app_image/logo_small.png')}}" alt="A Waste Logo" widith="150px" height="150px" />--}}
{{--                </div>--}}
{{--            </div>--}}
            <div class="p-6 w-full sm:max-w-md mt-6 px-6 py-4 bg-white shadow-md overflow-hidden sm:rounded-lg">
                <form method="POST" action="{{ route('students.search.wallet') }}">
                @csrf

                <!-- Email Address -->
                    <div>
                        <div class="form__header">
                            <div class="logo">
                                <img class="form__logo" src="https://joginsa.studyonline.tech/uploads/app_image/logo.png">
                            </div>
                            <h1 class="form__title">St Joseph's Girls SSS Nsambya</h1>
{{--                            <h3 class="form__title">School Wallet</h3>--}}
                        </div>
                    </div>
                    <div class="mt-5 mb-5">
                        <x-label class="" for="code" :value="__('Kindly Enter Your Student Wallet No Below')" />

                        <x-input id="code" class="block mt-1 w-full" type="number" name="code" :value="old('code')" required autofocus />


                    </div>


                    <!-- Password -->
{{--                    <div class="mt-4">--}}
{{--                        <x-label for="password" :value="__('Password')" />--}}

{{--                        <x-input id="password" class="block mt-1 w-full"--}}
{{--                                 type="password"--}}
{{--                                 name="password"--}}
{{--                                 required autocomplete="current-password" />--}}
{{--                    </div>--}}

                    <!-- Remember Me -->


                    <div class="flex items-center mt-5 mt-5">
                        {{--                    @if (Route::has('password.request'))--}}
                        {{--                        <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('password.request') }}">--}}
                        {{--                            {{ __('Forgot your password?') }}--}}
                        {{--                        </a>--}}
                        {{--                    @endif--}}

                        <x-button class="btn-block bg-danger">
                            {{ __('Search') }}
                        </x-button>
                    </div>
                </form>
            </div>
        </div>
        </div>

    </div>
    </div>
@endsection
{{--<x-guest-layout>--}}
{{--    <x-auth-card>--}}
{{--        <x-slot name="logo">--}}
{{--            <a href="/">Kamwe Trading Company External Recruitment Agency--}}
{{--                <x-application-logo class="w-20 h-20 fill-current text-gray-500" />--}}
{{--            </a>--}}
{{--        </x-slot>--}}

{{--        <!-- Session Status -->--}}
{{--        <x-auth-session-status class="mb-4" :status="session('status')" />--}}

{{--        <!-- Validation Errors -->--}}
{{--        <x-auth-validation-errors class="mb-4" :errors="$errors" />--}}

{{--        --}}
{{--    </x-auth-card>--}}
{{--</x-guest-layout>--}}

