@extends('layouts.main')
@section('content')
    <section class="panel">
        <div class="tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#list" data-toggle="tab">
                        <i class="fas fa-list-ul"></i> Branches List
                    </a>
                </li>
                <li>
                    <a href="#add" data-toggle="tab">
                        <i class="far fa-edit"></i> Register New Branch
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane box active mb-md" id="list">
                    <table class="table table-bordered table-hover mb-none table-condensed table-export">
                        <thead>
                        <tr>
                            <th>SN</th>
                            <th>Branch Name</th>
                            <th>Branch Link</th>
                            <th>Branch Mobile</th>
                            <th>Branch Email</th>
                            <th>Total Settlements</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(!empty($branches))
                            @foreach($branches as $branch)
                                <tr>
                                    <td>{{$branch->id}}</td>
                                    <td>{{$branch->branch_name}}</td>
                                    <td>{{$branch->branch_url}}</td>
                                    <td>{{$branch->branch_mobile}}</td>
                                    <td>{{$branch->branch_email}}</td>
                                    <td>{{number_format($branch->settlements->sum('branch_settled_amount')-($branch->settlements->sum('branch_settled_amount')*3/100.0),0)}}</td>
                                    <td><a href="{{route('branches.edit',['branch'=>$branch->id])}}"
                                           data-toggle="tooltip" data-original-title="Edit"
                                           class="btn btn-circle btn-default icon">
                                            <i class="fas fa-pen-nib"></i>
                                             </a>
                                        <a href="{{route('branches.settlements.data',['branch'=>$branch->branch_ref])}}"
                                           data-toggle="tooltip" data-original-title="Settlements"
                                           class="btn btn-circle btn-success icon">
                                            <i class="fas fa-list"></i>
                                            </a>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>

                <div class="tab-pane" id="add">
                    <form action="{{route('branches.store')}}" method="post">
                        @csrf
                        <div class="panel-body">

                            <div class="row">
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Branch Name<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="branch_name"
                                               value="{{old('branch_name')}}"/>
                                        <span class="error"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Branch Link<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="branch_url"
                                               value="{{old('branch_url')}}"/>
                                        <span class="error"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Branch Mobile<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="branch_mobile"
                                               value="{{old('branch_mobile')}}"/>
                                        <span class="error"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Branch Email<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <input type="email" class="form-control" name="branch_email"
                                               value="{{old('branch_email')}}"/>
                                        <span class="error"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <footer class="panel-footer">
                            <div class="row">
                                <div class="col-md-offset-10 col-md-2">
                                    <button type="submit" name="save" value="1"
                                            class="btn btn btn-dark btn-block center">
                                        <i class="fas fa-plus-circle"></i> Save
                                    </button>
                                </div>
                            </div>
                        </footer>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
