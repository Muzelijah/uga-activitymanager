@extends('layouts.main')
@section('content')
    <section class="panel">
        <div class="tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#list" data-toggle="tab">
                        <i class="fas fa-list-ul"></i> Branch Settlements
                    </a>
                </li>
{{--                <li>--}}
{{--                    <a href="#add" data-toggle="tab">--}}
{{--                        <i class="far fa-edit"></i> Register New Branch--}}
{{--                    </a>--}}
{{--                </li>--}}
            </ul>
            <div class="tab-content">
                <div class="tab-pane box active mb-md" id="list">
                    <table class="table table-bordered table-hover mb-none table-condensed table-export">
                        <thead>
                        <tr>
                            <th>SN</th>
                            <th>Branch Name</th>
                            <th>Transaction Ref</th>
                            <th>Total Settlements</th>
                            <th>Amount</th>
                            <th>Status</th>
                            <th>Settled Date</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(!empty($branch_settlements))
                            @foreach($branch_settlements as $branch_settlement)
                                <tr>
                                    <td>{{$branch_settlement->id}}</td>
                                    <td>{{$branch_settlement->branch->branch_name}}</td>
                                    <td>{{$branch_settlement->transaction_ref}}</td>
                                    <td><a href="#"  data-toggle="tooltip" data-original-title="{{$branch_settlement->settlement_references}}"
                                           class="">
                                            <span class='label label-primary-custom'>{{number_format(count(json_decode($branch_settlement->settlement_references)),0)}}</span>
                                        </a>
                                        </td>
                                    <td>{{number_format($branch_settlement->branch_settled_amount,0)}}</td>
                                    <td>@if($branch_settlement->branch_settlement_status == 1)
                                            <a href="#" data-toggle="tooltip" data-original-title="Settlement Is Pending"
                                               class="">
                                                <span class='label label-info-custom'>Pending</span>
                                            </a>
                                        @else
                                            <a href="#"  data-toggle="tooltip" data-original-title="Settlement Is Success"
                                               class="">
                                                <span class='label label-success-custom'>Success</span>
                                            </a>
                                        @endif</td>
                                    <td>{{$branch_settlement->date_added}}</td>
                                    <td><a href="{{route('branches.edit',['branch'=>$branch_settlement->id])}}"
                                           data-toggle="tooltip" data-original-title="Edit"
                                           class="btn btn-circle btn-default icon">
                                            <i class="fas fa-pen-nib"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>

{{--                <div class="tab-pane" id="add">--}}
{{--                    <form action="{{route('branches.store')}}" method="post">--}}
{{--                        @csrf--}}
{{--                        <div class="panel-body">--}}

{{--                            <div class="row">--}}
{{--                                <div class="form-group">--}}
{{--                                    <label class="col-md-3 control-label text-right">Branch Name<span--}}
{{--                                            class="required">*</span></label>--}}
{{--                                    <div class="col-md-6">--}}
{{--                                        <input type="text" class="form-control" name="branch_name"--}}
{{--                                               value="{{old('branch_name')}}"/>--}}
{{--                                        <span class="error"></span>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="form-group">--}}
{{--                                    <label class="col-md-3 control-label text-right">Branch Link<span--}}
{{--                                            class="required">*</span></label>--}}
{{--                                    <div class="col-md-6">--}}
{{--                                        <input type="text" class="form-control" name="branch_url"--}}
{{--                                               value="{{old('branch_url')}}"/>--}}
{{--                                        <span class="error"></span>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="form-group">--}}
{{--                                    <label class="col-md-3 control-label text-right">Branch Mobile<span--}}
{{--                                            class="required">*</span></label>--}}
{{--                                    <div class="col-md-6">--}}
{{--                                        <input type="text" class="form-control" name="branch_mobile"--}}
{{--                                               value="{{old('branch_mobile')}}"/>--}}
{{--                                        <span class="error"></span>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="form-group">--}}
{{--                                    <label class="col-md-3 control-label text-right">Branch Email<span--}}
{{--                                            class="required">*</span></label>--}}
{{--                                    <div class="col-md-6">--}}
{{--                                        <input type="email" class="form-control" name="branch_email"--}}
{{--                                               value="{{old('branch_email')}}"/>--}}
{{--                                        <span class="error"></span>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <footer class="panel-footer">--}}
{{--                            <div class="row">--}}
{{--                                <div class="col-md-offset-10 col-md-2">--}}
{{--                                    <button type="submit" name="save" value="1"--}}
{{--                                            class="btn btn btn-dark btn-block center">--}}
{{--                                        <i class="fas fa-plus-circle"></i> Save--}}
{{--                                    </button>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </footer>--}}
{{--                    </form>--}}
{{--                </div>--}}
            </div>
        </div>
    </section>
@endsection
