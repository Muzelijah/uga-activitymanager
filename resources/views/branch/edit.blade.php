@extends('layouts.main')
@section('content')
    <section class="panel">
        <div class="tabs-custom">
            <ul class="nav nav-tabs">
                <li>
                    <a href="{{route('branches.data')}}">
                        <i class="fas fa-list-ul"></i> Branch List
                    </a>
                </li>
                <li class="active">
                    <a href="#edit" data-toggle="tab">
                        <i class="far fa-edit"></i> Edit Branch
                    </a>
                </li>

            </ul>
            <div class="tab-content">
{{--                <div class="tab-pane box active mb-md" id="list">--}}
{{--                    <table class="table table-bordered table-hover mb-none table-condensed table-export">--}}
{{--                        <thead>--}}
{{--                        <tr>--}}
{{--                            <th>SN</th>--}}
{{--                            <th>Reference</th>--}}
{{--                            <th>Names</th>--}}
{{--                            <th>Gender</th>--}}
{{--                            <th>NIN</th>--}}
{{--                            <th>Age</th>--}}
{{--                            <th>Contact</th>--}}
{{--                            <th>Next of Kin</th>--}}
{{--                            <th>Kin Contact</th>--}}
{{--                            <th>Pre Medical</th>--}}
{{--                            <th>Own Passport</th>--}}
{{--                            <th>District</th>--}}
{{--                            <th>Last Updated By</th>--}}
{{--                            <th>Actions</th>--}}
{{--                        </tr>--}}
{{--                        </thead>--}}
{{--                        <tbody>--}}
{{--                        @if(!empty($applicants))--}}
{{--                            @foreach($applicants as $applicant)--}}
{{--                                <tr>--}}
{{--                                    <td>{{$applicant->id}}</td>--}}
{{--                                    <td>{{$applicant->applicant_ref}}</td>--}}
{{--                                    <td>{{$applicant->applicant_sur_name}} {{$applicant->applicant_given_name}}</td>--}}
{{--                                    <td>{{$applicant->applicant_gender}}</td>--}}
{{--                                    <td>{{$applicant->applicant_nin}}</td>--}}
{{--                                    <td>{{$applicant->applicant_age}}</td>--}}
{{--                                    <td>{{$applicant->applicant_contact}}</td>--}}
{{--                                    <td>{{$applicant->applicant_next_of_kin}}</td>--}}
{{--                                    <td>{{$applicant->applicant_kin_mobile}}</td>--}}
{{--                                    <td>{{$applicant->applicant_pre_medical}}</td>--}}
{{--                                    <td>{{$applicant->applicant_own_passport}}</td>--}}
{{--                                    <td>{{$applicant->district->district_name}}</td>--}}
{{--                                    <td>{{$applicant->user->name}}</td>--}}
{{--                                    <td><a href="{{route('applicants.edit',['applicant'=>$applicant->id])}}"--}}
{{--                                           data-toggle="tooltip" data-original-title="Edit"--}}
{{--                                           class="btn btn-circle btn-default icon">--}}
{{--                                            <i class="fas fa-pen-nib"></i>--}}
{{--                                        </a></td>--}}
{{--                                </tr>--}}
{{--                            @endforeach--}}
{{--                        @endif--}}
{{--                        </tbody>--}}
{{--                    </table>--}}
{{--                </div>--}}

                <div class="tab-pane active" id="edit">
                    <form action="{{route('branches.update',['branch'=>$branch->id])}}" method="post">
                        @csrf
                        <div class="panel-body">

                            <div class="row">
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Branch Name<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="branch_name"
                                               value="{{old('branch_name',$branch->branch_name)}}"/>
                                        <span class="error"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Branch Link<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="branch_url"
                                               value="{{old('branch_url',$branch->branch_url)}}"/>
                                        <span class="error"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Branch Mobile<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="branch_mobile"
                                               value="{{old('branch_mobile',$branch->branch_mobile)}}"/>
                                        <span class="error"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Branch Email<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <input type="email" class="form-control" name="branch_email"
                                               value="{{old('branch_email',$branch->branch_email)}}"/>
                                        <span class="error"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <footer class="panel-footer">
                            <div class="row">
                                <div class="col-md-offset-10 col-md-2">
                                    <button type="submit" name="save" value="1"
                                            class="btn btn btn-dark btn-block center">
                                        <i class="fas fa-plus-circle"></i> Save
                                    </button>
                                </div>
                            </div>
                        </footer>
                    </form>
                </div>

            </div>
        </div>
    </section>
@endsection
