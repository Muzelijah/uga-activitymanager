@extends('layouts.main')
@section('content')
    <section class="panel" xmlns="http://www.w3.org/1999/html">
        <div class="tabs-custom">
            <ul class="nav nav-tabs">
                <li>
                    <a href="{{route('budget.codes.data')}}">
                        <i class="fas fa-list-ul"></i> Budget Code List
                    </a>
                </li>
                <li  class="active">
                    <a href="#add"  data-toggle="tab">
                        <i class="fas fa-list-ul"></i> Edit Budget Code
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane box  mb-md" id="list">


                </div>

                <div class="tab-pane active" id="add">
                    <form action="{{route('budget.codes.update',['code'=>$budget_code->budget_code_ref])}}" id="vendor_form" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="panel-body">

                            <div class="row">
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Budget Code<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <input type="text" required class="form-control"
                                               name="budget_code_name"
                                               value="{{old('budget_code_name',$budget_code->budget_code_name)}}"/>
                                        <span class="error"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Budget Code Description<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control"
                                               name="budget_code_description"
                                               value="{{old('budget_code_description',$budget_code->budget_code_description)}}"/>
                                        <span class="error"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <footer class="panel-footer">
                            <div class="row">
                                <div class="col-md-offset-10 col-md-2">
                                    <button type="submit" id="save_budget_code" name="save" value="1"
                                            class="btn btn btn-dark btn-block center" data-action="save-png">
                                        <i class="fas fa-plus-circle"></i> Save
                                    </button>
                                </div>
                            </div>
                        </footer>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('myscript')
    <script>
        var wrapper = document.getElementById("signature-pad");
        var clearButton = wrapper.querySelector("[data-action=clear]");
        var savePNGButton = wrapper.querySelector("[data-action=save-png]");
        var canvas = wrapper.querySelector("canvas");
        var el_note = document.getElementById("note");
        var save_vendor = document.getElementById("save_vendor");
        var signaturePad;
        signaturePad = new SignaturePad(canvas);

        clearButton.addEventListener("click", function (event) {
            document.getElementById("note").innerHTML = "The signature should be inside box";
            signaturePad.clear();
        });
        document.getElementById('save_vendor').addEventListener("click", function() {
            if (signaturePad.isEmpty()) {
                alert("Please provide signature first.");
                event.preventDefault();
            } else {
                var canvas = document.getElementById("the_canvas");
                var dataUrl = canvas.toDataURL();
                document.getElementById("signature").value = dataUrl;
                document.getElementById("vendor_form").submit();
            }
        });
        // savePNGButton.addEventListener("click", function (event) {
        //     if (signaturePad.isEmpty()) {
        //         alert("Please provide signature first.");
        //         event.preventDefault();
        //     } else {
        //         var canvas = document.getElementById("the_canvas");
        //         var dataUrl = canvas.toDataURL();
        //         document.getElementById("signature").value = dataUrl;
        //         document.getElementById("participant_form").submit();
        //     }
        // });

        function my_function() {
            document.getElementById("note").innerHTML = "";
        }
    </script>
@endsection
