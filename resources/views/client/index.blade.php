@extends('layouts.main')
@section('content')
    <section class="panel">
        <div class="tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#list" data-toggle="tab">
                        <i class="fas fa-list-ul"></i> Clients List
                    </a>
                </li>
                <li>
                    <a href="#add" data-toggle="tab">
                        <i class="far fa-edit"></i> Register New Client
                    </a>
                </li>
                <li>
                    <a href="#upload" data-toggle="tab">
                        <i class="far fa-edit"></i> Upload Clients
                    </a>
                </li>

            </ul>
            <div class="tab-content">
                <div class="tab-pane box active mb-md" id="list">
                    <table class="table table-bordered table-hover mb-none table-condensed table-export">
                        <thead>
                        <tr>
                            <th>SN</th>
                            <th>New Code</th>
                            <th>Old Code</th>
                            <th>Client Names</th>
                            <th>Email</th>
                            <th>Contact</th>
                            <th>Join Date</th>
                            <th>Zone</th>
                            <th>Signed Amount</th>
                            <th>Balance</th>
                            <th>Last Updated</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(!empty($clients))
                            @foreach($clients as $client)
                                <tr>
                                    <td>{{$client->id}}</td>
                                    <td>{{$client->client_code}}</td>
                                    <td>{{$client->client_old_code}}</td>
                                    <td>{{$client->client_sur_name}} {{$client->client_other_names}}</td>
                                    <td>{{$client->client_email}}</td>
                                    <td>{{$client->client_primary_contact}}</td>
                                    <td>{{$client->client_join_date}}</td>
                                    <td>{{$client->zone->zone_name}}</td>
                                    <td>{{number_format($client->client_signed_amount,0)}}</td>
                                    <td>{{number_format($client->client_counters->sum('amount')-$client->client_payments->sum('paid_amount'),0)}}</td>
                                    <td>{{$client->user->name}}</td>
                                    <td><a href="{{route('clients.edit',['client'=>$client->id])}}"
                                           data-toggle="tooltip" data-original-title="Edit"
                                           class="btn btn-circle btn-default icon">
                                            <i class="fas fa-pen-nib"></i>
                                        </a>

                                        <a href="{{route('clients.details',['client'=>$client->id])}}"
                                           data-toggle="tooltip" data-original-title="Details"
                                           class="btn btn-circle btn-success icon">
                                            <i class="fas fa-eye"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>

                <div class="tab-pane" id="add">
                    <form action="{{route('clients.store')}}" method="post">
                        @csrf
                        <div class="panel-body">

                            <div class="row">
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Old Code<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="client_old_code"
                                               value="{{old('client_old_code',0)}}"/>
                                        <span class="error"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Surname<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="client_sur_name"
                                               value="{{old('client_sur_name')}}"/>
                                        <span class="error"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Other Names<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="client_other_names"
                                               value="{{old('client_other_names')}}"/>
                                        <span class="error"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Email<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <input type="email" class="form-control" name="client_email"
                                               value="{{old('client_email')}}"/>
                                        <span class="error"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Join Date<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="far fa-calendar-alt"></i></span>
                                            <input type="text" class="form-control" name="client_join_date"
                                                   value="{{old('client_join_date',date('Y-m-d'))}}"
                                                   data-plugin-datepicker
                                                   data-plugin-options='{ "todayHighlight" : true }'/>
                                        </div>
                                        <span class="error"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Primary Contact<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="client_primary_contact"
                                               value="{{old('client_primary_contact')}}"/>
                                        <span class="error"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Secondary Contact<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="client_secondary_contact"
                                               value="{{old('client_secondary_contact')}}"/>
                                        <span class="error"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Zone<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <select name="zone_id" class="form-control select2-hidden-accessible"
                                                id="zone_id" data-plugin-selecttwo="" data-width="100%" tabindex="-1"
                                                aria-hidden="true" required>
                                            <option value="">Select</option>
                                            @if(!empty($zones))
                                                @foreach($zones as $zone)
                                                    <option
                                                        value="{{$zone->id}}">{{$zone->zone_name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Signed Amount<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <input type="number" min="5000" class="form-control" name="client_signed_amount"
                                               value="{{old('client_signed_amount')}}"/>
                                        <span class="error"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Billing Start Month<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <select name="client_start_month_id"
                                                class="form-control select2-hidden-accessible"
                                                id="client_start_month_id" data-plugin-selecttwo="" data-width="100%"
                                                tabindex="-1"
                                                aria-hidden="true" required>
                                            <option value="">Select</option>
                                            @if(!empty($months))
                                                @foreach($months as $month)
                                                    <option
                                                        value="{{$month->id}}"
                                                        @if($month->id == \App\Models\Month::getMonthIdByDate(date('Y-m-d'))->id) selected @endif>
                                                        {{$month->month_name}}
                                                    </option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-white"><span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <div class="checkbox-replace mt-lg">
                                            <label class="i-checks">
                                                <input type="checkbox" checked name="send_notification_sms" value="1">
                                                <i></i> Send Notification SMS
                                            </label>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                        <footer class="panel-footer">
                            <div class="row">
                                <div class="col-md-offset-10 col-md-2">
                                    <button type="submit" name="save" value="1"
                                            class="btn btn btn-dark btn-block center">
                                        <i class="fas fa-plus-circle"></i> Save
                                    </button>
                                </div>
                            </div>
                        </footer>
                    </form>
                </div>
                <div class="tab-pane" id="upload">

                    <form action="{{route('clients.upload')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="panel-body">
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Upload File<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <input type="file" class="" name="attachment_file"
                                               value="{{old('attachment_file')}}"/>
                                        <span class="error"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-white"><span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <div class="checkbox-replace mt-lg">
                                            <label class="i-checks">
                                                <input type="checkbox" name="send_notification_sms" value="1">
                                                <i></i> Send Notification SMS
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <footer class="panel-footer">
                            <div class="row">
                                <div class="col-md-offset-10 col-md-2">
                                    <button type="submit" name="save" value="1"
                                            class="btn btn btn-dark btn-block center">
                                        <i class="fas fa-plus-circle"></i> Upload
                                    </button>
                                </div>
                            </div>
                        </footer>
                    </form>
                </div>
            </div>
    </section>
@endsection
