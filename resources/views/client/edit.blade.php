@extends('layouts.main')
@section('content')
    <section class="panel">
        <div class="tabs-custom">
            <ul class="nav nav-tabs">
                <li>
                    <a href="{{route('clients.data')}}">
                        <i class="fas fa-list-ul"></i> Clients List
                    </a>
                </li>
                <li class="active">
                    <a href="#edit" data-toggle="tab">
                        <i class="far fa-edit"></i> Edit Client
                    </a>
                </li>

            </ul>
            <div class="tab-content">
{{--                <div class="tab-pane box active mb-md" id="list">--}}
{{--                    <table class="table table-bordered table-hover mb-none table-condensed table-export">--}}
{{--                        <thead>--}}
{{--                        <tr>--}}
{{--                            <th>SN</th>--}}
{{--                            <th>Reference</th>--}}
{{--                            <th>Names</th>--}}
{{--                            <th>Gender</th>--}}
{{--                            <th>NIN</th>--}}
{{--                            <th>Age</th>--}}
{{--                            <th>Contact</th>--}}
{{--                            <th>Next of Kin</th>--}}
{{--                            <th>Kin Contact</th>--}}
{{--                            <th>Pre Medical</th>--}}
{{--                            <th>Own Passport</th>--}}
{{--                            <th>District</th>--}}
{{--                            <th>Last Updated By</th>--}}
{{--                            <th>Actions</th>--}}
{{--                        </tr>--}}
{{--                        </thead>--}}
{{--                        <tbody>--}}
{{--                        @if(!empty($applicants))--}}
{{--                            @foreach($applicants as $applicant)--}}
{{--                                <tr>--}}
{{--                                    <td>{{$applicant->id}}</td>--}}
{{--                                    <td>{{$applicant->applicant_ref}}</td>--}}
{{--                                    <td>{{$applicant->applicant_sur_name}} {{$applicant->applicant_given_name}}</td>--}}
{{--                                    <td>{{$applicant->applicant_gender}}</td>--}}
{{--                                    <td>{{$applicant->applicant_nin}}</td>--}}
{{--                                    <td>{{$applicant->applicant_age}}</td>--}}
{{--                                    <td>{{$applicant->applicant_contact}}</td>--}}
{{--                                    <td>{{$applicant->applicant_next_of_kin}}</td>--}}
{{--                                    <td>{{$applicant->applicant_kin_mobile}}</td>--}}
{{--                                    <td>{{$applicant->applicant_pre_medical}}</td>--}}
{{--                                    <td>{{$applicant->applicant_own_passport}}</td>--}}
{{--                                    <td>{{$applicant->district->district_name}}</td>--}}
{{--                                    <td>{{$applicant->user->name}}</td>--}}
{{--                                    <td><a href="{{route('applicants.edit',['applicant'=>$applicant->id])}}"--}}
{{--                                           data-toggle="tooltip" data-original-title="Edit"--}}
{{--                                           class="btn btn-circle btn-default icon">--}}
{{--                                            <i class="fas fa-pen-nib"></i>--}}
{{--                                        </a></td>--}}
{{--                                </tr>--}}
{{--                            @endforeach--}}
{{--                        @endif--}}
{{--                        </tbody>--}}
{{--                    </table>--}}
{{--                </div>--}}

                <div class="tab-pane active" id="edit">
                    <form action="{{route('clients.update',['client'=>$client->id])}}" method="post">
                        @csrf
                        <div class="panel-body">

                            <div class="row">
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">New Code<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <input type="text" readonly class="form-control" name="client_code"
                                               value="{{$client->client_code}}"/>
                                        <span class="error"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Old Code<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="client_old_code"
                                               value="{{$client->client_old_code}}"/>
                                        <span class="error"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Surname<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="client_sur_name"
                                               value="{{$client->client_sur_name}}"/>
                                        <span class="error"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Other Names<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="client_other_names"
                                               value="{{$client->client_other_names}}"/>
                                        <span class="error"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Email<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <input type="email" class="form-control" name="client_email"
                                               value="{{$client->client_email}}"/>
                                        <span class="error"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Join Date<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="far fa-calendar-alt"></i></span>
                                            <input type="text" class="form-control" name="client_join_date"
                                                   value="{{$client->client_join_date}}"
                                                   data-plugin-datepicker
                                                   data-plugin-options='{ "todayHighlight" : true }'/>
                                        </div>
                                        <span class="error"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Primary Contact<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="client_primary_contact"
                                               value="{{$client->client_primary_contact}}"/>
                                        <span class="error"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Secondary Contact<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="client_secondary_contact"
                                               value="{{$client->client_secondary_contact}}"/>
                                        <span class="error"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Zone<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <select name="zone_id" class="form-control select2-hidden-accessible"
                                                id="zone_id" data-plugin-selecttwo="" data-width="100%" tabindex="-1"
                                                aria-hidden="true" required>
                                            <option value="">Select</option>
                                            @if(!empty($zones))
                                                @foreach($zones as $zone)
                                                    <option
                                                        value="{{$zone->id}}" @if($zone->id == $client->zone_id) selected @endif>{{$zone->zone_name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Signed Amount<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <input type="number" min="5000" class="form-control" name="client_signed_amount"
                                               value="{{$client->client_signed_amount}}"/>
                                        <span class="error"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-right">Billing Start Month<span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <select name="client_start_month_id"
                                                class="form-control select2-hidden-accessible"
                                                id="client_start_month_id" data-plugin-selecttwo="" data-width="100%"
                                                tabindex="-1"
                                                aria-hidden="true" required>
                                            <option value="">Select</option>
                                            @if(!empty($months))
                                                @foreach($months as $month)
                                                    <option
                                                        value="{{$month->id}}"
                                                        @if($month->id == $client->client_start_month_id) selected @endif>
                                                        {{$month->month_name}}
                                                    </option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label text-white"><span
                                            class="required">*</span></label>
                                    <div class="col-md-6">
                                        <div class="checkbox-replace mt-lg">
                                            <label class="i-checks">
                                                <input type="checkbox" name="send_notification_sms" value="1">
                                                <i></i> Send Notification SMS
                                            </label>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                        <footer class="panel-footer">
                            <div class="row">
                                <div class="col-md-offset-10 col-md-2">
                                    <button type="submit" name="save" value="1"
                                            class="btn btn btn-dark btn-block center">
                                        <i class="fas fa-plus-circle"></i> Save
                                    </button>
                                </div>
                            </div>
                        </footer>
                    </form>
                </div>

            </div>
        </div>
    </section>
@endsection
