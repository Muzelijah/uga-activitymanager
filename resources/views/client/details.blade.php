@extends('layouts.main')
@section('content')
    <section class="panel">
        <div class="tabs-custom">
            <ul class="nav nav-tabs">
                <li>
                    <a href="{{route('clients.data')}}">
                        <i class="fas fa-list-ul"></i> Clients List
                    </a>
                </li>
                <li class="active">
                    <a href="#details" data-toggle="tab">
                        <i class="far fa-edit"></i> Client Details
                    </a>
                </li>
                <li>
                    <a href="#collect" data-toggle="tab">
                        <i class="far fa-dollar-sign"></i> Collect Payment
                    </a>
                </li>

            </ul>
            <div class="tab-content">
                {{--                <div class="tab-pane box active mb-md" id="list">--}}
                {{--                    <table class="table table-bordered table-hover mb-none table-condensed table-export">--}}
                {{--                        <thead>--}}
                {{--                        <tr>--}}
                {{--                            <th>SN</th>--}}
                {{--                            <th>Reference</th>--}}
                {{--                            <th>Names</th>--}}
                {{--                            <th>Gender</th>--}}
                {{--                            <th>NIN</th>--}}
                {{--                            <th>Age</th>--}}
                {{--                            <th>Contact</th>--}}
                {{--                            <th>Next of Kin</th>--}}
                {{--                            <th>Kin Contact</th>--}}
                {{--                            <th>Pre Medical</th>--}}
                {{--                            <th>Own Passport</th>--}}
                {{--                            <th>District</th>--}}
                {{--                            <th>Last Updated By</th>--}}
                {{--                            <th>Actions</th>--}}
                {{--                        </tr>--}}
                {{--                        </thead>--}}
                {{--                        <tbody>--}}
                {{--                        @if(!empty($applicants))--}}
                {{--                            @foreach($applicants as $applicant)--}}
                {{--                                <tr>--}}
                {{--                                    <td>{{$applicant->id}}</td>--}}
                {{--                                    <td>{{$applicant->applicant_ref}}</td>--}}
                {{--                                    <td>{{$applicant->applicant_sur_name}} {{$applicant->applicant_given_name}}</td>--}}
                {{--                                    <td>{{$applicant->applicant_gender}}</td>--}}
                {{--                                    <td>{{$applicant->applicant_nin}}</td>--}}
                {{--                                    <td>{{$applicant->applicant_age}}</td>--}}
                {{--                                    <td>{{$applicant->applicant_contact}}</td>--}}
                {{--                                    <td>{{$applicant->applicant_next_of_kin}}</td>--}}
                {{--                                    <td>{{$applicant->applicant_kin_mobile}}</td>--}}
                {{--                                    <td>{{$applicant->applicant_pre_medical}}</td>--}}
                {{--                                    <td>{{$applicant->applicant_own_passport}}</td>--}}
                {{--                                    <td>{{$applicant->district->district_name}}</td>--}}
                {{--                                    <td>{{$applicant->user->name}}</td>--}}
                {{--                                    <td><a href="{{route('applicants.edit',['applicant'=>$applicant->id])}}"--}}
                {{--                                           data-toggle="tooltip" data-original-title="Edit"--}}
                {{--                                           class="btn btn-circle btn-default icon">--}}
                {{--                                            <i class="fas fa-pen-nib"></i>--}}
                {{--                                        </a></td>--}}
                {{--                                </tr>--}}
                {{--                            @endforeach--}}
                {{--                        @endif--}}
                {{--                        </tbody>--}}
                {{--                    </table>--}}
                {{--                </div>--}}

                <div class="tab-pane active" id="details">

                    <div class="panel-body">

                        <!-- academic details-->
                        <div class="row">
                            <div class="headers-line">
                                <i class="fas fa-school"></i> General Information
                            </div>
                            <div class="col-md-2 mb-sm">

                                <label class="control-label">Client Reference</label>
                            </div>
                            <div class="col-md-4 mb-sm">
                                {{old('client_code', $client->client_code)}}
                            </div>
                            <div class="col-md-2 mb-sm">

                                <label class="control-label">Client Status</label>
                            </div>
                            <div class="col-md-4 mb-sm">
                                {{old('client_loan_ref',$client->client_status)}}
                            </div>
                        </div>
                        <!-- academic details-->
                        <div class="row">
                            <div class="headers-line">
                                <i class="fas fa-school"></i> Client Details
                            </div>
                            <div class="row">

                                <div class="col-md-2 mb-sm">

                                    <label class="control-label">Client Names</label>
                                </div>
                                <div class="col-md-4 mb-sm">
                                    {{$client->client_sur_name}} {{$client->client_other_names}}
                                </div>
                                <div class="col-md-2 mb-sm">
                                    <label class="control-label">Signed Amount</label>
                                </div>
                                <div class="col-md-4 mb-sm">
                                    {{number_format($client->client_signed_amount,0)}}
                                </div>


                            </div>
                            <div class="row">
                                <div class="col-md-2 mb-sm">
                                    <label class="control-label">Contacts</label>
                                </div>
                                <div class="col-md-4 mb-sm">
                                    {{$client->client_primary_contact}} / {{$client->client_secoundary_contact}}
                                </div>
                                <div class="col-md-2 mb-sm">

                                    <label class="control-label">Email</label>
                                </div>
                                <div class="col-md-4 mb-sm">
                                    {{$client->client_email}}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="headers-line">
                                <i class="fas fa-list"></i> Client Counter
                            </div>
                            <div class="col-lg-12">
                                <table class="table table-bordered table-hover mb-none table-condensed">
                                    <thead>
                                    <tr>
                                        <th>SN</th>
                                        <th>Month</th>
                                        <th>Amount</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(!empty($client->client_counters))
                                        @php $rows = 1; @endphp
                                        @foreach($client->client_counters as $client_counter)
                                            <tr>
                                                <td>{{$rows++}}</td>
                                                <td>{{$client_counter->month->month_name}}</td>
                                                <td>{{number_format($client_counter->amount,0)}}</td>
                                                <td>
                                                    <div id="myClientCounterModal{{$client_counter->id}}"
                                                         class="modal fade" role="dialog">
                                                        <div class="modal-dialog modal-lg">

                                                            <!-- Modal content-->
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close"
                                                                            data-dismiss="modal">X
                                                                    </button>
                                                                    <h1 class="modal-title center">Edit Client
                                                                        Counter</h1>
                                                                </div>
                                                                <div class="modal-body bg-light">
                                                                    <div class="alert alert-subl">
                                                                        <form action="" method="post">
                                                                            @csrf
                                                                            <div class="row">
                                                                                <input type="hidden"
                                                                                       class="form-control" required
                                                                                       name="client_counter_id"
                                                                                       value="{{old('client_counter_id',$client_counter->id)}}"/>
                                                                                <input type="hidden"
                                                                                       class="form-control" required
                                                                                       name="client_id"
                                                                                       value="{{old('client_id',$client_counter->client_id)}}"/>
                                                                                <div class="col-md-12 mb-sm">
                                                                                    <div class="form-group">
                                                                                        <label class="control-label">Amount<span
                                                                                                class="required">*</span></label>
                                                                                        <input type="number"
                                                                                               class="form-control"
                                                                                               required
                                                                                               name="amount"
                                                                                               value="{{old('amount',$client_counter->amount)}}"/>
                                                                                        <span class="error"></span>
                                                                                    </div>
                                                                                </div>
                                                                                <button type="submit"
                                                                                        name="approve_loan" value="1"
                                                                                        class="btn btn btn-dark btn-block">
                                                                                    <i class="fas fa-plus-circle"></i>
                                                                                    Edit Client Counter
                                                                                </button>
                                                                            </div>

                                                                        </form>
                                                                    </div>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-default"
                                                                            data-dismiss="modal">Close
                                                                    </button>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <button type="button"
                                                            onclick="LoadCounterForm({{$client_counter->id}})"
                                                            class="btn btn-circle btn-default icon">
                                                        <i class="fas fa-pen-nib"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                        @endforeach
                                        <tr>
                                            <th>Total</th>
                                            <th></th>
                                            <th>{{number_format($client->client_counters->sum('amount'),0)}} </th>
                                            <th></th>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="headers-line">
                                <i class="fas fa-list"></i> Client Payments
                            </div>
                            <div class="col-lg-12">
                                <table class="table table-bordered table-hover mb-none table-condensed">
                                    <thead>
                                    <tr>
                                        <th>SN</th>
                                        <th>Payment Date</th>
                                        <th>Pending Balance</th>
                                        <th>Paid Amount</th>
                                        <th>Paid Via</th>
                                        <th>Balance</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(!empty($client->client_payments))
                                        @php $rows = 1; @endphp
                                        @foreach($client->client_payments as $client_payment)
                                            <tr>
                                                <td>{{$rows++}}</td>
                                                <td>{{$client_payment->payment_date}}</td>
                                                <td>{{number_format($client_payment->pending_balance,0)}}</td>
                                                <td>{{number_format($client_payment->paid_amount,0)}}</td>
                                                <td>{{$client_payment->payment_type->payment_type_name}}</td>
                                                <td>{{number_format($client_payment->balance,0)}}</td>
                                            </tr>
                                        @endforeach
                                        <tr>
                                            <th>Total</th>
                                            <th></th>
                                            <th></th>
                                            <th>{{number_format($client->client_payments->sum('paid_amount'),0)}} </th>
                                            <th></th>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <footer class="panel-footer">
                        <div class="row">
                            <div class="col-md-offset-10 col-md-2">
                                <button type="submit" name="save" value="1" class="btn btn btn-default btn-block">
                                    <i class="fas fa-plus-circle"></i> Save
                                </button>
                            </div>
                        </div>
                    </footer>

                </div>
                <div class="tab-pane" id="collect">
                    <form action="{{route('clients.add.payment',['client'=>$client->id])}}" method="post" >
                        @csrf

                    <div class="row">
                        <div class="form-group">
                            <label class="col-md-3 control-label text-right">Client Names<span
                                    class="required">*</span></label>
                            <div class="col-md-6">
                                <label class="col-md-12 control-label text-left">{{$client->client_sur_name}} {{$client->client_other_names}} - {{$client->client_code}}</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label text-right">Zone<span
                                    class="required">*</span></label>
                            <div class="col-md-6">
                                <label class="col-md-12 control-label text-left">{{$client->zone->zone_name}}</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label text-right">Monthly Rate<span
                                    class="required">*</span></label>
                            <div class="col-md-6">
                                <input type="number" min="5000" readonly class="form-control" id="monthly_rate" name="monthly_rate"
                                       value="{{$client->client_signed_amount}}"/>
                                <span class="error"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label text-right">Pending Balance<span
                                    class="required">*</span></label>
                            <div class="col-md-6">
                                <input type="number" readonly min="5000" class="form-control" name="pending_balance"
                                       value="{{$client->client_counters->sum('amount')-$client->client_payments->sum('paid_amount')}}"/>
                                <span class="error"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label text-right">Date<span
                                    class="required">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="far fa-calendar-alt"></i></span>
                                    <input type="text" class="form-control" name="payment_date"
                                           value="{{date('Y-m-d')}}" data-plugin-datepicker
                                           data-plugin-options='{ "todayHighlight" : true }'/>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label text-right">Amount<span
                                    class="required">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-danger btn-number" data-type="minus" data-field="quant[2]">
                                            <span class="glyphicon glyphicon-minus"></span>
                                        </button>
                                    </span>
                                    <input type="text" name="quant[2]" readonly class="form-control input-number" value="{{$client->client_signed_amount}}"
                                           min="{{$client->client_signed_amount}}" max="1000000">
                                    <span class="input-group-btn">
                                    <button type="button" class="btn btn-success btn-number" data-type="plus" data-field="quant[2]">
                                        <span class="glyphicon glyphicon-plus"></span>
                                    </button>
          </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label text-right">Payment Method<span
                                    class="required">*</span></label>
                            <div class="col-md-6">
                                <select name="payment_type_id" class="form-control select2-hidden-accessible"
                                        id="zone_id" data-plugin-selecttwo="" data-width="100%" tabindex="-1"
                                        aria-hidden="true" required>
                                    <option value="">Select</option>
                                    @if(!empty($payment_types))
                                        @foreach($payment_types as $payment_type)
                                            <option
                                                value="{{$payment_type->id}}" >{{$payment_type->payment_type_name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label text-right">Payment Ref<span
                                    class="required">*</span></label>
                            <div class="col-md-6">
                                <input type="text" required  class="form-control" id="payment_ref" name="payment_ref"
                                       value=""/>
                                <span class="error"></span>
                            </div>
                        </div>
                        <div class="col-md-12 mb-sm">
                            <div class="checkbox-replace mt-lg">
                                <label class="i-checks">
                                    <input type="checkbox" name="send_notification_sms" value="1" >
                                    <i></i> Send Notification SMS
                                </label>
                            </div>
                        </div>
                    </div>
                    <footer class="panel-footer">
                        <div class="row">
                            <div class="col-md-offset-10 col-md-2">
                                <button type="submit" name="save" value="1" class="btn btn btn-default btn-block">
                                    <i class="fas fa-plus-circle"></i> Save
                                </button>
                            </div>
                        </div>
                    </footer>
                    </form>


                </div>

            </div>
        </div>
    </section>
@endsection
@section('myscript')
    <script type="text/javascript">
        function LoadCounterForm(id) {
            var form_id = "#myClientCounterModal" + id;
            $(form_id).modal('show');
        }
        $('.btn-number').click(function(e){
            e.preventDefault();

            fieldName = $(this).attr('data-field');
            type      = $(this).attr('data-type');
            var monthly_rate = parseInt(document.getElementById('monthly_rate').value);
            var input = $("input[name='"+fieldName+"']");
            var currentVal = parseInt(input.val());
            if (!isNaN(currentVal)) {
                if(type == 'minus') {

                    if(currentVal > input.attr('min')) {
                        input.val(currentVal - monthly_rate).change();
                    }
                    if(parseInt(input.val()) == input.attr('min')) {
                        $(this).attr('disabled', true);
                    }

                } else if(type == 'plus') {

                    if(currentVal < input.attr('max')) {
                        input.val(currentVal + monthly_rate).change();
                    }
                    if(parseInt(input.val()) == input.attr('max')) {
                        $(this).attr('disabled', true);
                    }

                }
            } else {
                input.val(0);
            }
        });
        $('.input-number').focusin(function(){
            $(this).data('oldValue', $(this).val());
        });
        $('.input-number').change(function() {

            minValue =  parseInt($(this).attr('min'));
            maxValue =  parseInt($(this).attr('max'));
            valueCurrent = parseInt($(this).val());

            name = $(this).attr('name');
            if(valueCurrent >= minValue) {
                $(".btn-number[data-type='minus'][data-field='"+name+"']").removeAttr('disabled')
            } else {
                alert('Sorry, the minimum value was reached');
                $(this).val($(this).data('oldValue'));
            }
            if(valueCurrent <= maxValue) {
                $(".btn-number[data-type='plus'][data-field='"+name+"']").removeAttr('disabled')
            } else {
                alert('Sorry, the maximum value was reached');
                $(this).val($(this).data('oldValue'));
            }


        });
        $(".input-number").keydown(function (e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
                // Allow: Ctrl+A
                (e.keyCode == 65 && e.ctrlKey === true) ||
                // Allow: home, end, left, right
                (e.keyCode >= 35 && e.keyCode <= 39)) {
                // let it happen, don't do anything
                return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });
    </script>
@endsection
